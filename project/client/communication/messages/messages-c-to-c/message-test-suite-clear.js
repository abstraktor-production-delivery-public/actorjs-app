
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestSuiteClear {
  constructor(tab) {
    this.msgId = AppProtocolConst.TEST_SUITE_CLEAR;
    this.tab = tab;
  }
}


module.exports = MessageTestSuiteClear;
