
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseNotStarted {
  constructor(resultId) {
    this.msgId = AppProtocolConst.TEST_CASE_NOT_STARTED;
    this.resultId = resultId;
  }
}


module.exports = MessageTestCaseNotStarted;
