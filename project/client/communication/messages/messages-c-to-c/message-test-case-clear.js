
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageTestCaseClear {
  constructor(tab) {
    this.msgId = AppProtocolConst.TEST_CASE_CLEAR;
    this.tab = tab;
  }
}


module.exports = MessageTestCaseClear;
