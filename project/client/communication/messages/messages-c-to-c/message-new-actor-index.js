
'use strict';

const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class MessageNewActorIndex {
  constructor(index) {
    this.msgId = AppProtocolConst.TEST_CASE_ACTOR_INDEX;
    this.index = index;
  }
}


module.exports = MessageNewActorIndex;
