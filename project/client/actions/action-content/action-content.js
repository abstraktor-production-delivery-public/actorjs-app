
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionContentTextGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentDocumentsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentImageGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentVideoGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentAudioGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentOtherGet extends Action {
  constructor() {
    super();
  }
}

export class ActionContentTextUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentDocumentsUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentImageUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentVideoUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentAudioUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentOtherUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionContentMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionContentMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionContentMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionContentMarkupSave extends Action {
  constructor() {
    super();
  }
}
