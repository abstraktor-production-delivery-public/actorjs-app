
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionContentTextGet, ActionContentDocumentsGet, ActionContentImageGet, ActionContentVideoGet, ActionContentAudioGet, ActionContentOtherGet, ActionContentTextUpdate, ActionContentDocumentsUpdate, ActionContentImageUpdate, ActionContentVideoUpdate, ActionContentAudioUpdate, ActionContentOtherUpdate } from './action-content';


export class DataActionContentTextGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentTextGet());
  }
}

export class DataActionContentDocumentsGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentDocumentsGet());
  }
}

export class DataActionContentImageGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentImageGet());
  }
}

export class DataActionContentVideoGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentVideoGet());
  }
}

export class DataActionContentAudioGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentAudioGet());
  }
}

export class DataActionContentOtherGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionContentOtherGet());
  }
}

export class DataActionContentTextUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentTextUpdate(globals, locals), globals, locals);
  }
}

export class DataActionContentDocumentsUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentDocumentsUpdate(globals, locals), globals, locals);
  }
}

export class DataActionContentImageUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentImageUpdate(globals, locals), globals, locals);
  }
}

export class DataActionContentVideoUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentVideoUpdate(globals, locals), globals, locals);
  }
}

export class DataActionContentAudioUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentAudioUpdate(globals, locals), globals, locals);
  }
}

export class DataActionContentOtherUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionContentOtherUpdate(globals, locals), globals, locals);
  }
}
