
'use strict';

import RealtimeAction from 'z-abs-corelayer-client/client/communication/realtime-action';
import { ActionTestCaseExecutionStop, ActionTestCaseDebugGetSource, ActionTestCaseDebugGetMembers } from './action-test-case';


export class RealtimeActionTestCaseExecutionStart extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseExecutionStop extends RealtimeAction {
  constructor(repoName, sutName, futName, tcName) {
    super();
    this.addRequest(new ActionTestCaseExecutionStop(repoName, sutName, futName, tcName), repoName, sutName, futName, tcName);
  }
}

export class RealtimeActionTestCaseDebugStart extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseDebugBreakpointSet extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseDebugBreakpointClear extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseDebugBreakpointUpdate extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseDebugGetMembers extends RealtimeAction {
  constructor(object, open, isCurrent) {
    super();
    this.addRequestAction(new ActionTestCaseDebugGetMembers(object, open, isCurrent), object, open);
  }
}

export class RealtimeActionTestCaseDebugGetProperty extends RealtimeAction {
  constructor(action) {
    super();
    this.addRequestAction(action);
  }
}

export class RealtimeActionTestCaseDebugGetSource extends RealtimeAction {
  constructor(scriptId) {
    super();
    this.addRequest(new ActionTestCaseDebugGetSource(scriptId), scriptId);
  }
}
