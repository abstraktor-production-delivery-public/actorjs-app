
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionTestCaseMounted extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseGet extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseAdd extends Action {
  constructor(repo, sut, fut, tc, description, stackName, templateName, testData, show) {
    super(repo, sut, fut, tc, description, stackName, templateName, testData, show);
  }
}

export class ActionTestCaseDelete extends Action {
  constructor(tces) {
    super(tces);
  }
}

export class ActionTestCaseRename extends Action {
  constructor(repo, sut, fut, currentTcName, newTcName, description, show) {
    super(repo, sut, fut, currentTcName, newTcName, description, show);
  }
}

export class ActionTestCaseUpdate extends Action {
  constructor(repo, sut, fut, tc, description, dataTc) {
    super(repo, sut, fut, tc, description, dataTc);
  }
}

export class ActionTestCaseMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionTestCaseMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseMarkupSave extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseMarkupCopy extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseExecutionStart extends Action {
  constructor(repo, sut, fut, tc, sutInstance, nodes, config) {
    super(repo, sut, fut, tc, sutInstance, nodes, config);
  }
}

export class ActionTestCaseExecutionStop extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseDebugGet extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseDebugStart extends Action {
  constructor(repo, sut, fut, tc, sutInstance, nodes, config) {
    super(repo, sut, fut, tc, sutInstance, nodes, config);
  }
}

export class ActionTestCaseDebugBreakpointSet extends Action {
  constructor(repo, sut, fut, tc, breakpoint) {
    super(repo, sut, fut, tc, breakpoint);
  }
}

export class ActionTestCaseDebugBreakpointClear extends Action {
  constructor(repo, sut, fut, tc, breakpoint) {
    super(repo, sut, fut, tc, breakpoint);
  }
}

export class ActionTestCaseDebugBreakpointClearAll extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseDebugStopOnRun extends Action {
  constructor(repo, sut, fut, tc) {
    super(repo, sut, fut, tc);
  }
}

export class ActionTestCaseDebugBreakpointUpdate extends Action {
  constructor(repo, sut, fut, tc, breakpoint) {
    super(repo, sut, fut, tc, breakpoint);
  }
}

export class ActionTestCaseClear extends Action {
  constructor(tab) {
    super(tab);
  }
}

export class ActionTestCaseRecentGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseRecentUpdate extends Action {
  constructor(repo, sutName, futName, tcName) {
    super(repo, sutName, futName, tcName);
  }
}

export class ActionTestCaseRecentDelete extends Action {
  constructor(repo, suts, futs, tces) {
    super(repo, suts, futs, tces);
  }
}

export class ActionTestCaseActorsFileGet extends Action {
  constructor(actorFiles, actorKey) {
    super(actorFiles, actorKey);
  }
}

export class ActionTestCaseDebugContinue extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugPause extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugStepOver extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugStepIn extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugStepOut extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugLevel extends Action {
  constructor(level, actorIndex) {
    super(level, actorIndex);
  }
}

export class ActionTestCaseDebugGetMembers extends Action {
  constructor(object, open, isCurrent) {
    super(object, open, isCurrent);
  }
}

export class ActionTestCaseDebugGetProperty extends Action {
  constructor(object, memberNameArray, type, name) {
    super(object, memberNameArray, type, name);
  }
}

export class ActionTestCaseDebugClearCurrentValue extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseDebugScopeOpen extends Action {
  constructor(name, index, open) {
    super(name, index, open);
  }
}

export class ActionTestCaseDebugGetSource extends Action {
  constructor(scriptId) {
    super(scriptId);
  }
}

export class ActionTestCaseAnalyze extends Action {
  constructor(repo, sut, fut, tc, sutInstance) {
    super(repo, sut, fut, tc, sutInstance);
  }
}

export class ActionTestCaseAnalyzeClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseButtonsAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseButtonsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseButtonsUpdate extends Action {
  constructor(tab, button, value) {
    super(tab, button, value);
  }
}

export class ActionTestCaseSpecificationMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionTestCaseSpecificationMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseSpecificationMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCaseSpecificationMarkupSave extends Action {
  constructor(repoName, sutName, futName, tcName, markup) {
    super(repoName, sutName, futName, tcName, markup);
  }
}

export class TestCaseSpecificationUpdate extends Action {
  constructor(repoName, sutName, futName, tcName, tc, description, specification) {
    super(repoName, sutName, futName, tcName, tc, description, specification);
  }
}

export class ActionTestCaseSpecificationUpdate extends Action {
  constructor(repoName, sutName, futName, tcName) {
    super(repoName, sutName, futName, tcName);
  }
}

export class ActionTestCaseStacksGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasePersistentDataAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasePersistentDataGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasePersistentDataUpdate extends Action {
  constructor(parentName, name, value) {
    super(parentName, name, value);
  }
}
