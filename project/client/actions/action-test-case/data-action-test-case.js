
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionTestCaseGet, ActionTestCaseAdd, ActionTestCaseDelete, ActionTestCaseRename, ActionTestCaseUpdate, ActionTestCaseRecentGet, ActionTestCaseRecentUpdate, ActionTestCaseRecentDelete, ActionTestCaseActorsFileGet, ActionTestCaseDebugGet, ActionTestCaseButtonsAdd, ActionTestCaseButtonsGet, ActionTestCaseSpecificationUpdate, ActionTestCaseStacksGet } from './action-test-case';


export class DataActionTestCaseGet extends DataAction {
  constructor(repo, sut, fut, tc) {
    super();
    this.addRequest(new ActionTestCaseGet(repo, sut, fut, tc), repo, sut, fut, tc);
  }
}

export class DataActionTestCaseAdd extends DataAction {
  constructor(repo, sut, fut, tc, description, dataTestCase, stackName, templateName, testData, show) {
    super();
    this.addRequest(new ActionTestCaseAdd(repo, sut, fut, tc, description, stackName, templateName, testData, show), repo, sut, fut, tc, description, dataTestCase, stackName, templateName, testData);
  }
}

export class DataActionTestCaseDelete extends DataAction {
  constructor(tces) {
    super();
    this.addRequest(new ActionTestCaseDelete(tces), tces);
  }
}

export class DataActionTestCaseRename extends DataAction {
  constructor(repo, sut, fut, currentTcName, newTcName, description, show) {
    super();
    this.addRequest(new ActionTestCaseRename(repo, sut, fut, currentTcName, newTcName, description, show), repo, sut, fut, currentTcName, newTcName, description);
  }
}

export class DataActionTestCaseUpdate extends DataAction {
  constructor(repo, sut, fut, tc, description, dataTc) {
    super();
    this.addRequest(new ActionTestCaseUpdate(repo, sut, fut, tc, description, dataTc), repo, sut, fut, tc, description, dataTc);
  }
}

export class DataActionTestCaseRecentGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestCaseRecentGet());
  }
}

export class DataActionTestCaseRecentUpdate extends DataAction {
  constructor(repo, sut, fut, tc) {
    super();
    this.addRequest(new ActionTestCaseRecentUpdate(repo, sut, fut, tc), repo, sut, fut, tc);
  }
}

export class DataActionTestCaseRecentDelete extends DataAction {
  constructor(repo, suts, futs, tcs) {
    super();
    this.addRequest(new ActionTestCaseRecentDelete(repo, suts, futs, tcs), repo, suts, futs, tcs);
  }
}

export class DataActionTestCaseActorsFileGet extends DataAction {
  constructor(actorFiles, actorKey) {
    super();
    this.addRequest(new ActionTestCaseActorsFileGet(actorFiles, actorKey), actorFiles);
  }
}

export class DataActionTestCaseDebugGet extends DataAction {
  constructor(repo, sut, fut, tc) {
    super();
    this.addRequest(new ActionTestCaseDebugGet(repo, sut, fut, tc), repo, sut, fut, tc);
  }
}

export class DataActionTestCaseAnalyze extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.repo, action.sut, action.fut, action.tc, action.sutInstance);
  }
}

export class DataActionTestCaseButtonsAdd extends DataAction {
  constructor(key, valueObject) {
    super();
    this.addRequest(new ActionTestCaseButtonsAdd(), key, valueObject);
  }
}

export class DataActionTestCaseButtonsGet extends DataAction {
  constructor(key) {
    super();
    this.addRequest(new ActionTestCaseButtonsGet(), key);
  }
}

export class DataActionTestCaseButtonsUpdate extends DataAction {
  constructor(action, key, valueObject) {
    super();
    this.addRequest(action, key, valueObject);
  }
}

export class DataActionTestCaseSpecificationUpdate extends DataAction {
  constructor(repoName, sutName, futName, tcName, testCase, specification) {
    super();
    this.addRequest(new ActionTestCaseSpecificationUpdate(repoName, sutName, futName, tcName), repoName, sutName, futName, tcName, testCase, specification);
  }
}

export class DataActionTestCaseDebugBreakpointClearAll extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.repo, action.sut, action.fut, action.tc);
  }
}

export class DataActionTestCaseDebugStopOnRun extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.repo, action.sut, action.fut, action.tc);
  }
}

export class DataActionTestCaseStacksGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestCaseStacksGet());
  }
}

