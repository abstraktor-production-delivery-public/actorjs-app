
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


// SITE

export class ActionTestCasesSutChecked extends Action {
  constructor(sut, checked) {
    super(sut, checked);
  }
}

export class ActionTestCasesFutChecked extends Action {
  constructor(repoName, sutName, futName, checked) {
    super(repoName, sutName, futName, checked);
  }
}

export class ActionTestCasesTcChecked extends Action {
  constructor(repoName, sutName, futName, tcName, checked) {
    super(repoName, sutName, futName, tcName, checked);
  }
}

export class ActionTestCasesSutClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasesFutClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasesTcClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestCasesSetView extends Action {
  constructor(currentView) {
    super(currentView);
  }
}
