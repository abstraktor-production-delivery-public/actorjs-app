
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionTestDataEnvironmentGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataGeneralGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataOutputGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataSystemGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataEnvironmentUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionTestDataGeneralUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionTestDataOutputUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionTestDataSystemUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionTestDataMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionTestDataMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionTestDataMarkupSave extends Action {
  constructor() {
    super();
  }
}
