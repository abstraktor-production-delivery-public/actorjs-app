
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionTestDataEnvironmentGet, ActionTestDataEnvironmentUpdate, ActionTestDataGeneralGet, ActionTestDataGeneralUpdate, ActionTestDataOutputGet, ActionTestDataOutputUpdate, ActionTestDataSystemGet, ActionTestDataSystemUpdate } from './action-test-data';


export class DataActionTestDataEnvironmentGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestDataEnvironmentGet());
  }
}

export class DataActionTestDataGeneralGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestDataGeneralGet());
  }
}

export class DataActionTestDataOutputGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestDataOutputGet());
  }
}

export class DataActionTestDataSystemGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestDataSystemGet());
  }
}

export class DataActionTestDataEnvironmentUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionTestDataEnvironmentUpdate(globals, locals), globals, locals);
  }
}

export class DataActionTestDataGeneralUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionTestDataGeneralUpdate(globals, locals), globals, locals);
  }
}

export class DataActionTestDataOutputUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionTestDataOutputUpdate(globals, locals), globals, locals);
  }
}

export class DataActionTestDataSystemUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionTestDataSystemUpdate(globals, locals), globals, locals);
  }
}
