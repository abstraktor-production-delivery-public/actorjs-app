
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionReposAnalyzeLocalReposGet extends Action {
  constructor() {
    super();
  }
}

export class ActionReposClone extends Action {
  constructor(type, url, newRepo) {
    super(type, url, newRepo);
  }
}

export class ActionReposGet extends Action {
  constructor() {
    super();
  }
}

export class ActionReposUpdate extends Action {
  constructor(contentLocal, dataLocal) {
    super(contentLocal, dataLocal);
  }
}

export class ActionReposVerifyGit extends Action {
  constructor() {
    super();
  }
}