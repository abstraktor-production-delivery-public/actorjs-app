
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionReposAnalyzeLocalReposGet, ActionReposGet, ActionReposUpdate, ActionReposVerifyGit } from './action-repos';


export class DataActionReposAnalyzeLocalReposGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionReposAnalyzeLocalReposGet());
  }
}

export class DataActionReposClone extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.type, action.url, action.newRepo);
  }
}

export class DataActionReposGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionReposGet());
  }
}

export class DataActionReposUpdate extends DataAction {
  constructor(contentLocal, dataLocal) {
    super();
    this.addRequest(new ActionReposUpdate(contentLocal, dataLocal), contentLocal, dataLocal);
  }
}

export class DataActionReposVerifyGit extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionReposVerifyGit());
  }
}

