
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionActorEditorFileGet extends Action {
  constructor(projectId, key, title, path, type) {
    super(projectId, key, title, path, type);
  }
}

export class ActionActorEditorFileInlineGet extends Action {
  constructor(url) {
    super(url);
  }
}

export class ActionActorEditorFileSet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionActorEditorFileAdd extends Action {
  constructor(projectId, title, path, type) {
    super(projectId, title, path, type);
  }
}

export class ActionActorEditorFileNew extends Action {
  constructor(projectId, path, name, type, templateName, dynamicTemplateName, dynamicTemplateName2) {
    super(projectId, path, name, type, templateName, dynamicTemplateName, dynamicTemplateName2);
  }
}

export class ActionActorEditorFileRename extends Action {
  constructor(projectId, path, title, newTitle) {
    super(projectId, path, title, newTitle);
  }
}

export class ActionActorEditorFileUpdate extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionActorEditorFileUpdateAll extends Action {
  constructor(keys) {
    super(keys);
  }
}

export class ActionActorEditorFileInlineUpdate extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionActorEditorFileClose extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionActorEditorFileRemove extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionActorEditorFileDelete extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionActorEditorFileEdit extends Action {
  constructor(projectId, key, content) {
    super(projectId, key, content);
  }
}

export class ActionActorEditorFileMove extends Action {
  constructor(fromKey, toKey) {
    super(fromKey, toKey);
  }
}

export class ActionActorEditorBuild extends Action {
  constructor() {
    super();
  }
}

export class ActionActorEditorStacksGet extends Action {
  constructor() {
    super();
  }
}
