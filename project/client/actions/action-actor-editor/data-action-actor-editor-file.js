
'use strict';


import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionActorEditorFileGet, ActionActorEditorFileInlineGet, ActionActorEditorFileUpdate, ActionActorEditorFileInlineUpdate, ActionActorEditorStacksGet } from './action-actor-editor-file';


export class DataActionActorEditorFileGet extends DataAction {
  constructor(file, projectId, key, title, path, type) {
    super();
    this.addRequest(new ActionActorEditorFileGet(projectId, key, title, path, type), file);
  }
}

export class DataActionActorEditorFileInlineGet extends DataAction {
  constructor(url) {
    super();
    this.addRequest(new ActionActorEditorFileInlineGet(url), url);
  }
}

export class DataActionActorEditorFileNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.path, action.name, action.type, action.templateName, action.dynamicTemplateName, action.dynamicTemplateName2);
  }
}

export class DataActionActorEditorFileRename extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionActorEditorFileUpdate extends DataAction {
  constructor(key, title, path, content) {
    super();
    this.addRequest(new ActionActorEditorFileUpdate(key), `${path}/${title}`, content);
  }
}

export class DataActionActorEditorFileInlineUpdate extends DataAction {
  constructor(key, url, content) {
    super();
    this.addRequest(new ActionActorEditorFileInlineUpdate(key), url, content);
  }
}

export class DataActionActorEditorFileDelete extends DataAction {
  constructor(file, action) {
    super();
    this.addRequest(action, file);
  }
}

export class DataActionActorEditorBuild extends DataAction {
  constructor(content, path, file, action) {
    super();
    this.addRequest(action, content, path, file);
  }
}

export class DataActionActorEditorStacksGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionActorEditorStacksGet());
  }
}
