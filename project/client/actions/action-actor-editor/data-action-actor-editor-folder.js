
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionActorEditorFolderNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`);
  }
}

export class DataActionActorEditorFolderUpdate extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionActorEditorFolderDelete extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`);
  }
}
