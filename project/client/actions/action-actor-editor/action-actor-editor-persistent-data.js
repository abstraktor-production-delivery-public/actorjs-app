
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionActorEditorPersistentDataAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionActorEditorPersistentDataGet extends Action {
  constructor() {
    super();
  }
}

export class ActionActorEditorPersistentDataUpdate extends Action {
  constructor(parentName, name, value) {
    super(parentName, name, value);
  }
}
