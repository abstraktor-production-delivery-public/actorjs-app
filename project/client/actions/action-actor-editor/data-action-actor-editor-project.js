
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionActorEditorProjectGet, ActionActorEditorProjectUpdate } from './action-actor-editor-project';


export class DataActionActorEditorProjectGet extends DataAction {
  constructor(url, query) {
    super();
    this.addRequest(new ActionActorEditorProjectGet(url, query));
  }
}

export class DataActionActorEditorProjectUpdate extends DataAction {
  constructor(projectId, path, content) {
    super();
    this.addRequest(new ActionActorEditorProjectUpdate(projectId), path, content);
  }
}

export class DataActionActorEditorProjectToggle extends DataAction {
  constructor(action, content, path) {
    super();
    this.addRequest(action, content, path);
  }
}
