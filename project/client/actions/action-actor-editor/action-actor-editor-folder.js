
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionActorEditorFolderGet extends Action {
  constructor(projectId, key, title, data) {
    super(projectId, key, title, data);
  }
}

export class ActionActorEditorFolderNew extends Action {
  constructor(projectId, title, data) {
    super(projectId, title, data);
  }
}

export class ActionActorEditorFolderAdd extends Action {
  constructor(projectId, title, data) {
    super(projectId, title, data);
  }
}

export class ActionActorEditorFolderUpdate extends Action {
  constructor(projectId, title, newTitle, data) {
    super(projectId, title, newTitle, data);
  }
}

export class ActionActorEditorFolderRemove extends Action {
  constructor(projectId, title, key, data) {
    super(projectId, title, key, data);
  }
}

export class ActionActorEditorFolderDelete extends Action {
  constructor(projectId, title, key, data) {
    super(projectId, title, key, data);
  }
}
