
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionActorEditorProjectGet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionActorEditorProjectUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionActorEditorProjectToggle extends Action {
  constructor(projectId, key, expanded) {
    super(projectId, key, expanded);
  }
}
