
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionLoginWsGet, ActionLoginGet, ActionLoginUpdate, ActionLoginReposGet, ActionLoginReposUpdate, ActionLoginPing, ActionLoginButtonsAdd, ActionLoginButtonsGet, ActionLoginButtonsUpdate, ActionLoginSpecificationUpdate, ActionLoginDataGet } from './action-login';


export class DataActionLoginGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionLoginGet());
  }
}

export class DataActionLoginWsGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionLoginWsGet());
  }
}

export class DataActionLoginUpdate extends DataAction {
  constructor(labId, userId, repo, systemUnderTest, systemUnderTestInstance, nodes, forceLocalhost) {
    super();
    this.addRequest(new ActionLoginUpdate(labId, userId, repo, systemUnderTest, nodes, forceLocalhost), labId, userId, repo, systemUnderTest, systemUnderTestInstance, nodes, forceLocalhost);
  }
}

export class DataActionLoginRepoUpdate extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionLoginRepoUpdate());
  }
}

export class DataActionLoginPing extends DataAction {
  constructor(chosenAddresses) {
    super();
    this.addRequest(new ActionLoginPing(), chosenAddresses);
  }
}

export class DataActionLoginCreateInterfaces extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.interfaceParameters, action.password);
  }
}

export class DataActionLoginDeleteInterfaces extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.interfaceParameters, action.password);
  }
}

export class DataActionLoginButtonsAdd extends DataAction {
  constructor(key, valueObject) {
    super();
    this.addRequest(new ActionLoginButtonsAdd(), key, valueObject);
  }
}

export class DataActionLoginButtonsGet extends DataAction {
  constructor(key) {
    super();
    this.addRequest(new ActionLoginButtonsGet(), key);
  }
}

export class DataActionLoginButtonsUpdate extends DataAction {
  constructor(key, valueObject) {
    super();
    this.addRequest(new ActionLoginButtonsUpdate(), key, valueObject);
  }
}

export class DataActionLoginDataGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionLoginDataGet());
  }
}
