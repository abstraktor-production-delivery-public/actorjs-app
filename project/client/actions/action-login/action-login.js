
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionLoginMounted extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginWsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginGet extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginUpdate extends Action {
  constructor(labId, userId, repo, systemUnderTest, systemUnderTestInstance, nodes, forceLocalhost) {
    super(labId, userId, repo, systemUnderTest, systemUnderTestInstance, nodes, forceLocalhost);
  }
}

export class ActionLoginReposGet extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginReposUpdate extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginReposClear extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginDataChanged extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginPing extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginCreateInterfaces extends Action {
  constructor(interfaceParameters, password) {
    super(interfaceParameters, password);
  }
}

export class ActionLoginDeleteInterfaces extends Action {
  constructor(interfaceParameters, password) {
    super(interfaceParameters, password);
  }
}

export class ActionLoginButtonsAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginButtonsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionLoginButtonsUpdate extends Action {
  constructor(tab, button, value) {
    super(tab, button, value);
  }
}

export class ActionLoginDataGet extends Action {
  constructor() {
    super();
  }
}
