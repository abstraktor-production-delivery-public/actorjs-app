
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionSystemUnderTestGet, ActionSystemUnderTestAdd, ActionSystemUnderTestDelete, ActionSystemUnderTestUpdate, ActionSystemUnderTestRepoAdd, ActionSystemUnderTestSpecificationUpdate, ActionSystemUnderTestInstancesUpdate, ActionSystemUnderTestNodesUpdate } from './action-system-under-test';


export class DataActionSystemUnderTestGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionSystemUnderTestGet());
  }
}

export class DataActionSystemUnderTestAdd extends DataAction {
  constructor(repo, name, description) {
    super();
    this.addRequest(new ActionSystemUnderTestAdd(repo, name, description), repo, name, description);
  }
}

export class DataActionSystemUnderTestDelete extends DataAction {
  constructor(suts) {
    super();
    this.addRequest(new ActionSystemUnderTestDelete(suts), suts);
  }
}

export class DataActionSystemUnderTestUpdate extends DataAction {
  constructor(systemUnderTests) {
    super();
    this.addRequest(new ActionSystemUnderTestUpdate(systemUnderTests), systemUnderTests);
  }
}

export class DataActionSystemUnderTestRepoAdd extends DataAction {
  constructor(repo) {
    super();
    this.addRequest(new ActionSystemUnderTestRepoAdd(repo), repo);
  }
}

export class DataActionSystemUnderTestRepoGet extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action);
  }
}

export class DataActionSystemUnderTestSpecificationUpdate extends DataAction {
  constructor(sut, specification) {
    super();
    this.addRequest(new ActionSystemUnderTestSpecificationUpdate(), sut, specification);
  }
}

export class DataActionSystemUnderTestInstancesUpdate extends DataAction {
  constructor(sut, instances) {
    super();
    this.addRequest(new ActionSystemUnderTestInstancesUpdate(), sut, instances);
  }
}

export class DataActionSystemUnderTestNodesUpdate extends DataAction {
  constructor(sut, instances) {
    super();
    this.addRequest(new ActionSystemUnderTestNodesUpdate(), sut, instances);
  }
}

