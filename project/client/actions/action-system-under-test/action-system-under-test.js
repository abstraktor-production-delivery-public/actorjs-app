
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionSystemUnderTestSet extends Action {
  constructor(name) {
    super(name);
  }
}

export class ActionSystemUnderTestGet extends Action {
  constructor() {
    super();
  }
}

export class ActionSystemUnderTestClear extends Action {
  constructor() {
    super();
  }
}

export class ActionSystemUnderTestAdd extends Action {
  constructor(repo, name, description) {
    super(repo, name, description);
  }
}

export class ActionSystemUnderTestDelete extends Action {
  constructor(suts) {
    super(suts);
  }
}

export class ActionSystemUnderTestUpdate extends Action {
  constructor(systemUnderTests) {
    super(systemUnderTests);
  }
}

export class ActionSystemUnderTestClearResult extends Action {
  constructor() {
    super();
  }
}

// SITE

export class ActionSystemUnderTestChecked extends Action {
  constructor(sutRepo, sutName, checked) {
    super(sutRepo, sutName, checked);
  }
}

export class ActionSystemUnderTestRepoAdd extends Action {
  constructor(repo) {
    super(repo);
  }
}

export class ActionSystemUnderTestRepoGet extends Action {
  constructor() {
    super();
  }
}

// - Specification

export class ActionSystemUnderTestSpecificationMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionSystemUnderTestSpecificationMarkup extends Action {
  constructor(markup) {
    super();
  }
}

export class ActionSystemUnderTestSpecificationMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionSystemUnderTestSpecificationMarkupSave extends Action {
  constructor(sut, markup) {
    super(sut, markup);
  }
}

export class ActionSystemUnderTestSpecificationUpdate extends Action {
  constructor() {
    super();
  }
}

// - Instances

export class ActionSystemUnderTestInstancesMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionSystemUnderTestInstancesMarkup extends Action {
  constructor(markup) {
    super();
  }
}

export class ActionSystemUnderTestInstancesMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionSystemUnderTestInstancesMarkupSave extends Action {
  constructor(sut, markup) {
    super(sut, markup);
  }
}

export class ActionSystemUnderTestInstancesUpdate extends Action {
  constructor() {
    super();
  }
}

// - Nodes

export class ActionSystemUnderTestNodesMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionSystemUnderTestNodesMarkup extends Action {
  constructor(markup) {
    super();
  }
}

export class ActionSystemUnderTestNodesMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionSystemUnderTestNodesMarkupSave extends Action {
  constructor(sut, markup) {
    super(sut, markup);
  }
}

export class ActionSystemUnderTestNodesUpdate extends Action {
  constructor() {
    super();
  }
}

