
'use strict';

import RealtimeAction from 'z-abs-corelayer-client/client/communication/realtime-action';
import { ActionTestSuiteExecutionStart, ActionTestSuiteExecutionStop } from './action-test-suite';


export class RealtimeActionTestSuiteExecutionStart extends RealtimeAction {
  constructor(repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, systemsUnderTestNames, systemsUnderTestNodes, config, iterationsTs) {
    super();
    this.addRequest(new ActionTestSuiteExecutionStart(repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, config), repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, systemsUnderTestNames, systemsUnderTestNodes, config, iterationsTs);
  }
}

export class RealtimeActionTestSuiteExecutionStop extends RealtimeAction {
  constructor(repoName, sutName, futName, tsName) {
    super();
    this.addRequest(new ActionTestSuiteExecutionStop(repoName, sutName, futName, tsName), repoName, sutName, futName, tsName);
  }
}
