
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionTestSuiteMounted extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteGet extends Action {
  constructor(repo, sut, fut, ts, key, commentOut, pendings) {
    super(repo, sut, fut, ts, key, commentOut, pendings ? pendings : {
      pendings: 0,
      testSuite: new Map()
    });
  }
}

export class ActionTestSuiteAdd extends Action {
  constructor(repo, sut, fut, ts, description) {
    super(repo, sut, fut, ts, description);
  }
}

export class ActionTestSuiteDelete extends Action {
  constructor(repo, sut, fut, tses) {
    super(repo, sut, fut, tses);
  }
}

export class ActionTestSuiteUpdate extends Action {
  constructor(repo, sut, fut, ts, description, dataTs) {
    super(repo, sut, fut, ts, description, dataTs);
  }
}

export class ActionTestSuiteButtonsAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteButtonsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteButtonsUpdate extends Action {
  constructor(tab, button, value) {
    super(tab, button, value);
  }
}

export class ActionTestSuiteMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionTestSuiteMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteMarkupSave extends Action {
  constructor(repo, sut, fut, ts) {
    super(repo, sut, fut, ts);
  }
}

export class ActionTestSuiteExecutionStart extends Action {
  constructor(repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, config) {
    super(repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, config);
  }
}

export class ActionTestSuiteExecutionStop extends Action {
  constructor(repoName, sutName, futName, tsName) {
    super(repoName, sutName, futName, tsName);
  }
}

export class ActionTestSuiteClear extends Action {
  constructor(tab) {
    super(tab);
  }
}

export class ActionTestSuiteRecentGet extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuiteRecentUpdate extends Action {
  constructor(repoName, sutName, futName, tsName) {
    super(repoName, sutName, futName, tsName);
  }
}

export class ActionTestSuiteRecentDelete extends Action {
  constructor(repo, sut, fut, tses) {
    super(repo, sut, fut, tses);
  }
}

export class ActionTestSuiteIterations extends Action {
  constructor(iterationsTs) {
    super(iterationsTs);
  }
}
