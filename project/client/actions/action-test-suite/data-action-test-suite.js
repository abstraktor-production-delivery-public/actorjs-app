
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionTestSuiteGet, ActionTestSuiteAdd, ActionTestSuiteDelete, ActionTestSuiteUpdate, ActionTestSuiteRecentGet, ActionTestSuiteRecentUpdate, ActionTestSuiteRecentDelete, ActionTestSuiteButtonsAdd, ActionTestSuiteButtonsGet } from './action-test-suite';


export class DataActionTestSuiteGet extends DataAction {
  constructor(repo, sut, fut, ts, key, commentOut, pendings) {
    super();
    this.addRequest(new ActionTestSuiteGet(repo, sut, fut, ts, key, commentOut, pendings), repo, sut, fut, ts);
  }
}

export class DataActionTestSuiteAdd extends DataAction {
  constructor(repo, sut, fut, ts, description, dataTestSuite) {
    super();
    this.addRequest(new ActionTestSuiteAdd(repo, sut, fut, ts, description), repo, sut, fut, ts, description, dataTestSuite);
  }
}

export class DataActionTestSuiteDelete extends DataAction {
  constructor(repo, sut, fut, tses) {
    super();
    this.addRequest(new ActionTestSuiteDelete(repo, sut, fut, tses), repo, sut, fut, tses);
  }
}

export class DataActionTestSuiteUpdate extends DataAction {
  constructor(repo, sut, fut, ts, description, dataTs) {
    super();
    this.addRequest(new ActionTestSuiteUpdate(repo, sut, fut, ts, description, dataTs), repo, sut, fut, ts, description, dataTs);
  }
}

export class DataActionTestSuiteRecentGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionTestSuiteRecentGet());
  }
}

export class DataActionTestSuiteRecentUpdate extends DataAction {
  constructor(repo, sut, fut, ts) {
    super();
    this.addRequest(new ActionTestSuiteRecentUpdate(repo, sut, fut, ts), repo, sut, fut, ts);
  }
}

export class DataActionTestSuiteRecentDelete extends DataAction {
  constructor(repo, sut, fut, tses) {
    super();
    this.addRequest(new ActionTestSuiteRecentDelete(repo, sut, fut, tses), repo, sut, fut, tses);
  }
}

export class DataActionTestSuiteButtonsAdd extends DataAction {
  constructor(key, valueObject) {
    super();
    this.addRequest(new ActionTestSuiteButtonsAdd(), key, valueObject);
  }
}

export class DataActionTestSuiteButtonsGet extends DataAction {
  constructor(key) {
    super();
    this.addRequest(new ActionTestSuiteButtonsGet(), key);
  }
}

export class DataActionTestSuiteButtonsUpdate extends DataAction {
  constructor(action, key, valueObject) {
    super();
    this.addRequest(action, key, valueObject);
  }
}
