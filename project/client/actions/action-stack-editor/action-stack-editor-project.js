
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionStackEditorProjectGet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionStackEditorProjectUpdate extends Action {
  constructor(projectId) {
    super(projectId);
  }
}

export class ActionStackEditorProjectToggle extends Action {
  constructor(projectId, key, expanded) {
    super(projectId, key, expanded);
  }
}

export class ActionStackEditorWizard extends Action {
  constructor(projectId, path, name, types, templateDatas, transportName) {
    super(projectId, path, name, types, templateDatas, transportName);
  }
}