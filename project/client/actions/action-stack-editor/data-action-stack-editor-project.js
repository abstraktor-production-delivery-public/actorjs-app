
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionStackEditorProjectGet, ActionStackEditorProjectUpdate } from './action-stack-editor-project';


export class DataActionStackEditorProjectGet extends DataAction {
  constructor(url, query) {
    super();
    this.addRequest(new ActionStackEditorProjectGet(url, query));
  }
}

export class DataActionStackEditorProjectUpdate extends DataAction {
  constructor(projectId, path, content) {
    super();
    this.addRequest(new ActionStackEditorProjectUpdate(projectId), path, content);
  }
}

export class DataActionStackEditorProjectToggle extends DataAction {
  constructor(action, content, path) {
    super();
    this.addRequest(action, content, path);
  }
}

export class DataActionStackEditorWizard extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.path, action.name, action.types, action.templateDatas, action.transportName);
  }
}
