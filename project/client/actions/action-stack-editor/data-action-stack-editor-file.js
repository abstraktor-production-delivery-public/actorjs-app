
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionStackEditorFileGet, ActionStackEditorFileUpdate } from './action-stack-editor-file';


export class DataActionStackEditorFileGet extends DataAction {
  constructor(file, projectId, key, title, path, type) {
    super();
    this.addRequest(new ActionStackEditorFileGet(projectId, key, title, path, type), file);
  }
}

export class DataActionStackEditorFileNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, action.path, action.name, action.type, action.templateName);
  }
}

export class DataActionStackEditorFileRename extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionStackEditorFileUpdate extends DataAction {
  constructor(key, title, path, content) {
    super();
    this.addRequest(new ActionStackEditorFileUpdate(key), `${path}/${title}`, content);
  }
}

export class DataActionStackEditorFileDelete extends DataAction {
  constructor(file, action) {
    super();
    this.addRequest(action, file);
  }
}

export class DataActionstackEditorBuild extends DataAction {
  constructor(content, path, file, action) {
    super();
    this.addRequest(action, content, path, file);
  }
}

