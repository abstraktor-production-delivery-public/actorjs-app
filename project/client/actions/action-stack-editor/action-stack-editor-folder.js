
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionStackEditorFolderGet extends Action {
  constructor(projectId, key, title, data) {
    super(projectId, key, title, data);
  }
}

export class ActionStackEditorFolderNew extends Action {
  constructor(projectId, title, data) {
    super(projectId, title, data);
  }
}

export class ActionStackEditorFolderAdd extends Action {
  constructor(projectId, title, data) {
    super(projectId, title, data);
  }
}

export class ActionStackEditorFolderUpdate extends Action {
  constructor(projectId, title, newTitle, data) {
    super(projectId, title, newTitle, data);
  }
}

export class ActionStackEditorFolderRemove extends Action {
  constructor(projectId, title, key, data) {
    super(projectId, title, key, data);
  }
}

export class ActionStackEditorFolderDelete extends Action {
  constructor(projectId, title, key, data) {
    super(projectId, title, key, data);
  }
}
