
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionStackEditorFolderNew extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`);
  }
}

export class DataActionStackEditorFolderUpdate extends DataAction {
  constructor(oldPath, newPath, action) {
    super();
    this.addRequest(action, oldPath, newPath);
  }
}

export class DataActionStackEditorFolderDelete extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action, `${action.data.path}/${action.title}`);
  }
}
