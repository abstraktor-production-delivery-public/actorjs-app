
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionStackEditorPersistentDataAdd extends Action {
  constructor() {
    super();
  }
}

export class ActionStackEditorPersistentDataGet extends Action {
  constructor() {
    super();
  }
}

export class ActionStackEditorPersistentDataUpdate extends Action {
  constructor(parentName, name, value) {
    super(parentName, name, value);
  }
}
