
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionStackEditorFileGet extends Action {
  constructor(projectId, key, title, path, type) {
    super(projectId, key, title, path, type);
  }
}

export class ActionStackEditorFileSet extends Action {
  constructor(url, query) {
    super(url, query);
  }
}

export class ActionStackEditorFileAdd extends Action {
  constructor(projectId, title, path, type) {
    super(projectId, title, path, type);
  }
}

export class ActionStackEditorFileNew extends Action {
  constructor(projectId, path, name, type, templateName) {
    super(projectId, path, name, type, templateName);
  }
}

export class ActionStackEditorFileRename extends Action {
  constructor(projectId, path, title, newTitle) {
    super(projectId, path, title, newTitle);
  }
}

export class ActionStackEditorFileUpdate extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionStackEditorFileUpdateAll extends Action {
  constructor(keys) {
    super(keys);
  }
}

export class ActionStackEditorFileClose extends Action {
  constructor(key) {
    super(key);
  }
}

export class ActionStackEditorFileRemove extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionStackEditorFileDelete extends Action {
  constructor(projectId, key) {
    super(projectId, key);
  }
}

export class ActionStackEditorFileEdit extends Action {
  constructor(projectId, key, content) {
    super(projectId, key, content);
  }
}

export class ActionStackEditorFileMove extends Action {
  constructor(fromKey, toKey) {
    super(fromKey, toKey);
  }
}

export class ActionStackEditorBuild extends Action {
  constructor() {
    super();
  }
}

