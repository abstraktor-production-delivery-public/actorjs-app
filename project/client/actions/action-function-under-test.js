
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class ActionFunctionUnderTestGet extends Action {
  constructor(repo, sut) {
    super(repo, sut);
  }
}

export class DataActionFunctionUnderTestGet extends DataAction {
  constructor(repo, sut) {
    super();
    this.addRequest(new ActionFunctionUnderTestGet(repo, sut), repo, sut);
  }
}

export class ActionFunctionUnderTestAdd extends Action {
  constructor(repo, sut, fut, description) {
    super(repo, sut, fut, description);
  }
}

export class ActionFunctionUnderTestDelete extends Action {
  constructor(currentRepo, currentSut, futs) {
    super(currentRepo, currentSut, futs);
  }
}

export class DataActionFunctionUnderTestAdd extends DataAction {
  constructor(repo, sut, fut, description) {
    super();
    this.addRequest(new ActionFunctionUnderTestAdd(repo, sut, fut, description), repo, sut, fut, description);
  }
}

export class DataActionFunctionUnderTestDelete extends DataAction {
  constructor(currentRepo, currentSut, futs) {
    super();
    this.addRequest(new ActionFunctionUnderTestDelete(currentRepo, currentSut, futs), futs);
  }
}
