
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionSutFilter extends Action {
  constructor(sut) {
    super(sut);
  }
}

export class ActionFutFilter extends Action {
  constructor(fut) {
    super(fut);
  }
}

export class ActionHideFilter extends Action {
  constructor(hide) {
    super(hide);
  }
}
