
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionConsoleClear extends DataAction {
  constructor(action) {
    super();
    this.addRequest(action);
  }
}