
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionPluginsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionPluginsBundleGet extends Action {
  constructor(bundle, type, guid) {
    super(bundle, type, guid);
  }
}
