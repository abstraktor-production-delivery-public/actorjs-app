
'use strict';

import { ActionPluginsGet, ActionPluginsBundleGet } from './action-plugins';
import DataAction from 'z-abs-corelayer-client/client/communication/data-action';


export class DataActionPluginsGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionPluginsGet());
  }
}

export class DataActionPluginsBundleGet extends DataAction {
  constructor(bundle, type, guid) {
    super();
    this.addRequest(new ActionPluginsBundleGet(bundle, type, guid), bundle, type);
  }
}
