
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


// SITE

export class ActionTestSuitesSutChecked extends Action {
  constructor(sut, checked) {
    super(sut, checked);
  }
}

export class ActionTestSuitesFutChecked extends Action {
  constructor(repoName, sutName, futName, checked) {
    super(repoName, sutName, futName, checked);
  }
}

export class ActionTestSuitesTsChecked extends Action {
  constructor(tsName, checked) {
    super(tsName, checked);
  }
}

export class ActionTestSuitesSetView extends Action {
  constructor(currentView) {
    super(currentView);
  }
}

export class ActionTestSuitesSutClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuitesFutClear extends Action {
  constructor() {
    super();
  }
}

export class ActionTestSuitesTsClear extends Action {
  constructor() {
    super();
  }
}
