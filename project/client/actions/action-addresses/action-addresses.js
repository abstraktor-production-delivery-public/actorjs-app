
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionAddressesSrcGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesSrcUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesDnsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesDnsUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesDstGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesDstUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesSrvGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesSrvUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesClientAddressGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesClientAddressUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesSutAddressGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesSutAddressUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesServerAddressGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesServerAddressUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesClientInterfaceGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesClientInterfaceUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesSutInterfaceGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesSutInterfaceUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesServerInterfaceGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesServerInterfaceUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesNetworkInterfaceGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesNetworkInterfaceUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesPortsGet extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesPortsUpdate extends Action {
  constructor(globals, locals) {
    super(globals, locals);
  }
}

export class ActionAddressesMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionAddressesMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesMarkupSave extends Action {
  constructor() {
    super();
  }
}

export class ActionAddressesLabUserFilter extends Action {
  constructor(labUserFilter) {
    super(labUserFilter);
  }
}

