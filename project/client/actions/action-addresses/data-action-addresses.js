
'use strict';

import DataAction from 'z-abs-corelayer-client/client/communication/data-action';
import { ActionAddressesSrcGet, ActionAddressesSrcUpdate, ActionAddressesDnsGet, ActionAddressesDnsUpdate, ActionAddressesDstGet, ActionAddressesDstUpdate, ActionAddressesSrvGet, ActionAddressesSrvUpdate, ActionAddressesClientAddressGet, ActionAddressesClientAddressUpdate, ActionAddressesSutAddressGet, ActionAddressesSutAddressUpdate, ActionAddressesServerAddressGet, ActionAddressesServerAddressUpdate, ActionAddressesClientInterfaceGet, ActionAddressesClientInterfaceUpdate, ActionAddressesSutInterfaceGet, ActionAddressesSutInterfaceUpdate, ActionAddressesServerInterfaceGet, ActionAddressesServerInterfaceUpdate, ActionAddressesNetworkInterfaceGet, ActionAddressesNetworkInterfaceUpdate, ActionAddressesPortsGet, ActionAddressesPortsUpdate } from './action-addresses';


export class DataActionAddressesSrcGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesSrcGet());
  }
}

export class DataActionAddressesSrcUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesSrcUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesDnsGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesDnsGet());
  }
}

export class DataActionAddressesDnsUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesDnsUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesDstGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesDstGet());
  }
}

export class DataActionAddressesDstUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesDstUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesSrvGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesSrvGet());
  }
}

export class DataActionAddressesSrvUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesSrvUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesClientAddressGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesClientAddressGet());
  }
}

export class DataActionAddressesClientAddressUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesClientAddressUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesSutAddressGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesSutAddressGet());
  }
}

export class DataActionAddressesSutAddressUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesSutAddressUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesServerAddressGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesServerAddressGet());
  }
}

export class DataActionAddressesServerAddressUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesServerAddressUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesClientInterfaceGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesClientInterfaceGet());
  }
}

export class DataActionAddressesClientInterfaceUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesClientInterfaceUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesSutInterfaceGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesSutInterfaceGet());
  }
}

export class DataActionAddressesSutInterfaceUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesSutInterfaceUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesServerInterfaceGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesServerInterfaceGet());
  }
}

export class DataActionAddressesServerInterfaceUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesServerInterfaceUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesNetworkInterfaceGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesNetworkInterfaceGet());
  }
}

export class DataActionAddressesNetworkInterfaceUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesNetworkInterfaceUpdate(globals, locals), globals, locals);
  }
}

export class DataActionAddressesPortsGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionAddressesPortsGet());
  }
}

export class DataActionAddressesPortsUpdate extends DataAction {
  constructor(globals, locals) {
    super();
    this.addRequest(new ActionAddressesPortsUpdate(globals, locals), globals, locals);
  }
}
