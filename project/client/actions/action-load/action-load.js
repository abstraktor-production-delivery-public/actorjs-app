
'use strict';

import Action from 'z-abs-corelayer-client/client/communication/action';


export class ActionLoadMarkupChange extends Action {
  constructor(markup) {
    super(markup);
  }
}

export class ActionLoadMarkup extends Action {
  constructor() {
    super();
  }
}

export class ActionLoadMarkupCancel extends Action {
  constructor() {
    super();
  }
}

export class ActionLoadMarkupSave extends Action {
  constructor() {
    super();
  }
}