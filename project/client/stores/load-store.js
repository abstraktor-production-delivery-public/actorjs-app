
'use strict';

import { DataActionLoadEnvironmentGet, DataActionLoadEnvironmentUpdate } from '../actions/action-load/data-action-load';
//import MarkupLoad from 'z-abs-complayer-markup-client/client/markup/markup-load/markup-load-environment';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class LoadStore extends StoreBaseData {
  constructor() {
    super({
      testDataLocalGlobals: [],
      testDataLocalLocals: [],
      currentView: '',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      }
    });
  }
  
  onActionLoadEnvironmentGet(action) {
    this.sendDataAction(new DataActionLoadEnvironmentGet());
  }
  
  onActionLoadMarkup(action) {
    let markup = this.markupStringify();
    this.updateState({markup: { definition: {$set: true}}});
    this.updateState({markup: { content: {$set: markup}}});
    this.updateState({markup: { contentOriginal: {$set: markup}}});
  }
  
  onActionLoadMarkupSave(action) {
    const result = this.markupParse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      if('environment' === this.state.currentView) {
        this.sendDataAction(new DataActionLoadEnvironmentUpdate(result.globals, result.locals));
      }
      this.clearMarkup();
    }
  }
  
  onActionLoadMarkupCancel(action) {
     this.clearMarkup();
  }
  
  onActionLoadMarkupChange(action) {
    const result = this.markupParse(action.markup);
    this.updateState({markup: { rows: {$set: result.rows}}});
    this.updateState({markup: { content: {$set: action.markup}}});
  }
  
  onDataActionLoadEnvironmentGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testDataEnvironmentGlobals: {$set: response.data[0]}});
      this.updateState({testDataEnvironmentLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'environment'}});
    }
  }
    
  onDataActionLoadEnvironmentUpdate(action) {
    this.sendDataAction(new DataActionLoadEnvironmentGet());
  }
  
  clearMarkup() {
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
  
  markupStringify() {
    if('environment' === this.state.currentView) {
      //return MarkupLoadEnvironment.stringify(this.state.testDataEnvironmentGlobals, this.state.testDataEnvironmentLocals);
    }
  }
  
  markupParse(markup) {
    if('environment' === this.state.currentView) {
      //return MarkupLoadEnvironment.parse(markup);
    }
  }
}


module.exports = LoadStore.export(LoadStore);
