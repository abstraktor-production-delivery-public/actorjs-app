
'use strict';

import MessageNewActorIndex from '../communication/messages/messages-c-to-c/message-new-actor-index';
import MessageTestCaseNotStarted from '../communication/messages/messages-c-to-c/message-test-case-not-started';
import MessageTestCaseClear from '../communication/messages/messages-c-to-c/message-test-case-clear';
import LoginStore from './login-store';
import { DataActionTestCaseGet, DataActionTestCaseAdd, DataActionTestCaseDelete, DataActionTestCaseRename, DataActionTestCaseUpdate, DataActionTestCaseRecentGet, DataActionTestCaseRecentUpdate, DataActionTestCaseRecentDelete, DataActionTestCaseActorsFileGet, DataActionTestCaseDebugGet, DataActionTestCaseDebugBreakpointClearAll, DataActionTestCaseAnalyze, DataActionTestCaseButtonsAdd, DataActionTestCaseButtonsGet, DataActionTestCaseButtonsUpdate, DataActionTestCaseSpecificationUpdate, DataActionTestCaseStacksGet } from '../actions/action-test-case/data-action-test-case';
import { RealtimeActionTestCaseExecutionStart, RealtimeActionTestCaseDebugStart, RealtimeActionTestCaseDebugBreakpointSet, RealtimeActionTestCaseDebugBreakpointClear, RealtimeActionTestCaseDebugBreakpointUpdate, RealtimeActionTestCaseDebugGetMembers, RealtimeActionTestCaseDebugGetProperty, RealtimeActionTestCaseDebugGetSource } from '../actions/action-test-case/realtime-action-test-case';
import MarkupTestCase from 'z-abs-complayer-markup-client/client/markup/markup-test-case/markup-test-case';
import MarkupDocumentationPage from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-page';
import DataTestCase  from 'z-abs-complayer-markup-client/client/data/data-test-case/data-test-case';
import MessageTestCaseDebugContinue from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-debug-continue';
import MessageTestCaseDebugPause from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-debug-pause';
import MessageTestCaseDebugStepOver from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-debug-step-over';
import MessageTestCaseDebugStepIn from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-debug-step-in';
import MessageTestCaseDebugStepOut from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-debug-step-out';
import MessageTestCaseStop from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-case-stop';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class TestCaseStore extends StoreBaseRealtime {
  static DEBUGGER_RUNNING = 0;
  static DEBUGGER_BREAK = 1;
  static DEBUGGER_NOT_RUNNING = 2;
  
  static DEBUG_LOCAL = 0;
  static DEBUG_REMOTE = 1;
  
  static EXECUTION_NOT_RUNNING = 0;
  static EXECUTION_RUNNING = 1;
  static EXECUTION_DISABLED = 2;
  
  static REALTIME_CONNECTION_CLOSED = 0;
  static REALTIME_CONNECTION_OPEN = 1;
  static REALTIME_CONNECTION_ERROR = 2;
  
  constructor() {
    super({
      testCase: undefined,
      error: null,
      analyze: {
        testData: new Map(),
        dependencies: new Map(),
        time: ''
      },
      dbugger: {
        breakpoints: [],
        state: TestCaseStore.DEBUGGER_NOT_RUNNING,
        stack: [],
        level: 0,
        actor: {
          index: -1,
          name: ''
        },
        scopesOpen: new Map(),
        currentThis: null,
        currentValue: null,
        objectValues: new Map(),
        actorFiles: new Map(),
        scripts: new Map()
      },
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      },
      execution: TestCaseStore.EXECUTION_NOT_RUNNING,
      realtimeConnection: TestCaseStore.REALTIME_CONNECTION_CLOSED,
      recentTestCases: [],
      testCases: [],
      buttonsLoaded: false,
      buttons: {
        sequenceDiagram: {
          zoom: 1.0,
          width: 180,
          scroll: true,
          
          executedStateEvents: true,
          notExecutedStateEvents: false,
          
          serverEvents: false,
          connectionEvents: false,
          messageEvents: true,
          messageDetailEvents: false,
          
          protocolData: true,
          protocolInfoData: false,
          protocolIpData: true,
          protocolAddressNameData: false,
          protocolNameData: true,
          protocolTransportData: false,
          protocolInstanceData: true,
          
          stackEvents: true,
          stackInfoEvents: false,
          stackInstanceEvents: true,
          stackProtocolEvents: true,
          
          guiEvents: true,
          guiObjectEvents: true,
          guiActionEvents: true,
          guiFunctionEvents: false,
          guiInfoData: true,
          guiInstanceData: false,
          guiProtocolData: false,
          
          phases: [true, true, true, true, true],
          
          ////////////////
          
          showMessageContent: false,
          showMessageContentOpen: false,
          showSentMessageNumbers: false,
          showReceivedMessageNumbers: false,
          showSentMessageDetailNumbers: false,
          showReceivedMessageDetailNumbers: false,
          
          showAddresses: false
        },
        log: {
          filterError: true,
          filterWarning: true,
          filterSuccess: true,
          filterFailure: true,
          filterEngine: false,
          filterTestData: false,
          filterDebug: false,
          filterIP: true,
          filterGUI: true,
          filterBrowserLog: true,
          filterBrowserErr: true,
          filterDetailEngine: false,
          filterDetailTestData: false,
          filterDetailDebug: false,
          filterOpenIp: false,
          filterOpenGui: false,
          filterOpenBrowserLog: false,
          filterOpenBrowserErr: false,
          zoom: 1.0,
          scroll: true
        },
        debug: {
          breakOnEnterRun: false,
          breakOnLeaveRun: false,
          breakOnIpEvent: false,
          breakOnGuiEvent: false,
          debugType: TestCaseStore.DEBUG_LOCAL
        },
        general: {
          asService: false,
          withLogs: true,
          slowOnIpEvent: true,
          slowOnGuiEvent: true
        }
      },
      persistentData: {
        darkMode: false
      },
      specification: {
        definition: false,
        document: [],
        content: undefined,
        contentLines: 0,
        documentationPreview: {
          document: []
          //embededDocuments: new Map(),
          //localNotes: new Map()
        }
      },
      wizard: {
        stacks: [],
        stackTemplates: new Map()
      }
    });
    this.sessionId = undefined;
    this.buttonsKey = '';
    this.on('DataActionLoginUpdate', (store) => {
      this.buttonsKey = `${store.state.login.labId}_${store.state.login.userId}`;
      this.sendDataAction(new DataActionTestCaseButtonsGet(this.buttonsKey));
    });
  }
  
  onActionTestCaseMounted(action) {
    const loginData = LoginStore.isLoggedIn();
    if(loginData) {
      this.buttonsKey = `${loginData.labId}_${loginData.userId}`;
      this.sendDataAction(new DataActionTestCaseButtonsGet(this.buttonsKey));
      this.sendDataAction(new DataActionTestCaseStacksGet());
    }
    else {
      this.once('DataActionLoginGet', (store) => {
        this.buttonsKey = `${store.state.login.labId}_${store.state.login.userId}`;
        this.sendDataAction(new DataActionTestCaseButtonsGet(this.buttonsKey));
        this.sendDataAction(new DataActionTestCaseStacksGet());
      });
    }
  }

  onActionTestCasePersistentDataUpdate(action) {
    const persistentData = this.deepCopy(this.state.persistentData);
    const parent = action.parentName ? Reflect.get(persistentData, action.parentName) : persistentData;
    Reflect.set(parent, action.name, action.value);
    this.updateState({persistentData: {$set: parent}});
  }
  
  onActionTestCaseGet(action) {
    this.clearMarkup();
    this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut, action.tc));
    if(undefined !== action.tc) {
      this.sendDataAction(new DataActionTestCaseRecentUpdate(action.repo, action.sut, action.fut, action.tc));
    }
  }
  
  onActionTestCaseAdd(action) {
    this.sendDataAction(new DataActionTestCaseAdd(action.repo, action.sut, action.fut, action.tc, action.description, new DataTestCase(), action.stackName, action.templateName, action.testData, action.show));
  }
  
  onActionTestCaseDelete(action) {
    this.sendDataAction(new DataActionTestCaseDelete(action.tces));
  }
  
  onActionTestCaseRename(action) {
    this.sendDataAction(new DataActionTestCaseRename(action.repo, action.sut, action.fut, action.currentTcName, action.newTcName, action.description, action.show));
  }
  
  onActionTestCaseMarkup(action) {
    const markupTc = MarkupTestCase.stringify(this.state.testCase);
    this.updateState({markup: {definition: {$set: true}}});
    this.updateState({markup: {content: {$set: markupTc}}});
    this.updateState({markup: {contentOriginal: {$set: markupTc}}});
  }
  
  onActionTestCaseMarkupSave(action) {
    const result = MarkupTestCase.parse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: {rows: {$set: result.rows}}});
    }
    else {
      const actors = this.state.testCase.tc.actors;
      result.tc.actors.forEach((actor) => {
        for(let i = 0; i < actors.length; ++i) {
          if(actors[i].name === actor.name) {
             actor.inlineCode = actors[i].inlineCode;
          }
        }
      });
      this.updateState({testCase: {tc: {$set: result.tc}}});
      this.sendDataAction(new DataActionTestCaseUpdate(action.repo, action.sut, action.fut, action.tc, undefined, this.state.testCase));
      this.clearMarkup();
    }
  }
  
  onActionTestCaseMarkupCancel(action) {
    this.clearMarkup();
  }
  
  onActionTestCaseMarkupChange(action) {
    const result = MarkupTestCase.parse(action.markup);
    this.updateState({markup: {content: {$set: action.markup}}});
    this.updateState({markup: {rows: {$set: result.rows}}});
  }
  
  onActionTestCaseMarkupCopy() {
    const markupTc = MarkupTestCase.stringify(this.state.testCase);
    navigator.clipboard.writeText(markupTc).then(() => {}, () => {});
  }
  
  onActionTestCaseExecutionStart(action) {
    if(TestCaseStore.EXECUTION_NOT_RUNNING === this.state.execution) {
      this.updateState({execution: {$set: TestCaseStore.EXECUTION_DISABLED}});
      this.sessionId = GuidGenerator.create();
      this.sendRealtimeAction(new RealtimeActionTestCaseExecutionStart(action), this.sessionId);
    }
  }
  
  onActionTestCaseExecutionStop(action) {
    this.updateState({execution: {$set: TestCaseStore.EXECUTION_DISABLED}});
    this.sendRealtimeMessage(new MessageTestCaseStop(this.sessionId), null);
  }
  
  onActionTestCaseDebugStart(action) {
    if(TestCaseStore.EXECUTION_NOT_RUNNING === this.state.execution) {
      this.updateState({execution: {$set: TestCaseStore.EXECUTION_DISABLED}});
      this.updateState({dbugger: {state: {$set: TestCaseStore.DEBUGGER_RUNNING}}});
      this.sessionId = GuidGenerator.create();
      this.sendRealtimeAction(new RealtimeActionTestCaseDebugStart(action), this.sessionId);
    }
  }
  
  onActionTestCaseDebugBreakpointSet(action) {
    this.sendRealtimeAction(new RealtimeActionTestCaseDebugBreakpointSet(action), this.sessionId);
  }
  
  onActionTestCaseDebugBreakpointClear(action) {
    this.sendRealtimeAction(new RealtimeActionTestCaseDebugBreakpointClear(action), this.sessionId);
  }
  
  onActionTestCaseDebugBreakpointClearAll(action) {
    this.sendDataAction(new DataActionTestCaseDebugBreakpointClearAll(action), this.sessionId);
  }
  
  onActionTestCaseDebugStopAfterRun(action) {
    this.sendDataAction(new DataActionTestCaseDebugStopAfterRun(action), this.sessionId);
  }
  
  onActionTestCaseDebugBreakpointUpdate(action) {
    this.sendRealtimeAction(new RealtimeActionTestCaseDebugBreakpointUpdate(action), this.sessionId);
  }
  
  onActionTestCaseRecentGet(action) {
    this.sendDataAction(new DataActionTestCaseRecentGet());
  }
  
  onActionTestCaseRecentDelete(action) {
    this.sendDataAction(new DataActionTestCaseRecentDelete(action.repo, action.suts, action.futs, action.tces));
  }
  
  onActionTestCaseActorsFileGet(action) {
    this.sendDataAction(new DataActionTestCaseActorsFileGet(action.actorFiles, action.actorKey));
  }
  
  onActionTestCaseDebugContinue(action) {
    this.sendRealtimeMessage(new MessageTestCaseDebugContinue(this.sessionId), null);
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_RUNNING}
    }});
  }
  
  onActionTestCaseDebugPause(action) {
    this.sendRealtimeMessage(new MessageTestCaseDebugPause(this.sessionId), null);
  }
  
  onActionTestCaseDebugStepOver(action) {
    this.sendRealtimeMessage(new MessageTestCaseDebugStepOver(this.sessionId), null);
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_RUNNING}
    }});
  }
  
  onActionTestCaseDebugStepIn(action) {
    this.sendRealtimeMessage(new MessageTestCaseDebugStepIn(this.sessionId), null);
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_RUNNING}
    }});
  }
  
  onActionTestCaseDebugStepOut(action) {
    this.sendRealtimeMessage(new MessageTestCaseDebugStepOut(this.sessionId), null);
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_RUNNING}
    }});
  }
  
  onActionTestCaseDebugLevel(action) {
    this.sendRealtimeAction(new RealtimeActionTestCaseDebugGetSource(this.state.dbugger.stack[action.level].location.scriptId), this.sessionId);
    const debugData = this._calculateDebugData(undefined, action.level, action.actorIndex);
    if(debugData.thisValue) {
      const name = 'this_' + debugData.thisValue.className;
      const thisScope = debugData.scopesOpen.get(name);
      if(thisScope && thisScope.open[0]) {
        this.sendRealtimeAction(new RealtimeActionTestCaseDebugGetMembers(debugData.thisValue, true), this.sessionId);
      }
    }
    this.updateState({dbugger: {
      level: {$set: action.level},
      actor: {
        index: {$set: debugData.actor.index},
        name: {$set: debugData.actor.name}
      },
      scopesOpen: {$set: debugData.scopesOpen}
    }});
    this.handleRealtimeMessage(new MessageNewActorIndex(debugData.actor.index));
  }

  onActionTestCaseDebugGetMembers(action) {
    if(action.open) {
      this.sendRealtimeAction(new RealtimeActionTestCaseDebugGetMembers(action.object, action.open), this.sessionId);
    }
    else {
      const objectData = this.state.dbugger.objectValues.get(action.object.objectId);
      this.updateState({dbugger: {objectValues: (objectValues) => {
        const members = undefined !== objectData ? objectData.members : undefined;
        const extraMembers = undefined !== objectData ? objectData.extraMembers : undefined;
        objectValues.set(action.object.objectId, {
          open: false,
          members: members,
          extraMembers: extraMembers
        });
      }}});
    }
  }
  
  onActionTestCaseDebugGetProperty(action) {
    this.sendRealtimeAction(new RealtimeActionTestCaseDebugGetProperty(action), this.sessionId);
  }
  
  onActionTestCaseDebugClearCurrentValue(action) {
    this.updateState({dbugger: {currentValue: {$set: ''}}});
  }
  
  onActionTestCaseDebugScopeOpen(action) {
    this.updateState({dbugger: {scopesOpen: (scopesOpen) => {
      const scopeOpen = scopesOpen.get(action.name);
      scopeOpen.open[action.index] = action.open;
    }}});
  }
  
  onActionTestCaseAnalyze(action) {
    this.sendDataAction(new DataActionTestCaseAnalyze(action));
  }
  
  onActionTestCaseAnalyzeClear(action) {
    this.updateState({analyze: {testData: {$set: new Map()}}});
    this.updateState({analyze: {dependencies: {$set: new Map()}}});
    this.updateState({analyze: {time: {$set: ''}}});
  }
  
  onActionTestCaseButtonsUpdate(action) {
    const buttons = this.deepCopy(this.state.buttons);
    const tab = Reflect.get(buttons, action.tab);
    Reflect.set(tab, action.button, action.value);
    this.sendDataAction(new DataActionTestCaseButtonsUpdate(action, this.buttonsKey, buttons));
  }
  
  onActionTestCaseZoom(action) {
    const buttons = this.deepCopy(this.state.buttons);
    buttons.zoom = action.zoom;
    this.sendDataAction(new DataActionTestCaseButtonsUpdate(action, this.buttonsKey, buttons));
  }

  onActionTestCaseSpecificationMarkup(action) {
    const markupDocument = MarkupDocumentationPage.stringify(this.state.specification.document);
    this.updateState({specification: {definition: {$set: true}}});
    this.updateState({specification: {content: {$set: markupDocument}}});
    this.updateState({specification: {contentLines: {$set: markupDocument.split('\n').length}}});
    this.updateState({specification: {documentationPreview: {document: {$set: this.state.specification.document}}}});
  }
  
  onActionTestCaseSpecificationMarkupChange(action) {
    this.updateState({specification: {content: {$set: action.markup}}});
    this.updateState({specification: {contentLines: {$set: action.markup.split('\n').length}}});
    const document = MarkupDocumentationPage.parse(action.markup);
    this.updateState({specification:{documentationPreview: {document: {$set: document}}}});
   // this._getInternalDocumentationData(document, this.state.documentationPreview.embededDocuments, true);
  }
  
  onActionTestCaseSpecificationMarkupCancel(action) {
    this._clearSpecificationMarkup();
    this._clearSpecificationPreview();
  }
  
  onActionTestCaseSpecificationMarkupSave(action) {
    this.sendDataAction(new DataActionTestCaseSpecificationUpdate(action.repoName, action.sutName, action.futName, action.tcName, this.state.testCase, action.markup));
    this._clearSpecificationMarkup();
    this._clearSpecificationPreview();
  }
  
  onDataActionTestCaseGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({error: {$set: null}});
      if(undefined !== response.data.testCases) {
        this.updateState({testCases: {$set: response.data.testCases}}); 
      }
      else if(undefined !== response.data.testCase) {
        this.updateState({testCase: {$set: response.data.testCase}});
        const document = MarkupDocumentationPage.parse(response.data.testCase.specification);
        this.updateState({specification: {document: {$set: document}}});
        this.calculateTestCases();
        const actorFiles = this.state.testCase.tc.actors.map((actor) => {
          return `./${actor.name.split('.').join('/')}.js`;
        });
        this.sendDataAction(new DataActionTestCaseDebugGet(action.repo, action.sut, action.fut, action.tc));
        if(0 !== actorFiles.length) {
          this.updateState({dbugger: {actorFiles: (actorFiles) => {
            actorFiles.clear();
          }}});
          this.sendDataAction(new DataActionTestCaseActorsFileGet(actorFiles));
        }
      }
    }
    else {
      this.updateState({error: {$set: response.result}});
    }
  }
  
  onDataActionTestCaseAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(action.show) {
        this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut, action.tc));
      }
      else {
        this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut));
      }
      this.calculateTestCases();
    }
  }
  
  onDataActionTestCaseUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut, action.tc));
      this.calculateTestCases();
    }
  }
  
  onDataActionTestCaseDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const firstTc = action.tces[0];
      this.sendDataAction(new DataActionTestCaseRecentDelete(firstTc.repo, firstTc.sut, firstTc.fut, action.tces));
      this.sendDataAction(new DataActionTestCaseGet(firstTc.repo, firstTc.sut, firstTc.fut));
      this.calculateTestCases();
    }
  }
  
  onDataActionTestCaseRename(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(action.show) {
        this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut, action.newTcName));
      }
      else {
        this.sendDataAction(new DataActionTestCaseGet(action.repo, action.sut, action.fut));
      }
      this.sendDataAction(new DataActionTestCaseRecentDelete(action.repo, action.sut, action.fut, [{
        repo: action.repo,
        sut: action.sut,
        fut: action.fut,
        tc: action.currentTcName
      }]));
      this.calculateTestCases();
    }
  }
  
  onDataActionTestCaseRecentGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({recentTestCases: {$set: response.data}});
    }
  }
  
  onDataActionTestCaseRecentUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestCaseRecentGet());
    }
  }
  
  onDataActionTestCaseRecentDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestCaseRecentGet());
    }
  }
  
  onDataActionTestCaseActorsFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(!Array.isArray(response.data)) {
        let actorFile = response.data;
        if(null === actorFile) {
          const actor = this.state.testCase.tc.actors.find((actor) => {
            const name = action.actorFiles[0].replaceAll('/', '.');
            return name === `..${actor.name}.js`;
          });
          if(actor) {
            actorFile = actor.inlineCode;
          }
        }
        if(actorFile) {
          const url = action.actorFiles[0];
          const actorFilesSplit = url.split('/');
          const title = actorFilesSplit.pop();
          const name = title.split('.').shift();
          const index = action.actorKey || 0;
          const breaks = this._findFunctionBreaks('*run', actorFile);
          this.updateState({dbugger: {actorFiles: (actorFiles) => {
            actorFiles.set(index, {
              url: url,
              title: title,
              name: name,
              path: actorFilesSplit.join('/'),
              type: url.split('.').pop(),
              content: actorFile,
              lineRunEnter: breaks.enter,
              lineRunLeave: breaks.leave
            });
          }}});
        }
      }
      else {
        let key = 0;
        const exclusiveFile = new Set();
        response.data.forEach((actorFile, index) => {
          if(null === actorFile) {
            const actor = this.state.testCase.tc.actors.find((actor) => {
              const name = action.actorFiles[0].replaceAll('/', '.');
              return name === `..${actor.name}.js`;
            });
            if(actor) {
              actorFile = actor.inlineCode;
            }
          }
          const url = action.actorFiles[index];
          if(!exclusiveFile.has(url)) {
            exclusiveFile.add(url);
            const actorFilesSplit = url.split('/');
            const title = actorFilesSplit.pop();
            const name = title.split('.').shift();
            const breaks = this._findFunctionBreaks('*run', actorFile);
            this.updateState({dbugger: {actorFiles: (actorFiles) => {
              actorFiles.set(key++, {
                url: url,
                title: title,
                name: name,
                path: actorFilesSplit.join('/'),
                type: url.split('.').pop(),
                content: actorFile,
                lineRunEnter: breaks.enter,
                lineRunLeave: breaks.leave
              });
            }}});
          }
        });
      }
    }
  }
  
  onDataActionTestCaseDebugGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {breakpoints: {$set: response.data.breakpoints}}});
    }
  }
  
  onDataActionTestCaseDebugBreakpointClearAll(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {breakpoints: {$set: response.data}}});
    }
  }
  
  onDataActionTestCaseAnalyze(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({analyze: {testData: {$set: new Map(response.data[0])}}});
      this.updateState({analyze: {dependencies: {$set: new Map(response.data[1])}}});
      this.updateState({analyze: {time: {$set: response.data[2]}}});
    }
  }
  
  onDataActionTestCaseButtonsAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({buttons: {$set: response.data}});
    }
  }
  
  onDataActionTestCaseButtonsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const keys = Reflect.ownKeys(this.state.buttons);
      const resutObject = {};
      keys.forEach((key) => {
        const stateObject = Reflect.get(this.state.buttons, key);
        const storedObject = Reflect.get(response.data, key);
        const mergedObject = {...stateObject, ...storedObject};
        Reflect.set(resutObject, key, mergedObject);
      });
      this.updateState({buttonsLoaded: {$set: true}, buttons: {$set: resutObject}});
    }
    else {
      this.sendDataAction(new DataActionTestCaseButtonsAdd(this.buttonsKey, this.state.buttons));
    }
  }
  
  onDataActionTestCaseButtonsUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if('sequenceDiagram' === action.tab) {
        this.updateState({buttons: {sequenceDiagram: {$set: response.data.sequenceDiagram}}});
      }
      else if('log' === action.tab) {
        this.updateState({buttons: {log: {$set: response.data.log}}});
      }
      else if('debug' === action.tab) {
        this.updateState({buttons: {debug: {$set: response.data.debug}}});
      }
      else if('general' === action.tab) {
        this.updateState({buttons: {general: {$set: response.data.general}}});
      }
    }
  }
  
  onDataActionTestCaseSpecificationUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestCaseGet(action.repoName, action.sutName, action.futName, action.tcName));
    }
  }
  
  onDataActionTestCaseStacksGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(response.data) {
        const stacks = ['none'];
        const stackTemplates = new Map();
        response.data.forEach((stack) => {
          stacks.push(stack[0]);
          const stackTemplateMap = new Map(stack[1]);
          stackTemplates.set(stack[0], stackTemplateMap);
        });
        this.updateState({wizard: {stacks: {$set: stacks}}});
        this.updateState({wizard: {stackTemplates: {$set: stackTemplates}}});
      }
    }
  }
  
  onRealtimeActionTestCaseExecutionStart(action) {
    const response = action.getResponse();
    if(!response.isSuccess()) {
      this.updateState({execution: {$set: TestCaseStore.EXECUTION_NOT_RUNNING}});
      const msg = new MessageTestCaseNotStarted(('N/A' === action?.response?.result?.msg) ? ActorResultConst.NA : ActorResultConst.ERROR);
      this.handleRealtimeMessage(msg);
    }
  }
  
  onRealtimeActionTestCaseDebugStart(action) {
    const response = action.getResponse();
    if(!response.isSuccess()) {
      this.updateState({execution: {$set: TestCaseStore.EXECUTION_NOT_RUNNING}});
      this.updateState({dbugger: {state: {$set: TestCaseStore.DEBUGGER_NOT_RUNNING}}});
      const msg = new MessageTestCaseNotStarted(('N/A' === action?.response?.result?.msg[0]) ? ActorResultConst.NA : ActorResultConst.ERROR);
      this.handleRealtimeMessage(msg);
    }
  }
  
  onRealtimeMessageTestCaseStarted(msg) {
    this.updateState({execution: {$set: TestCaseStore.EXECUTION_RUNNING}});
  }
  
  onRealtimeMessageTestCaseStopped(msg) {
    this._clearSession();
    this.updateState({execution: {$set: TestCaseStore.EXECUTION_NOT_RUNNING}});
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_NOT_RUNNING},
      actor: {
        index: {$set: -1},
        name: {$set: ''}
      }
    }});
  }
  
  onRealtimeActionTestCaseDebugBreakpointSet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {breakpoints: {$set: response.data.breakpoints}}});
    }
  }
  
  onRealtimeActionTestCaseDebugBreakpointClear(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {breakpoints: {$set: response.data.breakpoints}}});
    }
  }
  
  onRealtimeActionTestCaseDebugBreakpointUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {breakpoints: {$set: response.data.breakpoints}}});
    }
  }
  
  onRealtimeActionTestCaseDebugGetMembers(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const object = {
        open: true,
        name: action.object.description,
        members: response.data.result,
        extraMembers: response.data.extraResult
      };
      this.updateState({dbugger: {objectValues: (objectValues) => {
        objectValues.set(action.object.objectId, object);
      }}});
      /*if(action.isCurrent) {
        this.updateState({dbugger: {
          currentThis: {$set: object},
          currentValue: {$set: object}
        }});
      }*/
    }
  }
  
  onRealtimeActionTestCaseDebugGetProperty(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(!response.data.name) {
        response.data.name = action.memberNameArray[action.memberNameArray.length - 1];
      }
      this.updateState({dbugger: {currentValue: {$set: response.data}}});
    }
  }
  
  onRealtimeActionTestCaseDebugGetSource(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({dbugger: {scripts: (scripts) => {
        scripts.set(action.scriptId, response.data);
      }}});
    }
  }
  
  onRealtimeMessageTestCaseDebugPaused(msg) {
    const level = 0;
    const debugData = this._calculateDebugData(msg.stack, level, msg.actorIndex);
    if(debugData.thisValue) {
      //const name = 'this_' + debugData.thisValue.className;
      //const thisScope = debugData.scopesOpen.get(name);
      //if(thisScope && thisScope.open[0]) {
        this.sendRealtimeAction(new RealtimeActionTestCaseDebugGetMembers(debugData.thisValue, true, true), this.sessionId);
      //}
    }
   //const objectData = this.state.dbugger.objectValues.get(action.object.objectId);
    /*if(!objectData) {
      
    }*/
    this.updateState({dbugger: {
      state: {$set: TestCaseStore.DEBUGGER_BREAK},
      stack: {$set: msg.stack},
      level: {$set: level},
      actor: {
        index: {$set: debugData.actor.index},
        name: {$set: debugData.actor.name}
      },
      scopesOpen: {$set: debugData.scopesOpen},
      currentThis: {$set: debugData.thisValue},
      currentValue: {$set: debugData.thisValue},
      objectValues: {$set: new Map()},
      scripts: (scripts) => {
        scripts.set(msg.scriptId, msg.script);
      }
    }});
    this.handleRealtimeMessage(new MessageNewActorIndex(debugData.actor.index));
  }
  
  onRealtimeMessageRealtimeOpen(msg) {
    this.updateState({realtimeConnection: {$set: TestCaseStore.REALTIME_CONNECTION_OPEN}});
  }
  
  onRealtimeMessageRealtimeClosed(msg) {
    this.updateState({realtimeConnection: {$set: TestCaseStore.REALTIME_CONNECTION_CLOSED}});
  }
  
  onActionTestCaseClear(action) {
    this.handleRealtimeMessage(new MessageTestCaseClear(action.tab));
  }
    
  calculateTestCases() {
    this.updateState({testCases: (testCases) => {
      testCases.sort((a, b) => {
        return a.name > b.name ? 1 : -1;
      });
    }});
  }
  
  clearMarkup() {
    this.updateState({markup: {definition: {$set: false}}});
    this.updateState({markup: {content: {$set: undefined}}});
    this.updateState({markup: {contentOriginal: {$set: undefined}}});
    this.updateState({markup: {rows: {$set: undefined}}});
  }
  
  _calculateScopes(debugStack, level) {
    const callFrame = debugStack[level];
    if(callFrame) {
      const previousScopesOpen = this.state.dbugger.scopesOpen;
      const scopeStatuses = new Map();
      const nameInstances = new Map();
      if(callFrame.this) {
        const name = 'this_' + callFrame.this.className;
        const thisPreviousScope = previousScopesOpen.get(name);
        if(thisPreviousScope) {
          scopeStatuses.set(name, {
            amount: 1,
            open: [thisPreviousScope.open[0]],
          });
        }
        else {
          scopeStatuses.set(name, {
            amount: 1,
            open: [true],
          });
        }
      }
      callFrame.scopes.forEach((scope) => {
        const name = scope.type + '_' + (scope.name ? scope.name : '');
        let open = !name.startsWith('global');
        const previousScopeOpenGroup = previousScopesOpen.get(name);
        let index = 0;
        if(previousScopeOpenGroup) {
          if(nameInstances.has(name)) {
            if(nameInstances.has(name)) {
              index = ++nameInstances.get(name).index;
            }
            else {
              nameInstances.set(name, {index: 0});
            }
          }
          const previousOpen = previousScopeOpenGroup.open[index];
          if(undefined !== previousOpen) {
            open = previousOpen;
          }
        }
        if(scopeStatuses.has(name)) {
          const scopeStatus = scopeStatuses.get(name);
          ++scopeStatus.amount;
          scopeStatus.open.push(open);
        }
        else {
          scopeStatuses.set(name, {
            amount: 1,
            open: [open],
          });
        }
      });
      return scopeStatuses;
    }
    else {
      return new Map();
    }
  }
  
  _calculateDebugData(debugStack, level, actorIndex) {
    if(!debugStack) {
      debugStack = this.state.dbugger.stack;
    }
    const scopeStatuses = this._calculateScopes(debugStack, level);
    const actorFiles = this.state.dbugger.actorFiles;
    const fileName = actorFiles.get(actorIndex).name;
    const stackLevel = debugStack[level];
    return {
      actor: {
        index: actorIndex,
        name: fileName
      },
      thisValue: stackLevel.this,
      scopesOpen: scopeStatuses
    };
    /*for(let j = 0; j < debugStack.length; ++j) {
      const stackLevel = debugStack[j];
      if(stackLevel.this && stackLevel.this.description && (fileName === stackLevel.this.description || `Proxy(${fileName})` === stackLevel.this.description)) {
        return {
          actor: {
            index: actorIndex,
            name: fileName
          },
          thisValue: stackLevel.this,
          scopesOpen: scopeStatuses
        };
      }
    }
    if(-1 !== this.state.dbugger.actor.index) {
      return {
        actor: this.state.dbugger.actor,
        thisValue: debugStack[level].this,
        scopesOpen: scopeStatuses
      }
    }
    else if(0 !== this.state.dbugger.actorFiles.length) {
      return {
        actor: {
          index: 0,
          name: ''
        },
        thisValue: debugStack[level].this,
        scopesOpen: scopeStatuses
      }
    }*/
  }
  
  // TODO: SHOULD BE STATIC
  STATIC() {
    return 'static';
  }
  
  VISIBLE() {
    return 'visible';
  }
    
  HIDDEN() {
    return 'hidden';
  }
  
  _clearSession() {
    this.sessionId = undefined;
  }
  
  _clearSpecificationMarkup() {
    this.updateState({specification: {definition: {$set: false}}});
    this.updateState({specification: {content: {$set: undefined}}});
    this.updateState({specification: {contentLines: {$set: 0}}});
  }
  
  _clearSpecificationPreview() {
    this.updateState({specification: {documentationPreview: {document: {$set: []}}}});
//    this.updateState({specification: {documentationPreview: {embededDocuments: {$set: new Map()}}}});
//    this.updateState({specification: {documentationPreview: {localNotes: {$set: new Map()}}}});
  }
  
  _findFirstCharacterOf(result, text, index, charString) {
    let bestFoundIndex = -1;
    let foundIndex = -1;
    for(let i = 0; i < charString.length; ++i) {
      const char = charString.charAt(i);
      foundIndex = text.indexOf(char, index);
      if(-1 !== foundIndex && (foundIndex < bestFoundIndex || -1 === bestFoundIndex)) {
        bestFoundIndex = foundIndex;
        result.char = char;
        result.index = foundIndex;
      }
    }
  }
  
  _findFunctionIndexies(actorFile, index) {
    let depth = 1;
    const searchResult = {
      index: -1,
      char: ''
    };
    const result = {
      start: -1,
      stop: -1
    };
    this._findFirstCharacterOf(searchResult, actorFile, index, '{}');
    if(-1 === searchResult.index || '{' !== searchResult.char) {
      return result;
    }
    result.start = searchResult.index;
    index = searchResult.index + 1;
    do {
      this._findFirstCharacterOf(searchResult, actorFile, index, '{}');
      if('{' === searchResult.char) {
        ++depth;
      }
      else {
        --depth;
      }
      index = searchResult.index + 1;
    } while(-1 !== searchResult.index && depth > 0);
    if(0 === depth) {
      if(-1 !== searchResult.index || '}' !== searchResult.char) {
        result.stop = searchResult.index;
      }
    }
    return result;
  }
  
  _findFunctionBreaks(name, actorFile) {
    const lineBreaks = {
      enter: -1,
      leave: -1
    };
    const index = actorFile.indexOf(name);
    if(-1 === index) {
      return lineBreaks;
    }
    const result = this._findFunctionIndexies(actorFile, index);
    if(-1 === result.start) {
      return lineBreaks;
    }
    let line = 0;
    let lineIndex = -1;
    do {
      ++line;
      lineIndex = actorFile.indexOf('\n', lineIndex + 1);
    } while (-1 !== lineIndex && lineIndex < result.start);
    lineBreaks.enter = line;
    do {
      ++line;
      lineIndex = actorFile.indexOf('\n', lineIndex + 1);
    } while (-1 !== lineIndex && lineIndex < result.stop);
    lineBreaks.leave = line - 1;
    return lineBreaks;
  }
}


module.exports = TestCaseStore.export(TestCaseStore);
