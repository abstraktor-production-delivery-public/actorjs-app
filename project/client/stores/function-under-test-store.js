
'use strict';

import TestCaseStore from './test-case-store';
import TestSuiteStore from './test-suite-store';
import { DataActionFunctionUnderTestGet, DataActionFunctionUnderTestAdd, DataActionFunctionUnderTestDelete } from '../actions/action-function-under-test';
import { ActionTestCaseRecentDelete } from '../actions/action-test-case/action-test-case';
import { ActionTestSuiteRecentDelete } from '../actions/action-test-suite/action-test-suite';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class FunctionUnderTestStore extends StoreBaseData {
  constructor() {
    super({
      functionUnderTests: []
    });
  }
  
  onActionFunctionUnderTestGet(action) {
    this.sendDataAction(new DataActionFunctionUnderTestGet(action.repo, action.sut));
  }
  
  onActionFunctionUnderTestDelete(action) {
    this.sendDataAction(new DataActionFunctionUnderTestDelete(action.currentRepo, action.currentSut, action.futs));
  }
  
  onActionFunctionUnderTestAdd(action) {
    this.sendDataAction(new DataActionFunctionUnderTestAdd(action.repo, action.sut, action.fut, action.description));
  }
  
  onDataActionFunctionUnderTestGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({functionUnderTests: {$set: response.data.functionUnderTests}});
    }
  }
  
  onDataActionFunctionUnderTestAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionFunctionUnderTestGet(action.repo, action.sut));
    }
  }
  
  onDataActionFunctionUnderTestDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionFunctionUnderTestGet(action.currentRepo, action.currentSut));
      action.futs.forEach((fut) => {
        TestCaseStore.dispatchAction(new ActionTestCaseRecentDelete(fut.repo, fut.sut, fut.fut));
        TestSuiteStore.dispatchAction(new ActionTestSuiteRecentDelete(fut.repo, fut.sut, fut.fut));
      });
    }
  }
}


module.exports = FunctionUnderTestStore.export(FunctionUnderTestStore);
