
'use strict';

import DocumentationStoreBase from 'z-abs-complayer-documentation-client/client/stores/documentation-store-base';


class EducationStore extends DocumentationStoreBase {
  constructor() {
    super('Education', 'actorjs');
  }
}


module.exports = EducationStore.export(EducationStore);