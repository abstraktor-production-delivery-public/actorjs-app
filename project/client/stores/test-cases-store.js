
'use strict';

import { DataActionTestCaseGet } from '../actions/action-test-case/data-action-test-case';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class TestCasesStore extends StoreBaseData {
  constructor() {
    super({
      systemUnderTestsChecked: new Map(),
      functionUnderTestsChecked: new Map(),
      testCasesChecked: new Map(),
      current: {
        view: 'none'
      }
    });
  }
  
  onActionTestCasesSetView(action) {
    this.updateState({current: {view: {$set: action.currentView}}});
  }    
  
  onActionTestCasesSutChecked(action) {
    if(action.checked) {
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.set(action.sut.name, action.sut);
      }});
    }
    else {
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.delete(action.sut.name);
      }});
    }
  }
  
  onActionTestCasesFutChecked(action) {
    if(action.checked) {
      this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
        functionUnderTestsChecked.set(action.futName, {
          repo: action.repoName,
          sut: action.sutName,
          fut: action.futName
        });
      }});
    }
    else {
      this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
        functionUnderTestsChecked.delete(action.futName);
      }});
    }
  }
  
  onActionTestCasesTcChecked(action) {
    if(action.checked) {
      this.updateState({testCasesChecked: (testCasesChecked) => {
        testCasesChecked.set(action.tcName, {
          repo: action.repoName,
          sut: action.sutName,
          fut: action.futName,
          tc: action.tcName,
          description: ''
        });
      }});
      this.sendDataAction(new DataActionTestCaseGet(action.repoName, action.sutName, action.futName, action.tcName));
    }
    else {
      this.updateState({testCasesChecked: (testCasesChecked) => {
        testCasesChecked.delete(action.tcName);
      }});
    }
  }
  
  onActionTestCasesSutClear(action) {
    this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
      systemUnderTestsChecked.clear();
    }});
  }
  
  onActionTestCasesFutClear(action) {
    this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
      functionUnderTestsChecked.clear();
    }});
  }
  
  onActionTestCasesTcClear(action) {
    this.updateState({testCasesChecked: (testCasesChecked) => {
      testCasesChecked.clear();
    }});
  }
  
  onDataActionTestCaseGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testCasesChecked: (testCasesChecked) => {
        const tc = testCasesChecked.get(action.tc);
        const tcCopy = this.shallowCopy(tc);
        tcCopy.description = response.data.testCase.description;
        testCasesChecked.set(action.tc, tcCopy);
      }});
    }
  }
}


module.exports = TestCasesStore.export(TestCasesStore);
