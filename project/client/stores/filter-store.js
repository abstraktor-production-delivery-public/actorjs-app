
'use strict';

import StoreBase from 'z-abs-corelayer-client/client/store/store-base';


class FilterStore extends StoreBase {
  constructor() {
    super({
      sut: '*',
      fut: '*',
      hide: true
    });
  }
  
  onActionSutFilter(action) {
    this.updateState({sut: {$set: action.sut}});
  }

  onActionFutFilter(action) {
    this.updateState({fut: {$set: action.fut}});
  }

  onActionHideFilter(action) {
    this.updateState({hide: {$set: action.hide}});
  }
}


module.exports = FilterStore.export(FilterStore);
