
'use strict';

import TestCaseStore from './test-case-store';
import TestSuiteStore from './test-suite-store';
import { DataActionSystemUnderTestGet, DataActionSystemUnderTestAdd, DataActionSystemUnderTestDelete, DataActionSystemUnderTestUpdate, DataActionSystemUnderTestRepoAdd, DataActionSystemUnderTestRepoGet, DataActionSystemUnderTestSpecificationUpdate, DataActionSystemUnderTestInstancesUpdate, DataActionSystemUnderTestNodesUpdate } from '../actions/action-system-under-test/data-action-system-under-test';
import { ActionTestCaseRecentDelete } from '../actions/action-test-case/action-test-case';
import { ActionTestSuiteRecentDelete } from '../actions/action-test-suite/action-test-suite';
import MarkupSystemUnderTestInstances from 'z-abs-complayer-markup-client/client/markup/markup-system-under-test/markup-system-under-test-instances';
import MarkupSystemUnderTestNodes from 'z-abs-complayer-markup-client/client/markup/markup-system-under-test/markup-system-under-test-nodes';
import MarkupDocumentationPage from 'z-abs-complayer-markup-client/client/markup/markup-documentation/markup-documentation-page';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class SystemUnderTestStore extends StoreBaseData {
  constructor() {
    super({
      currentSut: null,
      currentName: null,
      systemUnderTests: [],
      systemsUnderTestNames: [],
      systemsUnderTestNodes: [],
      repos: [],
      systemUnderTestsChecked: new Set(),
      checkedVisibles: false,
      checkedHiddens: false,
      specification: {
        definition: false,
        document: [],
        content: undefined,
        contentLines: 0,
        contentOriginal: undefined,
        documentationPreview: {
          document: []
        }
      },
      instances: {
        instances: [],
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      },
      nodes: {
        nodes: [],
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      }
    });
    TestSuiteStore.setSystemUnderTestStore(this);
    this.sendDataAction(new DataActionSystemUnderTestGet());
  }
  
  onActionSystemUnderTestSet(action) {
    if(this.state.currentName !== action.name) {
      this.updateState({currentName: {$set: action.name}});
      this.sendDataAction(new DataActionSystemUnderTestGet());
    }
  }
  
  onActionSystemUnderTestClear(action) {
    this.updateState({currentName: {$set: null}, currentSut: {$set: null}});
  }
  
  onActionSystemUnderTestGet(action) {
    this.sendDataAction(new DataActionSystemUnderTestGet());
  }
  
  onActionSystemUnderTestAdd(action) {
    this.sendDataAction(new DataActionSystemUnderTestAdd(action.repo, action.name, action.description));
  }
  
  onActionSystemUnderTestDelete(action) {
    this.sendDataAction(new DataActionSystemUnderTestDelete(action.suts));
  }
  
  onActionSystemUnderTestUpdate(action) {
    this.sendDataAction(new DataActionSystemUnderTestUpdate(action.systemUnderTests));
  }

  onActionSystemUnderTestSpecificationMarkup(action) {
    const markupDocument = MarkupDocumentationPage.stringify(this.state.specification.document);
    this.updateState({specification: {definition: {$set: true}}});
    this.updateState({specification: {content: {$set: markupDocument}}});
    this.updateState({specification: {contentLines: {$set: markupDocument.split('\n').length}}});
    this.updateState({specification: {contentOriginal: {$set: markupDocument}}});
    this.updateState({specification: {documentationPreview: { document: {$set: this.state.specification.document}}}});
  }
  
  onActionSystemUnderTestSpecificationMarkupChange(action) {
    this.updateState({specification: {content: {$set: action.markup}}});
    this.updateState({specification: {contentLines: {$set: action.markup.split('\n').length}}});
    const document = MarkupDocumentationPage.parse(action.markup);
    this.updateState({specification:{documentationPreview: { document: {$set: document}}}});
  }
  
  onActionSystemUnderTestSpecificationMarkupCancel(action) {
    this._clearSpecificationMarkup();
    this._clearSpecificationPreview();
  }
  
  onActionSystemUnderTestSpecificationMarkupSave(action) {
    this.sendDataAction(new DataActionSystemUnderTestSpecificationUpdate(action.sut, action.markup));
    this._clearSpecificationMarkup();
    this._clearSpecificationPreview();
  }
  
  onActionSystemUnderTestInstancesMarkup(action) {
    const markup = MarkupSystemUnderTestInstances.stringify(this.state.instances.instances);
    this.updateState({instances: { definition: {$set: true}}});
    this.updateState({instances: { content: {$set: markup}}});
    this.updateState({instances: { contentOriginal: {$set: markup}}});
  }
  
  onActionSystemUnderTestInstancesMarkupChange(action) {
    const result = MarkupSystemUnderTestInstances.parse(action.markup);
    this.updateState({instances: { rows: {$set: result.rows}}});
    this.updateState({instances: { content: {$set: action.markup}}});
  }
  
  onActionSystemUnderTestInstancesMarkupCancel(action) {
    this._clearInstancesMarkup();
  }
  
  onActionSystemUnderTestInstancesMarkupSave(action) {
    const result = MarkupSystemUnderTestInstances.parse(action.markup);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      this.sendDataAction(new DataActionSystemUnderTestInstancesUpdate(action.sut, result.instances));
      this._clearInstancesMarkup();
    }
  }
  
  onActionSystemUnderTestNodesMarkup(action) {
    const markup = MarkupSystemUnderTestNodes.stringify(this.state.nodes.nodes);
    this.updateState({nodes: { definition: {$set: true}}});
    this.updateState({nodes: { content: {$set: markup}}});
    this.updateState({nodes: { contentOriginal: {$set: markup}}});
  }
  
  onActionSystemUnderTestNodesMarkupChange(action) {
    const result = MarkupSystemUnderTestNodes.parse(action.markup);
    this.updateState({nodes: { rows: {$set: result.rows}}});
    this.updateState({nodes: { content: {$set: action.markup}}});
  }
  
  onActionSystemUnderTestNodesMarkupCancel(action) {
    this._clearNodesMarkup();
  }
  
  onActionSystemUnderTestNodesMarkupSave(action) {
    const result = MarkupSystemUnderTestNodes.parse(action.markup);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      this.sendDataAction(new DataActionSystemUnderTestNodesUpdate(action.sut, result.nodes));
      this._clearNodesMarkup();
    }
  }
  
  onDataActionSystemUnderTestGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({systemUnderTests: {$set: response.data.systemUnderTests}});
      this.updateState({systemsUnderTestNames: {$set: response.data.systemUnderTests.map((systemUnderTest) => {return systemUnderTest.name;})}});
      this.updateState({systemsUnderTestNodes: {$set: response.data.systemUnderTests.map((systemUnderTest) => {return systemUnderTest.nodes;})}});
      const foundSut = this.state.systemUnderTests.find((sut) => {
        return sut.name === this.state.currentName;
      });
      const chosenSut = foundSut ? foundSut : null;
      if(null !== chosenSut) {
        const document = MarkupDocumentationPage.parse(chosenSut.specification);
        this.updateState({specification: {document: {$set: document}}});
        this.updateState({instances: {instances: {$set: chosenSut.instances}}});
        this.updateState({nodes: {nodes: {$set: chosenSut.nodes}}});
      }
      this.updateState({currentSut: {$set: chosenSut}});
      this.calculateChecked();
    }
  }
  
  onActionSystemUnderTestChecked(action) {
    this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
      if(action.checked) {
        systemUnderTestsChecked.add(`${action.sutRepo}_${action.sutName}`);
      }
      else {
        systemUnderTestsChecked.delete(`${action.sutRepo}_${action.sutName}`);
      }
    }});
    this.calculateChecked();
  }
  
  onActionSystemUnderTestRepoAdd(action) {
    this.sendDataAction(new DataActionSystemUnderTestRepoAdd(action.repo));
  }
  
  onActionSystemUnderTestRepoGet(action) {
    this.sendDataAction(new DataActionSystemUnderTestRepoGet(action));
  }
  
  onDataActionSystemUnderTestAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionSystemUnderTestGet());
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.clear();
      }});
      this.calculateChecked();
    }
  }
  
  onDataActionSystemUnderTestDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionSystemUnderTestGet());
      action.suts.forEach((sut) => {
        TestCaseStore.dispatchAction(new ActionTestCaseRecentDelete(sut.repo, sut.name));
        TestSuiteStore.dispatchAction(new ActionTestSuiteRecentDelete(sut.repo, sut.name));
      });
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.clear();
      }});
      this.calculateChecked();
    }
  }
  
  onDataActionSystemUnderTestUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionSystemUnderTestGet());
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.clear();
      }});
      this.calculateChecked();
    }
  }
  
  onDataActionSystemUnderTestRepoAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionSystemUnderTestGet());
    }
  }
  
  onDataActionSystemUnderTestRepoGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({repos: {$set: response.data}});
      this.sendDataAction(new DataActionSystemUnderTestGet());
    }
  }
  
  onDataActionSystemUnderTestSpecificationUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const sut = response.data;
      this.updateState({currentSut: {$set: sut}});
      const document = MarkupDocumentationPage.parse(sut.specification);
      this.updateState({specification: {document: {$set: document}}});
    }
  }
  
  onDataActionSystemUnderTestInstancesUpdate(action) {
    this.sendDataAction(new DataActionSystemUnderTestGet());
  }

  onDataActionSystemUnderTestNodesUpdate(action) {
    this.sendDataAction(new DataActionSystemUnderTestGet());
  }
  
  _clearSpecificationMarkup() {
    this.updateState({specification: { definition: {$set: false}}});
    this.updateState({specification: {content: {$set: undefined}}});
    this.updateState({specification: {contentLines: {$set: 0}}});
    this.updateState({specification: {contentOriginal: {$set: undefined}}});
  }
  
  _clearSpecificationPreview() {
    this.updateState({specification: {documentationPreview: { document: {$set: []}}}});
  }

  _clearInstancesMarkup() {
    this.updateState({instances: { definition: {$set: false}}});
    this.updateState({instances: { content: {$set: undefined}}});
    this.updateState({instances: { contentOriginal: {$set: undefined}}});
    this.updateState({instances: { rows: {$set: undefined}}});
  }
    
  _clearNodesMarkup() {
    this.updateState({nodes: { definition: {$set: false}}});
    this.updateState({nodes: { content: {$set: undefined}}});
    this.updateState({nodes: { contentOriginal: {$set: undefined}}});
    this.updateState({nodes: { rows: {$set: undefined}}});
  }
  
  calculateChecked() {
    this.calculateVisibles();
    this.calculateHiddens();
  }

  calculateVisibles() {
    let visibles = this.state.systemUnderTests.filter((sut) => {
      return sut.status === this.VISIBLE();
    });
    this.updateState({checkedVisibles: {$set: (-1 !== visibles.findIndex((sut) => {
      return this.state.systemUnderTestsChecked.has(`${sut.repo}_${sut.name}`);
    }))}});
  }

  calculateHiddens() {
    let hiddens = this.state.systemUnderTests.filter((sut) => {
      return sut.status === this.HIDDEN();
    });
    this.updateState({checkedHiddens: {$set: (-1 !== hiddens.findIndex((sut) => {
      return this.state.systemUnderTestsChecked.has(`${sut.repo}_${sut.name}`);
    }))}});
  }
  
  // TODO: SHOULD BE STATIC
  STATIC() {
    return 'static';
  }
  
  VISIBLE() {
    return 'visible';
  }
    
  HIDDEN() {
    return 'hidden';
  }
}


module.exports = SystemUnderTestStore.export(SystemUnderTestStore);
