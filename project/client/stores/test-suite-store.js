 
'use strict';

import LoginStore from './login-store';
import { DataActionTestSuiteGet, DataActionTestSuiteAdd, DataActionTestSuiteDelete, DataActionTestSuiteUpdate, DataActionTestSuiteRecentGet, DataActionTestSuiteRecentUpdate, DataActionTestSuiteRecentDelete, DataActionTestSuiteButtonsAdd, DataActionTestSuiteButtonsGet, DataActionTestSuiteButtonsUpdate } from '../actions/action-test-suite/data-action-test-suite';
import { RealtimeActionTestSuiteExecutionStart } from '../actions/action-test-suite/realtime-action-test-suite';
import MessageTestSuiteClear from '../communication/messages/messages-c-to-c/message-test-suite-clear';
import MarkupTestSuite from 'z-abs-complayer-markup-client/client/markup/markup-test-suite/markup-test-suite';
import DataTestSuite from 'z-abs-complayer-markup-client/client/data/data-test-suite/data-test-suite';
import MessageTestSuiteStop from 'z-abs-funclayer-engine-cs/clientServer/communication/messages/messages-client-to-server/message-test-suite-stop';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import GuidGenerator from 'z-abs-corelayer-cs/clientServer/guid-generator';


class TestSuiteStore extends StoreBaseRealtime {
  static EXECUTION_NOT_RUNNING = 0;
  static EXECUTION_RUNNING = 1;
  static EXECUTION_DISABLED = 2;

  static REALTIME_CONNECTION_CLOSED = 0;
  static REALTIME_CONNECTION_OPEN = 1;
  static REALTIME_CONNECTION_ERROR = 2;

  constructor() {
    super({
      testSuite: undefined,
      iterationsTs: 1,
      run: {
        testSuite: new Map()
      },
      result: 'None',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      },
      execution: TestSuiteStore.EXECUTION_NOT_RUNNING,
      executionGuid: '',
      realtimeConnection: TestSuiteStore.REALTIME_CONNECTION_CLOSED,
      recentTestSuites: [],
      testSuites: [],
      buttonsLoaded: false,
      buttons: {
        sequenceDiagram: {
          zoom: 1.0,
          width: 180,
          scroll: true,
          
          executedStateEvents: true,
          notExecutedStateEvents: false,
          
          serverEvents: false,
          connectionEvents: false,
          messageEvents: true,
          messageDetailEvents: false,
          
          protocolData: true,
          protocolInfoData: false,
          protocolIpData: true,
          protocolAddressNameData: false,
          protocolNameData: true,
          protocolTransportData: false,
          protocolInstanceData: true,
          
          stackEvents: true,
          stackInfoEvents: false,
          stackInstanceEvents: true,
          stackProtocolEvents: true,
          
          guiEvents: true,
          guiObjectEvents: true,
          guiActionEvents: true,
          guiFunctionEvents: false,
          guiInfoData: true,
          guiInstanceData: false,
          guiProtocolData: false,
          
          phases: [true, true, true, true, true],
          
          ////////////////
          showMessages: true,
          
          
          
          showMessageContent: false,
          showMessageContentOpen: false,
          showSentMessageNumbers: false,
          showReceivedMessageNumbers: false,
          showSentMessageDetailNumbers: false,
          showReceivedMessageDetailNumbers: false,
          
          showAddresses: false
        },
        log: {
          filterError: true,
          filterWarning: true,
          filterSuccess: true,
          filterFailure: true,
          filterEngine: false,
          filterTestData: false,
          filterDebug: false,
          filterIP: true,
          filterGUI: true,
          filterBrowserLog: true,
          filterBrowserErr: true,
          filterDetailEngine: false,
          filterDetailTestData: false,
          filterDetailDebug: false,
          filterOpenIp: false,
          filterOpenGui: false,
          filterOpenBrowserLog: false,
          filterOpenBrowserErr: false,
          zoom: 1.0,
          scroll: true
        }
      }
    });
    this.systemUnderTestStore = null;
    this.buttonsKey = '';
    this.on('DataActionLoginUpdate', (store) => {
      this.buttonsKey = `${store.state.login.labId}_${store.state.login.userId}`;
      this.sendDataAction(new DataActionTestSuiteButtonsGet(this.buttonsKey));
    });
  }
  
  onActionTestSuiteMounted(action) {
    const loginData = LoginStore.isLoggedIn();
    if(loginData) {
      this.buttonsKey = `${loginData.labId}_${loginData.userId}`;
      this.sendDataAction(new DataActionTestSuiteButtonsGet(this.buttonsKey));
    }
    else {
      this.once('DataActionLoginGet', (store) => {
        this.buttonsKey = `${store.state.login.labId}_${store.state.login.userId}`;
        this.sendDataAction(new DataActionTestSuiteButtonsGet(this.buttonsKey));
      });
    }
  }
  
  setSystemUnderTestStore(systemUnderTestStore) {
    this.systemUnderTestStore = systemUnderTestStore;
  }
  
  onActionTestSuiteGet(action) {
    this.clearMarkup();
    this.sendDataAction(new DataActionTestSuiteGet(action.repo, action.sut, action.fut, action.ts));
    if(undefined !== action.ts) {
      this.sendDataAction(new DataActionTestSuiteRecentUpdate(action.repo, action.sut, action.fut, action.ts));
    }
  }
  
  onActionTestSuiteAdd(action) {
    this.sendDataAction(new DataActionTestSuiteAdd(action.repo, action.sut, action.fut, action.ts, action.description, new DataTestSuite()));
  }
  
  onActionTestSuiteDelete(action) {
    this.sendDataAction(new DataActionTestSuiteDelete(action.repo, action.sut, action.fut, action.tses));
  }
  
  onActionTestSuiteMarkup(action) {
    const markupTs = MarkupTestSuite.stringify(this.state.testSuite);
    this.updateState({markup: {definition: {$set: true}}});
    this.updateState({markup: {content: {$set: markupTs}}});
    this.updateState({markup: {contentOriginal: {$set: markupTs}}});
  }
  
  onActionTestSuiteMarkupSave(action) {
    const result = MarkupTestSuite.parse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      this.updateState({testSuite: { ts: {$set: result.ts}}});
      this.sendDataAction(new DataActionTestSuiteUpdate(action.repo, action.sut, action.fut, action.ts, undefined, this.state.testSuite));
      this.clearMarkup();
    }  
  }
  
  onActionTestSuiteMarkupCancel(action) {
    this.clearMarkup();
  }
  
  onActionTestSuiteMarkupChange(action) {
    const result = MarkupTestSuite.parse(action.markup);
 //   if(result.success) {
  //    const markupTs = MarkupTestSuite.stringify({ts: result.ts});
    //  this.updateState({markup: {content: {$set: markupTs}}});
//    }
 //   else {
      this.updateState({markup: { content: {$set: action.markup}}});
//    }
    this.updateState({markup: { rows: {$set: result.rows}}});
  }
  
  onActionTestSuiteButtonsUpdate(action) {
    const buttons = this.deepCopy(this.state.buttons);
    const tab = Reflect.get(buttons, action.tab);
    Reflect.set(tab, action.button, action.value);
    this.sendDataAction(new DataActionTestSuiteButtonsUpdate(action, this.buttonsKey, buttons));
  }
  
  onActionTestSuiteZoom(action) {
    const buttons = this.deepCopy(this.state.buttons);
    buttons.zoom = action.zoom;
    this.sendDataAction(new DataActionTestSuiteButtonsUpdate(action, this.buttonsKey, buttons));
  }
  
  onActionTestSuiteExecutionStart(action) {
    this.updateState({execution: {$set: TestSuiteStore.EXECUTION_DISABLED}});
    this.updateState({executionGuid: {$set: GuidGenerator.create()}});
    this.sendRealtimeAction(new RealtimeActionTestSuiteExecutionStart(action.repoName, action.sutName, action.futName, action.tsName, action.stagedSut, action.stagedSutInstance, action.stagedSutNodes, this.systemUnderTestStore.state.systemsUnderTestNames, this.systemUnderTestStore.state.systemsUnderTestNodes, {slow:false,asService:false}, this.state.iterationsTs), this.state.executionGuid);
  }
  
  onActionTestSuiteExecutionStop(action) {
    this.updateState({execution: {$set: TestSuiteStore.EXECUTION_DISABLED}});
    this.handleRealtimeMessage(new MessageTestSuiteStop());
    this.sendRealtimeMessage(new MessageTestSuiteStop(this.state.executionGuid), null);
  }
  
  onActionTestSuiteRecentGet(action) {
    this.sendDataAction(new DataActionTestSuiteRecentGet());
  }
  
  onActionTestSuiteRecentDelete(action) {
    this.sendDataAction(new DataActionTestSuiteRecentDelete(action.repo, action.sut, action.fut, action.tses));
  }
  
  onActionTestSuiteIterations(action) {
    this.updateState({iterationsTs: {$set: action.iterationsTs}});
  }
  
  _addExecTestSuite(abstractions, repoName, tsName, key, commentOut, pendings) {
    let current = 1;
    const testCases = [];
    abstractions.forEach((abstraction) => {
      if(abstraction.name.startsWith('tc')) {
        testCases.push({
          tsName: tsName,
          tcName: abstraction.name,
          type: 0,
          iterationsTs: !abstraction.iterationsTs ? 1 : abstraction.iterationsTs,
          commentOut: commentOut || abstraction._commentOut_
        });
      }
      else if(abstraction.name.startsWith('ts')) {
        const parameters = abstraction.name.split('.');
        parameters.splice(0, 1);
        if(3 === parameters.length) {
          const currentKey = `${key}_${current.toString().padStart(5, '0')}`;
          ++current;
          ++pendings.pendings;
          this.sendDataAction(new DataActionTestSuiteGet(repoName, ...parameters, currentKey, commentOut || abstraction._commentOut_, pendings));
        }
      }
      else if(abstraction.name.startsWith('stage')) {
        testCases.push({
          tsName: tsName,
          tcName: abstraction.name,
          type: 1,
          iterationsTs: 1,
          commentOut: commentOut || abstraction._commentOut_
        });
      }
    });
    pendings.testSuite.set(key, testCases);
    if(0 === pendings.pendings) {
      const sortedTestSuite = new Map([...pendings.testSuite].sort());
      this.updateState({run: {testSuite: {$set: sortedTestSuite}}});
    }
  }
  
  onDataActionTestSuiteGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(!action.key) {
        if(undefined !== response.data.testSuites) {
          this.updateState({testSuites: {$set: response.data.testSuites}});
          this.calculateTestSuites();
        }
        else if(undefined !== response.data.testSuite) {
          this.updateState({testSuite: {$set: response.data.testSuite}});
          const currentKey = '1';
          this.updateState({run: {testSuite: (testSuite) => {
            testSuite.clear();
            testSuite.set(currentKey, []);
          }}});
          this._addExecTestSuite(response.data.testSuite.ts.abstractions, action.repo, `ts.${action.sut}.${action.fut}.${action.ts}`, currentKey, action.commentOut, action.pendings);
        }
      }
      else {
        --action.pendings.pendings;
        this._addExecTestSuite(response.data.testSuite.ts.abstractions, action.repo, `ts.${action.sut}.${action.fut}.${action.ts}`, action.key, action.commentOut, action.pendings);
      }
    }
  }
  
  onDataActionTestSuiteAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestSuiteGet(action.repo, action.sut, action.fut));
      this.calculateTestSuites();
    }
  }
  
  onDataActionTestSuiteUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestSuiteGet(action.repo, action.sut, action.fut, action.ts));
      this.calculateTestSuites();
    }
  }
  
  onDataActionTestSuiteDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestSuiteGet(action.repo, action.sut, action.fut));
      this.sendDataAction(new DataActionTestSuiteRecentDelete(action.repo, action.sut, action.fut, action.tses));
      this.calculateTestSuites();
    }
  }
  
  onDataActionTestSuiteButtonsAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({buttons: {$set: response.data}});
    }
  }
  
  onDataActionTestSuiteButtonsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const keys = Reflect.ownKeys(this.state.buttons);
      const resutObject = {};
      keys.forEach((key) => {
        const stateObject = Reflect.get(this.state.buttons, key);
        const storedObject = Reflect.get(response.data, key);
        const mergedObject = {...stateObject, ...storedObject};
        Reflect.set(resutObject, key, mergedObject);
      });
      this.updateState({buttonsLoaded: {$set: true}, buttons: {$set: resutObject}});
    }
    else {
      this.sendDataAction(new DataActionTestSuiteButtonsAdd(this.buttonsKey, this.state.buttons));
    }
  }
  
  onDataActionTestSuiteButtonsUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if('sequenceDiagram' === action.tab) {
        this.updateState({buttons: {sequenceDiagram: {$set: response.data.sequenceDiagram}}});
      }
      else if('log' === action.tab) {
        this.updateState({buttons: {log: {$set: response.data.log}}});
      }
    }
  }
  
  onDataActionTestSuiteRecentGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({recentTestSuites: {$set: response.data}});
    }
  }
  
  onDataActionTestSuiteRecentUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestSuiteRecentGet());
    }
  }
  
  onDataActionTestSuiteRecentDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionTestSuiteRecentGet());
    }
  } 
  
  onRealtimeActionTestSuiteExecutionStart(action) {
    const response = action.getResponse();
    if(!response.isSuccess()) {
      this.updateState({execution: {$set: TestSuiteStore.EXECUTION_RUNNING}});
    }
  }
  
  onRealtimeMessageExecutionStarted(action) {
    this.updateState({execution: {$set: TestSuiteStore.EXECUTION_RUNNING}});
  }
  
  onRealtimeMessageExecutionStopped(action) {
    this.updateState({execution: {$set: TestSuiteStore.EXECUTION_NOT_RUNNING}});
  }
    
  onRealtimeMessageRealtimeOpen(action) {
    this.updateState({realtimeConnection: {$set: TestSuiteStore.REALTIME_CONNECTION_OPEN}});
  }
  
  onRealtimeMessageRealtimeClosed(action) {
    this.updateState({realtimeConnection: {$set: TestSuiteStore.REALTIME_CONNECTION_CLOSED}});
  }
  
  onActionTestSuiteClear(action) {
    this.handleRealtimeMessage(new MessageTestSuiteClear(action.tab));
  }
    
  calculateTestSuites(state) {
    this.updateState({testSuites: (testSuites) => {
      testSuites.sort((a, b) => {
        return a.name > b.name ? 1 : -1;
      });
    }});
  }
  
  clearMarkup() {
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
  
  // TODO: SHOULD BE STATIC
  STATIC() {
    return 'static';
  }
  
  VISIBLE() {
    return 'visible';
  }
    
  HIDDEN() {
    return 'hidden';
  }
}


module.exports = TestSuiteStore.export(TestSuiteStore);
