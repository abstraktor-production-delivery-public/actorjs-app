
'use strict';

import LoginStore from './login-store';
import { ActionLoginDataChanged } from '../actions/action-login/action-login';
import { DataActionAddressesSrcGet, DataActionAddressesSrcUpdate, DataActionAddressesDstGet, DataActionAddressesDstUpdate, DataActionAddressesSrvGet, DataActionAddressesSrvUpdate, DataActionAddressesClientAddressGet, DataActionAddressesClientAddressUpdate, DataActionAddressesSutAddressGet, DataActionAddressesSutAddressUpdate, DataActionAddressesServerAddressGet, DataActionAddressesServerAddressUpdate, DataActionAddressesClientInterfaceGet, DataActionAddressesClientInterfaceUpdate, DataActionAddressesSutInterfaceGet, DataActionAddressesSutInterfaceUpdate, DataActionAddressesServerInterfaceGet, DataActionAddressesServerInterfaceUpdate, DataActionAddressesNetworkInterfaceGet, DataActionAddressesNetworkInterfaceUpdate, DataActionAddressesPortsGet, DataActionAddressesPortsUpdate, DataActionAddressesDnsGet, DataActionAddressesDnsUpdate } from '../actions/action-addresses/data-action-addresses';
import MarkupAddressesSrc from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-src';
import MarkupAddressesDst from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-dst';
import MarkupAddressesSrv from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-srv';
import MarkupAddressesClientAddress from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-client-address';
import MarkupAddressesSutAddress from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-sut-address';
import MarkupAddressesServerAddress from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-server-address';
import MarkupAddressesClientInterface from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-client-interface';
import MarkupAddressesSutInterface from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-sut-interface';
import MarkupAddressesServerInterface from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-server-interface';
import MarkupAddressesNetworkInterface from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-network-interface';
import MarkupAddressesPorts from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-ports';
import MarkupAddressesDns from 'z-abs-complayer-markup-client/client/markup/markup-addresses/markup-addresses-dns';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class AddressesStore extends StoreBaseData {
  constructor() {
    super({
      addressesSrcGlobals: [],
      addressesSrcLocals: [],
      addressesDnsGlobals: [],
      addressesDnsLocals: [],
      addressesDstGlobals: [],
      addressesDstLocals: [],
      addressesSrvGlobals: [],
      addressesSrvLocals: [],
      addressesClientAddressGlobals: [],
      addressesClientAddressLocals: [],
      addressesSutAddressGlobals: [],
      addressesSutAddressLocals: [],
      addressesServerAddressGlobals: [],
      addressesServerAddressLocals: [],
      addressesClientInterfaceGlobals: [],
      addressesClientInterfaceLocals: [],
      addressesSutInterfaceGlobals: [],
      addressesSutInterfaceLocals: [],
      addressesServerInterfaceGlobals: [],
      addressesServerInterfaceLocals: [],
      addressesNetworkInterfaceGlobals: [],
      addressesNetworkInterfaceLocals: [],
      addressesPortsGlobals: [],
      addressesPortsLocals: [],
      currentView: '',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      },
      labUserFilter: true
    });
  }
  
  onActionAddressesSrcGet(action) {
    this.sendDataAction(new DataActionAddressesSrcGet());
  }
  
  onActionAddressesDstGet(action) {
    this.sendDataAction(new DataActionAddressesDstGet());
  }
  
  onActionAddressesSrvGet(action) {
    this.sendDataAction(new DataActionAddressesSrvGet());
  }

    onActionAddressesClientAddressGet(action) {
    this.sendDataAction(new DataActionAddressesClientAddressGet());
  }
  
  onActionAddressesSutAddressGet(action) {
    this.sendDataAction(new DataActionAddressesSutAddressGet());
  }
  
  onActionAddressesServerAddressGet(action) {
    this.sendDataAction(new DataActionAddressesServerAddressGet());
  }

  onActionAddressesClientInterfaceGet(action) {
    this.sendDataAction(new DataActionAddressesClientInterfaceGet());
  }
  
  onActionAddressesSutInterfaceGet(action) {
    this.sendDataAction(new DataActionAddressesSutInterfaceGet());
  }
  
  onActionAddressesServerInterfaceGet(action) {
    this.sendDataAction(new DataActionAddressesServerInterfaceGet());
  }

  onActionAddressesNetworkInterfaceGet(action) {
    this.sendDataAction(new DataActionAddressesNetworkInterfaceGet());
  }
  
  onActionAddressesPortsGet(action) {
    this.sendDataAction(new DataActionAddressesPortsGet());
  }
  
  onActionAddressesDnsGet(action) {
    this.sendDataAction(new DataActionAddressesDnsGet());
  }

  onActionAddressesMarkup(action) {
    const markup = this.markupStringify();
    this.updateState({markup: { definition: {$set: true}}});
    this.updateState({markup: { content: {$set: markup}}});
    this.updateState({markup: { contentOriginal: {$set: markup}}});
  }
  
  onActionAddressesMarkupSave(action) {
    const result = this.markupParse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      if('Src' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesSrcUpdate(result.globals, result.locals));
      }
      else if('Dst' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesDstUpdate(result.globals, result.locals));
      }
      else if('Srv' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesSrvUpdate(result.globals, result.locals));
      }
      else if('ClientAddress' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesClientAddressUpdate(result.globals, result.locals));
      }
      else if('SutAddress' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesSutAddressUpdate(result.globals, result.locals));
      }
      else if('ServerAddress' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesServerAddressUpdate(result.globals, result.locals));
      }
      else if('ClientInterface' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesClientInterfaceUpdate(result.globals, result.locals));
      }
      else if('SutInterface' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesSutInterfaceUpdate(result.globals, result.locals));
      }
      else if('ServerInterface' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesServerInterfaceUpdate(result.globals, result.locals));
      }
      else if('NetworkInterface' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesNetworkInterfaceUpdate(result.globals, result.locals));
      }
      else if('Ports' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesPortsUpdate(result.globals, result.locals));
      }
      else if('Dns' === this.state.currentView) {
        this.sendDataAction(new DataActionAddressesDnsUpdate(result.globals, result.locals));
      }
      this.clearMarkup();
      LoginStore.dispatchAction(new ActionLoginDataChanged());
    }
  }
  
  onActionAddressesMarkupCancel(action) {
     this.clearMarkup();
  }
  
  onActionAddressesMarkupChange(action) {
    const result = this.markupParse(action.markup);
    this.updateState({markup: { rows: {$set: result.rows}}});
    this.updateState({markup: { content: {$set: action.markup}}});
  }
  
  onActionAddressesLabUserFilter(action) {
    this.updateState({labUserFilter: {$set: action.labUserFilter}});
  }
  
  onDataActionAddressesSrcGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesSrcGlobals: {$set: response.data[0]}});
      this.updateState({addressesSrcLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'Src'}});
    }
  }
  
  onDataActionAddressesSrcUpdate(action) {
    this.sendDataAction(new DataActionAddressesSrcGet());
  }
    
  onDataActionAddressesDstGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesDstGlobals: {$set: response.data[0]}});
      this.updateState({addressesDstLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'Dst'}});
    }
  }
  
  onDataActionAddressesDstUpdate(action) {
    this.sendDataAction(new DataActionAddressesDstGet());
  }
  
  onDataActionAddressesSrvGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesSrvGlobals: {$set: response.data[0]}});
      this.updateState({addressesSrvLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'Srv'}});
    }
  }
  
  onDataActionAddressesSrvUpdate(action) {
    this.sendDataAction(new DataActionAddressesSrvGet());
  }
  
  onDataActionAddressesClientAddressGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesClientAddressGlobals: {$set: response.data[0]}});
      this.updateState({addressesClientAddressLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'ClientAddress'}});
    }
  }
  
  onDataActionAddressesClientAddressUpdate(action) {
    this.sendDataAction(new DataActionAddressesClientAddressGet());
  }
  
  onDataActionAddressesSutAddressGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesSutAddressGlobals: {$set: response.data[0]}});
      this.updateState({addressesSutAddressLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'SutAddress'}});
    }
  }
  
  onDataActionAddressesSutAddressUpdate(action) {
    this.sendDataAction(new DataActionAddressesSutAddressGet());
  }
  
  onDataActionAddressesServerAddressGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesServerAddressGlobals: {$set: response.data[0]}});
      this.updateState({addressesServerAddressLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'ServerAddress'}})
    }
  }
  
  onDataActionAddressesServerAddressUpdate(action) {
    this.sendDataAction(new DataActionAddressesServerAddressGet());
  }

  onDataActionAddressesClientInterfaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesClientInterfaceGlobals: {$set: response.data[0]}});
      this.updateState({addressesClientInterfaceLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'ClientInterface'}});
    }
  }
  
  onDataActionAddressesClientInterfaceUpdate(action) {
    this.sendDataAction(new DataActionAddressesClientInterfaceGet());
  }
  
  onDataActionAddressesSutInterfaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesSutInterfaceGlobals: {$set: response.data[0]}});
      this.updateState({addressesSutInterfaceLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'SutInterface'}});
    }
  }
  
  onDataActionAddressesSutInterfaceUpdate(action) {
    this.sendDataAction(new DataActionAddressesSutInterfaceGet());
  }
  
  onDataActionAddressesServerInterfaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesServerInterfaceGlobals: {$set: response.data[0]}});
      this.updateState({addressesServerInterfaceLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'ServerInterface'}});
    }
  }
  
  onDataActionAddressesServerInterfaceUpdate(action) {
    this.sendDataAction(new DataActionAddressesServerInterfaceGet());
  }

  onDataActionAddressesNetworkInterfaceGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesNetworkInterfaceGlobals: {$set: response.data[0]}});
      this.updateState({addressesNetworkInterfaceLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'NetworkInterface'}});
    }
  }
  
  onDataActionAddressesNetworkInterfaceUpdate(action) {
    this.sendDataAction(new DataActionAddressesNetworkInterfaceGet());
  }
  
  onDataActionAddressesPortsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesPortsGlobals: {$set: response.data[0]}});
      this.updateState({addressesPortsLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'Ports'}});
    }
  }
  
  onDataActionAddressesPortsUpdate(action) {
    this.sendDataAction(new DataActionAddressesPortsGet());
  }
  
  onDataActionAddressesDnsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({addressesDnsGlobals: {$set: response.data[0]}});
      this.updateState({addressesDnsLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'Dns'}});
    }
  }
  
  onDataActionAddressesDnsUpdate(action) {
    this.sendDataAction(new DataActionAddressesDnsGet());
  }
  
  clearMarkup(state) {
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
  
  markupStringify() {
    if('Src' === this.state.currentView) {
      return MarkupAddressesSrc.stringify(this.state.addressesSrcGlobals, this.state.addressesSrcLocals);
    }
    else if('Dst' === this.state.currentView) {
      return MarkupAddressesDst.stringify(this.state.addressesDstGlobals, this.state.addressesDstLocals);
    }
    else if('Srv' === this.state.currentView) {
      return MarkupAddressesSrv.stringify(this.state.addressesSrvGlobals, this.state.addressesSrvLocals);
    }
    else if('ClientAddress' === this.state.currentView) {
      return MarkupAddressesClientAddress.stringify(this.state.addressesClientAddressGlobals, this.state.addressesClientAddressLocals);
    }
    else if('SutAddress' === this.state.currentView) {
      return MarkupAddressesSutAddress.stringify(this.state.addressesSutAddressGlobals, this.state.addressesSutAddressLocals);
    }
    else if('ServerAddress' === this.state.currentView) {
      return MarkupAddressesServerAddress.stringify(this.state.addressesServerAddressGlobals, this.state.addressesServerAddressLocals);
    }
    else if('ClientInterface' === this.state.currentView) {
      return MarkupAddressesClientInterface.stringify(this.state.addressesClientInterfaceGlobals, this.state.addressesClientInterfaceLocals);
    }
    else if('SutInterface' === this.state.currentView) {
      return MarkupAddressesSutInterface.stringify(this.state.addressesSutInterfaceGlobals, this.state.addressesSutInterfaceLocals);
    }
    else if('ServerInterface' === this.state.currentView) {
      return MarkupAddressesServerInterface.stringify(this.state.addressesServerInterfaceGlobals, this.state.addressesServerInterfaceLocals);
    }
    else if('NetworkInterface' === this.state.currentView) {
      return MarkupAddressesNetworkInterface.stringify(this.state.addressesNetworkInterfaceGlobals, this.state.addressesNetworkInterfaceLocals);
    }
    else if('Ports' === this.state.currentView) {
      return MarkupAddressesPorts.stringify(this.state.addressesPortsGlobals, this.state.addressesPortsLocals);
    }
    else if('Dns' === this.state.currentView) {
      return MarkupAddressesDns.stringify(this.state.addressesDnsGlobals, this.state.addressesDnsLocals);
    }
  }
  
  markupParse(markup) {
    if('Src' === this.state.currentView) {
      return MarkupAddressesSrc.parse(markup);
    }
    else if('Dst' === this.state.currentView) {
      return MarkupAddressesDst.parse(markup);
    }
    else if('Srv' === this.state.currentView) {
      return MarkupAddressesSrv.parse(markup);
    }
    else if('ClientAddress' === this.state.currentView) {
      return MarkupAddressesClientAddress.parse(markup);
    }
    else if('SutAddress' === this.state.currentView) {
      return MarkupAddressesSutAddress.parse(markup);
    }
    else if('ServerAddress' === this.state.currentView) {
      return MarkupAddressesServerAddress.parse(markup);
    }
    else if('ClientInterface' === this.state.currentView) {
      return MarkupAddressesClientInterface.parse(markup);
    }
    else if('SutInterface' === this.state.currentView) {
      return MarkupAddressesSutInterface.parse(markup);
    }
    else if('ServerInterface' === this.state.currentView) {
      return MarkupAddressesServerInterface.parse(markup);
    }
    else if('NetworkInterface' === this.state.currentView) {
      return MarkupAddressesNetworkInterface.parse(markup);
    }
    else if('Ports' === this.state.currentView) {
      return MarkupAddressesPorts.parse(markup);
    }
    else if('Dns' === this.state.currentView) {
      return MarkupAddressesDns.parse(markup);
    }
  }
}


module.exports = AddressesStore.export(AddressesStore);
