
'use strict';

import ActorDefault from '../actor-default';
import DataConfig from 'z-abs-corelayer-client/client/communication/data-config';
import { DataActionLoginWsGet, DataActionLoginGet, DataActionLoginPing, DataActionLoginUpdate, DataActionLoginCreateInterfaces, DataActionLoginDeleteInterfaces, DataActionLoginButtonsAdd, DataActionLoginButtonsGet, DataActionLoginButtonsUpdate, DataActionLoginDataGet } from '../actions/action-login/data-action-login';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class LoginStore extends StoreBaseData {
  constructor() {
    super({
      login: {
        labId: '',
        userId: '',
        repo: '',
        systemUnderTest: ActorDefault.NAME(),
        systemUnderTestInstance: '',
        nodes: [],
        forcedLocalhost: false
      },
      loginReport: [],
      virtualNetworks: [],
      osNetworks: [],
      chosenAddresses: {},
      pingResults: new Map(),
      cpus: [],
      loggedIn: false,
      loginDataChanged: false,
      working: false,
      buttons: {
        toolbar: {
          forceLocalhost: false
        }
      }
    });
    this.buttonsKey = '';
    this.sendDataAction(new DataActionLoginWsGet());
    this.sendDataAction(new DataActionLoginGet());
  }
  
  isLoggedIn() {
    return this.state.loggedIn ? this.state.login : null;
  }
  
  onActionLoginMounted(action) {
    this.sendDataAction(new DataActionLoginGet());
    this.once('DataActionLoginGet', (store) => {
      this.buttonsKey = `${store.state.login.labId}_${store.state.login.userId}`;
      this.sendDataAction(new DataActionLoginButtonsGet(this.buttonsKey));
      this.sendDataAction(new DataActionLoginDataGet());
    });
  }
  
  onActionLoginGet(action) {
    this.updateState({working: { $set: true}});
    this.sendDataAction(new DataActionLoginGet());
  }
  
  onActionLoginUpdate(action) {
    this.updateState({working: { $set: true}});
    this.sendDataAction(new DataActionLoginUpdate(action.labId, action.userId, action.repo, action.systemUnderTest, action.systemUnderTestInstance, action.nodes, action.forceLocalhost));
  }
  
  onActionLoginDataChanged(action) {
    this.updateState({loginDataChanged: { $set: true}});
  }
  
  onActionLoginPing(action) {
    this.updateState({working: { $set: true}});
    this.sendDataAction(new DataActionLoginPing(this.state.chosenAddresses));
  }
  
  onActionLoginCreateInterfaces(action) {
    this.updateState({working: { $set: true}});
    this.sendDataAction(new DataActionLoginCreateInterfaces(action));
  }
  
  onActionLoginDeleteInterfaces(action) {
    this.updateState({working: { $set: true}});
    this.sendDataAction(new DataActionLoginDeleteInterfaces(action));
  }
  
  onActionLoginButtonsUpdate(action) {
    const tab = Reflect.get(this.state.buttons, action.tab);
    Reflect.set(tab, action.button, action.value);
    this.sendDataAction(new DataActionLoginButtonsUpdate(this.buttonsKey, this.state.buttons));
  }
  
  onDataActionLoginGet(action) {
    this.updateState({working: { $set: false}});
    const response = action.getResponse();
    if(response.isSuccess()) {
      let i = -1;
      const login = response.data[++i];
      const nodes = login.nodes.filter((node) => {
        if(node.instanceName === login.systemUnderTestInstance) {
          return node;
        }
      });
      this.updateState({login: { labId: {$set: login.labId}}});
      this.updateState({login: { userId: {$set: login.userId}}});
      this.updateState({login: { repo: {$set: login.repo}}});
      this.updateState({login: { systemUnderTest: {$set: login.systemUnderTest}}});
      this.updateState({login: { systemUnderTestInstance: {$set: login.systemUnderTestInstance}}});
      this.updateState({login: { nodes: {$set: nodes}}});
      this.updateState({login: { forcedLocalhost: {$set: login.forcedLocalhost}}});
      this.updateState({virtualNetworks: { $set: response.data[++i]}});
      this.updateState({loginReport: { $set: response.data[++i]}});
      this.updateState({chosenAddresses: { $set: response.data[++i]}});
      this.updateState({loggedIn: {$set: true}});
    }
    else {
      this.updateState({loggedIn: {$set: false}});
    }
  }
    
  onDataActionLoginWsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      DataConfig.setWs(response.data);
    }
  }
  
  onDataActionLoginUpdate(action) {
    this.updateState({working: { $set: false}});
    const response = action.getResponse();
    if(response.isSuccess()) {
      let i = -1;
      const login = response.data[++i];
      const nodes = login.nodes.filter((node) => {
        if(node.instanceName === login.systemUnderTestInstance) {
          return node;
        }
      });
      this.updateState({login: { labId: {$set: login.labId}}});
      this.updateState({login: { userId: {$set: login.userId}}});
      this.updateState({login: { repo: {$set: login.repo}}});
      this.updateState({login: { systemUnderTest: {$set: login.systemUnderTest}}});
      this.updateState({login: { systemUnderTestInstance: {$set: login.systemUnderTestInstance}}});
      this.updateState({login: { nodes: {$set: nodes}}});
      this.updateState({login: { forcedLocalhost: {$set: login.forcedLocalhost}}});
      this.updateState({virtualNetworks: {$set: response.data[++i]}});
      this.updateState({loginReport: { $set: response.data[++i]}});
      this.updateState({chosenAddresses: { $set: response.data[++i]}});
      this.updateState({loginDataChanged: { $set: false}});
      this.updateState({pingResults: { $set: new Map()}});
      this.updateState({loggedIn: {$set: true}});
      this.buttonsKey = `${this.state.login.labId}_${this.state.login.userId}`;
      this.sendDataAction(new DataActionLoginButtonsGet(this.buttonsKey));
    }
    else {
      this.updateState({loggedIn: {$set: false}});
    }
  }
  
  onDataActionLoginDataGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let i = -1;
      this.updateState({cpus: { $set: response.data[++i]}});
      this.updateState({osNetworks: { $set: response.data[++i]}});
    }
  }
  
  onDataActionLoginPing(action) {
    this.updateState({working: { $set: false}});
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({pingResults: { $set: new Map(response.data)}});
    }
  }
  
  onDataActionLoginCreateInterfaces(action) {
    this.updateState({working: { $set: false}});
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({loginDataChanged: { $set: true}});
    }
  } 
  
  onDataActionLoginDeleteInterfaces(action) {
    this.updateState({working: { $set: false}});
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({loginDataChanged: { $set: true}});
    }
  }
  
  onDataActionLoginButtonsAdd(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({buttons: {$set: response.data}});
    }
  }
  
  onDataActionLoginButtonsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const keys = Reflect.ownKeys(this.state.buttons);
      const resutObject = {};
      keys.forEach((key) => {
        const stateObject = Reflect.get(this.state.buttons, key);
        const storedObject = Reflect.get(response.data, key);
        const mergedObject = {...stateObject, ...storedObject};
        Reflect.set(resutObject, key, mergedObject);
      });
      this.updateState({buttons: {$set: resutObject}});
    }
    else {
      this.sendDataAction(new DataActionLoginButtonsAdd(this.buttonsKey, this.state.buttons));
    }
  }
  
  onDataActionLoginButtonsUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({buttons: {$set: response.data}});
    }
  }
}


module.exports = LoginStore.export(LoginStore);
