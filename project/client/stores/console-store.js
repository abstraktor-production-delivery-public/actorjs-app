
'use strict';


import { DataActionConsoleClear } from '../actions/action-console/data-action-console';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';


class ConsoleStore extends StoreBaseRealtime {
  constructor() {
    super({
      dummy: false
    });
  }
  
  onActionConsoleClear(action) {
    this.sendDataAction(new DataActionConsoleClear(action));
  }
  
  onDataActionConsoleClear(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {}
  }
}


module.exports = ConsoleStore.export(ConsoleStore);
