
'use strict';

import DocumentationStoreBase from 'z-abs-complayer-documentation-client/client/stores/documentation-store-base';


class DocumentationStore extends DocumentationStoreBase {
  constructor() {
    super('Documentation', 'actorjs');
  }
}


module.exports = DocumentationStore.export(DocumentationStore);
