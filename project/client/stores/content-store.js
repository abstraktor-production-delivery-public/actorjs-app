
'use strict';

import { DataActionContentTextGet, DataActionContentDocumentsGet, DataActionContentImageGet, DataActionContentVideoGet, DataActionContentAudioGet, DataActionContentOtherGet, DataActionContentTextUpdate, DataActionContentDocumentsUpdate, DataActionContentImageUpdate, DataActionContentVideoUpdate, DataActionContentAudioUpdate, DataActionContentOtherUpdate } from '../actions/action-content/data-action-content';
import MarkupContentText from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-text';
import MarkupContentDocuments from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-documents';
import MarkupContentImage from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-image';
import MarkupContentVideo from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-video';
import MarkupContentAudio from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-audio';
import MarkupContentOther from 'z-abs-complayer-markup-client/client/markup/markup-content/markup-content-other';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class ContentStore extends StoreBaseData {
  constructor() {
    super({
      contentTextGlobals: [],
      contentTextLocals: [],
      contentDocumentsGlobals: [],
      contentDocumentsLocals: [],
      contentImageGlobals: [],
      contentImageLocals: [],
      contentVideoGlobals: [],
      contentVideoLocals: [],
      contentAudioGlobals: [],
      contentAudioLocals: [],
      contentOtherGlobals: [],
      contentOtherLocals: [],
      currentView: '',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      }
    });
  }
  
  onActionContentTextGet(action) {
    this.sendDataAction(new DataActionContentTextGet());
  }
  
  onActionContentDocumentsGet(action) {
    this.sendDataAction(new DataActionContentDocumentsGet());
  }
  
  onActionContentImageGet(action) {
    this.sendDataAction(new DataActionContentImageGet());
  }
  
  onActionContentVideoGet(action) {
    this.sendDataAction(new DataActionContentVideoGet());
  }
  
  onActionContentAudioGet(action) {
    this.sendDataAction(new DataActionContentAudioGet());
  }
  
  onActionContentOtherGet(action) {
    this.sendDataAction(new DataActionContentOtherGet());
  }
    
  onActionContentMarkup(action) {
    let markup = this.markupStringify();
    this.updateState({markup: { definition: {$set: true}}});
    this.updateState({markup: { content: {$set: markup}}});
    this.updateState({markup: { contentOriginal: {$set: markup}}});
  }
  
  onActionContentMarkupSave(action) {
    let result = this.markupParse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      if('text' === this.state.currentView) {
        this.sendDataAction(new DataActionContentTextUpdate(result.globals, result.locals));
      }
      else if('documents' === this.state.currentView) {
        this.sendDataAction(new DataActionContentDocumentsUpdate(result.globals, result.locals));
      }
      else if('image' === this.state.currentView) {
        this.sendDataAction(new DataActionContentImageUpdate(result.globals, result.locals));
      }
      else if('video' === this.state.currentView) {
        this.sendDataAction(new DataActionContentVideoUpdate(result.globals, result.locals));
      }
      else if('audio' === this.state.currentView) {
        this.sendDataAction(new DataActionContentAudioUpdate(result.globals, result.locals));
      }
      else if('other' === this.state.currentView) {
        this.sendDataAction(new DataActionContentOtherUpdate(result.globals, result.locals));
      }
      this.clearMarkup();
    }
  }
  
  onActionContentMarkupCancel(action) {
    this.clearMarkup();
  }
  
  onActionContentMarkupChange(action) {
    const result = this.markupParse(action.markup);
    this.updateState({markup: { rows: {$set: result.rows}}});
    this.updateState({markup: { content: {$set: action.markup}}});
  }
  
  onDataActionContentTextGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentTextGlobals: {$set: response.data[0]}});
      this.updateState({contentTextLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'text'}});
    }
  }
  
  onDataActionContentDocumentsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentDocumentsGlobals: {$set: response.data[0]}});
      this.updateState({contentDocumentsLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'documents'}});
    }
  }
  
  onDataActionContentImageGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentImageGlobals: {$set: response.data[0]}});
      this.updateState({contentImageLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'image'}});
    }
  }
  
  onDataActionContentVideoGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentVideoGlobals: {$set: response.data[0]}});
      this.updateState({contentVideoLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'video'}});
    }
  }
  
  onDataActionContentAudioGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentAudioGlobals: {$set: response.data[0]}});
      this.updateState({contentAudioLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'audio'}});
    }
  }
  
  onDataActionContentOtherGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({contentOtherGlobals: {$set: response.data[0]}});
      this.updateState({contentOtherLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'other'}});
    }
  }
  
  onDataActionContentTextUpdate(action) {
    this.sendDataAction(new DataActionContentTextGet());
  }

  onDataActionContentDocumentsUpdate(action) {
    this.sendDataAction(new DataActionContentDocumentsGet());
  }
  
  onDataActionContentImageUpdate(action) {
    this.sendDataAction(new DataActionContentImageGet());
  }
  
  onDataActionContentVideoUpdate(action) {
    this.sendDataAction(new DataActionContentVideoGet());
  }
  
  onDataActionContentAudioUpdate(action) {
    this.sendDataAction(new DataActionContentAudioGet());
  }
  
  onDataActionContentOtherUpdate(action) {
    this.sendDataAction(new DataActionContentOtherGet());
  }

  clearMarkup() {
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
  
  markupStringify() {
    if('text' === this.state.currentView) {
      return MarkupContentText.stringify(this.state.contentTextGlobals, this.state.contentTextLocals);
    }
    else if('documents' === this.state.currentView) {
      return MarkupContentDocuments.stringify(this.state.contentDocumentsGlobals, this.state.contentDocumentsLocals);
    }
    else if('image' === this.state.currentView) {
      return MarkupContentImage.stringify(this.state.contentImageGlobals, this.state.contentImageLocals);
    }
    else if('video' === this.state.currentView) {
      return MarkupContentVideo.stringify(this.state.contentVideoGlobals, this.state.contentVideoLocals);
    }
    else if('audio' === this.state.currentView) {
      return MarkupContentAudio.stringify(this.state.contentAudioGlobals, this.state.contentAudioLocals);
    }
    else if('other' === this.state.currentView) {
      return MarkupContentOther.stringify(this.state.contentOtherGlobals, this.state.contentOtherLocals);
    }
  }
  
  markupParse(markup) {
    if('text' === this.state.currentView) {
      return MarkupContentText.parse(markup);
    }
    else if('documents' === this.state.currentView) {
      return MarkupContentDocuments.parse(markup);
    }
    else if('image' === this.state.currentView) {
      return MarkupContentImage.parse(markup);
    }
    else if('video' === this.state.currentView) {
      return MarkupContentVideo.parse(markup);
    }
    else if('audio' === this.state.currentView) {
      return MarkupContentAudio.parse(markup);
    }
    else if('other' === this.state.currentView) {
      return MarkupContentOther.parse(markup);
    }
  }
}


module.exports = ContentStore.export(ContentStore);
