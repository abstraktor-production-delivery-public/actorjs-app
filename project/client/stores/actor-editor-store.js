
'use strict';

import { DataActionActorEditorProjectGet, DataActionActorEditorProjectUpdate, DataActionActorEditorProjectToggle } from '../actions/action-actor-editor/data-action-actor-editor-project';
import { DataActionActorEditorFileGet, DataActionActorEditorFileInlineGet, DataActionActorEditorFileNew, DataActionActorEditorFileRename, DataActionActorEditorFileUpdate, DataActionActorEditorFileInlineUpdate, DataActionActorEditorFileDelete, DataActionActorEditorBuild, DataActionActorEditorStacksGet } from '../actions/action-actor-editor/data-action-actor-editor-file';
import { DataActionActorEditorFolderNew, DataActionActorEditorFolderUpdate, DataActionActorEditorFolderDelete } from '../actions/action-actor-editor/data-action-actor-editor-folder';
import { ActionActorEditorFileClose } from '../actions/action-actor-editor/action-actor-editor-file';
import { ActionModalDialogResult } from 'z-abs-complayer-modaldialog-client/client/actions/action-modal-dialog';
import StoreBaseRealtime from 'z-abs-corelayer-client/client/store/store-base-realtime';
import Project from 'z-abs-corelayer-cs/clientServer/project';


class ActorEditorStore extends StoreBaseRealtime {
  static FOLDER = 'folder';
  static FILE = 'file';
  static FILE_INLINE = 'file_inline';
  static FILE_INVALID = 'invalid_file';

  constructor() {
    super({
      projectGlobal: new Project(),
      projectLocal: new Project(),
      files: new Map(),
      current: {
        file: null,
        folder: null,
        type: '',
        query: null
      },
      build: {
        result: false,
        success: false,
        errors: []
      },
      persistentData: {
        darkMode: false,
        emptyLine: true
      },
      stacks: new Map(),
      stackTemplates: new Map()
    });
    this.sendDataAction(new DataActionActorEditorStacksGet());
  }

  onActionActorEditorPersistentDataUpdate(action) {
    const persistentData = this.deepCopy(this.state.persistentData);
    const parent = action.parentName ? Reflect.get(persistentData, action.parentName) : persistentData;
    Reflect.set(parent, action.name, action.value);
    this.updateState({persistentData: {$set: parent}});
  }
  
  onActionActorEditorFileGet(action) {
    const file = this.state.files.get(action.key);
    if(undefined === file) {
      if(action.key.startsWith('./Actors-inline')) {
        this.sendDataAction(new DataActionActorEditorFileInlineGet(action.path));
      }
      else {
        this.sendDataAction(new DataActionActorEditorFileGet(`${action.path}/${action.title}`, action.projectId, action.key, action.title, action.path, action.type));
      }
    }
    else {
      const currentType = action.path.startsWith('./Actors-inline') ? ActorEditorStore.FILE_INLINE : ActorEditorStore.FILE;
      this.updateState(this._exec(action.projectId, (project) => {
        project.select(action.key, true);
      }));
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: currentType}}});
      this.updateState({build: {result: {$set: false}}});
      if(ActorEditorStore.FILE === currentType) {
        action.history(this._generateQuery(action.path, action.title), {replace: true});
      }
      else {
        action.history(action.path, {replace: true});
      }
    }
  }
  
  onActionActorEditorFileSet(action) {
    if(undefined !== action.url) {
      if(undefined !== action.query) {
        this.updateState({current: {query: {$set: this._createQuery(action.url, action.query)}}});
      }
      const node = this._getNode(action.url);
      if(undefined !== node) {
        if(!node.node.folder) {
          const file = this.state.files.get(node.node.key);
          if(undefined === file) {
            this.sendDataAction(new DataActionActorEditorFileGet(action.url, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type));
          }
          else {
            this.updateState({current: {file: {$set: file}}});
            this.updateState({current: {type: {$set: ActorEditorStore.FILE}}});
          }
        }
        else {
          const folder = this._createFolder(node.projectId, node.node);
          this.updateState({current: {folder: {$set: folder}}});
          this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
        }
      }
      else {
        if(action.url.startsWith('./Actors-inline')) {
          this.sendDataAction(new DataActionActorEditorFileInlineGet(action.url));
        }
        this.sendDataAction(new DataActionActorEditorProjectGet(action.url, action.query));
      }
    }
    else {
      this.sendDataAction(new DataActionActorEditorProjectGet());
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: null}}});
      this.updateState({current: {type: {$set: ''}}});
      this.updateState({files: (files) => {
        if(0 !== files.size) {
          files.clear();
        }
      }});
    }
  }
  
  onActionActorEditorFileNew(action) {
    this.sendDataAction(new DataActionActorEditorFileNew(action));
  }
  
  onActionActorEditorFileAdd(action) {
    this.updateState(this._exec(action.projectId, (project) => {
      const node = project.addFile(action.title, action.path, action.type);
      project.sortParent(node);
      this.sendDataAction(new DataActionActorEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type));
    }));
  }
  
  onActionActorEditorFileRename(action) {
    this.sendDataAction(new DataActionActorEditorFileRename(`${action.path}/${action.title}`, `${action.path}/${action.newTitle}`, action));
  }
  
  onActionActorEditorFileUpdate(action) {
    const file = this.state.files.get(action.key);
    if(ActorEditorStore.FILE === file.fileType) {
      this.sendDataAction(new DataActionActorEditorFileUpdate(action.key, file.title, file.path, file.content));
    }
    else {
      this.sendDataAction(new DataActionActorEditorFileInlineUpdate(action.key, file.path, file.content));
    }
  }
  
  onActionActorEditorFileUpdateAll(action) {
    action.keys.forEach((key) => {
      const file = this.state.files.get(key);
      if(ActorEditorStore.FILE === file.fileType) {
        this.sendDataAction(new DataActionActorEditorFileUpdate(key, file.title, file.path, file.content));
      }
      else {
        this.sendDataAction(new DataActionActorEditorFileInlineUpdate(key, file.path, file.content));
      }
    });
  }
    
  onActionActorEditorFileEdit(action) {
    const file = this.state.files.get(action.key);
    this.updateState({current: {file: { content: {$set: action.content}}}});
    this.updateState({current: {file: { codeChanged: {$set: action.content !== file.contentOriginal}}}});
    this.updateState({files: (files) => {
      files.set(action.key, this.state.current.file);
    }});
    this.updateState({build: {result: {$set: false}}});
  }
  
  onActionActorEditorFileClose(action) {
    const file = this.state.files.get(action.key);
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    if(undefined !== nextFile) {
      const currentType = nextFile.path.startsWith('./Actors-inline') ? ActorEditorStore.FILE_INLINE : ActorEditorStore.FILE;
      this.updateState({current: {file: {$set: nextFile}}});
      this.updateState({current: {type: {$set: currentType}}});
      action.history(this._generateQuery(nextFile.path, nextFile.title), {replace: true});
    }
    else {
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: null}}});
      this.updateState({current: {type: {$set: ''}}});
      action.history('.', {replace: true});
    }
  }
  
  onActionActorEditorFileRemove(action) {
    const file = this.state.files.get(action.key) || this.state.current.file;
    const nextFile = this._getNextFile(file);
    this.updateState({files: (files) => {
      files.delete(action.key);
    }});
    this.updateState(this._exec(action.projectId, (project) => {
      project.removeNode(file.path, file.key);
    }));
    if(undefined !== nextFile) {
      this.updateState({current: {file: {$set: nextFile}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FILE}}});
      action.history(this._generateQuery(nextFile.path, nextFile.title), {replace: true});
    }
    else {
      const node = this._getNode(file.path);
      const folder = this._createFolder(node.projectId, node.node);
      this.updateState({current: {file: {$set: null}}});
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onActionActorEditorFileDelete(action) {
    const file = this.state.files.get(action.key);
    this.sendDataAction(new DataActionActorEditorFileDelete(`${file.path}/${file.title}`, action));
  }
  
  onActionActorEditorFolderGet(action) {
    const node = this.getProject(action.projectId).findNode(`${action.data.path}/${action.title}` );
    const folder = this._createFolder(action.projectId, node);
    this.updateState(this._exec(action.projectId, (project) => {
      project.select(action.key, true);
    }));
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
    this.updateState({build: {result: {$set: false}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionActorEditorFolderNew(action) {
    this.sendDataAction(new DataActionActorEditorFolderNew(action));
  }
  
  onActionActorEditorFolderAdd(action) {
    let node;
    this.updateState(this._exec(action.projectId, (project) => {
      node = project.addFolder(action.title, action.data);
      project.sortParent(node);
    }));
    const folder = this._createFolder(action.projectId, node);
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionActorEditorFolderUpdate(action) {
    this.sendDataAction(new DataActionActorEditorFolderUpdate(`${action.data.path}/${action.title}`, `${action.data.path}/${action.newTitle}`, action));
  }
  
  onActionActorEditorFolderRemove(action) {
    const node = this._getNode(`${action.data.path}/${action.title}`);
    const children = this.getProject(node.projectId).getAllFileChildren(node.node);
    
    children.forEach((child) => {
      const file = this.state.files.get(child.key);
      if(undefined !== file) {
        this.onActionActorEditorFileClose(new ActionActorEditorFileClose(child.key));
      }
    });
    
    const nodeParent = this._getNode(node.node.data.path);
    const folder = this._createFolder(nodeParent.projectId, nodeParent.node);
    
    this.updateState(this._exec(action.projectId, (project) => {
      project.removeNode(action.data.path, action.key);
    }));
    
    this.updateState({current: {folder: {$set: folder}}});
    this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
    action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
  }
  
  onActionActorEditorFolderDelete(action) {
    this.sendDataAction(new DataActionActorEditorFolderDelete(action));
  }
  
  onActionActorEditorFileMove(action) {
    let found = false;
    let foundFromKey = false;
    let foundToKey = false;
    let newMap = new Map();
    let fromFile = null;
    this.state.files.forEach((file, key) => {
      if(!found) {
        if(action.fromKey === key) {
          foundFromKey = true;
          found = true;
        }
        else if(action.toKey === key) {
          fromFile = this.state.files.get(action.fromKey);
          newMap.set(action.fromKey, fromFile);
          newMap.set(action.toKey, file);
          foundToKey = true;
          found = true;
        }
        else {
          newMap.set(key, file);
        }
      }
      else {
        if(foundFromKey) {
          if(action.toKey === key) {
            newMap.set(action.toKey, file);
            fromFile = this.state.files.get(action.fromKey);
            newMap.set(action.fromKey, fromFile);
          }
          else if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
        else if(foundToKey) {
           if(action.fromKey !== key) {
            newMap.set(key, file);
          }
        }
      }
    });
    this.updateState({files: {$set: newMap}});
    this.updateState({current: {file: {$set: fromFile}}});
    this.updateState({current: {type: {$set: ActorEditorStore.FILE}}});
    action.history(`/${fromFile.path}/${fromFile.title}`, {replace: true});
  }
  
  onActionActorEditorProjectGet(action) {
    this.sendDataAction(new DataActionActorEditorProjectGet(action.url, action.query));
  }
  
  onActionActorEditorProjectUpdate(action) {
    if(!this.state.projectGlobal.isSaved()) {
      this.sendDataAction(new DataActionActorEditorProjectUpdate(this.state.projectGlobal.projectId, this.state.projectGlobal.getRootName(), this.state.projectGlobal.source));
    }
    if(!this.state.projectLocal.isSaved()) {
      this.sendDataAction(new DataActionActorEditorProjectUpdate(this.state.projectLocal.projectId, this.state.projectLocal.getRootName(), this.state.projectLocal.source));
    }
  }
  
  onActionActorEditorProjectToggle(action) {
    this.updateState(this._exec(action.projectId, (project) => {
      project.toggle(action.key, action.expanded);
    }));
    const project = this.getProject(action.projectId);
    this.sendDataAction(new DataActionActorEditorProjectToggle(action, project.source, project.getRootName()));
  }
    
  onActionActorEditorBuild(action) {
    this.sendDataAction(new DataActionActorEditorBuild(this.state.current.file.content, this.state.current.file.path, this.state.current.file.title, action));
  }
  
  onDataActionActorEditorStacksGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(response.data) {
        const stacks = new Map();
        const stackTemplates = new Map();
        response.data.forEach((stack) => {
          const stackName = stack[0];
          const mapStacks = stack[1];
          mapStacks.forEach((mapStack) => {
            const actorType = mapStack[0];
            if(0 !== mapStack[1].names.length) {
              let storedStacks = stacks.get(actorType);
              if(!storedStacks) {
                storedStacks = ['none'];
                stacks.set(actorType, storedStacks);
              }
              storedStacks.push(stackName);
            }
          });
          const actorTypeMap = new Map(mapStacks);
          stackTemplates.set(stackName, actorTypeMap);
        });
        this.updateState({stacks: {$set: stacks}});
        this.updateState({stackTemplates: {$set: stackTemplates}});
      }
    }
  }
  
  onDataActionActorEditorFileGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      if(this.state.current.file?.key !== action.key) {
        const file = {
          projectId: action.projectId,
          key: action.key,
          title: action.title,
          path: action.path,
          fileType: ActorEditorStore.FILE,
          type: action.type,
          content: response.data,
          contentOriginal: response.data,
          codeChanged: false,
          codeBuilt: false
        };
        this.updateState(this._exec(action.projectId, (project) => {
          project.select(action.key, true);
        }));
        this.updateState({files: (files) => { files.set(action.key, file); }});
        this.updateState({current: {file: {$set: file}}});
        this.updateState({current: {type: {$set: ActorEditorStore.FILE}}});
        this.updateState({build: {result: {$set: false}}});
        this.updateState(this._exec(action.projectId, (project) => {
          const node = project.findNode(`${action.path}/${action.title}`);
          node.data.valid = true;
        }));
        action.history(this._generateQuery(action.path, action.title), {replace: true});
      }
    }
    else {
      this.updateState(this._exec(action.projectId, (project) => {
        project.select(action.key, true);
        const node = project.findNode(`${action.path}/${action.title}`);
        node.data.valid = false;
      }));
      const file = {
        projectId: action.projectId,
        key: action.key,
        title: action.title,
        path: action.path,
        fileType: ActorEditorStore.FILE_INVALID,
        type: action.type,
        codeChanged: false,
        codeBuilt: false
      };
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FILE_INVALID}}});
      this.updateState({build: {result: {$set: false}}});
      action.history(this._generateQuery(action.path, action.title), {replace: true});
    }
  }
  
  onDataActionActorEditorFileInlineGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const inlineCode = response.data[0];
      const actorName = response.data[1];
      const file = {
        key: actorName,
        title: actorName,
        path: action.url,
        fileType: ActorEditorStore.FILE_INLINE,
        type: 'js',
        content: inlineCode,
        contentOriginal: inlineCode,
        codeChanged: false,
        codeBuilt: false
      };
      this.updateState({files: (files) => { files.set(actorName, file); }});
      this.updateState({current: {file: {$set: file}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FILE_INLINE}}});
    }
  }
  
  onDataActionActorEditorFileNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState(this._exec(action.projectId, (project) => {
        const node = project.addFile(response.data, action.path, action.type);
        project.sortParent(node);
        this.sendDataAction(new DataActionActorEditorFileGet(`${node.data.path}/${node.title}`, action.projectId, node.key, node.title, node.data.path, node.data.type));
      }));
    }
  }
  
  onDataActionActorEditorFileRename(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const node = this._getNode(`${action.path}/${action.title}`);
      this.updateState(this._exec(node.projectId, (project) => {
        project.renameFile(node.node, action.newTitle);
      }));
      let file = this.state.files.get(node.node.key);
      const updatedFile = this.shallowCopy(file);
      updatedFile.title = action.newTitle;
      this.updateState({files: (files) => {
        file = files.set(node.node.key, updatedFile);
      }});
      this.updateState({current: {file: {$set: updatedFile}}});
      action.history(this._generateQuery(updatedFile.path, updatedFile.title), {replace: true});
    }
  }
  
  onDataActionActorEditorFileUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let file = this.state.files.get(action.key);
      if(undefined !== file) {
        let updatedFile = this.shallowCopy(file);
        updatedFile.codeChanged = false;
        updatedFile.contentOriginal = action.content;
        this.updateState({files: (files) => {
          files.set(action.key, updatedFile);
        }});
        if(this.state.current.file.key === action.key) {
          this.updateState({current: {file: {$set: updatedFile}}});
        }        
      }
    }
  }
  
  onDataActionActorEditorFileInlineUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let file = this.state.files.get(action.key);
      if(undefined !== file) {
        let updatedFile = this.shallowCopy(file);
        updatedFile.codeChanged = false;
        updatedFile.contentOriginal = action.content;
        this.updateState({files: (files) => {
          files.set(action.key, updatedFile);
        }});
        if(this.state.current.file.key === action.key) {
          this.updateState({current: {file: {$set: updatedFile}}});
        }        
      }
    }
  }
  
  onDataActionActorEditorFileDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionActorEditorFileRemove(action);
    }
  }
  
  onDataActionActorEditorFolderNew(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node;
      this.updateState(this._exec(action.projectId, (project) => {
        node = project.addFolder(action.title, action.data);
        project.sortParent(node);
      }));
      const folder = this._createFolder(action.projectId, node);
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onDataActionActorEditorFolderUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      let node = this._getNode(`${action.data.path}/${action.title}`);
      const children = this.getProject(node.projectId).getAllFileChildren(node.node);
      this.updateState(this._exec(node.projectId, (project) => {
        project.renamePathRecursive(node.node, action.newTitle);
      }));
      node = this._getNode(`${action.data.path}/${action.newTitle}`);
      const folder = this._createFolder(node.projectId, node.node);
      this.updateState({current: {folder: {$set: folder}}});
      this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
      children.forEach((child) => {
        let file;
        this.updateState({files: (files) => {
          file = files.get(child.key);
          if(undefined !== file) {
            file = this.shallowCopy(file);
            file.path = child.data.path;
            files.set(child.key, file);
          }
        }});
        if(this.state.current.file && this.state.current.file.key === child.key) {
          this.updateState({current: {file: {$set: file}}});
        }
      });
      action.history(this._generateQuery(folder.data.path, folder.title), {replace: true});
    }
  }
  
  onDataActionActorEditorFolderDelete(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.onActionActorEditorFolderRemove(action);
    }
  }
  
  onDataActionActorEditorProjectGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({projectLocal: (project) => { project.set(response.data[0]); }});
      this.updateState({projectGlobal: (project) => { project.set(response.data[1]); }});
    }
    if(action.url) {
      const node = this._getNode(action.url);
      if(node) {
        if(!node.node.folder) {
          this.sendDataAction(new DataActionActorEditorFileGet(action.url, node.projectId, node.node.key, node.node.title, node.node.data.path, node.node.data.type));
        }
        else {
          const folder = this._createFolder(node.projectId, node.node);
          this.updateState({current: {folder: {$set: folder}}});
          this.updateState({current: {type: {$set: ActorEditorStore.FOLDER}}});
        }
      }
    }
  }
  
  onDataActionActorEditorProjectUpdate(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState(this._exec(action.projectId, (project) => {
        project.setSaved();
      }));
    }
  }
  
  onDataActionActorEditorProjectToggle(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
    }
  }
  
  onDataActionActorEditorBuild(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({build: {errors: {$set: []}}});
      const resultChecker = response.data[0];
      /*if('success' === resultChecker.check) {
        this.updateState({build: {checker: {$set: null}}});
      }
      else {
        this.updateState({build: {checker: {$set: resultChecker.err}}});
      }*/
      let success = true;
      const resultBundle = response.data[1];
      if('failure' === resultBundle.check) {
        success = false;
        this.updateState({build: {errors: (errors) => {
          errors.push(resultBundle.err);
        }}});
      }
      const resultJsHints = response.data[2];
      if('failure' === resultJsHints.check) {
        success = false;
        this.updateState({build: {errors: (errors) => {
          resultJsHints.errors.forEach((resultJsHint) => {
            errors.push(resultJsHint);
          });
        }}});
      }
      this.updateState({build: {result: {$set: true}}});
      this.updateState({build: {success: {$set: success}}});
    }
  }
  
  getProject(projectId) {
    if(projectId === this.state.projectGlobal.projectId) {
      return this.state.projectGlobal;
    }
    else if(projectId === this.state.projectLocal.projectId) {
      return this.state.projectLocal;
    }
  }
  
  _getNextFile(file) {
    let next;
    let mapIter = this.state.files.values();
    let iter = mapIter.next();
    while(!iter.done) {
      if(iter.value.key === file.key) {
        if(undefined !== next) {
          return next;
        }
        else {
          return mapIter.next().value;
        }  
      }
      next = iter.value;
      iter = mapIter.next();
    }
  }
  
  _getNode(file) {
    let node = this.state.projectGlobal.findNode(file);
    if(undefined !== node) {
      return {
        projectId: this.state.projectGlobal.projectId,
        node: node
      };
    }
    node = this.state.projectLocal.findNode(file);
    if(undefined !== node) {
      return {
        projectId: this.state.projectLocal.projectId,
        node: node
      };
    }
  }
  
  _createFolder(projectId, node) {
    return {
      projectId: projectId,
      key: node.key,
      title: node.title,
      data: node.data
    };
  }
  
  _exec(projectId, exec) {
    if(projectId === this.state.projectGlobal.projectId) {
      return {projectGlobal: exec};
    }
    else if(projectId === this.state.projectLocal.projectId) {
      return {projectLocal: exec};
    }
  }
  
  _createQuery(url, search) {
    const query = {};
    if(search && search.length > 1) {
      const searchs = search.substring(1).split('&');
      if(searchs && searchs.length >= 1) {
        searchs.forEach((searchPart) => {
          const queryParam = searchPart.split('=');
          if(2 === queryParam.length) {
            Reflect.set(query, queryParam[0], queryParam[1]);
          }
        });
      }
    }
    if(undefined !== query && undefined !== query.line) {
      return {
        url: url,
        line: Number.parseInt(query.line),
        type: query.type ? Number.parseInt(query.type) : 0
      }
    }
  }
  
  _generateQuery(path, title) {
    const url = `${path}/${title}`
    const query = this.state.current.query;
    if(query && query.url && query.url === url) {
      let queries = [];
      let resultQuery = '';
      if(query.line) {
        queries.push(`line=${query.line}`);
      }
      if(query.type) {
        queries.push(`type=${query.type}`);
      }
      if(0 !== queries.length) {
         resultQuery = `?${queries[0]}`;
      }
      for(let i = 1; i < queries.length; ++i) {
        resultQuery += `&${queries[i]}`;
      }
      return `${url.substring(1)}${resultQuery}`;
    }
    else {
      return url.substring(1);
    }
  }
}


module.exports = ActorEditorStore.export(ActorEditorStore);
