
'use strict';

import StoreBase from 'z-abs-corelayer-client/client/store/store-base';


class TestSuitesStore extends StoreBase {
  constructor() {
    super({
      systemUnderTestsChecked: new Map(),
      functionUnderTestsChecked: new Map(),
      testSuitesChecked: new Set(),
      current: {
        view: 'none'
      }
    });
  }
  
  onActionTestSuitesSetView(action) {
    this.updateState({current: {view: {$set: action.currentView}}});
  }    
  
  onActionTestSuitesSutChecked(action) {
    if(action.checked) {
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.set(action.sut.name, action.sut);
      }});
    }
    else {
      this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
        systemUnderTestsChecked.delete(action.sut.name);
      }});
    }
  }
  
  onActionTestSuitesFutChecked(action) {
    if(action.checked) {
      this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
        functionUnderTestsChecked.set(action.futName, {
          repo: action.repoName,
          sut: action.sutName,
          fut: action.futName
        });
      }});
    }
    else {
      this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
        functionUnderTestsChecked.delete(action.futName);
      }});
    }
  }

  
  onActionTestSuitesTsChecked(action) {
    if(action.checked) {
      this.updateState({testSuitesChecked: (testSuitesChecked) => {
        testSuitesChecked.add(action.tsName);
      }});
    }
    else {
      this.updateState({testSuitesChecked: (testSuitesChecked) => {
        testSuitesChecked.delete(action.tsName);
      }});
    }
  }
  
  onActionTestSuitesSutClear(action) {
    this.updateState({systemUnderTestsChecked: (systemUnderTestsChecked) => {
      systemUnderTestsChecked.clear();
    }});
  }
  
  onActionTestSuitesFutClear(action) {
    this.updateState({functionUnderTestsChecked: (functionUnderTestsChecked) => {
      functionUnderTestsChecked.clear();
    }});
  }
  
  onActionTestSuitesTsClear(action) {
    this.updateState({testSuitesChecked: (testSuitesChecked) => {
      testSuitesChecked.clear();
    }});
  }
}


module.exports = TestSuitesStore.export(TestSuitesStore);
