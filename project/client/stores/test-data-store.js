
'use strict';

import { DataActionTestDataEnvironmentGet, DataActionTestDataEnvironmentUpdate, DataActionTestDataGeneralGet, DataActionTestDataGeneralUpdate, DataActionTestDataOutputGet, DataActionTestDataOutputUpdate, DataActionTestDataSystemGet, DataActionTestDataSystemUpdate } from '../actions/action-test-data/data-action-test-data';
import MarkupTestDataEnvironment from 'z-abs-complayer-markup-client/client/markup/markup-test-data/markup-test-data-environment';
import MarkupTestDataGeneral from 'z-abs-complayer-markup-client/client/markup/markup-test-data/markup-test-data-general';
import MarkupTestDataOutput from 'z-abs-complayer-markup-client/client/markup/markup-test-data/markup-test-data-output';
import MarkupTestDataSystem from 'z-abs-complayer-markup-client/client/markup/markup-test-data/markup-test-data-system';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class TestDataStore extends StoreBaseData {
  constructor() {
    super({
      testDataEnvironmentGlobals: [],
      testDataEnvironmentLocals: [],
      testDataEnvironmentStatics: [],
      testDataOutputGlobals: [],
      testDataOutputLocals: [],
      testDataGeneralGlobals: [],
      testDataGeneralLocals: [],
      testDataSystemGlobals: [],
      testDataSystemLocals: [],
      currentView: '',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      }
    });
  }
  
  onActionTestDataEnvironmentGet(action) {
    this.sendDataAction(new DataActionTestDataEnvironmentGet());
  }
  
  onActionTestDataGeneralGet(action) {
    this.sendDataAction(new DataActionTestDataGeneralGet());
  }
  
  onActionTestDataOutputGet(action) {
    this.sendDataAction(new DataActionTestDataOutputGet());
  }
  
  onActionTestDataSystemGet(action) {
    this.sendDataAction(new DataActionTestDataSystemGet());
  }
  
  onActionTestDataMarkup(action) {
    let markup = this.markupStringify();
    this.updateState({markup: { definition: {$set: true}}});
    this.updateState({markup: { content: {$set: markup}}});
    this.updateState({markup: { contentOriginal: {$set: markup}}});
  }
  
  onActionTestDataMarkupSave(action) {
    const result = this.markupParse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      if('environment' === this.state.currentView) {
        this.sendDataAction(new DataActionTestDataEnvironmentUpdate(result.globals, result.locals));
      }
      else if('general' === this.state.currentView) {
        this.sendDataAction(new DataActionTestDataGeneralUpdate(result.globals, result.locals));
      }
      else if('output' === this.state.currentView) {
        this.sendDataAction(new DataActionTestDataOutputUpdate(result.globals, result.locals));
      }
      else if('system' === this.state.currentView) {
        this.sendDataAction(new DataActionTestDataSystemUpdate(result.globals, result.locals));
      }
      console.log('onActionTestDataMarkupSave');
      this.clearMarkup();
    }
  }
  
  onActionTestDataMarkupCancel(action) {
    console.log('onActionTestDataMarkupCancel');
    this.clearMarkup();
  }
  
  onActionTestDataMarkupChange(action) {
    const result = this.markupParse(action.markup);
    this.updateState({markup: { rows: {$set: result.rows}}});
    this.updateState({markup: { content: {$set: action.markup}}});
  }
  
  onDataActionTestDataEnvironmentGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testDataEnvironmentGlobals: {$set: response.data[0]}});
      this.updateState({testDataEnvironmentLocals: {$set: response.data[1]}});
      this.updateState({testDataEnvironmentStatics: {$set: response.data[2]}});
      this.updateState({currentView: {$set: 'environment'}});
    }
  }
  
  onDataActionTestDataGeneralGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testDataGeneralGlobals: {$set: response.data[0]}});
      this.updateState({testDataGeneralLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'general'}});
    }
  }
  
  onDataActionTestDataOutputGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testDataOutputGlobals: {$set: response.data[0]}});
      this.updateState({testDataOutputLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'output'}});
    }
  }
  
  onDataActionTestDataSystemGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({testDataSystemGlobals: {$set: response.data[0]}});
      this.updateState({testDataSystemLocals: {$set: response.data[1]}});
      this.updateState({currentView: {$set: 'system'}});
    }
  }
  
  onDataActionTestDataEnvironmentUpdate(action) {
    this.sendDataAction(new DataActionTestDataEnvironmentGet());
  }

  onDataActionTestDataGeneralUpdate(action) {
    this.sendDataAction(new DataActionTestDataGeneralGet());
  }

  onDataActionTestDataOutputUpdate(action) {
    this.sendDataAction(new DataActionTestDataOutputGet());
  }

  onDataActionTestDataSystemUpdate(action) {
    this.sendDataAction(new DataActionTestDataSystemGet());
  }

  clearMarkup() {
    console.log('clearMarkup');
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
  
  markupStringify() {
    if('environment' === this.state.currentView) {
      return MarkupTestDataEnvironment.stringify(this.state.testDataEnvironmentGlobals, this.state.testDataEnvironmentLocals);
    }
    else if('general' === this.state.currentView) {
      return MarkupTestDataGeneral.stringify(this.state.testDataGeneralGlobals, this.state.testDataGeneralLocals);
    }
    else if('output' === this.state.currentView) {
      return MarkupTestDataOutput.stringify(this.state.testDataOutputGlobals, this.state.testDataOutputLocals);
    }
    else if('system' === this.state.currentView) {
      return MarkupTestDataSystem.stringify(this.state.testDataSystemGlobals, this.state.testDataSystemLocals);
    }
  }
  
  markupParse(markup) {
    if('environment' === this.state.currentView) {
      return MarkupTestDataEnvironment.parse(markup);
    }
    else if('general' === this.state.currentView) {
      return MarkupTestDataGeneral.parse(markup);
    }
    else if('output' === this.state.currentView) {
      return MarkupTestDataOutput.parse(markup);
    }
    else if('system' === this.state.currentView) {
      return MarkupTestDataSystem.parse(markup);
    }
  }
}


module.exports = TestDataStore.export(TestDataStore);
