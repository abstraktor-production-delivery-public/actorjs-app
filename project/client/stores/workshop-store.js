
'use strict';

import DocumentationStoreBase from 'z-abs-complayer-documentation-client/client/stores/documentation-store-base';


class WorkshopStore extends DocumentationStoreBase {
  constructor() {
    super('Workshop', 'actorjs');
  }
}


module.exports = WorkshopStore.export(WorkshopStore);

