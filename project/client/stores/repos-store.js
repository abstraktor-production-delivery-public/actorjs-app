
'use strict';

import { DataActionReposAnalyzeLocalReposGet, DataActionReposClone, DataActionReposGet, DataActionReposUpdate, DataActionReposVerifyGit } from '../actions/action-repos/data-action-repos';
import MarkupRepos from 'z-abs-complayer-markup-client/client/markup/markup-repos/markup-repos';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class ReposStore extends StoreBaseData {
  constructor() {
    super({
      handleRepos: false,
      actorjsContentLocalExists: false,
      actorjsDataLocalExists: false,
      actorjsContentLocalRepos: [],
      actorjsDataLocalRepos: [],
      localContentUrl: '',
      localDataUrl: '',
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      },
      progress: {
        clone: false
      },
      error: {
        error: false,
        text: ''
      },
      gitConfigs: []
    });
  }
    
  _clearError() {
    this.updateState({error: {error: { $set: false}}});
    this.updateState({error: {text: { $set: ''}}});
  }
  
  onActionReposAnalyzeLocalReposGet(action) {
    this.sendDataAction(new DataActionReposAnalyzeLocalReposGet());
    this._clearError();
  }
  
  onActionReposGet(action) {
    this.sendDataAction(new DataActionReposGet());
    this._clearError();
  }

  onActionReposClone(action) {
    this.sendDataAction(new DataActionReposClone(action));
    this.updateState({progress: {clone: { $set: true}}});
    this._clearError();
  }
  
  onActionReposVerifyGit(action) {
    this.sendDataAction(new DataActionReposVerifyGit());
  }
  
  onActionReposMarkup(action) {
    let markup = MarkupRepos.stringify(this.state.reposContentLocal, this.state.actorjsContentLocalRepos);
    this.updateState({markup: { definition: {$set: true}}});
    this.updateState({markup: { content: {$set: markup}}});
    this.updateState({markup: { contentOriginal: {$set: markup}}});
  }
  
  onActionReposMarkupSave(action) {
    const result = MarkupRepos.stringify(this.state.reposContentLocal, this.state.actorjsContentLocalRepos);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      this.sendDataAction(new DataActionReposUpdate(result.contentRepos, result.dataRepos));
      this.clearMarkup();
    }
  }
  
  onActionReposMarkupCancel(action) {
     this.clearMarkup();
  }
  
  onActionReposMarkupChange(action) {
    const result = MarkupRepos.parse(action.markup);
    this.updateState({markup: { rows: {$set: result.rows}}});
    this.updateState({markup: { content: {$set: action.markup}}});
  }
  
  onActionTestCaseMarkupCopy() {
    const markupTc = MarkupRepos.stringify(this.state.reposContentLocal, this.state.actorjsContentLocalRepos);
    navigator.clipboard.writeText(markupTc).then(() => {}, () => {});
  }
  
  onDataActionReposAnalyzeLocalReposGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const contentData = response.data[0];
      const dataData = response.data[1];
      const reposData = response.data[2];
      this.updateState({actorjsContentLocalExists: { $set: contentData.isRepo}});
      this.updateState({actorjsDataLocalExists: { $set: dataData.isRepo}});
      this.updateState({handleRepos: { $set: !contentData.isRepo || !dataData.isRepo}});
      if(reposData) {
        this.updateState({actorjsContentLocalRepos: { $set: reposData.actorjsContentRepos}});
        this.updateState({actorjsDataLocalRepos: { $set: reposData.actorjsDataRepos}});
        this.updateState({localContentUrl: { $set: contentData.isRepo ? contentData.url : reposData.actorjsContentRepos[0]?.url}});
        this.updateState({localDataUrl: { $set: dataData.isRepo ? dataData.url : reposData.actorjsDataRepos[0]?.url}});
      }
      else {
        this.updateState({actorjsContentLocalRepos: { $set: []}});
        this.updateState({actorjsDataLocalRepos: { $set: []}});
        this.updateState({localContentUrl: { $set: ''}});
        this.updateState({localDataUrl: { $set: ''}});
      }
    }
    else {
      this.updateState({error: {error: { $set: true}}});
      this.updateState({error: {text: { $set: response.result.msg[0]}}});
    }
  }
  
  onDataActionReposGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const actorjsContentLocalExists = -1 !== response.data.actorjsContentRepos.find((repo) => {
        return 'actorjs-content-local' === repo.name;
      });
      this.updateState({actorjsContentLocalExists: { $set: actorjsContentLocalExists}});
      this.updateState({actorjsContentLocalRepos: { $set: response.data.actorjsContentRepos}});
      const actorjsDataLocalExists = -1 !== response.data.actorjsDataRepos.find((repo) => {
        return 'actorjs-data-local' === repo.name;
      });
      this.updateState({actorjsDataLocalExists: { $set: actorjsDataLocalExists}});
      this.updateState({actorjsDataLocalRepos: { $set: response.data.actorjsDataRepos}});
    }
  }
   
  onDataActionReposClone(action) {
    const response = action.getResponse();
    this.updateState({progress: {clone: { $set: false}}});
    if(response.isSuccess()) {
      this.sendDataAction(new DataActionReposAnalyzeLocalReposGet());
      this._clearError();
    }
    else {
      this.updateState({error: {error: { $set: true}}});
      this.updateState({error: {text: { $set: response.result.msg[0]}}});
    }
  }
  
  onDataActionReposVerifyGit(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      this.updateState({gitConfigs: { $set: response.data}});
    }
  }
}


module.exports = ReposStore.export(ReposStore);
