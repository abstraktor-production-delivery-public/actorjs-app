
'use strict';

import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class PlusServicesStore extends StoreBaseData {
  constructor() {
    super({
      plusServices: [],
      plusServicesSubscriptions: []
    });
  }
  
  onActionPlusServicesGet(action) {
    
  }
}


module.exports = PlusServicesStore.export(PlusServicesStore);
