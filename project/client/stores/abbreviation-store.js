
'use strict';

import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class AbbreviationStore extends StoreBaseData {
  constructor() {
    super({
      markup: {
        definition: false,
        content: undefined,
        contentOriginal: undefined,
        rows: undefined
      } 
    });
  }
  
  onActionAbbreviationMarkup(action) {
    let markupTc = MarkupAbbreviation.stringify(this.state.testCase.tc);
    this.updateState({markup: {definition: {$set: true}}});
    this.updateState({markup: {content: {$set: markupTc}}});
    this.updateState({markup: {contentOriginal: {$set: markupTc}}});
  }
  
  onActionAbbreviationMarkupSave(action) {
    let result = MarkupAbbreviation.parse(this.state.markup.content);
    if(!result.success) {
      this.updateState({markup: { rows: {$set: result.rows}}});
    }
    else {
      this.updateState({testCase: { tc: {$set: result.tc}}});
    //  this.sendDataAction(new DataActionAbbreviationUpdate(action.sut, action.fut, action.tc, undefined, this.state.testCase));
      this.clearMarkup();
    }
  }
  
  onActionAbbreviationMarkupCancel(action) {
    this.clearMarkup();
  }
  
  onActionAbbreviationMarkupChange(action) {
    let result = MarkupAbbreviation.parse(action.markup);
    this.updateState({markup: { content: {$set: action.markup}}});
    this.updateState({markup: { rows: {$set: result.rows}}});
  }

  clearMarkup() {
    this.updateState({markup: { definition: {$set: false}}});
    this.updateState({markup: { content: {$set: undefined}}});
    this.updateState({markup: { contentOriginal: {$set: undefined}}});
    this.updateState({markup: { rows: {$set: undefined}}});
  }
}


module.exports = AbbreviationStore.export(AbbreviationStore);
