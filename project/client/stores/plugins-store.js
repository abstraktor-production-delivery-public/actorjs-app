
'use strict';

//import Os from 'os';
//import Zlib from 'zlib';
import { DataActionPluginsGet, DataActionPluginsBundleGet } from '../actions/action-plugins/data-action-plugins';
import StoreBaseData from 'z-abs-corelayer-client/client/store/store-base-data';


class PluginsStore extends StoreBaseData {
  constructor() {
    super({
      plugins: new Map(),
      loaded: false,
      error: null
    });
    this.sendDataAction(new DataActionPluginsGet());
  }
    
  onDataActionPluginsGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const plugins = new Map();
      response.data.forEach((plugin) => {
        plugins.set(plugin.guid, plugin);
      });
      this.updateState({plugins: {$set: plugins}});
      plugins.forEach((plugin) => {
        this.sendDataAction(new DataActionPluginsBundleGet(plugin.bundle, 'js', plugin.guid));
        this.sendDataAction(new DataActionPluginsBundleGet(plugin.cssBundle, 'css', plugin.guid));
      });
    }
    else {
      
    }
  }
  
  onDataActionPluginsBundleGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const f = (production, data, done) => {
        if(production) {
         // Zlib.gunzip(data, done);
        }
        else {
          done(null, data);
        }
      };
      f(response.data[0], response.data[1], (err, data) => {
        if('js' === action.type) {
          const script = document.createElement('script');
          script.type = 'text/javascript';
          script.text = data;
          document.body.appendChild(script);
          this.updateState({plugins: (plugins) => {
            const plugin = plugins.get(action.guid);
            const nextPlugin = this.deepCopy(plugin);
            nextPlugin.loaded = true;
            plugins.set(action.guid, nextPlugin);
          }});
          let loaded = true;
          this.state.plugins.forEach((plugin) => {
            loaded &= plugin.loaded;
          });
          if(loaded) {
            this.updateState({loaded: {$set: true}});
          }
        }
        else if('css' === action.type) {
          const css = document.createElement('style');
          css.type = 'text/css';
          if(css.styleSheet) {
            css.styleSheet.cssText = data;
          }
          else {
            css.appendChild(document.createTextNode(data));
          }
          document.getElementsByTagName("head")[0].appendChild(css);
        }
        else {
          ddb.error('unknown:', data);
        }
      });
    }
    else {
      ddb.error('error:', response);
    }
  }
}


module.exports = PluginsStore.export(PluginsStore);

