
'use strict';

import Header from './components/header/header';
import Middle from './components/middle/middle';
import Footer from './components/footer/footer';
import Router from 'z-abs-complayer-router-client/client/react-component/router';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';
import { createRoot } from 'react-dom/client';


class Actor extends ReactComponentBase {
  constructor(props) {
    super(props);
    Actor.routerRef = React.createRef();
    window.abstractorReleaseData = {
      appName: 'actorjs',
      appTitle: 'ActorJs'
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="main">
        <Router ref={Actor.routerRef}>
          <Header />
          <Middle />
          <Footer />
        </Router>
      </div>
    );
  }
}

async function abstractorRerender(uri) {
  return new Promise((resolve, reject) => {
    if(null !== Actor.routerRef.current) {
      Actor.routerRef.current.rerender(uri, () => {
        resolve();
      });
    }
    else {
      resolve();
    }
  });	
}

window.abstractorRerender = abstractorRerender;
Actor.routerRef = null;


const container = document.getElementById('app');
const root = createRoot(container);
root.render(<Actor />);

