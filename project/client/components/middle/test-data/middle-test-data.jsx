
'use strict';

import React from 'react';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import MiddleTestDataToolbar from './middle-test-data-toolbar';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import MiddleSidebar from '../middle_sidebar';
import MiddleTestDataView from './middle-test-data-view';


export default class MiddleTestData extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Test Data',
        prefix: 'TD: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle middle_view_test_data">
        <MiddleTestDataToolbar />
        <MiddleSidebar />
        <div className="middle_view middle_view_test_data">
          <Route switch="/:tab" handler={MiddleTestDataView} />
        </div>
      </div>
    );
  }
}
