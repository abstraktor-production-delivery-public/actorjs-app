
'use strict';

import TestDataStore from '../../../stores/test-data-store';
import { ActionTestDataMarkupChange } from '../../../actions/action-test-data/action-test-data';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataTestDataGeneralGlobal from 'z-abs-complayer-markup-client/client/data/data-test-data/data-test-data-general-global';
import DataTestDataGeneralLocal from 'z-abs-complayer-markup-client/client/data/data-test-data/data-test-data-general-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestDataViewGeneral extends ReactComponentStore {
  constructor(props) {
    super(props, [TestDataStore]);
    this.dataTestDataGeneralGlobal = new DataTestDataGeneralGlobal();
    this.dataTestDataGeneralLocal = new DataTestDataGeneralLocal();
  }
  
  shouldUpdate(nextProps, nextState) {
    return 'general' === nextState.TestDataStore.currentView
      && (
        !this.shallowCompare(this.state.TestDataStore.markup, nextState.TestDataStore.markup)
        || !this.deepCompare(this.state.TestDataStore.testDataGeneralLocals, nextState.TestDataStore.testDataGeneralLocals)
        || !this.deepCompare(this.state.TestDataStore.testDataGeneralGlobals, nextState.TestDataStore.testDataGeneralGlobals)
      );
  }

  renderMarkup() {
    return (
      <div className="test_data_form_group">
        <label htmlFor="test_data_general_markup_textarea">Test Data - Markup</label>
        <ComponentMarkedTextarea id="test_data_general_markup_textarea" className="form-control test_data_definition same_size_as_parent" rows="10" value={this.state.TestDataStore.markup.content} results={this.state.TestDataStore.markup.rows}
          onChange={(value) => {
            this.dispatch(TestDataStore, new ActionTestDataMarkupChange(value));
          }}
        />
      </div>
    );
  }


  render() {
    if(this.state.TestDataStore.markup.definition) {
      return (
        <div className="test_data_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="test_data_tab_view">
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataTestDataGeneralLocal} values={this.state.TestDataStore.testDataGeneralLocals}/>
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataTestDataGeneralGlobal} values={this.state.TestDataStore.testDataGeneralGlobals}/>
        </div>
      );
    }
  }
}
