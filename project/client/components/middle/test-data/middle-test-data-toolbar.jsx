
'use strict';


import React from 'react';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import TestDataStore from '../../../stores/test-data-store';
import { ActionTestDataMarkup, ActionTestDataMarkupSave, ActionTestDataMarkupCancel } from '../../../actions/action-test-data/action-test-data';


export default class MiddleTestDataToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [TestDataStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }

  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.TestDataStore.markup, nextState.TestDataStore.markup);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(TestDataStore, new ActionTestDataMarkup());
  }
  
  _markupSave() {
    this.dispatch(TestDataStore, new ActionTestDataMarkupSave());
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    this.dispatch(TestDataStore, new ActionTestDataMarkupCancel());
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this.state.TestDataStore.markup.definition;
    return (
      <Button placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = this.state.TestDataStore.markup.content === this.state.TestDataStore.markup.contentOriginal;
    return (
      <Button placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.TestDataStore.markup.definition;
    return (
      <Button placement="bottom" heading="Help" content="Markup"shortcut="Ctrl+?"  disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this.state.TestDataStore.markup.definition;
    return (
      <Button placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
          </div>
        </div>
      </div>
    );
  }
}
