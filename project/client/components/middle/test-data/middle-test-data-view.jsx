
'use strict';


import MiddleTestDataViewEnvironment from './middle-test-data-view-environment';
import MiddleTestDataViewGeneral from './middle-test-data-view-general';
import MiddleTestDataViewOutput from './middle-test-data-view-output';
import MiddleTestDataViewSystem from './middle-test-data-view-system';
import TestDataStore from '../../../stores/test-data-store';
import { ActionTestDataEnvironmentGet, ActionTestDataGeneralGet, ActionTestDataOutputGet, ActionTestDataSystemGet, ActionTestDataMarkupCancel } from '../../../actions/action-test-data/action-test-data';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestDataViewTestData extends ReactComponentStore {
  constructor(props) {
    super(props, [TestDataStore], {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['General', 'System', 'Output', 'Environment']);
    this.state.activeKey = this.helperTab.getIndex(props.tab);
  }
  
  didMount() {
    this._getData(this.state.activeKey);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
      || !this.shallowCompare(this.state.TestDataStore.markup.definition, nextState.TestDataStore.markup.definition);
  }
  
  didUpdate(prevProps) {
    const nextKey = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== nextKey) {
      this.updateState({activeKey: {$set: nextKey}});
      this._getData(nextKey);
    }
  }
  
  render() {
    let index = 0;
    return (
      <div className="same_size_as_parent">
        <div className="test_data_tabs">
          <Tabs id="test_data_tabs" className="test_data_tabs_view" activeKey={this.state.activeKey}
            onSelect={(key) => {
              if(undefined !== key && this.state.activeKey !== key) {
                if(this.state.TestDataStore.markup.definition) {
                  // TODO: Ask if we really want to leve markup.
                  this.dispatch(TestDataStore, new ActionTestDataMarkupCancel());
                }
                this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
              }
            }}
          >
            <Tab id="global_test_data_general" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleTestDataViewGeneral />
            </Tab>
            <Tab id="global_test_data_system" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleTestDataViewSystem />
            </Tab>           
            <Tab id="global_test_data_output" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleTestDataViewOutput />
            </Tab>
            <Tab id="global_test_data_environment" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleTestDataViewEnvironment />
            </Tab>           
          </Tabs>
        </div>
      </div>
    );
  }
  
  _getData(key) {
    const tabName = this.helperTab.getTabName(key);
    if('Environment' === tabName) {
      this.dispatch(TestDataStore, new ActionTestDataEnvironmentGet());
    }
    else if('General' === tabName) {
      this.dispatch(TestDataStore, new ActionTestDataGeneralGet());
    }
    else if('Output' === tabName) {
      this.dispatch(TestDataStore, new ActionTestDataOutputGet());
    }
    else if('System' === tabName) {
      this.dispatch(TestDataStore, new ActionTestDataSystemGet());
    }
  }
}


MiddleTestDataViewTestData.contextType = RouterContext;
