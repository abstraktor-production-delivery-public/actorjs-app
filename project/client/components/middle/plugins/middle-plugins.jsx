
'use strict';

import PluginsStore from '../../../stores/plugins-store';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';
let Plugin = null;

export default class MiddlePlugins extends ReactComponentStore {
  constructor(props) {
    super(props, [PluginsStore]);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Plugins'
      };
    });
    this.chosenPlugin = null;
    this.chosenPluginPath = null;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath)
      || !this.shallowCompareMapValues(this.state.PluginsStore.plugins, nextState.PluginsStore.plugins)
      || !this.shallowCompare(this.state.PluginsStore.loaded, nextState.PluginsStore.loaded)
      || !this.shallowCompare(this.state.PluginsStore.error, nextState.PluginsStore.error);
  }
  
  renderPlugin() {
    let chosenPlugin = null;
    this.state.PluginsStore.plugins.forEach((plugin) => {
      if(this.props._uriPath.startsWith(plugin.path)) {
        chosenPlugin = plugin;
      }
    });
    if(!chosenPlugin) {
      return null;
    }
    const pluginPath = `${chosenPlugin.pluginName}-client/components/plugin-view`;
    if(this.chosenPluginPath !== pluginPath) {
      this.chosenPluginPath = pluginPath;
      this.chosenPlugin = require(pluginPath).default;
    }
    return (
      <Route switch="*" handler={ this.chosenPlugin} />
    );
  }
  
  render() {
    const pluginsStore = this.state.PluginsStore;
    if(!pluginsStore.plugins.size || !pluginsStore.loaded) {
      return null;
    }
    else {
      return (
        <div className="middle middle_plugins">
          {this.renderPlugin()}
        </div>
      );
    }
  }
}

MiddlePlugins.contextType = RouterContext;
