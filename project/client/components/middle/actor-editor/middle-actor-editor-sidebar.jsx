
'use strict';

import ActorEditorStore from '../../../stores/actor-editor-store';
import { ActionActorEditorFileGet } from '../../../actions/action-actor-editor/action-actor-editor-file';
import { ActionActorEditorFolderGet } from '../../../actions/action-actor-editor/action-actor-editor-folder';
import { ActionActorEditorProjectToggle } from '../../../actions/action-actor-editor/action-actor-editor-project';
import FileIcons from 'z-abs-complayer-bootstrap-client/client/icons/file-icons';
import FolderIcons from 'z-abs-complayer-bootstrap-client/client/icons/folder-icons';
import Tree from 'z-abs-complayer-tree-client/client/react-components/tree';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleActorEditorSidebar extends ReactComponentStore {
  constructor(props) {
    super(props, [ActorEditorStore]);
    this.boundMouseUp = this._boundMouseUp.bind(this);
    this.boundMouseMove = this._boundMouseMove.bind(this);
    this.sidebarNode = null;
    this.sidebarWidthMin = 180;
    this.sidebarWidthMax = 720;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.ActorEditorStore.projectLocal, nextState.ActorEditorStore.projectLocal)
      || !this.shallowCompare(this.state.ActorEditorStore.projectGlobal, nextState.ActorEditorStore.projectGlobal)
      || !this.shallowCompare(this.state.ActorEditorStore.current, nextState.ActorEditorStore.current)
      || !this.shallowCompare(this.state.ActorEditorStore.persistentData, nextState.ActorEditorStore.persistentData);
  }
  
  _boundMouseUp(e) {
    window.removeEventListener('mouseup', this.boundMouseUp, true);
    window.removeEventListener('mousemove', this.boundMouseMove, true);
    this.sidebarNode = null;
  }
  
  _boundMouseMove(e) {
    let width = e.clientX + 2;
    if(width < this.sidebarWidthMin) {
      width = this.sidebarWidthMin;
    }
    else if(width > this.sidebarWidthMax) {
      width = this.sidebarWidthMax;
    }
    this.sidebarNode.style.width = `${width}px`;
    this.sidebarNode.nextSibling.style.left = `${width}px`;
  }
  
  render() {
    const stylePanel = {
      height:'100%'
    };
    const darkMode = this.state.ActorEditorStore.persistentData.darkMode;
    return (
      <div className={this.theme(darkMode, "sidebar", "middle_actor_editor_sidebar")}>
        <div className={this.theme(darkMode, "panel-default", "panel")} style={stylePanel}>
          <div className={this.theme(darkMode, "panel_body_sidebar")}>
            <div id="actor_editor_tree_root">
              <Tree key={this.state.ActorEditorStore.projectLocal.projectId} name={'local'} id="actor_editor_tree_code_local" project={this.state.ActorEditorStore.projectLocal} current={this.state.ActorEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.ActorEditorStore.persistentData.darkMode}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorFileGet(this.state.ActorEditorStore.projectLocal.projectId, key, title, path, type));
                }}
                onClickFolder={(key, title, data) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorFolderGet(this.state.ActorEditorStore.projectLocal.projectId, key, title, data));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorProjectToggle(this.state.ActorEditorStore.projectLocal.projectId, key, expanded));
                }}
              />
              <Tree key={this.state.ActorEditorStore.projectGlobal.projectId} name={'global'} id="actor_editor_tree_code_global" project={this.state.ActorEditorStore.projectGlobal} current={this.state.ActorEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.ActorEditorStore.persistentData.darkMode}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorFileGet(this.state.ActorEditorStore.projectGlobal.projectId, key, title, path, type));
                }}
                onClickFolder={(key, title, data) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorFolderGet(this.state.ActorEditorStore.projectGlobal.projectId, key, title, data));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(ActorEditorStore, new ActionActorEditorProjectToggle(this.state.ActorEditorStore.projectGlobal.projectId, key, expanded));
                }}
              />
            </div>
          </div>
        </div>
        <div className={this.theme(darkMode, "sidebar_mover")}
          onMouseDown={(e) => {
            if(0 === e.button) {
              this.sidebarNode = e.target.parentNode;
              window.addEventListener('mouseup', this.boundMouseUp, true);
              window.addEventListener('mousemove', this.boundMouseMove, true);
            }
          }}
        ></div>
      </div>
    );
  }
}


MiddleActorEditorSidebar.contextType = RouterContext;
