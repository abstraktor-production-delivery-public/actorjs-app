
'use strict';
 
import ActorEditorStore from '../../../stores/actor-editor-store';
import { ActionActorEditorFileGet, ActionActorEditorFileSet, ActionActorEditorFileEdit, ActionActorEditorFileClose, ActionActorEditorFileMove } from '../../../actions/action-actor-editor/action-actor-editor-file';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/client/code-mirror-editor';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import LogType from 'z-abs-funclayer-engine-cs/clientServer/log/log-type';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleActorEditorView extends ReactComponentStore {
  constructor(props) {
    super(props, [ActorEditorStore]);
    this.close = false;
  }
  
  didMount() {
    if(this.props._uriPath && 0 < this.props._uriPath.length) {
      this.dispatch(ActorEditorStore, new ActionActorEditorFileSet(`.${this.props._uriPath}`, this.props.location.search));
    }
    else {
      this.dispatch(ActorEditorStore, new ActionActorEditorFileSet());
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath)
      || !this.shallowCompare(this.props.location.search, nextProps.location.search)
      || !this.shallowCompareMapValues(this.state.ActorEditorStore.files, nextState.ActorEditorStore.files)
      || !this.shallowCompare(this.state.ActorEditorStore.current, nextState.ActorEditorStore.current)
      || !this.shallowCompare(this.state.ActorEditorStore.persistentData, nextState.ActorEditorStore.persistentData)
      || !this.shallowCompare(this.state.ActorEditorStore.build, nextState.ActorEditorStore.build);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props._uriPath !== prevProps._uriPath) {
      this.dispatch(ActorEditorStore, new ActionActorEditorFileSet(`.${this.props._uriPath}`, this.props.location.search));
    }
  }
  
  renderTab(options) {
    const result = [];
    const queryState = this.state.ActorEditorStore.current.query;  
    const query = !queryState ? undefined : {
      url: queryState.url,
      line: queryState.line,
      typeCss: LogType.csses[Number.parseInt(this.state.ActorEditorStore.current.query.type)]
    };
    this.state.ActorEditorStore.files.forEach((file, key) => {
      if(null !== file.content) {
        const currentType = 'file_inline' === this.state.ActorEditorStore.current.type ? 'file' : this.state.ActorEditorStore.current.type;
        result.push(
          <Tab eventKey={key} key={key} id={key} 
            title={
              <div>
                {file.title}
                <button type="button" className={this.theme(this.state.ActorEditorStore.persistentData.darkMode, 'middle_code_editor_close')} aria-label="Close"
                  onClick={(e, a) => {
                    this.close = true;
                    this.dispatch(ActorEditorStore, new ActionActorEditorFileClose(key));
                  }}
                >
                  ×
                </button>
              </div>
            }
            onMove={(fromKey, toKey) => {
              this.dispatch(ActorEditorStore, new ActionActorEditorFileMove(fromKey, toKey));
            }}
          >
            <CodeMirrorEditor id={`actor_editor_view_${key}`} projectId={file.projectId} docKey={key} code={file.content} build={this.state.ActorEditorStore.build} options={this.getOptions(file.type, key)} currentType={currentType} currentFile={this.state.ActorEditorStore.current.file} currentQuery={query} darkMode={this.state.ActorEditorStore.persistentData.darkMode}
              onChanged={(projectId, docKey, code) => {
                this.dispatch(ActorEditorStore, new ActionActorEditorFileEdit(projectId, docKey, code));
              }}
              onFocusChange={(focused, docKey) => {
                if(focused) {
                  const file = this.state.ActorEditorStore.files.get(docKey);
                  if(file) {
                    this.dispatch(ActorEditorStore, new ActionActorEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
                  }
                }
              }}
            />
          </Tab>
        );
      }
    });
    return result;
  }

  renderTabs(options) {
    if(0 !== this.state.ActorEditorStore.files.size) {
      return (
        <Tabs id="actor_editor_tabs" draggable="true" activeKey={null !== this.state.ActorEditorStore.current.file ? this.state.ActorEditorStore.current.file.key : null} darkMode={this.state.ActorEditorStore.persistentData.darkMode}
          onSelect={(selectedKey) => {
            if(!this.close) {
              const file = this.state.ActorEditorStore.files.get(selectedKey);
              this.dispatch(ActorEditorStore, new ActionActorEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
            }
            else {
              this.close = false;
            }
          }}
        >
          {this.renderTab(options)}
        </Tabs>
      );
    }
    else {
      return null;
    }
  }

  getOptions(type, key) {
    return {
      lineNumbers: true,
      highlightActiveLineGutter: true,
      highlightActiveLine: true,
      closeBrackets: true,
      bracketMatching: true,
      drawSelection: true,
      history: true,
      autocompletion: true,

      tabKeyAction: 'indent', 
      tabSize: 2,
      indentType: 'spaces',
      indentUnit: 2,

      darkMode: true,
      breakpointGutter: false,
      foldGutter: true,
      emptyLine: true,

      buildGutter: true,
      
      type
    };
  }
  
  render() {
    const darkMode = this.state.ActorEditorStore.persistentData.darkMode;
    return (
      <div className={this.theme(darkMode, "view")}>
        {this.renderTabs()}
      </div>
    );
  }
}


MiddleActorEditorView.contextType = RouterContext;
MiddleActorEditorView.REMOVE_KEY_LENGTH = 'actor_editor_tabs-tab-'.length;
