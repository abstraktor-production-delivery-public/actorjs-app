
'use strict';

import ActorEditorStore from '../../../stores/actor-editor-store';
import MiddleActorEditorToolbar from './middle-actor-editor-toolbar';
import MiddleActorEditorSidebar from './middle-actor-editor-sidebar';
import MiddleActorEditorView from './middle-actor-editor-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleActorEditor extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Actors',
        prefix: 'AE: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle middle_code_editor">
        <Route switch="*" handler={MiddleActorEditorToolbar} />
        <Route switch="*" handler={MiddleActorEditorSidebar} />
        <Route switch="*" handler={MiddleActorEditorView} />
      </div>
    );
  }
}
