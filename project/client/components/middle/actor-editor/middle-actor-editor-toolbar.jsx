
'use strict';

import ActorEditorStore from '../../../stores/actor-editor-store';
import { ActionActorEditorPersistentDataUpdate } from '../../../actions/action-actor-editor/action-actor-editor-persistent-data';
import { ActionActorEditorProjectUpdate } from '../../../actions/action-actor-editor/action-actor-editor-project';
import { ActionActorEditorFolderNew, ActionActorEditorFolderAdd, ActionActorEditorFolderUpdate, ActionActorEditorFolderRemove, ActionActorEditorFolderDelete } from '../../../actions/action-actor-editor/action-actor-editor-folder';
import { ActionActorEditorFileNew, ActionActorEditorFileAdd, ActionActorEditorFileRename, ActionActorEditorFileUpdate, ActionActorEditorFileUpdateAll, ActionActorEditorFileRemove, ActionActorEditorFileDelete, ActionActorEditorBuild } from '../../../actions/action-actor-editor/action-actor-editor-file';
import ModalDialogStore from 'z-abs-complayer-modaldialog-client/client/stores/modal-dialog-store';
import { ActionModalDialogShow, ActionModalDialogHide, ActionModalDialogPending, ActionModalDialogResult } from 'z-abs-complayer-modaldialog-client/client/actions/action-modal-dialog';
import ActorImage from 'z-abs-complayer-modaldialog-client/client/images/actor-image';
import FileImage from 'z-abs-complayer-modaldialog-client/client/images/file-image';
import MsgImage from 'z-abs-complayer-modaldialog-client/client/images/msg-image';
import ModalDialogFolderAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-add';
import ModalDialogFolderNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-new';
import ModalDialogFolderProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-properties';
import ModalDialogFolderRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-remove';
import ModalDialogFileAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-add';
import ModalDialogFileNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-new';
import ModalDialogFileProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-properties';
import ModalDialogFileRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-remove';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import { ActionDialogFileGet, ActionDialogFileSet } from 'z-abs-corelayer-client/client/actions/action-dialog-file';
import DialogFileStore from 'z-abs-corelayer-client/client/stores/dialog-file-store';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleActorEditorToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [ActorEditorStore, ModalDialogStore, DialogFileStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.buttonFileSaveDisabled = true;
    this.buttonFileSaveAllDisabled = true;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.ActorEditorStore.projectLocal, nextState.ActorEditorStore.projectLocal)
      || !this.shallowCompare(this.state.ActorEditorStore.projectGlobal, nextState.ActorEditorStore.projectGlobal)
      || !this.shallowCompare(this.state.ActorEditorStore.current, nextState.ActorEditorStore.current)
      || !this.shallowCompareMapValues(this.state.ActorEditorStore.files, nextState.ActorEditorStore.files)
      || !this.shallowCompare(this.state.ActorEditorStore.persistentData, nextState.ActorEditorStore.persistentData)
      || !this.shallowCompare(this.state.ActorEditorStore.build, nextState.ActorEditorStore.build)
      || !this.shallowCompareMapValues(this.state.ActorEditorStore.stacks, nextState.ActorEditorStore.stacks)
      || !this.shallowCompareMapValues(this.state.ActorEditorStore.stackTemplates, nextState.ActorEditorStore.stackTemplates)
      || !this.shallowCompareMapValues(this.state.ModalDialogStore.modalResults, nextState.ModalDialogStore.modalResults)
      || !this.shallowCompare(this.state.DialogFileStore.project, nextState.DialogFileStore.project)
      || !this.shallowCompare(this.state.DialogFileStore.current, nextState.DialogFileStore.current)
      || !this.shallowCompare(this.state.DialogFileStore.original, nextState.DialogFileStore.original);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _fileSave() {
    this.dispatch(ActorEditorStore, new ActionActorEditorFileUpdate(this.state.ActorEditorStore.current.file.key));
  }
  
  _fileSaveAll() {
    const keys = [];
    this.state.ActorEditorStore.files.forEach((file) => { 
      if(file.codeChanged) {
        keys.push(file.key);
      }
    });
    this.dispatch(ActorEditorStore, new ActionActorEditorFileUpdateAll(keys));
  }
  
  _keyDown(e) {
    if(e.ctrlKey && e.shiftKey && 'S' === e.key) {
      if(!this.buttonFileSaveAllDisabled) {
        e.preventDefault();
        this._fileSaveAll();
      }
    }
    if(e.ctrlKey && 's' === e.key) {
      if(!this.buttonFileSaveDisabled) {
        e.preventDefault();
        this._fileSave();
      }
    }
  }
  
  renderButtonSaveProject() {
    const disabled = this.state.ActorEditorStore.projectGlobal.isSaved() && this.state.ActorEditorStore.projectLocal.isSaved();
    return (
      <Button id="actor_save_project" placement="bottom" heading="Save" content="Project" disabled={disabled}
        onClick={(e) => {
          if(!this.state.ActorEditorStore.projectGlobal.isSaved()) {
            this.dispatch(ActorEditorStore, new ActionActorEditorProjectUpdate(this.state.ActorEditorStore.projectGlobal.projectId));
          }
          if(!this.state.ActorEditorStore.projectLocal.isSaved()) {
            this.dispatch(ActorEditorStore, new ActionActorEditorProjectUpdate(this.state.ActorEditorStore.projectLocal.projectId));
          }
        }}
      >
        <span className="glyphicon glyphicon_project glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_project glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderNew() {
    return (
      <Button id="actor_folder_new" placement="bottom" heading="New" content="Folder" disabled={'folder' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_Folder_New'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFolderAdd() {
    return (
      <Button id="actor_folder_add" placement="bottom" heading="Add" content="Folder" disabled={'folder' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          const folder = this.state.ActorEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = ActorEditorStore.getProject(folder.projectId);
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'folder'));
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_Folder_Add'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderProperties() {
    return (
      <Button id="actor_folder_properties" placement="bottom" heading="Properties" content="Folder" disabled={'folder' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_Folder_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderDelete() {
    const disabled = 'folder' !== this.state.ActorEditorStore.current.type || !(this.state.ActorEditorStore.projectGlobal.isSaved() && this.state.ActorEditorStore.projectLocal.isSaved());
    return (
      <Button id="actor_folder_delete" placement="bottom" heading="Remove or Delete" content="Folder" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_Folder_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileNew() {
    return (
      <Button id="actor_file_new" placement="bottom" heading="New" content="File" disabled={'folder' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_File_New'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFileAdd() {
    return (
      <Button id="actor_file_add" placement="bottom" heading="Add" content="File" disabled={'folder' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          const folder = this.state.ActorEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = ActorEditorStore.getProject(folder.projectId);
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'file'));
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_File_Add'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSave() {
    this.buttonFileSaveDisabled = ('file' !== this.state.ActorEditorStore.current.type && 'file_inline' !== this.state.ActorEditorStore.current.type) || !this.state.ActorEditorStore.current.file.codeChanged;
    return (
      <Button id="actor_file_save" placement="bottom" heading="Save" content="File" shortcut="Ctrl+S" disabled={this.buttonFileSaveDisabled }
        onClick={(e) => {
           this._fileSave();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSaveAll() {
    this.buttonFileSaveAllDisabled = true;
    this.state.ActorEditorStore.files.forEach((file) => { 
      this.buttonFileSaveAllDisabled = this.buttonFileSaveAllDisabled && !file.codeChanged;
    });
    return (
      <Button id="actor_file_save_all" placement="bottom" heading="Save" content="All Files" shortcut="Ctrl+Shift+S" disabled={this.buttonFileSaveAllDisabled}
        onClick={(e) => {
          this._fileSaveAll();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: -1, left: -4, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: 3, left: -10, width: 0, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileProperties() {
    return (
      <Button id="actor_file_properties" placement="bottom" heading="Properties" content="File" disabled={'file' !== this.state.ActorEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_File_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileDelete() {
   const disabled = ('file' !== this.state.ActorEditorStore.current.type && 'invalid_file' !== this.state.ActorEditorStore.current.type) || !(this.state.ActorEditorStore.projectGlobal.isSaved() && this.state.ActorEditorStore.projectLocal.isSaved());
   return (
      <Button id="actor_file_delete" placement="bottom" heading="Remove or Delete" content="File" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Actor_File_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderBuildButton() {
    const result = this.state.ActorEditorStore.build.result;
    const success = this.state.ActorEditorStore.build.success;
    let className;
    if(result) {
      if(success) {
        className = "actor_editor_success";
      }
      else {
        className = "actor_editor_error";
      }
    }
    const disabled = 'file' !== this.state.ActorEditorStore.current.type && 'file_inline' !== this.state.ActorEditorStore.current.type;
    return (
      <Button id="actor_file_check" className={className} placement="bottom" heading="Check" content="Code" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ActorEditorStore, new ActionActorEditorBuild());
        }}
      >
        <span className="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderModalDialogFolderNew() {
    const name = 'Actor_Folder_New';
    return (
      <ModalDialogFolderNew name={name} folder={this.state.ActorEditorStore.current.folder} heading="New Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderNew={(projectId, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFolderNew(projectId, title, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderAdd() {
    const name = 'Actor_Folder_Add';
    const folder = this.state.ActorEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFolderAdd name={name} heading="Add Folder" folder={this.state.ActorEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFolderAdd(projectId, title, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderProperties() {
    const name = 'Actor_Folder_Properties';
    return (
      <ModalDialogFolderProperties name={name} folder={this.state.ActorEditorStore.current.folder} heading="Folder Properties" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderProperties={(projectId, title, newTitle, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFolderUpdate(projectId, title, newTitle, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderRemove() {
    const name = 'Actor_Folder_Remove';
    return (
      <ModalDialogFolderRemove name={name} folder={this.state.ActorEditorStore.current.folder} heading="Remove or Delete Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderRemove={(projectId, title, key, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFolderRemove(projectId, title, key, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFolderDelete={(projectId, title, key, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFolderDelete(projectId, title, key, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileNew() {
    const name = 'Actor_File_New';
    const heading = 'Stack';
    return (
      <ModalDialogFileNew name={name} heading="New Actor File" templateNames={MiddleActorEditorToolbar.TEMPLATE_NAMES} folder={this.state.ActorEditorStore.current.folder} modalResults={this.state.ModalDialogStore.modalResults}
        onRenderIcon={(template, templateName) => {
          if('actor' === template.icon) {
            return (<ActorImage template={template} stackName={templateName ? templateName : 'unknown'} />);
          }
          else if('msg' === template.icon) {
            return (<MsgImage id="msg_image_icon" />);
          }
          else if('class' === template.icon) {
            return (<FileImage id="file_image_icon" />);
          }
          else if('none' === template.icon) {
            return (<FileImage id="empty_file_image_icon" />);
          }
        }}
        onFileNew={(projectId, path, title, type, templateName, dynamicTemplateName, dynamicTemplateName2) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFileNew(projectId, path, title, type, templateName, dynamicTemplateName, dynamicTemplateName2), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onTemplate={(templateName) => {
          if(this.state.ActorEditorStore.stacks.has(templateName)) {
            const dynamicTemplates = this.state.ActorEditorStore.stacks.get(templateName);
            return {
              names: dynamicTemplates ? dynamicTemplates : [],
              heading: heading
            }
          }
        }}
        onDynamicTemplate={(templateName, dynamicTemplateName) => {
          const templateMap = this.state.ActorEditorStore.stackTemplates.get(dynamicTemplateName);
          if(templateMap) {
            const templateData = templateMap.get(templateName);
            return {
              heading: 'Template',
              names: templateData.names,
              markupNodes: templateData.markupNodes,
              markupStyles: templateData.markupStyles,
              markups: templateData.markups
            };
          }
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileAdd() {
    const name = 'Actor_File_Add';
    const folder = this.state.ActorEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFileAdd name={name} heading="Add Actor File" folder={this.state.ActorEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, path, type) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFileAdd(projectId, title, path, type));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }  
  
  renderModalDialogFileProperties() {
    const name = 'Actor_File_Properties';
    return (
      <ModalDialogFileProperties name={name} heading="File Properties" file={this.state.ActorEditorStore.current.file} modalResults={this.state.ModalDialogStore.modalResults}
        onFileProperties={(projectId, path, title, newTitle) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFileRename(projectId, path, title, newTitle), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileRemove() {
    const name = 'Actor_File_Remove';
    return (
      <ModalDialogFileRemove name={name} file={this.state.ActorEditorStore.current.file} doDelete={'invalid_file' !== this.state.ActorEditorStore.current.type} heading="Remove or Delete Actor File" modalResults={this.state.ModalDialogStore.modalResults}
        onFileRemove={(projectId, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFileRemove(projectId, key));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFileDelete={(projectId, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(ActorEditorStore, new ActionActorEditorFileDelete(projectId, key), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  _getActorFullName() {
    const file = this.state.ActorEditorStore.current.file;
    return file.path.replace(/(.\/)/, '').replace(new RegExp('[//]', 'g'), '.') + '.' + file.title.replace(/(.\js)/, '');
  }
  
  renderMarkupName() {
    return (
      <>
        <div className="editor_markup_name">Markup name: {this._getActorFullName()}</div>
      </>
    );
  }
  
  renderCopyButton() {
    return (
      <Button placement="bottom" heading="Copy" content="Markup name" shortcut="Ctrl+C"
        onClick={(e) => {
          navigator.clipboard.writeText(this._getActorFullName()).then(() => {
            /* clipboard successfully set */
          }, () => {
            /* clipboard write failed */
          });
        }}
      >
        <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupInfo() {
    if('file' === this.state.ActorEditorStore.current.type) {
      return (
        <>
          {this.renderCopyButton()}
          {this.renderMarkupName()}
        </>
      );
    }
  }

  renderButtonDarkMode() {
    const darkMode = this.state.ActorEditorStore.persistentData.darkMode;
    return (
      <Button active={darkMode} placement="bottom" heading="View" content="Dark mode"
        onClick={(e) => {
          this.dispatch(ActorEditorStore, new ActionActorEditorPersistentDataUpdate('', 'darkMode', !darkMode));
        }}
      >
        <span className="glyphicon glyphicon-certificate" aria-hidden="true"></span>
      </Button>
    );
  }

  renderButtonEmptyLine() {
    const emptyLine = this.state.ActorEditorStore.persistentData.emptyLine;
    return (
      <Button active={emptyLine} placement="bottom" heading="View" content="Empty Line"
        onClick={(e) => {
          this.dispatch(ActorEditorStore, new ActionActorEditorPersistentDataUpdate('', 'emptyLine', !emptyLine));
        }}
      >
        _
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_code_editor_toolbar">
        {this.renderModalDialogFolderNew()}
        {this.renderModalDialogFolderAdd()}
        {this.renderModalDialogFolderProperties()}
        {this.renderModalDialogFolderRemove()}
        {this.renderModalDialogFileNew()}
        {this.renderModalDialogFileAdd()}
        {this.renderModalDialogFileProperties()}
        {this.renderModalDialogFileRemove()}
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-groupp btn-groupp_sm" role="group" aria-label="...">
            {this.renderButtonSaveProject()}
          </div>
          <div className="btn-groupp btn-groupp_sm" role="group" aria-label="...">
            {this.renderButtonFolderNew()}
            {this.renderButtonFolderAdd()}
            {this.renderButtonFolderProperties()}
            {this.renderButtonFolderDelete()}
          </div>
          <div className="btn-groupp btn-groupp_sm" role="group" aria-label="...">
            {this.renderButtonFileNew()}
            {this.renderButtonFileAdd()}
            {this.renderButtonFileSave()}
            {this.renderButtonFileSaveAll()}
            {this.renderButtonFileProperties()}
            {this.renderButtonFileDelete()}
          </div>
          {/*<div className="btn-groupp btn-groupp_sm" role="group" aria-label="...">
            {this.renderBuildButton()}
          </div>*/}
          <div className="btn-groupp btn-groupp_sm" role="group" aria-label="..." style={{marginLeft:'4px',display:"inline-block"}}>
            {this.renderMarkupInfo()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderButtonEmptyLine()}
            {this.renderButtonDarkMode()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleActorEditorToolbar.TEMPLATE_NAMES = [
  {name: 'Originating', type:'js', icon: 'actor', properties:{face:'orig',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Orig Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'Terminating', type:'js', icon: 'actor', properties:{face:'term',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Term Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'Intercepting', type:'js', icon: 'actor', properties:{face:'term',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Inter Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'Proxy', type:'js', icon: 'actor', properties:{face:'proxy',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Proxy Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px',
    width:'106%'
  }},
  {name: 'Condition', type:'js', icon: 'actor', properties:{face:'cond',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Cond Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'Local', type:'js', icon: 'actor', properties:{face:'local',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Local Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px',
    width:'106%'
  }},
  {name: 'Sut', type:'js', icon: 'actor', properties:{face:'sut',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty Sut Actor***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'ActorPartPre', type:'js', icon: 'actor', properties:{face:'proxy_left',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty ActorPartPre***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'ActorPartPost', type:'js', icon: 'actor', properties:{face:'proxy_right',eyes:'happy',mouth:'happy'}, markup: `#### ***Empty ActorPartPost***
  
***Make your own implementation.***`, markupStyle: {
    padding:'8px'
  }},
  {name: 'Msg', type:'js',  icon: 'msg', properties:{}, markup: '', markupStyle: {}},
  {name: 'Class [class js file]',  icon: 'class', properties:{}, markup: `\`\`\`javascript
class Foo {
  constructor() {
    
  }
}
\`\`\``, markupStyle: {}},
  {name: 'None [empty js file]', type:'js', icon: 'none', properties:{}, markup: `\`\`\`javascript



\`\`\``, markupStyle: {}}
];

