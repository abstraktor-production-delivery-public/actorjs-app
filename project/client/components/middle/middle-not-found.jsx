
'use strict';

import React from 'react';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';


export default class MiddleNotFound extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div >
        NOT FOUND
      </div>
    );	
  }
}
