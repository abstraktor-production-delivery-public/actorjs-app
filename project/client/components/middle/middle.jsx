
'use strict';

import MiddleAbbreviation from './abbreviation/middle-abbreviation';
import MiddleActorEditor from './actor-editor/middle-actor-editor';
import MiddleAddresses from './addresses/middle-addresses';
import MiddleCodeEditor from 'z-abs-complayer-codeeditor-client/client/components/middle-code-editor';
import MiddleContent from './content/middle-content';
import MiddleSidebar from './middle_sidebar';
import MiddleDocumentation from 'z-abs-complayer-documentation-client/client/react-components/documentation/middle-documentation';
import MiddleStaging from './staging/middle-staging';
import MiddlePlugins from './plugins/middle-plugins';
import MiddlePlusServices from './plus-services/middle-plus-services';
import MiddleRepos from './repos/middle-repos';
import MiddleStackEditor from './stack-editor/middle-stack-editor';
import MiddleStart from './start/middle-start';
import MiddleSupport from './support/middle-support';
import MiddleSystemUnderTest from './system-under-test/middle-system-under-test';
import MiddleTestCases from './test-cases/middle-test-cases';
import MiddleTestData from './test-data/middle-test-data';
import MiddleTestSuites from './test-suites/middle-test-suites';
import MiddleUser from './user/middle-user';
import MiddleVideos from './videos/middle-videos';
import DocumentationStore from '../../stores/documentation-store';
import EducationStore from '../../stores/education-store';
import WorkshopStore from '../../stores/workshop-store';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';

 
export default class Middle extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.serviceExistsDocs = false;
  }
    
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderRouteServiceDocs() {
    if(this.serviceExists('z-abs-servicelayer-docs-client')) {
      const DocsStore = require('z-abs-servicelayer-docs-client/client/stores/docs-store');
      const repoName = 'Local';
      const docsStore = new DocsStore();
      return (
        <Route switch={`/local-documentation-${repoName}*`} documentationStore={docsStore} sidebar={MiddleSidebar} titleText={`Docs ${repoName}`} titlePrefix="DL" handler={MiddleDocumentation} />
      );
    } 
  }
  
  renderRouteServices() {
    return (
      <>
        {this.renderRouteServiceDocs()}
      </>
    );
  }
  
  render() {
    return (
      <div className="middle_outer">
        <Route switch="/abbreviation*" handler={MiddleAbbreviation} />
        <Route switch="/actor-editor*" handler={MiddleActorEditor} />
        <Route switch="/addresses*" handler={MiddleAddresses} />
        <Route switch="/code-editor*" handler={MiddleCodeEditor} />
        <Route switch="/content*" handler={MiddleContent} />
        <Route switch="/documentation*" documentationStore={DocumentationStore} sidebar={MiddleSidebar} titleText="Documentation" titlePrefix="DOC" handler={MiddleDocumentation} />
        <Route switch="/education*" documentationStore={EducationStore} sidebar={MiddleSidebar} titleText="Education" titlePrefix="EDU" handler={MiddleDocumentation} />
        <Route switch="/stage*" handler={MiddleStaging} />
        <Route switch="/plugins*" handler={MiddlePlugins} /> 
        <Route switch="/plus-services*" handler={MiddlePlusServices} />
        <Route switch="/repos*" handler={MiddleRepos} />
        <Route switch="/stack-editor*" handler={MiddleStackEditor} />
        <Route switch="/start*" handler={MiddleStart} />
        <Route switch="/support*" handler={MiddleSupport} />
        <Route switch="/systems-under-test*" handler={MiddleSystemUnderTest} />
        <Route switch="/test-cases*" handler={MiddleTestCases} />
        <Route switch="/test-suites*" handler={MiddleTestSuites} />
        <Route switch="/test-data*" handler={MiddleTestData} />
        <Route switch="/user*" handler={MiddleUser} />
        <Route switch="/videos*" handler={MiddleVideos} />
        <Route switch="/workshop*" documentationStore={WorkshopStore} sidebar={MiddleSidebar} titleText="Workshop" titlePrefix="WS" handler={MiddleDocumentation} />
        {this.renderRouteServices()}
      </div>
    );
  }
}


