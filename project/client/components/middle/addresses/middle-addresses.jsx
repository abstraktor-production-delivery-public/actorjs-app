
'use strict';

import MiddleAddressesToolbar from './middle-addresses-toolbar';
import MiddleSidebar from '../middle_sidebar';
import MiddleAddressesView from './middle-addresses-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleAddresses extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Addresses',
        prefix: 'ADDR: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle">
        <MiddleAddressesToolbar />
        <MiddleSidebar />
        <div className="middle_view middle_view_address">
          <Route switch="/:tab" handler={MiddleAddressesView} />
        </div>
      </div>
    );
  }
}
