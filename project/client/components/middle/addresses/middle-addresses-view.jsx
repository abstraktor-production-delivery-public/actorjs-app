
'use strict';

import AddressesStore from '../../../stores/addresses-store';
import LoginStore from '../../../stores/login-store';
import MiddleAddressesViewSrc from './middle-addresses-view-src';
import MiddleAddressesViewDst from './middle-addresses-view-dst';
import MiddleAddressesViewSrv from './middle-addresses-view-srv';
import MiddleAddressesViewClientAddress from './middle-addresses-view-client-address';
import MiddleAddressesViewSutAddress from './middle-addresses-view-sut-address';
import MiddleAddressesViewServerAddress from './middle-addresses-view-server-address';
import MiddleAddressesViewClientInterface from './middle-addresses-view-client-interface';
import MiddleAddressesViewSutInterface from './middle-addresses-view-sut-interface';
import MiddleAddressesViewServerInterface from './middle-addresses-view-server-interface';
import MiddleAddressesViewNetworkInterface from './middle-addresses-view-network-interface';
import MiddleAddressesViewPorts from './middle-addresses-view-ports';
import MiddleAddressesViewDns from './middle-addresses-view-dns';
import { ActionAddressesSrcGet, ActionAddressesDstGet, ActionAddressesSrvGet, ActionAddressesClientAddressGet, ActionAddressesSutAddressGet, ActionAddressesServerAddressGet, ActionAddressesClientInterfaceGet, ActionAddressesSutInterfaceGet, ActionAddressesServerInterfaceGet, ActionAddressesNetworkInterfaceGet, ActionAddressesPortsGet, ActionAddressesDnsGet, ActionAddressesMarkupCancel } from '../../../actions/action-addresses/action-addresses';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewAddresses extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore, LoginStore], {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['Src', 'Dst', 'Srv', 'Client-address', 'Sut-address', 'Server-address', 'Client-interface', 'Sut-interface', 'Server-interface', 'Network-interface', 'Ports', 'Dns']);
    this.state.activeKey = this.helperTab.getIndex(props.tab);
  }
  
  didMount() {
    this._getData(this.state.activeKey);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
      || !this.shallowCompare(this.state.AddressesStore.markup.definition, nextState.AddressesStore.markup.definition)
      || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login);
  }

  didUpdate(prevProps) {
    const nextKey = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== nextKey) {
      this.updateState({activeKey: {$set: nextKey}});
      this._getData(nextKey);
    }
  }
  
  render() {
    let index = 0;
    return (
      <div className="same_size_as_parent">
        <div className="address_tabs">
           <Tabs id="address_tabs" activeKey={this.state.activeKey}
            onSelect={(key) => {
              if(undefined !== key && this.state.activeKey !== key) {
                if(this.state.AddressesStore.markup.definition) {
                  // TODO: Ask if we really want to leve markup.
                  this.dispatch(AddressesStore, new ActionAddressesMarkupCancel());
                }
                this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
              }
            }}
          >
            <Tab id="addresses_src_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewSrc stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_dst_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewDst stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_srv_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewSrv stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_client_address_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewClientAddress stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_sut_address_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewSutAddress stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_server_address_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewServerAddress stage={this.state.LoginStore.login} />
            </Tab>  
            <Tab id="addresses_client_interface_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewClientInterface stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_sut_interface_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewSutInterface stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_server_interface_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewServerInterface stage={this.state.LoginStore.login} />
            </Tab> 
            <Tab id="addresses_network_interface_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewNetworkInterface stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_ports_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewPorts stage={this.state.LoginStore.login} />
            </Tab>
            <Tab id="addresses_dns_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleAddressesViewDns stage={this.state.LoginStore.login} />
            </Tab>
          </Tabs>
        </div>
      </div>
    );
  }
  
  _getData(key) {
    let tabName = this.helperTab.getTabName(key);
    if('Src' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesSrcGet());
    }
    else if('Dst' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesDstGet());
    }
    else if('Srv' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesSrvGet());
    }
    else if('Client-address' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesClientAddressGet());
    }
    else if('Sut-address' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesSutAddressGet());
    }
    else if('Server-address' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesServerAddressGet());
    }
    else if('Client-interface' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesClientInterfaceGet());
    }
    else if('Sut-interface' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesSutInterfaceGet());
    }
    else if('Server-interface' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesServerInterfaceGet());
    }
    else if('Network-interface' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesNetworkInterfaceGet());
    }
    else if('Ports' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesPortsGet());
    }
    else if('Dns' === tabName) {
      this.dispatch(AddressesStore, new ActionAddressesDnsGet());
    }
  }
}


MiddleAddressesViewAddresses.contextType = RouterContext;
