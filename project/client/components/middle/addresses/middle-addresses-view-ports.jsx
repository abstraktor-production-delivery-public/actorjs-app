
'use strict';

import StageFilter from './helpers/stage-filter';
import AddressesStore from '../../../stores/addresses-store';
import { ActionAddressesMarkupChange } from '../../../actions/action-addresses/action-addresses';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataAddressesPortsGlobal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-ports-global';
import DataAddressesPortsLocal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-ports-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewPorts extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore]);
    this.dataAddressesPortsGlobal = new DataAddressesPortsGlobal();
    this.dataAddressesPortsLocal = new DataAddressesPortsLocal();
  }

  shouldUpdate(nextProps, nextState) {
    return 'Ports' === nextState.AddressesStore.currentView
      && (
        !this.shallowCompare(this.props.stage, nextProps.stage)
        || !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesPortsLocals, nextState.AddressesStore.addressesPortsLocals)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesPortsGlobals, nextState.AddressesStore.addressesPortsGlobals)
        || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      );
  }
  
  renderMarkup() {
    return (
      <div className="address_group">
        <label htmlFor="address_ports_markup_textarea">Address - Markup</label>
        <ComponentMarkedTextarea id="address_ports_markup_textarea" className="form-control address_definition same_size_as_parent" rows="10" value={this.state.AddressesStore.markup.content} results={this.state.AddressesStore.markup.rows}
          onChange={(value) => {
            this.dispatch(AddressesStore, new ActionAddressesMarkupChange(value));
          }}
          />
      </div>
    );
  }
  
  render() {
    if(this.state.AddressesStore.markup.definition) {
      return (
        <div className="address_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="address_tab_view">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataAddressesPortsLocal} values={this.state.AddressesStore.addressesPortsLocals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataAddressesPortsGlobal} values={this.state.AddressesStore.addressesPortsGlobals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
        </div>
      );
    }
  }
}
