
'use strict';

import StageFilter from './helpers/stage-filter';
import AddressesStore from '../../../stores/addresses-store';
import { ActionAddressesMarkupChange } from '../../../actions/action-addresses/action-addresses';
import MarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataAddressesClientInterfaceGlobal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-client-interface-global';
import DataAddressesClientInterfaceLocal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-client-interface-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewClientInterface extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore]);
    this.dataAddressesClientInterfaceGlobal = new DataAddressesClientInterfaceGlobal();
    this.dataAddressesClientInterfaceLocal = new DataAddressesClientInterfaceLocal();
  }

  shouldUpdate(nextProps, nextState) {
    return 'ClientInterface' === nextState.AddressesStore.currentView
      && (
        !this.shallowCompare(this.props.stage, nextProps.stage)
        || !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesClientInterfaceLocals, nextState.AddressesStore.addressesClientInterfaceLocals)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesClientInterfaceGlobals, nextState.AddressesStore.addressesClientInterfaceGlobals)
        || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      );
  }
  
  renderMarkup() {
    return (
      <div className="address_group">
        <label htmlFor="client_interface_markup_textarea">Address - Markup</label>
        <MarkedTextarea id="client_interface_markup_textarea" className="form-control address_definition same_size_as_parent" rows="10" value={this.state.AddressesStore.markup.content} results={this.state.AddressesStore.markup.rows}
          onChange={(value) => {
            this.dispatch(AddressesStore, new ActionAddressesMarkupChange(value));
          }}
          />
      </div>
    );
  }


  render() {
    if(this.state.AddressesStore.markup.definition) {
      return (
        <div className="address_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="address_tab_view">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataAddressesClientInterfaceLocal} values={this.state.AddressesStore.addressesClientInterfaceLocals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.sutFilter(this.props.stage, value, filterData);
          }}/>
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataAddressesClientInterfaceGlobal} values={this.state.AddressesStore.addressesClientInterfaceGlobals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.sutFilter(this.props.stage, value, filterData);
          }}/>
        </div>
      );
    }
  }
}
