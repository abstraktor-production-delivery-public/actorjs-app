
'use strict';


class StageFilter {
  static filter(stage, value, filter, sutMandatory= false) {
    if(!filter) {
      return true;
    }
    if(stage.systemUnderTest !== value.sut && (sutMandatory || '' !== value.sut)) {
      return false;
    }
    else if(stage.labId === value.labId && stage.userId === value.userId) {
      return true;
    }
    else if('' === value.labId && '' === value.userId) {
      return true;
    }
    else {
      return false;
    }
  }
  
  static sutFilter(stage, value, filter) {
    if(!filter) {
      return true;
    }
    return stage.systemUnderTest === value.sut;
  }
}

module.exports = StageFilter;
