
'use strict';

import StageFilter from './helpers/stage-filter';
import AddressesStore from '../../../stores/addresses-store';
import { ActionAddressesMarkupChange } from '../../../actions/action-addresses/action-addresses';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataAddressesSutInterfaceGlobal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-sut-interface-global';
import DataAddressesSutInterfaceLocal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-sut-interface-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewSutInterface extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore]);
    this.dataAddressesSutInterfaceGlobal = new DataAddressesSutInterfaceGlobal();
    this.dataAddressesSutInterfaceLocal = new DataAddressesSutInterfaceLocal();
  }

  shouldUpdate(nextProps, nextState) {
    return 'SutInterface' === nextState.AddressesStore.currentView
      && (
        !this.shallowCompare(this.props.stage, nextProps.stage)
        || !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSutInterfaceLocals, nextState.AddressesStore.addressesSutInterfaceLocals)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSutInterfaceGlobals, nextState.AddressesStore.addressesSutInterfaceGlobals)
        || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      );
  }
  
  renderMarkup() {
    return (
      <div className="address_group">
        <label htmlFor="sut_interface_markup_textarea">Address - Markup</label>
        <ComponentMarkedTextarea id="sut_interface_markup_textarea" className="form-control address_definition same_size_as_parent" rows="10" value={this.state.AddressesStore.markup.content} results={this.state.AddressesStore.markup.rows}
          onChange={(value) => {
            this.dispatch(AddressesStore, new ActionAddressesMarkupChange(value));
          }}
          />
      </div>
    );
  }
  
  render() {
    if(this.state.AddressesStore.markup.definition) {
      return (
        <div className="address_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="address_tab_view">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSutInterfaceLocal} values={this.state.AddressesStore.addressesSutInterfaceLocals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.sutFilter(this.props.stage, value, filterData);
          }}/>
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSutInterfaceGlobal} values={this.state.AddressesStore.addressesSutInterfaceGlobals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.sutFilter(this.props.stage, value, filterData);
          }}/>
        </div>
      );
    }
  }
}
