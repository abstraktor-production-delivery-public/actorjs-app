
'use strict';

import StageFilter from './helpers/stage-filter';
import AddressesStore from '../../../stores/addresses-store';
import { ActionAddressesMarkupChange } from '../../../actions/action-addresses/action-addresses';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataAddressesSrvGlobal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-src-global';
import DataAddressesSrvLocal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-src-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewSrc extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore]);
    this.dataAddressesSrvGlobal = new DataAddressesSrvGlobal();
    this.dataAddressesSrvLocal = new DataAddressesSrvLocal();
  }

  shouldUpdate(nextProps, nextState) {
    return 'Src' === nextState.AddressesStore.currentView
      && (
        !this.shallowCompare(this.props.stage, nextProps.stage)
        || !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSrcLocals, nextState.AddressesStore.addressesSrcLocals)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSrcGlobals, nextState.AddressesStore.addressesSrcGlobals)
        || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      );
  }

  renderMarkup() {
    return (
      <div className="address_group">
        <label htmlFor="address_src_markup_textarea">Address - Markup</label>
        <ComponentMarkedTextarea id="address_src_markup_textarea" className="form-control address_definition same_size_as_parent" rows="10" value={this.state.AddressesStore.markup.content} results={this.state.AddressesStore.markup.rows}
          onChange={(value) => {
            this.dispatch(AddressesStore, new ActionAddressesMarkupChange(value));
          }}
          />
      </div>
    );
  }

  render() {
    if(this.state.AddressesStore.markup.definition) {
      return (
        <div className="address_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="address_tab_view">, 
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSrvLocal} values={this.state.AddressesStore.addressesSrcLocals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSrvGlobal} values={this.state.AddressesStore.addressesSrcGlobals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
        </div>
      );
    }
  }
}
