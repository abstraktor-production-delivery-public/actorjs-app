
'use strict';

import AddressesStore from '../../../stores/addresses-store';
import LoginStore from '../../../stores/login-store';
import { ActionAddressesMarkup, ActionAddressesMarkupSave, ActionAddressesMarkupCancel, ActionAddressesLabUserFilter } from '../../../actions/action-addresses/action-addresses';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore, LoginStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }

  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
      || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(AddressesStore, new ActionAddressesMarkup());
  }
  
  _markupSave() {
    this.dispatch(AddressesStore, new ActionAddressesMarkupSave());
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    this.dispatch(AddressesStore, new ActionAddressesMarkupCancel());
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this.state.AddressesStore.markup.definition;
    return (
      <Button id="addresses_markup_open" placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = this.state.AddressesStore.markup.content === this.state.AddressesStore.markup.contentOriginal;
    return (
      <Button id="addresses_markup_save" placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.AddressesStore.markup.definition;
    return (
      <Button id="addresses_markup_help" placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this.state.AddressesStore.markup.definition;
    return (
      <Button id="addresses_markup_cancel" placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderLabUserFilterInfo() {
    if(this.state.AddressesStore.labUserFilter) {
      return (
        <div id="addresses_lab_user_filter_info_div" className="btn-group btn-group-sm middle_documentation_heading labuser_filter" role="group" aria-label="...">
          <h4 id="addresses_lab_user_filter_info_h4" className="middle_documentation_heading">{`Filter - Sut: ${this.state.LoginStore.login.systemUnderTest}`}</h4>
        </div>
      );
    }
  }
  
  renderLabUserFilter() {
    const filter = this.state.AddressesStore.labUserFilter ? 'show all' : 'show staged sut';
    return (
      <>
        <Button id="addresses_lab_user_filter" className="labuser_filter" placement="bottom" heading="Filter" content={filter} active={this.state.AddressesStore.labUserFilter}
          onClick={(e) => {
            this.dispatch(AddressesStore, new ActionAddressesLabUserFilter(!this.state.AddressesStore.labUserFilter));
          }}
        >
          <span className="glyphicon glyphicon-filter" aria-hidden="true"></span>
        </Button>
        {this.renderLabUserFilterInfo()}
      </>
    );
  }
  
  renderStageButton() {
    return (
      <Button id="from_start_to_stage" placement="bottom" heading="Goto" content="Stage"
        onClick={(e) => {
          this.context.history(`/stage`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-star-empty" aria-hidden="true" style={{top: -4, left: -4, width: 0, transform: 'scale(0.9)'}}></span>
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderLabUserFilter()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderStageButton()}
          </div>
        </div>
      </div>
    );
  }
}

MiddleAddressesToolbar.contextType = RouterContext;
