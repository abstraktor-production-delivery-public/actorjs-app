
'use strict';

import StageFilter from './helpers/stage-filter';
import AddressesStore from '../../../stores/addresses-store';
import { ActionAddressesMarkupChange } from '../../../actions/action-addresses/action-addresses';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataAddressesSutAddressGlobal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-sut-address-global';
import DataAddressesSutAddressLocal from 'z-abs-complayer-markup-client/client/data/addresses/data-addresses-sut-address-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleAddressesViewSutAddress extends ReactComponentStore {
  constructor(props) {
    super(props, [AddressesStore]);
    this.dataAddressesSutAddressGlobal = new DataAddressesSutAddressGlobal();
    this.dataAddressesSutAddressLocal = new DataAddressesSutAddressLocal();
  }
  
  shouldUpdate(nextProps, nextState) {
    return 'SutAddress' === nextState.AddressesStore.currentView
      && (
        !this.shallowCompare(this.props.stage, nextProps.stage)
        || !this.shallowCompare(this.state.AddressesStore.markup, nextState.AddressesStore.markup)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSutAddressLocals, nextState.AddressesStore.addressesSutAddressLocals)
        || !this.shallowCompareArrayValues(this.state.AddressesStore.addressesSutAddressGlobals, nextState.AddressesStore.addressesSutAddressGlobals)
        || !this.shallowCompare(this.state.AddressesStore.labUserFilter, nextState.AddressesStore.labUserFilter)
      );
  }

  renderMarkup() {
    return (
      <div className="address_group">
        <label htmlFor="sut_address_markup_textarea">Address - Markup</label>
        <ComponentMarkedTextarea id="sut_address_markup_textarea" className="form-control address_definition same_size_as_parent" rows="10" value={this.state.AddressesStore.markup.content} results={this.state.AddressesStore.markup.rows}
          onChange={(value) => {
            this.dispatch(AddressesStore, new ActionAddressesMarkupChange(value));
          }}
          />
      </div>
    );
  }
  
  render() {
    if(this.state.AddressesStore.markup.definition) {
      return (
        <div className="address_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="address_tab_view">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSutAddressLocal} values={this.state.AddressesStore.addressesSutAddressLocals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataAddressesSutAddressGlobal} values={this.state.AddressesStore.addressesSutAddressGlobals} stage={this.props.stage} filterData={this.state.AddressesStore.labUserFilter} filter={(value, stage, filterData) => {
            return StageFilter.filter(this.props.stage, value, filterData);
          }}/>
        </div>
      );
    }
  }
}
