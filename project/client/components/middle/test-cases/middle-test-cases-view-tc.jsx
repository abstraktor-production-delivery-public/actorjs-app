
'use strict';

import FilterStore from '../../../stores/filter-store';
import TestCaseStore from '../../../stores/test-case-store';
import TestCasesStore from '../../../stores/test-cases-store';
import { ActionTestCasesTcChecked, ActionTestCasesTcClear } from '../../../actions/action-test-cases';
import { ActionTestCaseGet } from '../../../actions/action-test-case/action-test-case';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewTc extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, TestCasesStore, TestCaseStore]);
    this.refTc = React.createRef();
  }
  
  didMount() {
    if(this.props.repo) {
      this.dispatch(TestCaseStore, new ActionTestCaseGet(this.props.repo, this.props.sut, this.props.fut));
    }
    if(this.props.scroll) {
      this.refTc.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.state.TestCaseStore.testCases, nextState.TestCaseStore.testCases)
      || !this.shallowCompareMapValues(this.state.TestCasesStore.testCasesChecked, nextState.TestCasesStore.testCasesChecked);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.repo && (this.props.repo !== prevProps.repo || this.props.sut !== prevProps.sut || this.props.fut !== prevProps.fut)) {
      this.dispatch(TestCaseStore, new ActionTestCaseGet(this.props.repo, this.props.sut, this.props.fut));
      this.dispatch(TestCasesStore, new ActionTestCasesTcClear());
    }
  }

  willUnmount() {
    this.dispatch(TestCasesStore, new ActionTestCasesTcClear());
    this.props.onRestoreData && this.props.onRestoreData(this.refTc.current.scrollTop);
  }
  
  renderCheckbox(tcName) {
    return (
      <input type="checkbox" id={`test_cases_input_checkbox_tc_${tcName}`} aria-label="..." autoComplete="off" checked={this.state.TestCasesStore.testCasesChecked.has(tcName)}
        onChange={(e) => {
          this.dispatch(TestCasesStore, new ActionTestCasesTcChecked(this.props.repo, this.props.sut, this.props.fut, tcName, e.currentTarget.checked));
        }}
      />
    );
  }

  renderTestCaseRows() {
    return this.state.TestCaseStore.testCases.map((tc, i) => {
      return (
        <tr key={`tc:tc_${this.props.sut}/${this.props.fut}/${tc.name}`}>
          <th scope="row">{i + 1}</th>
          <td>{this.renderCheckbox(tc.name)}</td>
          <td>
            <Link className="test_case_link" href={`/${this.props.sut}/${this.props.fut}/${tc.name}/definition`}>
              {tc.name}
            </Link>
          </td>
          <td>{tc.description}</td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div ref={this.refTc} className="test_case_column">
        <div className="test_cases_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">
                [
                <Link className="test_case_link" global href={`/system-under-tests/${this.props.sut}/`}>
                  {this.props.sut}
                </Link>
                ] - [{this.props.fut}] - Tc
              </h3>
            </div>
            <table id="tc_tc_table" className="table test_cases_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                {this.renderTestCaseRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
