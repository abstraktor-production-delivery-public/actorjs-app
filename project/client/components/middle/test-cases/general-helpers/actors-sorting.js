
'use strict';


class ActorsSorting {
  static sortPhase(actors) {
    let index = 0;
    const preRows = actors.filter((actor) => {
      return 'pre' === actor.phase || 'cond' === actor.type;
    }).map((actor) => {
      return {
        actor: actor,
        phase: 'pre',
        index: index++
      }
    });
    const execRows = actors.filter((actor) => {
      return (undefined === actor.phase || '' === actor.phase || 'exec' === actor.phase) && 'cond' !== actor.type;
    }).map((actor) => {
      return {
        actor: actor,
        phase: 'exec',
        index: index++
      }
    });
    let postRowsTemp = [];
    actors.filter((actor) => {
      return 'post' === actor.phase || 'cond' === actor.type;
    }).forEach((actor) => {
      if('cond' === actor.type) {
        postRowsTemp.splice(0, 0, actor);
      }
      else {
        postRowsTemp.push(actor);
      }
    });
    const postRows = postRowsTemp.map((actor) => {
      return {
        actor: actor,
        phase: 'post',
        index: index++
      }
    });
    return {
      preRows: preRows,
      execRows: execRows,
      postRows: postRows
    };
  }
}

module.exports = ActorsSorting;
