
'use strict';

import TestCaseStore from '../../../stores/test-case-store';
import MiddleTestCasesTabDefinition from './tabs/middle-test-cases-tab-definition';
import MiddleTestCasesTabExecution from './tabs/middle-test-cases-tab-execution';
import MiddleTestCasesTabDebug from './tabs/middle-test-cases-tab-debug';
import MiddleTestCasesTabLog from './tabs/middle-test-cases-tab-log';
import MiddleTestCasesTabSequenceDiagram from './tabs/middle-test-cases-tab-sequence-diagram';
import MiddleTestCasesTabSpecification from './tabs/middle-test-cases-tab-specification';
import MiddleTestCasesTabAnalyze from './tabs/middle-test-cases-tab-analyze';
import { ActionTestCaseGet } from '../../../actions/action-test-case/action-test-case';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewTestCase extends ReactComponentStore {
  constructor(props) {
    super(props, [], {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['Definition', 'Execution', 'Log', 'Sequence Diagram', 'Debug', 'Specification', 'Analyze']);
    if(undefined !== props.tab) {
      this.state.activeKey = this.helperTab.getIndex(props.tab);
    }
  }
  
  didMount() {
    if(this.props.repo) {
      this.dispatch(TestCaseStore, new ActionTestCaseGet(this.props.repo, this.props.sut, this.props.fut, this.props.tc));
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.tc, nextProps.tc)
      || !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.repo !== prevProps.repo || this.props.sut !== prevProps.sut || this.props.fut !== prevProps.fut || this.props.tc !== prevProps.tc) {
      this.dispatch(TestCaseStore, new ActionTestCaseGet(this.props.repo, this.props.sut, this.props.fut, this.props.tc));
    }
    const nextKey = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== nextKey) {
      this.updateState({activeKey: {$set: nextKey}});
    }
  }
  
  getTabPath(index) {
    return `${this.props.tab ? '' : this.props.tc + '/'}${this.helperTab.getRoute(index)}`;
  }
  
  render() {
    let index = 0;
    return (
      <div className="test_case_tabs">
        <Tabs id="test_case_tabs" activeKey={this.state.activeKey}
          onSelect={(key) => {
            if(undefined !== key) {
              this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
            }
          }}
        >
          <Tab id="test_case_definition" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleTestCasesTabDefinition active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
          <Tab id="test_case_execution" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="execution">
            <MiddleTestCasesTabExecution active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
          <Tab id="test_case_log" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="log">
            <MiddleTestCasesTabLog active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
          <Tab id="test_case_sequence_diagram" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="sequence-diagram">
            <MiddleTestCasesTabSequenceDiagram active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
          <Tab id="test_case_debug" tab={this.getTabPath(index)} className="same_size_as_parent" title={this.helperTab.getTabName(index++)} colorMark="debug">
            <Route handler={MiddleTestCasesTabDebug} switch={["/:file.:ext", ""]} active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} tab={this.props.tab} />
          </Tab>
          <Tab id="test_case_specification" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleTestCasesTabSpecification active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
          <Tab id="test_case_analyze" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleTestCasesTabAnalyze active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
          </Tab>
        </Tabs>
      </div>
    );
  }
}


MiddleTestCasesViewTestCase.contextType = RouterContext;
