
'use strict';

import FilterStore from '../../../stores/filter-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import TestCasesStore from '../../../stores/test-cases-store';
import { ActionTestCasesSutChecked, ActionTestCasesSutClear } from '../../../actions/action-test-cases';
import { ActionSystemUnderTestGet } from '../../../actions/action-system-under-test/action-system-under-test';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewSut extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, SystemUnderTestStore, TestCasesStore]);
    this.refSut = React.createRef();
  }

  didMount() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestGet());
    if(this.props.scroll) {
      this.refSut.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return this.state.FilterStore !== nextState.FilterStore
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompareMapValues(this.state.TestCasesStore.systemUnderTestsChecked, nextState.TestCasesStore.systemUnderTestsChecked);
  }
  
  willUnmount() {
    this.dispatch(TestCasesStore, new ActionTestCasesSutClear());
    this.props.onRestoreData && this.props.onRestoreData(this.refSut.current.scrollTop);
  }
  
  renderCheckbox(sut) {
    if('Global' === sut.repo && 'Actor' === sut.name) {
      return;
    }
    else {
      return (
        <input type="checkbox" id={`test_cases_input_checkbox_sut_${sut.name}`} aria-label="..." autoComplete="off" checked={this.state.TestCasesStore.systemUnderTestsChecked.has(sut.name)}
          onChange={(e) => {
            this.dispatch(TestCasesStore, new ActionTestCasesSutChecked(sut, e.currentTarget.checked));
          }}
        />
      );
    }
  }
  
  renderSystemUnderTestRows() {
    if('*' === this.state.FilterStore.sut) {
      return this.state.SystemUnderTestStore.systemUnderTests.map((sut, i) => {
        return (
          <tr key={`tc:sut_${sut.name}`}>
            <th scope="row">{i + 1}</th>    
            <td>{this.renderCheckbox(sut)}</td>
            <td>
               <Link className="test_case_link" href={`/${sut.name}`}>
                {sut.name}
              </Link>
            </td>
            <td>{sut.repo}</td>
          </tr>
        );
      });
    }
  }
  
  render() {
    return (
      <div ref={this.refSut} className="test_case_column">
        <div className="test_cases_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Sut</h3>
            </div>
            <table id="tc_sut_table" className="table test_cases_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th>Name</th>
                  <th>Repo</th>
                </tr>
              </thead>
              <tbody>
                {this.renderSystemUnderTestRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
