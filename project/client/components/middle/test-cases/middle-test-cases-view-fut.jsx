
'use strict';

import FilterStore from '../../../stores/filter-store';
import FunctionUnderTestStore from '../../../stores/function-under-test-store';
import TestCasesStore from '../../../stores/test-cases-store';
import { ActionTestCasesFutChecked, ActionTestCasesFutClear } from '../../../actions/action-test-cases';
import { ActionFunctionUnderTestGet } from '../../../actions/action-function-under-test';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewFut extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, FunctionUnderTestStore, TestCasesStore]);
    this.refFut = React.createRef();
  }
  
  didMount() {
    if(this.props.repo) {
      this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestGet(this.props.repo, this.props.sut));
    }
    if(this.props.scroll) {
      this.refFut.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompareArrayValues(this.state.FunctionUnderTestStore.functionUnderTests, nextState.FunctionUnderTestStore.functionUnderTests)
      || !this.shallowCompareMapValues(this.state.TestCasesStore.functionUnderTestsChecked, nextState.TestCasesStore.functionUnderTestsChecked);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.repo && (this.props.repo !== prevProps.repo || this.props.sut !== prevProps.sut)) {
      this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestGet(this.props.repo, this.props.sut));
      this.dispatch(TestCasesStore, new ActionTestCasesFutClear());
    }
  }
  
  willUnmount() {
    this.dispatch(TestCasesStore, new ActionTestCasesFutClear());
    this.props.onRestoreData && this.props.onRestoreData(this.refFut.current.scrollTop);
  }
  
  renderCheckbox(futName) {
    return (
      <input type="checkbox" id={`test_cases_input_checkbox_fut_${futName}`} aria-label="..." autoComplete="off" checked={this.state.TestCasesStore.functionUnderTestsChecked.has(futName)}
        onChange={(e) => {
          this.dispatch(TestCasesStore, new ActionTestCasesFutChecked(this.props.repo, this.props.sut, futName, e.currentTarget.checked));
        }}
      />
    );
  }

  renderFunctionUnderTestRows() {
    const functionUnderTestRows = this.state.FunctionUnderTestStore.functionUnderTests.map((fut, i) => {
    return (
      <tr key={`tc:fut_${this.props.sut}/${fut.name}`}>
        <th scope="row">{i + 1}</th>
        <td>{this.renderCheckbox(fut.name)}</td>
        <td>
          <Link className="test_case_link" href={`/${this.props.sut}/${fut.name}`}>
            {fut.name}
          </Link>
        </td>
      </tr>)
    })
  	return functionUnderTestRows;
  }

  render() {
    return (
      <div ref={this.refFut} className="test_case_column">      
        <div className="test_cases_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">
                [
                <Link className="test_case_link" href={`/${this.props.sut}`}>
                  {this.props.sut}
                </Link>
                ]&nbsp;- Fut
              </h3>
            </div>
            <table id="tc_fut_table" className="table test_cases_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {this.renderFunctionUnderTestRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
