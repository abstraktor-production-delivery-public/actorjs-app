
'use strict';


import TestCaseStore from '../../../../stores/test-case-store';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import { ActionTestCaseButtonsUpdate } from '../../../../actions/action-test-case/action-test-case';
import LogFilter from 'z-abs-complayer-visualizationlog-client/client/logic/log-filter';
import Log from 'z-abs-complayer-visualizationlog-client/client/react-component/log';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ButtonLogScroll from 'z-abs-complayer-visualizationlog-client/client/react-component/buttons/button-log-scroll';
import ButtonLogFilter from 'z-abs-complayer-visualizationlog-client/client/react-component/buttons/button-log-filter';
import LogType from 'z-abs-funclayer-engine-cs/clientServer/log/log-type';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestCasesTabLog extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestCaseStore]);
    this.refLog = React.createRef();
    this.refScrollButton = null;
    this.logFilter = new LogFilter();
    this.logFilter.init(this.state.TestCaseStore.buttons.log);
    this.cbIsRealtime = () => {
      return this.state.TestCaseStore.execution !== TestCaseStore.EXECUTION_NOT_RUNNING;
    };
  }
  
  didMount() {
    this.refLog.current.active(this.props.active);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.tc, nextProps.tc)
      || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
      || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase)
      || !this.shallowCompare(this.state.TestCaseStore.buttonsLoaded, nextState.TestCaseStore.buttonsLoaded)
      || !this.shallowCompare(this.state.TestCaseStore.buttons.log, nextState.TestCaseStore.buttons.log);
  }
  
  didUpdate(prevProps, prevState) {
    this.refLog.current.active(this.props.active);
  }
  
  realtimeUpdate(nextProps, nextState) {
    this.refLog.current.active(nextProps.active);
  }
  
  /*renderFilterDetailButton(className, help, logFilter, log) {
    const checked = Reflect.get(log, logFilter);
    return (
      <Button active={checked} placement="bottom" heading={`${checked ? 'Show All' : 'Use Filter'}`} content={help}
        onClick={(e) => {
          if(this.cbIsRealtime()) {
            e.currentTarget.classList.toggle('active');
            e.currentTarget.firstChild.classList.toggle('log_not_active');
          }
          this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('log', logFilter, !Reflect.get(log, logFilter)));
        }}
      >
        <span className={`btn_abs_s glyphicon glyphicon-${checked ? 'eye-open' : 'filter'} ${className}`} aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderFilterOpenButton(className, help, logFilter, log) {
    const checked = Reflect.get(log, logFilter);
    const logOpen = checked ? ` ${className}_open` : ` ${className}_closed`;
    return (
      <Button placement="bottom" heading="Filter Open" content={help}
        onClick={(e) => {
         if(this.cbIsRealtime()) {
            e.currentTarget.firstChild.classList.toggle(`${className}_open`);
            e.currentTarget.firstChild.classList.toggle(`${className}_closed`);
          }
          this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('log', logFilter, !Reflect.get(log, logFilter)));
        }}
      >
        <span className={`btn_abs_s glyphicon glyphicon-play${logOpen}`} aria-hidden="true"></span>
      </Button>
    );
  }*/
  
  _fireButtonAction(valueName, value, rt) {
    this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('log', valueName, value));
    if(this.cbIsRealtime()) {
      this.renderRealtime(() => {
        rt();
      });
    }
  }
  
  render() {
    if(this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound code={this.state.TestCaseStore.error.code} error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else {
      const log = this.state.TestCaseStore.buttons.log;
      return (
        <>
          <div className="log_panel">
            <div className="log_panel_inner btn-toolbar" role="toolbar" aria-label="...">
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogScroll buttonValue={log.scroll}
                  onLoad={(e) => {
                    this.refScrollButton = e;
                  }}
                  onAction={this._fireButtonAction.bind(this, 'scroll')}
                />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.ERROR]} buttonValue={log.filterError} colorMark="log_error" onAction={this._fireButtonAction.bind(this, 'filterError')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.WARNING]} buttonValue={log.filterWarning} colorMark="log_warning" onAction={this._fireButtonAction.bind(this, 'filterWarning')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.SUCCESS]} buttonValue={log.filterSuccess} colorMark="log_verify_success" onAction={this._fireButtonAction.bind(this, 'filterSuccess')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.FAILURE]} buttonValue={log.filterFailure} colorMark="log_verify_failure" onAction={this._fireButtonAction.bind(this, 'filterFailure')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.ENGINE]} buttonValue={log.filterEngine} colorMark="log_engine" onAction={this._fireButtonAction.bind(this, 'filterEngine')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.DEBUG]} buttonValue={log.filterDebug} colorMark="log_debug" onAction={this._fireButtonAction.bind(this, 'filterDebug')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.TEST_DATA]} buttonValue={log.filterTestData} colorMark="log_test_data" onAction={this._fireButtonAction.bind(this, 'filterTestData')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.IP]} buttonValue={log.filterIP} colorMark="log_ip" onAction={this._fireButtonAction.bind(this, 'filterIP')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.GUI]} buttonValue={log.filterGUI} colorMark="log_gui" onAction={this._fireButtonAction.bind(this, 'filterGUI')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.BROWSER_ERR]} buttonValue={log.filterBrowserErr} colorMark="log_browser_err" onAction={this._fireButtonAction.bind(this, 'filterBrowserErr')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.BROWSER_LOG]} buttonValue={log.filterBrowserLog} colorMark="log_browser_log" onAction={this._fireButtonAction.bind(this, 'filterBrowserLog')} />
              </div>
            </div>
          </div>
          <Log ref={this.refLog} filter={this.logFilter} name="test-case-log" clearMsgPrefix="TestCase" store={TestCaseStore} storeName="TestCaseStore" buttonsLoaded={this.state.TestCaseStore.buttonsLoaded}
            onOnit={() => {}}
            onAutoScroll={(autoScroll) => {
              this.refScrollButton.click();
            }}
          />
        </>
      );
    }
  }
}

