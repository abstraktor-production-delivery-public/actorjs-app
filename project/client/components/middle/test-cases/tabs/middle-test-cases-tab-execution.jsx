
'use strict';

import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import TestCaseStore from '../../../../stores/test-case-store';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import HighResolutionDate from 'z-abs-corelayer-cs/clientServer/time/high-resolution-date';
import HighResolutionDuration from 'z-abs-corelayer-cs/clientServer/time/high-resolution-duration';
import React from 'react';


export default class MiddleTestCasesTabExecution extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestCaseStore]);
    this.refTableSumTable = React.createRef();
    this.refTablePhaseSumTable = React.createRef();
    this.refTbodyActors = React.createRef();
    this.actorLength = 0;
    this.sumResultValue = ActorResultConst.NONE;
    this.groupData = null;
  }
  
  didMount() {
    this.groupData = this.addRealtimeFrameGroup();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.tc, nextProps.tc)
      || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
      || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.refTableSumTable.current) {
      const sumRowNode = this.refTableSumTable.current.firstChild.firstChild;
      this.addRealtimeFrame('SUM_RESULT', this.groupData, sumRowNode.childNodes[1]);
      this.addRealtimeFrame('SUM_TIME', this.groupData, sumRowNode.childNodes[3].firstChild);
      this.addRealtimeFrame('SUM_DURATION', this.groupData, sumRowNode.childNodes[5].firstChild);

      const settings = this.state.TestCaseStore.testCase.tc.settings;
      if(undefined !== settings && 1 === settings.length && this.refTablePhaseSumTable.current) {
        const phaseSumBodyNode = this.refTablePhaseSumTable.current.firstChild;
        this.addRealtimeFrame('PHASE_PRE_RESULT', this.groupData, phaseSumBodyNode.firstChild.childNodes[2]);
        this.addRealtimeFrame('PHASE_EXEC_RESULT', this.groupData, phaseSumBodyNode.childNodes[1].childNodes[2]);
        this.addRealtimeFrame('PHASE_POST_RESULT', this.groupData, phaseSumBodyNode.lastChild.childNodes[2]);
      }
    
      if(this.refTbodyActors.current) {
        const actorsTbodyNode = this.refTbodyActors.current;
        this.actorLength = actorsTbodyNode.childNodes.length;
        for(let i = 0; i < this.actorLength; ++i) {
          this.addRealtimeFrame(`TC_ACTOR_${i}_STATE_0_RESULT`, this.groupData, actorsTbodyNode.childNodes[i].childNodes[3]);
          this.addRealtimeFrame(`TC_ACTOR_${i}_STATE_1_RESULT`, this.groupData, actorsTbodyNode.childNodes[i].childNodes[4]);
          this.addRealtimeFrame(`TC_ACTOR_${i}_STATE_2_RESULT`, this.groupData, actorsTbodyNode.childNodes[i].childNodes[5]);
          this.addRealtimeFrame(`TC_ACTOR_${i}_STATE_3_RESULT`, this.groupData, actorsTbodyNode.childNodes[i].childNodes[6]);
          this.addRealtimeFrame(`TC_ACTOR_${i}_STATE_4_RESULT`, this.groupData, actorsTbodyNode.childNodes[i].lastChild);
        }
      } 
    }
  }
  
  willUnmount() {
    this.removeRealtimeFrameGroup(this.groupData);
  }
  
  clean() {
    this.replaceRealtimeFrameObject(`SUM_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
    this.replaceRealtimeFrameText(`SUM_TIME`, this.groupData, '');
    this.replaceRealtimeFrameText(`SUM_DURATION`, this.groupData, '');
      
    const settings = this.state.TestCaseStore.testCase.tc.settings;
    if(undefined !== settings && 1 === settings.length) {
      this.replaceRealtimeFrameObject(`PHASE_PRE_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`PHASE_EXEC_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`PHASE_POST_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
    }

    for(let i = 0; i < this.actorLength; ++i) {
      this.replaceRealtimeFrameObject(`TC_ACTOR_${i}_STATE_0_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`TC_ACTOR_${i}_STATE_1_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`TC_ACTOR_${i}_STATE_2_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`TC_ACTOR_${i}_STATE_3_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameObject(`TC_ACTOR_${i}_STATE_4_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[ActorResultConst.NONE].cloneNode(true));
    }
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.TEST_CASE_STATE === msg.msgId) {
      this.sumResultValue = Math.max(this.sumResultValue, msg.stateResultIndex);
      this.replaceRealtimeFrameObject(`TC_ACTOR_${msg.actorOrderIndex}_STATE_${msg.stateIndex}_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.stateResultIndex].cloneNode(true));
    }
    else if(AppProtocolConst.TEST_CASE_STARTED === msg.msgId) {
      this.sumResultValue = ActorResultConst.NONE;
      this.clean(); 
      this.replaceRealtimeFrameText(`SUM_TIME`, this.groupData, new HighResolutionDate(msg.timestamp).getDateMilliSeconds());
    }
    else if(AppProtocolConst.TEST_CASE_NOT_STARTED === msg.msgId) {
      this.replaceRealtimeFrameObject(`SUM_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.resultId].cloneNode(true));
    }
    else if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId) {
      this.replaceRealtimeFrameObject(`SUM_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.resultId].cloneNode(true));
      const settings = this.state.TestCaseStore.testCase.tc.settings;
      if(undefined !== settings && 1 === settings.length) {
        this.replaceRealtimeFrameObject(`PHASE_PRE_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.resultIdPre].cloneNode(true));
        this.replaceRealtimeFrameObject(`PHASE_EXEC_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.resultIdExec].cloneNode(true));
        this.replaceRealtimeFrameObject(`PHASE_POST_RESULT`, this.groupData, resultFactoryTestCase.resultTemplates[msg.resultIdPost].cloneNode(true));
      }
      this.replaceRealtimeFrameText(`SUM_DURATION`, this.groupData, new HighResolutionDuration(msg.duration).getDuration());
    }
    else if(AppProtocolConst.TEST_CASE_CLEAR === msg.msgId) {
      if('execution' === msg.tab) {
        this.renderRealtime(() => {
          this.clean();
        });
      }
    }
  }
  
  renderActorsColumnState() {
    return (
      <td className={ActorResultConst.getClass(ActorResultConst.NONE)}>
        {ActorResultConst.getName(ActorResultConst.NONE)}
      </td>
    );
  }
  
  renderActorsTableRow(actor, phase, index) {
    const actorPaths = actor.name.split('.');
    return (
      <tr key={index}>
        <td className={`test_case_phase_${phase}`}>
          {index + 1}
        </td>
        <td className={`test_case_phase_${phase}`}>
          {phase}
        </td>
        <td>
          <Link className="test_case_link" global href={`/actor-editor/${actorPaths.join('/')}.js`}>{actor.name}</Link>
        </td>
        {this.renderActorsColumnState()}
        {this.renderActorsColumnState()}
        {this.renderActorsColumnState()}
        {this.renderActorsColumnState()}
        {this.renderActorsColumnState()}
      </tr>
    );
  }
  
  renderActorsTableRows() {
    let index = 0;
    const actors = this.state.TestCaseStore.testCase.tc.actors;
    const preRows = actors.filter((actor) => {
      return 'pre' === actor.phase || 'cond' === actor.type;
    }).map((actor) => {
      return this.renderActorsTableRow(actor, 'pre', index++);
    });
    const execRows = actors.filter((actor) => {
      return (undefined === actor.phase || '' === actor.phase || 'exec' === actor.phase) && 'cond' !== actor.type;
    }).map((actor) => {
      return this.renderActorsTableRow(actor, 'exec', index++);
    });
    const postRowsTemp = [];
    actors.filter((actor) => {
      return 'post' === actor.phase || 'cond' === actor.type;
    }).forEach((actor) => {
      if('cond' === actor.type) {
        postRowsTemp.splice(0, 0, actor);
      }
      else {
        postRowsTemp.push(actor);
      }
    });
    const postRows = postRowsTemp.map((actor) => {
      return this.renderActorsTableRow(actor, 'post', index++);
    });
    return (
      <tbody ref={this.refTbodyActors}>
        {preRows}
        {execRows}
        {postRows}
      </tbody>
    );
  }
  
  renderActorsTable() {
    return (
      <table className="test_case_table table table-bordered table-striped table-condensed table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>phase</th>
            <th>name</th>
            <th>data</th>
            <th>initServer</th>
            <th>initClient</th>
            <th>run</th>
            <th>exit</th>
          </tr>
        </thead>
        {this.renderActorsTableRows()}
      </table>
    );
  }

  renderPhaseSumRow(name, resultId, index) {
    return (
      <tr key={index}>
        <th scope="row">{name}</th>
        <th scope="row">result</th>
        <td className="test_none">
          {ActorResultConst.getName(ActorResultConst.NONE)}
        </td>
        <th scope="row">expected</th>
        <td className={ActorResultConst.classes[resultId]}>
          {ActorResultConst.results[resultId]}
        </td>
      </tr>
    );
  }
  
  renderPhaseSumRows() {
    const settings = this.state.TestCaseStore.testCase.tc.settings;
    if(undefined !== settings && 1 === settings.length) {
      let index = 0;
      return (
        <table ref={this.refTablePhaseSumTable} className="test_case_table test_case_phase_table table table-bordered table-striped table-condensed table-hover">
          <tbody>
            {this.renderPhaseSumRow('pre', this._getTableResultId(settings[0].preResult), ++index)}
            {this.renderPhaseSumRow('exec', this._getTableResultId(settings[0].execResult), ++index)}
            {this.renderPhaseSumRow('post', this._getTableResultId(settings[0].postResult), ++index)}
          </tbody>
        </table>
      );
    }
  }
  
  renderSumTable() {
    return (
      <div>
        <table ref={this.refTableSumTable} className="test_case_table table table-bordered table-striped table-condensed table-hover">
          <tbody>
            <tr key="0">
              <th scope="row">result</th>
              <td className="test_none">
                {ActorResultConst.getName(ActorResultConst.NONE)}
              </td>
              <th scope="row">time</th>
              <td> </td>
              <th scope="row">duration</th>
              <td> </td>
            </tr>
          </tbody>
        </table>
        {this.renderPhaseSumRows()}
      </div>
    );
  }
  
  renderActorsTableHeading() {
    return (
      <div className="panel-heading test_case_table_heading">{this.state.TestCaseStore.testCase.name}</div>
    );
  }
  
  render() {
    if(null !== this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound code={this.state.TestCaseStore.error.code} error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else if(undefined !== this.state.TestCaseStore.testCase) {
      return (
        <div className="test_cases_tab_view test_case_table_fit_content">
          {this.renderSumTable()}
          {this.renderActorsTableHeading()}
          {this.renderActorsTable()}
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  _getTableResultId(tableResultName) {
    const resultId = ActorResultConst.getTableNameResultIds(tableResultName);
    if(undefined === resultId) {
      return ActorResultConst.SUCCESS;
    }
    return resultId;
  }
}

class ResultFactoryTestCase {
  constructor() {
    this.resultTemplates = [];
    this.td = document.createElement('td');
    ActorResultConst.resultIds.forEach((resultId) => {
      this.resultTemplates.push(this._createTemplate(resultId));
    });
  }
  
  _createTemplate(resultId) {
    const td = this.td.cloneNode(false);
    td.classList.add(ActorResultConst.classes[resultId]);
    td.appendChild(document.createTextNode(ActorResultConst.results[resultId]));
    return td;
  }
}


const resultFactoryTestCase = new ResultFactoryTestCase();
