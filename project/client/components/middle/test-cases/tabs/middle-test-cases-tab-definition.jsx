
'use strict';

import TestCaseStore from '../../../../stores/test-case-store';
import { ActionTestCaseMarkupChange } from '../../../../actions/action-test-case/action-test-case';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataTestCaseSettings from 'z-abs-complayer-markup-client/client/data/data-test-case/data-test-case-settings';
import DataActor from 'z-abs-complayer-markup-client/client/data/data-test-case/data-actor';
import DataTestDataTestCase from 'z-abs-complayer-markup-client/client/data/data-test-case/data-test-data-test-case';
import DataTestDataIteration from 'z-abs-complayer-markup-client/client/data/data-test-case/data-test-data-iteration';
import DataVerificationTestCase from 'z-abs-complayer-markup-client/client/data/data-test-case/data-verification-test-case';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesTabDefinition extends ReactComponentStore {
  constructor(props) {
    super(props, [TestCaseStore]);
    this.dataTestCaseSettings = new DataTestCaseSettings();
    this.dataActor = new DataActor();
    this.dataTestDataTestCase = new DataTestDataTestCase();
    this.dataTestDataIteration = new DataTestDataIteration();
    this.dataVerificationTestCase = new DataVerificationTestCase();
  }
    
  shouldUpdate(nextProps, nextState) {
    return nextProps.active
      && (
        !this.shallowCompare(this.props.active, nextProps.active)
        || !this.shallowCompare(this.props.repo, nextProps.repo)
        || !this.shallowCompare(this.props.sut, nextProps.sut)
        || !this.shallowCompare(this.props.fut, nextProps.fut)
        || !this.shallowCompare(this.props.tc, nextProps.tc)
        || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
        || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase)
        || !this.shallowCompare(this.state.TestCaseStore.markup, nextState.TestCaseStore.markup)
      );
  }
  
  renderTables() {
    let key = 0;
    if(undefined !== this.state.TestCaseStore.testCase) {
      return (
        <div className="same_size_as_parent" >
          <ComponentTableDataTable classTable="test_case_table_fit_content" classHeading="test_case_table_heading" classRow="test_case_table" key={++key} dataTable={this.dataTestCaseSettings} values={this.state.TestCaseStore.testCase.tc.settings} />
          <ComponentTableDataTable classTable="test_case_table_fit_content" classHeading="test_case_table_heading" classRow="test_case_table" key={++key} dataTable={this.dataActor} values={this.state.TestCaseStore.testCase.tc.actors} hideColumsIfNotUsed={["node", "testData", "verification"]} />
          <ComponentTableDataTable classTable="test_case_table_fit_content" classHeading="test_case_table_heading" classRow="test_case_table" key={++key} dataTable={this.dataTestDataTestCase} values={this.state.TestCaseStore.testCase.tc.testDataTestCases} />
          <ComponentTableDataTable classTable="test_case_table_fit_content" classHeading="test_case_table_heading" classRow="test_case_table" key={++key} dataTable={this.dataTestDataIteration} values={this.state.TestCaseStore.testCase.tc.testDataIteration} />
          <ComponentTableDataTable classTable="test_case_table_fit_content" classHeading="test_case_table_heading" classRow="test_case_table" key={++key} dataTable={this.dataVerificationTestCase} values={this.state.TestCaseStore.testCase.tc.verificationTestCases} />
        </div>
      );
    }
  }

  renderMarkup() {
    if(undefined !== this.state.TestCaseStore.testCase) {
      return (
        <div className="test_cases_group">
          <label htmlFor="test_cases_definition_markup_textarea">Test Case - Markup</label>
          <ComponentMarkedTextarea id="test_cases_definition_markup_textarea" className="form-control content_definition same_size_as_parent" rows="10" value={this.state.TestCaseStore.markup.content} results={this.state.TestCaseStore.markup.rows}
            onChange={(value) => {
              this.dispatch(TestCaseStore, new ActionTestCaseMarkupChange(value));
            }}
            />
        </div>
      );
    }
  }
  
  render() {
    if(!this.props.active) {
      return null;
    }
    if(this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound code={this.state.TestCaseStore.error.code} error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else if(this.state.TestCaseStore.testCase) {
      if(this.state.TestCaseStore.markup.definition) {
        return (
          <div className="test_cases_tab_view">
            {this.renderMarkup()}
          </div>
        );
      }
      else {
        return (
          <div className="test_cases_tab_view">
            {this.renderTables()}
          </div>
        );
      }
    }
    else {
      return null;
    }
  }
}
