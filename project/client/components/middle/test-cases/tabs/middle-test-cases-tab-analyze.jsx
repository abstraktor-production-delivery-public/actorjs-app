
'use strict';

import LoginStore from '../../../../stores/login-store';
import TestCaseStore from '../../../../stores/test-case-store';
import { ActionTestCaseAnalyze, ActionTestCaseAnalyzeClear} from '../../../../actions/action-test-case/action-test-case';
import ActorsSorting from '../general-helpers/actors-sorting';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesTabAnalyze extends ReactComponentStore {
  constructor(props) {
    super(props, [TestCaseStore, LoginStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return nextProps.active
      && (
        !this.shallowCompare(this.props.active, nextProps.active)
        || !this.shallowCompare(this.props.repo, nextProps.repo)
        || !this.shallowCompare(this.props.sut, nextProps.sut)
        || !this.shallowCompare(this.props.fut, nextProps.fut)
        || !this.shallowCompare(this.props.tc, nextProps.tc)
        || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
        || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase)
        || !this.shallowCompare(this.state.TestCaseStore.analyze, nextState.TestCaseStore.analyze)
        || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login)
      );
  }
  
  renderAddressesDescription() {
    return (
      <div>
        <h3>Analyze - Addresses</h3>
      </div>
    );
  }

  renderActorDependencyAnalyzeRow(index, phase, name, dependencies) {
    let tdIndex = 0;
    let formattedDependencies = dependencies.map((dependeny) => {
      if(-1 === dependeny) {
        return "phase start";
      }
      else {
        return (dependeny + 1).toString();
      }
    });
    return (
      <tr>
        <td key={++tdIndex} className={`test_case_phase_${phase}`}>{index + 1}</td>
        <td key={++tdIndex} className={`test_case_phase_${phase}`}>{phase}</td>
        <td key={++tdIndex}>{name}</td>
        <td key={++tdIndex}>{formattedDependencies.join(', ')}</td>
      </tr>
    );
  }
  
  renderActorDependencyAnalyzeRows(actor, index, phase) {
    const key = `${actor.name}_${index}`;
    let dependencies = this.state.TestCaseStore.analyze.dependencies.get(key);
    if(undefined !== dependencies) {
      return (
        <React.Fragment key={key}>
          {this.renderActorDependencyAnalyzeRow(index, phase, actor.name, dependencies.dependencies)}
        </React.Fragment>
      );
    }
    else {
      return (
        <React.Fragment key={key}>
          {this.renderActorDependencyAnalyzeRow(index, phase, actor.name, [])}
        </React.Fragment>
      );
    }
  }
  
  renderActorDependenciesAnalyzeTable() {
    const result = ActorsSorting.sortPhase(this.state.TestCaseStore.testCase.tc.actors);
    const actorRowsPre = result.preRows.map((actorData) => {
      return this.renderActorDependencyAnalyzeRows(actorData.actor, actorData.index, actorData.phase);
    });
    const actorRowsExec = result.execRows.map((actorData) => {
      return this.renderActorDependencyAnalyzeRows(actorData.actor, actorData.index, actorData.phase);
    });
    const actorRowsPost = result.postRows.map((actorData) => {
      return this.renderActorDependencyAnalyzeRows(actorData.actor, actorData.index, actorData.phase);
    });    
    return (
      <table className="test_case_table">
        <thead className="">
          <tr>
            <th className="">#</th>
            <th className="">phase</th>
            <th className="">actor name</th>
            <th className="">dependency</th>
          </tr>
        </thead>  
        <tbody className="">
          {actorRowsPre}
          {actorRowsExec}
          {actorRowsPost}
        </tbody>
      </table>
    );
  }
  
  renderActorDependenciesAnalyze() {
    return (
      <div>
        <h3>Analyze - Dependencies</h3>
        {this.renderActorDependenciesAnalyzeTable()}
      </div>
    );
  }
  
  renderTestDataAnalyzeColumns(testData) {
    let index = 1;
    testData = undefined !== testData ? testData : {
      result: '',
      name: '',
      type: '',
      depth: 0,
      value: undefined,
      layer: '',
      default: '',
      raw: undefined,
      children: []
    };
    let resultClass = 'table_header_status';
    if('ok' === testData.result) {
      resultClass += ' header_status_success'
    }
    else if('' !== testData.result) {
      resultClass += ' header_status_failure';
    }
    return (
      <>
        <td key={++index} className={resultClass}>{testData.result}</td>
        <td key={++index} className={resultClass}>{testData.name}</td>
        <td key={++index} className={resultClass}>{testData.type}</td>
        <td key={++index} className={resultClass}>{0 !== testData.depth ? testData.depth : testData.depth}</td>
        <td key={++index} className={resultClass}>{JSON.stringify(testData.value)}</td>
        <td key={++index} className={resultClass}>{testData.layer}</td>
        <td key={++index} className={resultClass}>{testData.default}</td>
        <td key={++index} className={resultClass}>{JSON.stringify(testData.raw)}</td>
      </>
    );
  }
  
  renderTestDataAnalyzeRow(testDatas, actorName) {
    if(undefined === testDatas) {
      return null;
    }
    return testDatas.map((testData, index) => {
      return (
        <tr key={actorName + '_' + (index + 1)}>
          {this.renderTestDataAnalyzeColumns(testData)}
        </tr>
      );
    });
  }
  
  renderTestDataAnalyzeRows(actor, index) {
    let testDatas = this.state.TestCaseStore.analyze.testData.get(actor.name);
    testDatas = undefined !== testDatas ? testDatas : [];
    let length = 0 < testDatas.length ? testDatas.length : 1;
    return (
      <React.Fragment key={actor.name + '_td_' + index}>
        <tr>
          <td className="table_header_status" rowSpan={length}>{actor.name}</td>
          <td className="table_header_status" rowSpan={length}>{actor.type}</td>
          {this.renderTestDataAnalyzeColumns(0 < testDatas.length ? testDatas[0] : undefined)}
        </tr>
        {this.renderTestDataAnalyzeRow(1 < testDatas.length ? testDatas.slice(1) : undefined, actor.name)}
      </React.Fragment>  
    );
  }
  
  renderTestDataAnalyzeTable() {
    let actors = this.state.TestCaseStore.testCase.tc.actors;
    let actorRows = actors.map((actor, index) => {
      return this.renderTestDataAnalyzeRows(actor, index);
    });
    return (
      <table className="test_case_table">
        <thead className="">
          <tr className="">
            <th className="" colSpan="2">Actor</th>
            <th className="" colSpan="7">Test Data</th>
          </tr>
          <tr>
            <th className="">Name</th>
            <th className="">Type</th>
            <th className="">Result</th>
            <th className="">Name</th>
            <th className="">Type</th>
            <th className="">Child</th>
            <th className="">Value</th>
            <th className="">Layer</th>
            <th className="">Default</th>
            <th className="">Raw</th>
          </tr>
        </thead>  
        <tbody className="">
          {actorRows}
        </tbody>
      </table>
    );
  }
  
  renderTestDataAnalyze() {
    return (
      <div>
        <h3>Analyze - Test Data</h3>
        {this.renderTestDataAnalyzeTable()}
      </div>
    );
  }
  
  renderAnalyzeButton() {
    return (
      <Button size="btn-xs" placement="bottom" heading="Analyze" content="Run"
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseAnalyze(this.props.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance));
        }}
      >
        <span className="glyphicon glyphicon-eye-open test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderAnalyzeClearButton() {
    return (
      <Button size="btn-xs" placement="bottom" heading="Analyze" content="Clear"
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseAnalyzeClear());
        }}
      >
        <span className="glyphicon glyphicon-erase test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderAnalyzeTime() {
    if(this.state.TestCaseStore.analyze.time && '' !== this.state.TestCaseStore.analyze.time) {
      return (
        <span className="analyze_time">
          {this.state.TestCaseStore.analyze.time}
        </span>
      );
    }
  }
  
  render() {
    if(!this.props.active) {
      return null;
    }
    if(this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else if(this.state.TestCaseStore.testCase) {
      return (
        <div>
          <div className="analyze_panel">
            <div className="btn-toolbar" role="toolbar" aria-label="...">
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                {this.renderAnalyzeButton()}
                {this.renderAnalyzeClearButton()}
                {this.renderAnalyzeTime()}
              </div>
            </div>
          </div>
          <div className="test_case_tab_view_with_toolbar">
            {this.renderTestDataAnalyze()}
            {this.renderActorDependenciesAnalyze()}
            {/*this.renderAddressesDescription()*/}
          </div>
        </div>
      );
    }
    else {
      return null;
    }
  }
}
