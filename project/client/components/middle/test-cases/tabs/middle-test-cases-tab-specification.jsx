
'use strict';

import TestCaseStore from '../../../../stores/test-case-store';
import TestCaseStores from '../../../../stores/test-cases-store';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import { ActionTestCaseSpecificationMarkup, ActionTestCaseSpecificationMarkupChange, ActionTestCaseSpecificationMarkupCancel, ActionTestCaseSpecificationMarkupSave } from '../../../../actions/action-test-case/action-test-case';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ScrollData from 'z-abs-complayer-markup-client/client/react-components/helper/scroll-data';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesTabSpecification extends ReactComponentStore {
  constructor(props) {
    super(props, [TestCaseStore, TestCaseStores], {
      autoScroll: true,
      breaks: false,
      whiteSpaceClass: 'no_word_wrap',
      documentationOrder: true
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.boundKeyUp = this._keyUp.bind(this);
    this.ctrlKey = false;
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
    this.refDocumentDiv = React.createRef();
    this.refDocumentComponentDocument = React.createRef();
    this.refPreviewDiv = React.createRef();
    this.refPreviewComponentDocument = React.createRef();
    this.refMarkupComponentMarkedTextarea = React.createRef();
    this.scrollData = new ScrollData();
  }
  
  didMount() {
    //window.addEventListener('keydown', this.boundKeyDown, true);
    //window.addEventListener('keyup', this.boundKeyUp, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return nextProps.active
      && (
        !this.shallowCompare(this.props.active, nextProps.active)
        || !this.shallowCompare(this.props.repo, nextProps.repo)
        || !this.shallowCompare(this.props.sut, nextProps.sut)
        || !this.shallowCompare(this.props.fut, nextProps.fut)
        || !this.shallowCompare(this.props.tc, nextProps.tc)
        || !this.shallowCompare(this.state.breaks, nextState.breaks)
        || !this.shallowCompare(this.state.whiteSpaceClass, nextState.whiteSpaceClass)
        || !this.shallowCompare(this.state.TestCasesStore, nextState.TestCasesStore)
        || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
        || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase)
        || !this.shallowCompare(this.state.TestCaseStore.specification, nextState.TestCaseStore.specification)
      );
  }
  
  didUpdate(prevProps, prevState) {
    if(this.state.TestCaseStore.specification.definition) {
      if(!prevState.TestCaseStore.specification.definition || this.state.documentationOrder !== prevState.documentationOrder) {
        const lines = this.state.TestCaseStore.specification.contentLines;
        const lineHeight = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.scrollHeight / lines;
        const visibleLines = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.clientHeight / lineHeight;
        this.scrollData.setLength(Math.floor(lines - visibleLines));
        this.refPreviewComponentDocument.current.scroll(this.scrollData);
        this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
      }
      else {
        const lineDelta = this.state.TestCaseStore.specification.contentLines - prevState.TestCaseStore.specification.contentLines;
        if(0 !== lineDelta) {
          this.scrollData.stepLines(lineDelta);
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }
      }
    }
    else {
      if(null !== this.refDocumentComponentDocument.current) {
        this.refDocumentComponentDocument.current.scroll(this.scrollData);
      }
    }
  }
  
  willUnmount() {
    //window.removeEventListener('keyup', this.boundKeyUp, true);
    //window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _keyDown(e) {
    if(e.ctrlKey) {
      this.ctrlKey = true;
    }
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  _keyUp(e) {
    if(!e.ctrlKey) {
      this.ctrlKey = false;
    }
  }
  
  renderMarkupEditButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button size="btn-xs" placement="bottom" heading="Open" content="Markup" disabled={this.state.TestCaseStore.specification.definition}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseSpecificationMarkup());
          }}
        >
          <span className="glyphicon glyphicon-edit testcase_specification_button_color" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupSaveButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button size="btn-xs" placement="bottom" heading="Save" content="Markup"disabled={this.state.TestCaseStore.specification.content === this.state.TestCaseStore.testCase.tc.specification}
          onClick={(e) => {
           this.dispatch(TestCaseStore, new ActionTestCaseSpecificationMarkupSave(this.props.repo, this.props.sut, this.props.fut, this.props.tc, this.state.TestCaseStore.specification.content));
          }}
        >
          <span className="glyphicon glyphicon-save-file testcase_specification_button_color" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupCloseButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button size="btn-xs" placement="bottom" heading="Cancel" content="Markup" disabled={!this.state.TestCaseStore.specification.definition}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseSpecificationMarkupCancel());
          }}
        >
          <span className="glyphicon glyphicon-remove-circle testcase_specification_button_color" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupHelpButton() {
    return (
      <Button size="btn-xs" placement="bottom" heading="Help" content="Markup" disabled={this.state.TestCaseStore.specification.definition}
        onClick={(e) => {
        }}
      >
        <span className="glyphicon glyphicon-question-sign testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderHorizontalOrVerticalButton() {
    let toolTipInstruction;
    let image1Style;
    let image2Style;
    if(this.state.documentationOrder) {
      toolTipInstruction = 'Vertical';
      image1Style = {top:'7px',left:'0px',transform:'scale(0.91, 0.67)'};
      image2Style = {width:'0px',top:'-2px',left:'-11px',transform:'scale(0.91, 0.67)'};
    }
    else {
      toolTipInstruction = 'Horizontal';
      image1Style = {top:'2px',left:'-6px',transform:'scale(0.91, 0.67)'};
      image2Style = {width:'0px',top:'2px',left:'-6px',transform:'scale(0.91, 0.67)'};
    }
    return (
      <Button size="btn-xs" placement="bottom" heading={`${toolTipInstruction}`} content="Document Order" disabled={!this.state.TestCaseStore.specification.definition}
        onClick={(e) => {
          this.updateState({
            documentationOrder: {$set: !this.state.documentationOrder}
          });
        }}
      >
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image1Style}></span>
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image2Style}></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollFirstLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="First line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineFirst();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollPreviousBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'previous_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollPreviousLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.decrement();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
        >
        <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollNextLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.increment();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
        >
        <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollNextBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'next_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
        >
        <span className="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollEndLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_right" type="transparent" placement="bottom" heading="Goto" content="End line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineLast();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
        >
        <span className="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkup() {
    return (
      <div className={`markup_filter_${this.state.documentationOrder ? 'horizontal' : 'vertical'}_2`}>
        <div className="test_cases_group">
          <div className="markup_heading">
            <p className="test_cases_specification">Specification - Markup</p>
            <p className="markup_checkbox">
              auto-scroll:
            </p>
            <input type="checkbox" id="test_cases_specification_markup_auto-scroll" className="markup_checkbox" aria-label="..." autoComplete="off" checked={this.state.autoScroll}
              onChange={(e) => {
                if(this.state.autoScroll) {
                  this.updateState({
                    autoScroll: {$set: false}
                  });
                }
                else {
                  this.updateState({
                    autoScroll: {$set: true}
                  });
                }
              }}
              />
            <p className="markup_checkbox">
              breaks:
            </p>
            <input type="checkbox" id="test_cases_specification_markup_breaks" className="markup_checkbox" aria-label="..." autoComplete="off" checked={this.state.breaks}
              onChange={(e) => {
                if(this.state.breaks) {
                  this.updateState({
                    breaks: {$set: false},
                    whiteSpaceClass: {$set: 'no_word_wrap'}
                  });
                }
                else {
                  this.updateState({
                    breaks: {$set: true},
                    whiteSpaceClass: {$set: 'word_wrap'}
                  });
                }
              }}
              />
            {this.renderSpecificationMarkupScrollEndLine()}
            {this.renderSpecificationMarkupScrollNextBlock()}
            {this.renderSpecificationMarkupScrollNextLine()}
            {this.renderSpecificationMarkupScrollPreviousLine()}
            {this.renderSpecificationMarkupScrollPreviousBlock()}
            {this.renderSpecificationMarkupScrollFirstLine()}
          </div>
          <ComponentMarkedTextarea ref={this.refMarkupComponentMarkedTextarea} id="test_cases_specification_markup_textarea" className={this.state.whiteSpaceClass + " form-control content_definition"} rows="10" value={this.state.TestCaseStore.specification.content}
            onScroll={(e) => {
              const lines = this.state.TestCaseStore.specification.contentLines;
              const scrollLines = (e.target.scrollHeight - e.target.clientHeight) / e.target.scrollHeight * lines;
              const scrollLine = e.target.scrollTop / (e.target.scrollHeight - e.target.clientHeight) * scrollLines;
              const line = Math.floor(scrollLine);
              this.scrollData.setLine(line, scrollLine - line);
              if(this.state.autoScroll) {
                this.refPreviewComponentDocument.current.scroll(this.scrollData);
              }
            }}
            onChange={(value) => {
              this.dispatch(TestCaseStore, new ActionTestCaseSpecificationMarkupChange(value));
            }}
            />
        </div>
      </div>
    );
  }
  
  renderSpecificationPreview() {
    const storeState = this.state.TestCaseStore.specification.documentationPreview;
    const lines = this.state.TestCaseStore.specification.contentLines;
    return (
      <div className={`markup_filter_${this.state.documentationOrder ? 'horizontal' : 'vertical'}_2`}>
        <div className="test_cases_group">
          <div className="markup_heading">
            <p className="test_cases_specification">Specification - Preview</p>
          </div>
          <div ref={this.refPreviewDiv} id="test_cases_specification_markup_preview" className="test_cases_specification_preview"
            onScroll={(e) => {
              this.scrollData.setScroll(e.target.scrollTop);
            }}
            >
            <div className="doc_nav_preview_middle">
              <div className="doc_nav_preview_inner"
                onMouseUp={(e) => {
                  if(this.ctrlKey) {
                    this.scrollData.setScroll(e.target.offsetTop);
                    this.refPreviewComponentDocument.current.calculateTop(this.scrollData);
                    this.refPreviewComponentDocument.current.scroll( this.scrollData);
                    this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
                  }
                }}
                >
                <ComponentDocument ref={this.refPreviewComponentDocument} preview lines={lines} document={storeState.document}
                  onEditorScroll={(top) => {
                    this.refPreviewDiv.current.scrollTop = top;
                  }}
                  />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  renderSpecification() {
    if(this.state.TestCaseStore.specification.definition) {
      return (
        <div className="test_cases_tab_view_inner">
          {this.renderSpecificationMarkup()}
          {this.renderSpecificationPreview()}
        </div>
      );
    }
    else {
      return (
        <div ref={this.refDocumentDiv} className="test_cases_tab_view_inner"
          onScroll={(e) => {
            this.scrollData.setScroll(e.target.scrollTop);
          }}
        >
        <ComponentDocument ref={this.refDocumentComponentDocument} document={this.state.TestCaseStore.specification.document}
          onEditorScroll={(top) => {
            this.refDocumentDiv.current.scrollTop = top;
          }}
        />
        </div>
      );
    }
  }
  
  renderDescription() {
    return (
      <div className="test_cases_specification_description">
        <h3 className="test_cases_specification">Test Case:&#160;</h3>
        <h3 className="test_cases_specification_sut">{this.state.TestCaseStore.testCase.name}</h3>
        <p className="test_cases_specification_data">Description:&#160;</p>
        <p>{this.state.TestCaseStore.testCase.description}</p>
        <p className="test_cases_specification_data_last">Requirements:&#160;</p>
        <p>RQ_00001,&nbsp;RQ_00002</p>
      </div>
    );
  }
  
  renderMarkup() {
    return (
      <div className="test_cases_specification_markup">
        <div className="test_cases_markup_panel">
          <div className="btn-toolbar" role="toolbar" aria-label="...">
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderMarkupEditButton()}
              {this.renderMarkupSaveButton()}
              {this.renderMarkupHelpButton()}
              {this.renderMarkupCloseButton()}
            </div>
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderHorizontalOrVerticalButton()}
            </div>
          </div>
        </div>
        {this.renderSpecification()}
      </div>
    );
  }
  
  render() {
    if(!this.props.active) {
      return null;
    }
    if(this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound code={this.state.TestCaseStore.error.code} error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else if(this.state.TestCaseStore.testCase) {
      return (
        <div className="test_cases_tab_view test_cases_tab_view_specification_outer">
          {this.renderDescription()}
          {this.renderMarkup()} 
        </div>
      );
    }
    else {
      return null;
    }
  }
}
