 
'use strict';

import TestCaseStore from '../../../../stores/test-case-store';
import { ActionTestCasePersistentDataUpdate } from '../../../../actions/action-test-case/action-test-case';
import { ActionTestCaseDebugBreakpointSet, ActionTestCaseDebugBreakpointClear, ActionTestCaseDebugBreakpointClearAll, ActionTestCaseDebugBreakpointUpdate, ActionTestCaseActorsFileGet, ActionTestCaseDebugContinue, ActionTestCaseDebugPause, ActionTestCaseDebugStepOver, ActionTestCaseDebugStepIn, ActionTestCaseDebugStepOut, ActionTestCaseDebugLevel, ActionTestCaseDebugGetMembers, ActionTestCaseDebugScopeOpen, ActionTestCaseDebugGetProperty, ActionTestCaseDebugClearCurrentValue} from '../../../../actions/action-test-case/action-test-case';
import MiddleTestCasesNotFound from '../middle-test-cases-not-found';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import CodeMirrorDebugVariable from 'z-abs-complayer-codemirror-client/client/code-mirror-debug-variable';
import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/client/code-mirror-editor';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import React from 'react';


export default class MiddleTestCasesTabDebug extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestCaseStore], {
      activeKey: 0,
      callstackOpen: true,
      scopeOpen: true,
      breakpointsOpen: true
    });
    this.helperTab = new HelperTab();
    this.firstKey = false;
    this.boundKeyDown = this._keyDown.bind(this);
    this.boundMouseUp = this._boundMouseUp.bind(this);
    this.boundMouseMove = this._boundMouseMove.bind(this);
    this.boundResize = this._boundResize.bind(this);
    this.sidebarNode = null;
    this.codeNode = null;
    this.codeWidthMin = 360;
    this.sidebarWidthMin = 180;
    this.sidebarWidthMax = 720;
  }
    
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
    window.addEventListener('resize', this.boundResize);
  }
  
  shouldUpdate(nextProps, nextState) {
    return nextProps.active
      && (
        !this.shallowCompare(this.props.active, nextProps.active)
        || !this.shallowCompare(this.props.file, nextProps.file)
        || !this.shallowCompare(this.props.ext, nextProps.ext)
        || !this.shallowCompare(this.props.tab, nextProps.tab)
        || !this.shallowCompare(this.props.repo, nextProps.repo)
        || !this.shallowCompare(this.props.sut, nextProps.sut)
        || !this.shallowCompare(this.props.fut, nextProps.fut)
        || !this.shallowCompare(this.props.tc, nextProps.tc)
        || !this.shallowCompare(this.state.TestCaseStore.error, nextState.TestCaseStore.error)
        || !this.shallowCompare(this.state.TestCaseStore.dbugger, nextState.TestCaseStore.dbugger)
        || !this.shallowCompare(this.state.TestCaseStore.execution, nextState.TestCaseStore.execution) && nextProps.active
        || !this.shallowCompare(this.state.TestCaseStore.realtimeConnection, nextState.TestCaseStore.realtimeConnection) && nextProps.active
        || !this.shallowCompare(this.state.TestCaseStore.markup, nextState.TestCaseStore.markup) && nextProps.active
        || !this.shallowCompare(this.state.TestCaseStore.testCase, nextState.TestCaseStore.testCase)
        || !this.shallowCompare(this.state.TestCaseStore.persistentData, nextState.TestCaseStore.persistentData)
        || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
        || !this.shallowCompare(this.state.callstackOpen, nextState.callstackOpen)
        || !this.shallowCompare(this.state.scopeOpen, nextState.scopeOpen)
        || !this.shallowCompare(this.state.breakpointsOpen, nextState.breakpointsOpen)
      );
  }
  
  didUpdate(prevProps, prevState) {
    if(!this.shallowCompare(prevState.TestCaseStore.dbugger.actorFiles, this.state.TestCaseStore.dbugger.actorFiles)) {
      this.helperTab = new HelperTab();
      this.state.TestCaseStore.dbugger.actorFiles.forEach((file) => {
        this.helperTab.add(file.title);
      });
    }
    if(!this.firstKey) {
      const index = this.helperTab.getIndex(`${this.props.file}.${this.props.ext}`);
      if(index && prevState.activeKey !== index) {
        this.updateState({activeKey: {$set: index}});
        this.firstKey = true;
      }
    }
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
    window.removeEventListener('resize', this.boundResize);
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.TEST_CASE_ACTOR_INDEX === msg.msgId) {
      this.updateState({activeKey: {$set: msg.index}});
    }
  }
  
  _keyDown(e) {
    if(!e.ctrlKey && 'F5' === e.key) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if(TestCaseStore.EXECUTION_RUNNING === this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
        this.dispatch(TestCaseStore, new ActionTestCaseDebugContinue());
      }
    }
    else if(!e.ctrlKey && 'F6' === e.key) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if(TestCaseStore.EXECUTION_RUNNING === this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
        this.dispatch(TestCaseStore, new ActionTestCaseDebugStepOver());
      }
    }
    else if(!e.ctrlKey && 'F7' === e.key) {
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      if(TestCaseStore.EXECUTION_RUNNING === this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
        this.dispatch(TestCaseStore, new ActionTestCaseDebugStepIn());
      }
    }
    else if(!e.ctrlKey && 'F8' === e.key) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if(TestCaseStore.EXECUTION_RUNNING === this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
        this.dispatch(TestCaseStore, new ActionTestCaseDebugStepOut());
      }
    }
    else if(!e.ctrlKey && 'F9' === e.key) {
      e.preventDefault();
      e.stopImmediatePropagation();
      if(TestCaseStore.EXECUTION_RUNNING === this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_RUNNING === this.state.TestCaseStore.dbugger.state) {
        this.dispatch(TestCaseStore, new ActionTestCaseDebugPause());
      }
    }
  }
  
  _boundMouseUp(e) {
    window.removeEventListener('mouseup', this.boundMouseUp, true);
    window.removeEventListener('mousemove', this.boundMouseMove, true);
  }
  
  _boundMouseMove(e) {
    const computedSidebarNode = window.getComputedStyle(this.sidebarNode);
    const computedCodeNode = window.getComputedStyle(this.codeNode);
    const sidebarNodeWidth = Number.parseInt(computedSidebarNode.width);
    const codeNodeWidth = Number.parseInt(computedCodeNode.width);
    const windowWidth = window.innerWidth - 8;
    const previousWidth = windowWidth - sidebarNodeWidth - codeNodeWidth;
    let newSidebarWidth = windowWidth - e.clientX;
    if(newSidebarWidth < this.sidebarWidthMin) {
      newSidebarWidth = this.sidebarWidthMin;
    }
    else if(newSidebarWidth > this.sidebarWidthMax) {
      newSidebarWidth = this.sidebarWidthMax;
    }
    const newCodeWidth = windowWidth - previousWidth - newSidebarWidth;
    this.sidebarNode.style.left = `calc(100% - ${newSidebarWidth + 4}px)`;
    this.sidebarNode.style.width = `${newSidebarWidth}px`;
    this.codeNode.style.width = `calc(100% - ${newSidebarWidth + 4}px)`;
  }
  
  _boundResize() {
    if(this.codeNode && this.sidebarNode) {
      // TODO: Implement when width less tman minimun
    }
  }
  
  renderTab() {
    const result = [];
    let key = 0;
    this.state.TestCaseStore.dbugger.actorFiles.forEach((value, index) => {
      let title = value.title;
      let name = value.url;
      if(TestCaseStore.DEBUGGER_NOT_RUNNING !== this.state.TestCaseStore.dbugger.state && index === this.state.TestCaseStore.dbugger.actor.index) {
        let stackLevel = this.state.TestCaseStore.dbugger.stack[this.state.TestCaseStore.dbugger.level];
        title = stackLevel.scriptName;
        if(0 !== this.state.TestCaseStore.dbugger.level || title !== value.title) {
          name = stackLevel.url;
        }
      }
      result.push(
        <Tab tab={`${this.props.file && this.props.ext ? '' : this.props.tab + '/'}${title}`} title={title} key={index}>
          <CodeMirrorEditor id={`test_cases_debug_${index}`} key={index} name={name} docKey={index} code={value.content} options={this.getOptions(value.type, index)} dbugger={this.state.TestCaseStore.dbugger} darkMode={this.state.TestCaseStore.persistentData.darkMode}
            onGutterChange={(docKey, lineNumber, exist) => {
              if(exist) {
                this.dispatch(TestCaseStore, new ActionTestCaseDebugBreakpointClear(this.props.repo, this.props.sut, this.props.fut, this.props.tc, {
                  docKey: docKey,
                  name: name,
                  lineNumber: lineNumber
                }));
              }
              else {
                this.dispatch(TestCaseStore, new ActionTestCaseDebugBreakpointSet(this.props.repo, this.props.sut, this.props.fut, this.props.tc, {
                  docKey: docKey,
                  name: name,
                  title: title,
                  pauseOnBreak: true,
                  lineNumber: lineNumber
                }));
              }
            }}
            onGetCurrentValueByName={(memberArray, type) => {
              let object = this.state.TestCaseStore.dbugger.stack[this.state.TestCaseStore.dbugger.level];
              if('this' === type) {
                this.dispatch(TestCaseStore, new ActionTestCaseDebugGetProperty(object.this, memberArray, type, 'this'));
              }
              else if('keyword' === type) {
                
              }
              else if('local' === type) {
                let foundObject = this._findDebugValue(object, 'local', memberArray);
                if(!foundObject) {
                  foundObject = this._findDebugValue(object, 'block', memberArray);
                  if(!foundObject) {
                    foundObject = this._findDebugValue(object, 'closure', memberArray);
                  }
                }
                if(foundObject) {
                  this.dispatch(TestCaseStore, new ActionTestCaseDebugGetProperty(foundObject.value, memberArray, type, foundObject.name));
                }
              }  
            }}
            onClearCurrentValue={() => {
              this.dispatch(TestCaseStore, new ActionTestCaseDebugClearCurrentValue());
            }}
            onGetMembers={(object, open) => {
              this.dispatch(TestCaseStore, new ActionTestCaseDebugGetMembers(object, open));
            }}
          />
        </Tab>
      );
    });
    return result;
  }

  _renderTabs() {
    return (
      <Tabs id="actor_debug_tabs"  activeKey={this.state.activeKey || 0} darkMode={this.state.TestCaseStore.persistentData.darkMode}
        onSelect={(selectedKey) => {
          if(this.state.activeKey !== selectedKey) {
            const file = this.state.TestCaseStore.dbugger.actorFiles.get(selectedKey);
            this.dispatch(TestCaseStore, new ActionTestCaseActorsFileGet([`${file.path}/${file.title}`], selectedKey));
            this.updateState({activeKey: {$set: selectedKey}});
            this.context.history(`${file.title}`);
          }
        }}
        >
        {this.renderTab()}
      </Tabs>
    );
  }

  renderTabs() {
    const activeKey = -1 !== this.state.activeKey ? this.state.activeKey : 0;
    const debugTabs = TestCaseStore.DEBUGGER_RUNNING === this.state.TestCaseStore.dbugger.state || TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state;
    if(debugTabs) {
	    return (
        <div className={'debug_tab_view debug_tabs'}>
          {this._renderTabs()}
        </div>
    	);
    }
    else {
      return (
        <div className={'debug_tab_view'} style={{width:'100%'}}>
          {this._renderTabs()}
        </div>
    	);
    }
  }

  getOptions(type, index) {
    return {
      lineNumbers: true,
      highlightActiveLineGutter: true,
      highlightActiveLine: false,
      closeBrackets: true,
      bracketMatching: true,
      drawSelection: true,
      
      tabKeyAction: 'indent', 
      tabSize: 2,
      indentType: 'spaces',
      indentUnit: 2,

      readOnly: true,
      breakpointGutter: true,
      linesBreakpoint: true,
      foldGutter: false,
      debugTooltip: true,

      darkMode: true,
      
      type
    };
  }
  
  renderDebugButtonContinue() {
    const enabled = this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state;
    return (
      <Button size="btn-xs" placement="bottom" heading="Debug" content="Continue" shortcut="F5" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugContinue());
        }}
      >
        <span className="glyphicon glyphicon-play test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDebugButtonStepOver() {
    const enabled = this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state;
    return (
      <Button size="btn-xs" placement="bottom" heading="Debug" content="Step over" shortcut="F6" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugStepOver());
        }}
      >
        <span className="glyphicon glyphicon-arrow-right test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDebugButtonStepIn() {
    const enabled = this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state;
    return (
      <Button size="btn-xs" placement="bottom" heading="Debug" content="Step in" shortcut="F7" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugStepIn());
        }}
      >
        <span className="glyphicon glyphicon-arrow-down test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDebugButtonStepOut() {
    const enabled = this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state;
    const nextClassName = ``;
    return (
      <Button size="btn-xs" placement="bottom" heading="Debug" content="Step out" shortcut="F8" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugStepOut());
        }}
      >
        <span className="glyphicon glyphicon-arrow-up test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDebugButtonPause() {
    const enabled = this.state.TestCaseStore.execution && TestCaseStore.DEBUGGER_RUNNING === this.state.TestCaseStore.dbugger.state;
    return (
      <Button size="btn-xs" placement="bottom" heading="Debug" content="Pause" shortcut="F9" disabled={!enabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugPause());
        }}
      >
        <span className="glyphicon glyphicon-pause test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderClearAllBreakpoints() {
    const disabled = this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.definition || (0 !== this.state.TestCaseStore.execution);
    return (
      <Button size="btn-xs" placement="bottom" heading="Breakpoints" content="Clear All" disabled={disabled}
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugBreakpointClearAll(this.props.repo, this.props.sut, this.props.fut, this.props.tc));
        }}
      >
        <span className="glyphicon glyphicon-erase test_case_breakpoints" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDebugStack() {
    let stack = [];
    if(TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
      this.state.TestCaseStore.dbugger.stack.forEach((stackLevel, index) => {
        stack.push(
          <li key={index} role="presentation" className={`debug_stack${this.state.TestCaseStore.dbugger.level === index ? ' active' : ''}`}>
            <a className="debug_stack" href="#"
              onClick={(e) => {
                e.preventDefault();
                this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index, this.state.TestCaseStore.dbugger.actor.index));
              }}>
              <p className="debug_stack_left">
                {stackLevel.functionName}
              </p>
              <p className="debug_stack_right">
                {' ' + stackLevel.scriptName + ': ' + (stackLevel.location.lineNumber + 1)}
              </p>
            </a>
          </li>
        );
      });
    }
    let svgRef;
    let svgClass = this.state.callstackOpen ? 'debug_body_open' : 'debug_body_closed';
    let divClass = this.state.callstackOpen ? 'debug_body_open' : 'debug_body_closed';
    return (
      <div className="panel panel-default debug_debug_group">
        <div className="panel-heading debug_stack_heading">
          <svg ref={(svg) => { svgRef = svg; }} className={svgClass} width="9" height="9"
            onClick={(e) => {
              this.updateState({callstackOpen: {$set: !this.state.callstackOpen}});
            }
          }>
            <g>
              <polygon points="1,1 1,7 7,4" stroke="Grey" strokeWidth="1" fill="white" />
            </g>
          </svg>
          <p className="debug_debug_group">
            Call Stack
          </p>
        </div>
        <div className={`panel-body debug_stack_body ${divClass}`}>
          <ul className="nav nav-pills nav-stacked">
            {stack}
          </ul>
        </div>
      </div>
    );
  }
  
  renderDebugScope(scope, name, index) {
    const scopesOpen = this.state.TestCaseStore.dbugger.scopesOpen;
    const open = scopesOpen.get(name).open[index];
    return  (
      <CodeMirrorDebugVariable key={`${name}_${index}`} name={scope.type} value={scope} objectValues={this.state.TestCaseStore.dbugger.objectValues} open={open} 
        onGetMembers={(object, open) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugGetMembers(object, open));
        }}
        onOpen={(scope, open) => {
          this.dispatch(TestCaseStore, new ActionTestCaseDebugScopeOpen(name, index, open));
        }}
      />
    );
  }
  
  renderDebugScopes() {
    const debugData = this.state.TestCaseStore.dbugger.stack[this.state.TestCaseStore.dbugger.level];
    if(!debugData.scopes) {
      return null;
    }
    else {
      const results = [];
      const nameInstances = new Map();
      debugData.scopes.forEach((scope, index) => {
        const name = scope.type + '_' + (scope.name ? scope.name : '');
        if(nameInstances.has(name)) {
          ++nameInstances.get(name).index;
        }
        else {
          nameInstances.set(name, {index: 0});
        }
        results.push(this.renderDebugScope(scope, name, nameInstances.get(name).index));
      });
      return results;
    }
  }
  
  renderDebugScopeThis() {
    const dbugger = this.state.TestCaseStore.dbugger;
    const debugData = dbugger.stack[this.state.TestCaseStore.dbugger.level];
    if(debugData.this) {
      const name = 'this_' + debugData.this.className;
      const scopesOpen = dbugger.scopesOpen;
      const open = scopesOpen.get(name).open[0];
      return  (
        <CodeMirrorDebugVariable key="this" name="this" value={debugData.this} objectValues={dbugger.objectValues} open={open} 
          onGetMembers={(object, open) => {
            this.dispatch(TestCaseStore, new ActionTestCaseDebugGetMembers(object, open));
          }}
          onOpen={(scope, open) => {
            this.dispatch(TestCaseStore, new ActionTestCaseDebugScopeOpen(name, 0, open));
          }}
        />
      );
    }
  }
  
  renderDebugAllScopesData(divClass) {
    if(TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
      const divClass = this.state.scopeOpen ? 'debug_body_open' : 'debug_body_closed';
      return (
        <div className={`panel-body debug_scope_body ${divClass}`}>
          {this.renderDebugScopes()}
          {this.renderDebugScopeThis()}
        </div>
      );
    }
  }
  
  renderDebugAllScopes() {
    const svgClass = this.state.scopeOpen ? 'debug_body_open' : 'debug_body_closed';
    return (
      <div className="panel panel-default debug_debug_group">
        <div className="panel-heading debug_scope_heading">
          <svg className={svgClass} width="9" height="9"
            onClick={(e) => {
              this.updateState({scopeOpen: {$set: !this.state.scopeOpen}});
            }
          }>
            <g>
              <polygon points="1,1 1,7 7,4" stroke="Grey" strokeWidth="1" fill="white" />
            </g>
          </svg>
          <p className="debug_debug_group">
            Scope
          </p>
        </div>
        {this.renderDebugAllScopesData()}
      </div>
    );
  }

  renderDebugBreakpointValue(breakpoint, index) {
    let className = (this.state.TestCaseStore.dbugger.stack[0].location.lineNumber === breakpoint.lineNumber &&
    this.state.TestCaseStore.dbugger.stack[0].scriptName === breakpoint.title) ? 'active' : undefined;
    return (
      <li key={index} role="presentation" className={className}>
        <a className="debug_breakpoint" href="#"
          onClick={(e) => {
            e.preventDefault();
      //      this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
        }}>
          <input type="checkbox" className="debug_breakpoint" aria-label="..." autoComplete="off" checked={breakpoint.pauseOnBreak}
            onChange={(e) => {
              breakpoint.pauseOnBreak = !breakpoint.pauseOnBreak;
              this.dispatch(TestCaseStore, new ActionTestCaseDebugBreakpointUpdate(this.props.repo, this.props.sut, this.props.fut, this.props.tc, breakpoint));
            }}
          />
          <p className="debug_breakpoint">
            {breakpoint.title}:{(breakpoint.lineNumber + 1)}
          </p>
        </a>
      </li>
    );
  }
  
  renderDebugBreakpointValues() {
    if(TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
      let divClass = this.state.breakpointsOpen ? 'debug_body_open' : 'debug_body_closed';
      let debugBreakpoints = this.state.TestCaseStore.dbugger.breakpoints.map((breakpoint, index) => {
        return this.renderDebugBreakpointValue(breakpoint, index);
      });
      return (
        <div className={`panel-body debug_breakpoint_body ${divClass}`}>
          <ul className="nav nav-pills nav-stacked">
            {debugBreakpoints}
          </ul>
        </div>
      );
    }
  }
  
  renderDebugBreakpoints() {
    let svgRef;
    let svgClass = this.state.breakpointsOpen ? 'debug_body_open' : 'debug_body_closed';
    return (
      <div className="panel panel-default debug_debug_group">
        <div className="panel-heading debug_breakpoint_heading">
          <svg ref={(svg) => { svgRef = svg; }} className={svgClass} width="9" height="9"
            onClick={(e) => {
              this.updateState({breakpointsOpen: {$set: !this.state.breakpointsOpen}});
            }
          }>
            <g>
              <polygon points="1,1 1,7 7,4" stroke="Grey" strokeWidth="1" fill="white" />
            </g>
          </svg>
          <p className="debug_debug_group">
            Breakpoints
          </p>
        </div>
        {this.renderDebugBreakpointValues()}
      </div>
    );
  }
  
  renderDebug() {
    if(TestCaseStore.DEBUGGER_RUNNING === this.state.TestCaseStore.dbugger.state || TestCaseStore.DEBUGGER_BREAK === this.state.TestCaseStore.dbugger.state) {
      return (
        <div className="debug_sidebar">
          <div className="debug_debug_mover"
            onMouseDown={(e) => {
              if(0 === e.button) {
                this.sidebarNode = e.target.parentNode;
                this.codeNode = this.sidebarNode.previousSibling;
                window.addEventListener('mouseup', this.boundMouseUp, true);
                window.addEventListener('mousemove', this.boundMouseMove, true);
              }
            }}
            ></div>
          <div className="debug_debug">
            {this.renderDebugStack()}
            {this.renderDebugAllScopes()}
            {this.renderDebugBreakpoints()}
          </div>
        </div>
      );
    }
  }

  renderButtonDarkMode() {
    const darkMode = this.state.TestCaseStore.persistentData.darkMode;
    return (
      <Button active={darkMode} placement="bottom" heading="View" content="Dark mode"
        onClick={(e) => {
          this.dispatch(TestCaseStore, new ActionTestCasePersistentDataUpdate('', 'darkMode', !darkMode));
        }}
      >
        <span className="glyphicon glyphicon-certificate" aria-hidden="true"></span>
      </Button>
    );
  }
  
  render () {
    if(!this.props.active) {
      return null;
    }
    if(this.state.TestCaseStore.error) {
      return (
        <div className="test_cases_tab_view">
          <MiddleTestCasesNotFound code={this.state.TestCaseStore.error.code} error={this.state.TestCaseStore.error} sut={this.props.sut} fut={this.props.fut} tc={this.props.tc} repo={this.props.repo} />
        </div>
      );
    }
    else {
      const darkMode = this.state.TestCaseStore.persistentData.darkMode;
      return (
        <div className="same_size_as_parent">
          <div className="debug_panel">
            <div className="btn-toolbar" role="toolbar" aria-label="...">
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                {this.renderDebugButtonContinue()}
                {this.renderDebugButtonStepOver()}
                {this.renderDebugButtonStepIn()}
                {this.renderDebugButtonStepOut()}
                {this.renderDebugButtonPause()}
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                {this.renderClearAllBreakpoints()}
              </div>
              <div className="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                {this.renderButtonDarkMode()}
              </div>
            </div>
          </div>
          {this.renderTabs()}
          {this.renderDebug()}
        </div>
      );
    }
  }
  
  _findDebugValue(object, scopeName, memberArray) {
    for(let i = 0; i < object.scopes.length; ++i) {
      if(object.scopes[i].type === scopeName) {
        let foundScopeObject = object.scopes[i].object;
        for(let j = 0; j < foundScopeObject.length; ++j) {
          if(foundScopeObject[j].name === memberArray[0]) {
            return foundScopeObject[j];
          }
        }
      }
    }
  }
}


MiddleTestCasesTabDebug.contextType = RouterContext;
