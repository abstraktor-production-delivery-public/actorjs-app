
'use strict';

import FilterStore from '../../../stores/filter-store';
import TestCaseStore from '../../../stores/test-case-store';
import { ActionTestCaseRecentGet } from '../../../actions/action-test-case/action-test-case';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewRecent extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, TestCaseStore]);
    this.refRecent = React.createRef();
  }
  
  didMount() {
    this.dispatch(TestCaseStore, new ActionTestCaseRecentGet());
    if(this.props.scroll) {
      this.refRecent.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.TestCaseStore.recentTestCases, nextState.TestCaseStore.recentTestCases);
  }
    
  willUnmount() {
    this.props.onRestoreData && this.props.onRestoreData(this.refRecent.current.scrollTop);
  }
  
  renderRecentTestCasesRows() {
    let recentTestCasesRows = this.state.TestCaseStore.recentTestCases.map((recentTc, i) => {
    return (
      <tr key={`tc.${recentTc.sut}.${recentTc.fut}.${recentTc.tc}`}>
        <th scope="row">{i + 1}</th>    
        <td>
          <Link className="test_case_link" href={`/${recentTc.sut}/${recentTc.fut}/${recentTc.tc}`}>
            {`tc.${recentTc.sut}.${recentTc.fut}.${recentTc.tc}`}
          </Link>
        </td>
        <td>{recentTc.repo}</td>
      </tr>)
    })
  	return recentTestCasesRows;
  }
  
  render() {
    return (
      <div ref={this.refRecent} className="test_case_column">
        <div className="test_cases_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Recent</h3>
            </div>
            <table id="tc_recent_table" className="table test_cases_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Repo</th>
                </tr>
              </thead>
              <tbody>
                {this.renderRecentTestCasesRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
