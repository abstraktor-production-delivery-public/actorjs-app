
'use strict';

import MiddleTestCasesToolbar from './middle-test-cases-toolbar';
import MiddleSidebar from '../middle_sidebar';
import MiddleTestCasesViewColumns from './middle-test-cases-view-columns';
import TestCaseStore from '../../../stores/test-case-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionTestCaseMounted, } from '../../../actions/action-test-case/action-test-case';
import { ActionSystemUnderTestClear } from '../../../actions/action-system-under-test/action-system-under-test';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCases extends ReactComponentStore {
  constructor(props) {
    super(props, [], {
      name: null
    });
    this.setTitle(() => {
      return {
        isPath: false,
        text: this.state.name ? this.state.name : 'Test Case',
        prefix: this.state.name ? 'TC: ' : 'ActorJs'
      };
    });
  }
  
  didMount() {
    this.dispatch(TestCaseStore, new ActionTestCaseMounted());
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.name, nextState.name);
  }
  
  willUnmount() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestClear());
  }
  
  render() {
    return (
      <div className="middle">
        <Route switch={["/:sut/:fut/:tc/:tab*", "/:sut/:fut/:tc", "/:sut/:fut", "/:sut", ""]} handler={MiddleTestCasesToolbar} />
        <MiddleSidebar />
        <div className="middle_view middle_view_test_cases">
          <Route switch={["/:sut/:fut/:tc*", "/:sut/:fut", "/:sut", ""]} handler={MiddleTestCasesViewColumns} onTcName={(name) => {
            this.updateState({name: {$set: name}});
          }}/>
        </div>
      </div>
    );
  }
}


MiddleTestCases.contextType = RouterContext;
