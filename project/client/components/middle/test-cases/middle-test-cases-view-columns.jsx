
'use strict';

import SystemUnderTestStore from '../../../stores/system-under-test-store';
import TestCaseStore from '../../../stores/test-case-store';
import TestCasesStore from '../../../stores/test-cases-store';
import MiddleTestCasesViewRecent from './middle-test-cases-view-recent';
import MiddleTestCasesViewSut from './middle-test-cases-view-sut';
import MiddleTestCasesViewFut from './middle-test-cases-view-fut';
import MiddleTestCasesViewTc from './middle-test-cases-view-tc';
import MiddleTestCasesViewTestCase from './middle-test-cases-view-test-case';
import { ActionSystemUnderTestSet } from '../../../actions/action-system-under-test/action-system-under-test';
import { ActionTestCasesSetView } from '../../../actions/action-test-cases';
import MiddleNotFound from '../middle-not-found';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ComponentBreadcrump from 'z-abs-complayer-bootstrap-client/client/breadcrump';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesViewColumns extends ReactComponentStore {
  constructor(props) {
    super(props, [TestCaseStore, SystemUnderTestStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.scrollRecent = 0;
    this.scrollFut = 0;
    this.scrollSut = 0;
    this.scrollTc = 0;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
    this.setView();
    if(this.props.onTcName && this.props.tc) {
      this.props.onTcName(this.props.tc);
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.tc, nextProps.tc)
      || this.state.TestCaseStore.markup.definition !== nextState.TestCaseStore.markup.definition
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests);
  }
  
  didUpdate(prevProps) {
    this.setView();
    if(this.props.tc !== prevProps.tc) {
      this.props.onTcName(this.props.tc);
    }
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _keyDown(e) {
    if(e.altKey && 'c' === e.key && !this._disabledCopy()) {
      e.preventDefault();
      this._copyMarkupName(e);      
    }
  }
  
  setView() {
    if(undefined === this.props.sut) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(null));
      this.dispatch(TestCasesStore, new ActionTestCasesSetView('none'));
    }
    else if(undefined === this.props.fut) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestCasesStore, new ActionTestCasesSetView('sut'));
    }
    else if(undefined === this.props.tc) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestCasesStore, new ActionTestCasesSetView('fut'));
    }
    else {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestCasesStore, new ActionTestCasesSetView('tc'));
    }
  }
  
  renderTestCases() {
    return (
      <div className="test_case_columns">
        <MiddleTestCasesViewRecent  />
        <MiddleTestCasesViewSut />
      </div>
    );
  }
  
  renderTestCasesSut(repo) {
    return (
      <div className="test_case_columns">
        <MiddleTestCasesViewRecent />
        <MiddleTestCasesViewSut />
        <MiddleTestCasesViewFut repo={repo} sut={this.props.sut} />
      </div>
    );
  }
  
  renderTestCasesFut(repo) {
    return (
      <div className="test_case_columns">
        <MiddleTestCasesViewRecent scroll={this.scrollRecent}
          onRestoreData={(scrollTop) => {
            this.scrollRecent = scrollTop;
          }}
        />
        <MiddleTestCasesViewSut scroll={this.scrollSut}
          onRestoreData={(scrollTop) => {
            this.scrollSut = scrollTop;
          }}
        />
        <MiddleTestCasesViewFut repo={repo} sut={this.props.sut} scroll={this.scrollFut}
          onRestoreData={(scrollTop) => {
            this.scrollFut = scrollTop;
          }}
        />
        <MiddleTestCasesViewTc repo={repo} sut={this.props.sut} fut={this.props.fut} scroll={this.scrollTc}
          onRestoreData={(scrollTop) => {
            this.scrollTc = scrollTop;
          }}
        />
      </div>
    );
  }
  
  renderTestCasesTc(repo) {
    return (
      <Route switch={["/:tab*", ""]} sut={this.props.sut} fut={this.props.fut} repo={repo} tc={this.props.tc} handler={MiddleTestCasesViewTestCase} />
    );
  }
  
  renderColumns() {
    const sut = !this.props.sut ? undefined : this.state.SystemUnderTestStore.systemUnderTests.find((sut) => {
      return sut.name === this.props.sut;
    });
    const repo = sut ? sut.repo : undefined;
    if(undefined !== this.props.tc) {
      return this.renderTestCasesTc(repo);
    }
    else if(undefined !== this.props.fut) {
      return this.renderTestCasesFut(repo);
    }
    else if(undefined !== this.props.sut) {
      return this.renderTestCasesSut(repo);
    }
    else {
      return this.renderTestCases();
    }
  }
  
  _getTestCaseFullName() {
    let testCaseFullName = 'tc';
    if(undefined !== this.props.sut) {
      testCaseFullName += `.${this.props.sut}`;
    }
    if(undefined !== this.props.fut) {
      testCaseFullName += `.${this.props.fut}`;
    }
    if(undefined !== this.props.tc) {
      testCaseFullName += `.${this.props.tc}`;
    }
    return testCaseFullName;
  }
  
  renderTestCaseFullName() {
    return (
      <div className="bootstrap_breadcrump_full_name">
        {'Markup name: ' + this._getTestCaseFullName()}   
      </div>
    );
  }
  
  _copyMarkupName(e) {
    navigator.clipboard.writeText(this._getTestCaseFullName()).then(() => {
      /* clipboard successfully set */
    }, () => {
      /* clipboard write failed */
    });
  }
  
  _disabledCopy() {
    return this.state.TestCaseStore.markup.definition;
  }
  
  renderCopyButton() {
    return (
      <Button placement="bottom" heading="Copy" content="Markup name" shortcut="Alt+c" disabled={this._disabledCopy()}
        onClick={(e) => {
          this._copyMarkupName(e);
        }}
      >
        <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
      </Button>
    );
  }
  
  render() {
    return (
      <div className="same_size_as_parent">
        <div className="test_case_breadcrump">
          <ComponentBreadcrump items={[
            {name: 'Test Cases', link: ''},
            {name: this.props.sut, link: `${this.props.sut}`},
            {name: this.props.fut, link: `${this.props.sut}/${this.props.fut}`},
            {name: this.props.tc, link: `${this.props.sut}/${this.props.fut}/${this.props.tc}`}
          ]}/>
          <div className="bootstrap_breadcrump_full_name">
            {'-'}   
          </div>
          <div className="btn-group btn-group-xs" role="group" aria-label="..." style={{marginLeft:'8px'}}>
            {this.renderCopyButton()}
          </div>
          {this.renderTestCaseFullName()}
        </div>
        {this.renderColumns()}
      </div>
    ); 
  }
}
