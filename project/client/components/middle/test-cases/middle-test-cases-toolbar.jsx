
'use strict';

import MiddleFilter from '../middle-sut-fut-filter';
import LoginStore from '../../../stores/login-store';
import TestCasesStore from '../../../stores/test-cases-store';
import TestCaseStore from '../../../stores/test-case-store';
import FunctionUnderTestStore from '../../../stores/function-under-test-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionSystemUnderTestAdd, ActionSystemUnderTestDelete, ActionSystemUnderTestRepoGet } from '../../../actions/action-system-under-test/action-system-under-test';
import { ActionFunctionUnderTestAdd, ActionFunctionUnderTestDelete } from '../../../actions/action-function-under-test';
import { ActionTestCaseAdd, ActionTestCaseDelete, ActionTestCaseRename, ActionTestCaseMarkup, ActionTestCaseMarkupSave, ActionTestCaseMarkupCancel, ActionTestCaseMarkupCopy, ActionTestCaseExecutionStart, ActionTestCaseExecutionStop, ActionTestCaseDebugStart, ActionTestCaseClear, ActionTestCaseButtonsUpdate } from '../../../actions/action-test-case/action-test-case';
import { ActionTestCasesSutClear, ActionTestCasesFutClear, ActionTestCasesTcClear } from '../../../actions/action-test-cases';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ModalDialogAbstractionAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-abstraction-add';
import ModalDialogAbstractionProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-abstraction-properties';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestCasesToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestCaseStore, TestCasesStore, SystemUnderTestStore, LoginStore]);
    this._modalDialogAbstractionAdd = null;
    this._modalDialogAbstractionProperties = null;
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
    this.repo = null;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.tc, nextProps.tc)
      || !this.shallowCompare(this.state.TestCaseStore.markup, nextState.TestCaseStore.markup)
      || !this.shallowCompare(this.state.TestCaseStore.execution, nextState.TestCaseStore.execution)
      || !this.shallowCompare(this.state.TestCaseStore.realtimeConnection, nextState.TestCaseStore.realtimeConnection)
      || !this.shallowCompare(this.state.TestCaseStore.buttons.debug, nextState.TestCaseStore.buttons.debug)
      || !this.shallowCompare(this.state.TestCaseStore.buttons.general, nextState.TestCaseStore.buttons.general)
      || !this.shallowCompare(this.state.TestCasesStore, nextState.TestCasesStore)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompareArrayValues(this.state.SystemUnderTestStore.repos, nextState.SystemUnderTestStore.repos)
      || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(TestCaseStore, new ActionTestCaseMarkup());
    this.tabNavigation('definition');
  }
  
  _markupSave() {
    this.dispatch(TestCaseStore, new ActionTestCaseMarkupSave(this.repo, this.props.sut, this.props.fut, this.props.tc));
    this.tabNavigation('definition');
  }
  
  _markupHelp() {
    this.context.history('../documentation/markup/markup-test-case');
  }
  
  _markupCancel() {
    this.dispatch(TestCaseStore, new ActionTestCaseMarkupCancel());
    this.tabNavigation('definition');
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this._markupSave();
      }
    }
    else if(!e.ctrlKey && 'F5' === e.key) {
      if(TestCaseStore.EXECUTION_NOT_RUNNING === this.state.TestCaseStore.execution) {
        const breakOnEnterRun = this.state.TestCaseStore.buttons.debug.breakOnEnterRun;
        const breakOnLeaveRun = this.state.TestCaseStore.buttons.debug.breakOnLeaveRun;
        const breakOnIpEvent = this.state.TestCaseStore.buttons.debug.breakOnIpEvent;
        const breakOnGuiEvent = this.state.TestCaseStore.buttons.debug.breakOnGuiEvent;
        const debugType = this.state.TestCaseStore.buttons.debug.debugType;
        e.preventDefault();
        e.stopImmediatePropagation();
        const breakOnLines = [];
        if(breakOnEnterRun || breakOnLeaveRun) {
          this._getAutoBreakeLines(breakOnLines, breakOnEnterRun, breakOnLeaveRun);
        }
        this.dispatch(TestCaseStore, new ActionTestCaseDebugStart(this.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:false,asService:false,breakOnEnterRun:breakOnEnterRun,breakOnLeaveRun:breakOnLeaveRun,breakOnLines:breakOnLines,breakOnIpEvent:breakOnIpEvent,breakOnGuiEvent:breakOnGuiEvent,debugType:debugType}));
        this.tabNavigation('debug');
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'A' === e.key) {
      const disabled = 'tc' === this.state.TestCasesStore.current.view;
      if(!disabled) {
        e.preventDefault();
        this._add(e);
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'D' === e.key) {
      const disabled = 0 === this.state.TestCasesStore.systemUnderTestsChecked.size && 0 === this.state.TestCasesStore.functionUnderTestsChecked.size && 0 === this.state.TestCasesStore.testCasesChecked.size;
      if(!disabled) {
        e.preventDefault();
        this._delete(e);
      }
    }
  }

  getText() {
    if('none' === this.state.TestCasesStore.current.view) {
      return 'System Under Test';
    }
    else if('sut' === this.state.TestCasesStore.current.view) {
      return 'Function Under Test';
    }
    else if('fut' === this.state.TestCasesStore.current.view) {
      return 'Test Case';
    }     
  }
  
  _add(e) {
    this._modalDialogAbstractionAdd.show();
  }
  
  _delete(e) {
    const suts = [];
    this.state.TestCasesStore.systemUnderTestsChecked.forEach((sut) => {
      suts.push(sut);
    });
    const futs = [];
    this.state.TestCasesStore.functionUnderTestsChecked.forEach((fut) => {
      const found = suts.find((sut) => {
        return sut.repo === fut.repo && sut.name === fut.sut;
      });
      if(!found) {
        futs.push(fut);
      }
    });
    const tcs = [];
    this.state.TestCasesStore.testCasesChecked.forEach((tc) => {
      const found = suts.find((sut) => {
        return sut.name === tc.sut;
      });
      if(!found) {
        const foundFut = futs.find((fut) => {
          return fut.repo === tc.repo && fut.sut === tc.sut && fut.fut === tc.fut;
        });
        if(!foundFut) {
          tcs.push(tc);
        }
      }
    });
    const isCurrentSut = suts.some((sut) => {
      return sut.name === this.props.sut;
    });
    const isCurrentFut = futs.some((fut) => {
      return fut.repo === this.repo && fut.sut === this.props.sut && fut.fut === this.props.fut;
    });
    this.dispatch(TestCasesStore, new ActionTestCasesSutClear());
    this.dispatch(TestCasesStore, new ActionTestCasesFutClear());
    this.dispatch(TestCasesStore, new ActionTestCasesTcClear());
    if(0 !== suts.length) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestDelete(suts));
      if(isCurrentSut) {
        return this.context.history('');
      }
    }
    if(0 !== futs.length) {
      this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestDelete(this.repo, this.props.sut, futs));
      if(isCurrentFut) {
        return this.context.history(`${this.props.sut}`);
      }
    }
    if(0 !== tcs.length) {
      this.dispatch(TestCaseStore, new ActionTestCaseDelete(tcs));
    }
  }
  
  renderAddButton() {
    if('tc' !== this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_add" placement="bottom" heading="Add" content={this.getText()} shortcut="Ctrl+Shift+A" disabled={!(0 === this.state.TestCasesStore.systemUnderTestsChecked.size && 0 === this.state.TestCasesStore.functionUnderTestsChecked.size && 0 === this.state.TestCasesStore.testCasesChecked.size)}
          onClick={(e) => {
            this._add(e);
          }}
        >
          <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderRemoveButton() {
    if('tc' !== this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_remove" placement="bottom" heading="Delete" content={this.getText()} shortcut="Ctrl+Shift+D" disabled={0 === this.state.TestCasesStore.systemUnderTestsChecked.size && 0 === this.state.TestCasesStore.functionUnderTestsChecked.size && 0 === this.state.TestCasesStore.testCasesChecked.size}
          onClick={(e) => {
            this._delete(e);
          }}
        >
          <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderButtonProperties() {
    const enabled = (0 === this.state.TestCasesStore.systemUnderTestsChecked.size && 0 === this.state.TestCasesStore.functionUnderTestsChecked.size && 1 === this.state.TestCasesStore.testCasesChecked.size);
    if('tc' !== this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_properties" placement="bottom" heading="Properties" content="Test Case" shortcut="Ctrl+Shift+R" disabled={!enabled}
          onClick={(e) => {
            this._modalDialogAbstractionProperties.show();
          }}
        >
          <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
          <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
        </Button>
      );
    }
  }
  
  renderButtonCopy() {
    const enabled = (0 === this.state.TestCasesStore.systemUnderTestsChecked.size && 0 === this.state.TestCasesStore.functionUnderTestsChecked.size && 1 === this.state.TestCasesStore.testCasesChecked.size);
    if('tc' !== this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_properties" placement="bottom" heading="Copy" content="Test Case" shortcut="Ctrl+Shift+R" disabled={!enabled}
          onClick={(e) => {
            //this._modalDialogAbstractionProperties.show(this.state.ActorEditorStore.current.file);
          }}
        >
          <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupOpenButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      this.markupDisabledOpen = this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.definition || (TestCaseStore.EXECUTION_NOT_RUNNING !== this.state.TestCaseStore.execution);
      return (
        <Button placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
          onClick={(e) => {
            this._markupOpen();
          }}
        >
          <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupSaveButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      this.markupDisabledSave = this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.content === this.state.TestCaseStore.markup.contentOriginal;
      return (
        <Button placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
          onClick={(e) => {
            this._markupSave();
          }}
        >
          <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.TestCaseStore.markup.definition;
    return (
      <Button placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      this.markupDisabledCancel = !this.state.TestCaseStore.markup.definition;
      return (
        <Button placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
          onClick={(e) => {
            this._markupCancel();
          }}
        >
          <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupCopyButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      this.markupDisabledCopy = this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.definition || (TestCaseStore.EXECUTION_NOT_RUNNING !== this.state.TestCaseStore.execution);
      return (
        <Button placement="bottom" heading="Copy" content="Markup" disabled={this.markupDisabledCopy}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseMarkupCopy());
          }}
        >
          <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderStartButton(tab, debug=false) {
    if('tc' === this.state.TestCasesStore.current.view) {
      const className = `glyphicon glyphicon-play test_case_${tab}`;
      const asService = this.state.TestCaseStore.buttons.general.asService;
      const slow = this.state.TestCaseStore.buttons.general.asService;
      const breakOnEnterRun = debug && this.state.TestCaseStore.buttons.debug.breakOnEnterRun;
      const breakOnLeaveRun = debug && this.state.TestCaseStore.buttons.debug.breakOnLeaveRun;
      const breakOnIpEvent = this.state.TestCaseStore.buttons.debug.breakOnIpEvent;
      const breakOnGuiEvent = this.state.TestCaseStore.buttons.debug.breakOnGuiEvent;
      const debugType = this.state.TestCaseStore.buttons.debug.debugType;
      const shortcut = debug ? 'F5' : undefined;
      const content = `in the ${tab} tab${asService ? ', as a service' : ''}`;
      return (
        <Button id={`test_case_start_${tab}${asService ? '_as_a_service' : ''}`} placement="bottom" heading="Run" content={content} shortcut={shortcut} disabled={this._isRunning() || this.state.LoginStore.login.systemUnderTest !== this.props.sut}
          onClick={(e) => {
            if(!debug) {
              this.dispatch(TestCaseStore, new ActionTestCaseExecutionStart(this.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:false,asService:asService}));
            }
            else {
              const breakOnLines = [];
              if(breakOnEnterRun || breakOnLeaveRun) {
                this._getAutoBreakeLines(breakOnLines, breakOnEnterRun, breakOnLeaveRun);
              }
              this.dispatch(TestCaseStore, new ActionTestCaseDebugStart(this.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:false,asService:asService,breakOnEnterRun:breakOnEnterRun,breakOnLeaveRun:breakOnLeaveRun,breakOnLines:breakOnLines,breakOnIpEvent:breakOnIpEvent,breakOnGuiEvent:breakOnGuiEvent,debugType:debugType}));
            }
            this.tabNavigation(tab);
          }}
        >
          <span className={className} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderPauseOnEnterRunButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.debug.breakOnEnterRun} placement="bottom" heading="Break" content={'on enter *run()'} className="btn_abs_top_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'breakOnEnterRun', !this.state.TestCaseStore.buttons.debug.breakOnEnterRun));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause test_case_debug" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-arrow-down test_case_debug" style={{top:'-0.5px',transform:'scale(0.6)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderPauseOnLeaveRunButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.debug.breakOnLeaveRun} placement="bottom" heading="Break" content={'on leave *run()'} className="btn_abs_bottom_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'breakOnLeaveRun', !this.state.TestCaseStore.buttons.debug.breakOnLeaveRun));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause test_case_debug" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-arrow-up test_case_debug" style={{top:'-1px',transform:'scale(0.6)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderPauseOnIpEventButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.debug.breakOnIpEvent} placement="bottom" heading="Break" content={'on ip event'} className="btn_abs_top_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'breakOnIpEvent', !this.state.TestCaseStore.buttons.debug.breakOnIpEvent));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause test_case_debug" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-sort test_case_debug" style={{top:'-1px',transform:'rotate(90deg) scale(0.7, 0.4)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderPauseOnGuiEventButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.debug.breakOnGuiEvent} placement="bottom" heading="Break" content={'on gui event'} className="btn_abs_bottom_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'breakOnGuiEvent', !this.state.TestCaseStore.buttons.debug.breakOnGuiEvent));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause test_case_debug" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-modal-window test_case_debug" style={{top:'-1px',transform:'scale(0.45, 0.6)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderDebugLocalButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={TestCaseStore.DEBUG_LOCAL === this.state.TestCaseStore.buttons.debug.debugType} placement="bottom" heading="Debugger" content={'local'} className="btn_abs_top_right"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'debugType', TestCaseStore.DEBUG_LOCAL));
            }
          }}
        >
          <span className="glyphicon glyphicon-play-circle test_case_debug" style={{transform:'scale(0.6, 0.6)',left:'0px',top:'-0.5px'}} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderDebugRemoteButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={TestCaseStore.DEBUG_REMOTE === this.state.TestCaseStore.buttons.debug.debugType} placement="bottom" heading="Debugger" content={'remote'} className="btn_abs_bottom_right"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('debug', 'debugType', TestCaseStore.DEBUG_REMOTE));
            }
          }}
        >
          <span className="glyphicon glyphicon-arrow-right test_case_debug" style={{top:'-1.5px',transform:'scale(0.45, 0.6)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-play-circle test_case_debug" style={{top:'-1px',transform:'scale(0.6, 0.6)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderAsServiceButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.general.asService} placement="bottom" heading="Execute" content={'as a service'} className="btn_abs_top_right"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('general', 'asService', !this.state.TestCaseStore.buttons.general.asService));
            }
          }}
        >
          <span className="glyphicon glyphicon-cog" style={{top:'-1px',transform:'scale(0.8)',left:'-0.5px'}} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderWithLogsButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.general.withLogs} placement="bottom" heading="Execute" content={'with logs'} className="btn_abs_bottom_right"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('general', 'withLogs', !this.state.TestCaseStore.buttons.general.withLogs));
            }
          }}
        >
          <span className="glyphicon glyphicon-list" style={{top:'-1.5px',transform:'scale(0.8)',left:'-0.5px'}} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderStartSlowMotionButton(tab, debug=false) {
    if('tc' === this.state.TestCasesStore.current.view) {
      const breakOnEnterRun = debug && this.state.TestCaseStore.buttons.debug.breakOnEnterRun;
      const breakOnLeaveRun = debug && this.state.TestCaseStore.buttons.debug.breakOnLeaveRun;
      const breakOnIpEvent = this.state.TestCaseStore.buttons.debug.breakOnIpEvent;
      const breakOnGuiEvent = this.state.TestCaseStore.buttons.debug.breakOnGuiEvent;
      const slowOnIpEvent = this.state.TestCaseStore.buttons.general.slowOnIpEvent;
      const slowOnGuiEvent = this.state.TestCaseStore.buttons.general.slowOnGuiEvent;
      return (
        <Button id="test_case_start_slow_motion" placement="bottom" heading="Run Slow Motion" content="in current tab" disabled={this._isRunning() || this.state.LoginStore.login.systemUnderTest !== this.props.sut}
          onClick={(e) => {
            //if(!debug) {
              this.dispatch(TestCaseStore, new ActionTestCaseExecutionStart(this.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:true,asService:false,slowOnIpEvent:slowOnIpEvent,slowOnGuiEvent:slowOnGuiEvent}));
            /*}
            else {
              const breakOnLines = [];
               if(breakOnEnterRun || breakOnLeaveRun) {
                this._getAutoBreakeLines(breakOnLines, breakOnEnterRun, breakOnLeaveRun);
              }
              this.dispatch(TestCaseStore, new ActionTestCaseDebugStart(this.repo, this.props.sut, this.props.fut, this.props.tc, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:true,asService:false,breakOnEnterRun:breakOnEnterRun,breakOnLeaveRun:breakOnLeaveRun,breakOnLines:breakOnLines,breakOnIpEvent:breakOnIpEvent,breakOnGuiEvent:breakOnGuiEvent}));
            }*/
          }}
        >
          <span className="glyphicon glyphicon-eject" aria-hidden="true" style={{transform: 'rotate(90deg)'}}></span>
        </Button>
      );
    }
  }
  
  renderSlowOnIpEventButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.general.slowOnIpEvent} placement="bottom" heading="Slow" content={'on ip events'} className="btn_abs_top_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('general', 'slowOnIpEvent', !this.state.TestCaseStore.buttons.general.slowOnIpEvent));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-sort" style={{top:'-1px',transform:'rotate(90deg) scale(0.7, 0.4)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderSlowOnGuiEventButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button active={this.state.TestCaseStore.buttons.general.slowOnGuiEvent} placement="bottom" heading="Slow" content={'on gui events'} className="btn_abs_bottom_middle"
          onClick={(e) => {
            if(!this._isRunning()) {
              this.dispatch(TestCaseStore, new ActionTestCaseButtonsUpdate('general', 'slowOnGuiEvent', !this.state.TestCaseStore.buttons.general.slowOnGuiEvent));
            }
          }}
        >
          <span className="glyphicon glyphicon-pause" style={{top:'-1.5px',transform:'scale(0.6, 0.8)',left:'-3px'}} aria-hidden="true"></span>
          <span className="glyphicon glyphicon-modal-window" style={{top:'-1px',transform:'scale(0.45, 0.6)',left:'-11px'}} aria-hidden="true" ></span>
        </Button>
      );
    }
  }
  
  renderStopButton(tab) {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_stop" placement="bottom" heading="Stop" content="in current tab" disabled={this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.definition || (1 !== this.state.TestCaseStore.execution)}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseExecutionStop());
          }}
        >
          <span className="glyphicon glyphicon-stop" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderClearButton(tab) {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button id={`test_case_clear_${tab}`} placement="bottom" heading="Clear" content={`In the ${tab} tab`} disabled={this.state.TestCaseStore.markup.definition || (0 !== this.state.TestCaseStore.execution)}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseClear(tab));
            this.tabNavigation(tab);
          }}
        >
          <span className={`glyphicon glyphicon-erase test_case_${tab}`} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderClearAllButton() {
    if('tc' === this.state.TestCasesStore.current.view) {
      return (
        <Button id="test_case_clear_all" placement="bottom" heading="Clear" content="all" disabled={this.state.TestCaseStore.markup.definition || (0 !== this.state.TestCaseStore.execution)}
          onClick={(e) => {
            this.dispatch(TestCaseStore, new ActionTestCaseClear('execution'));
            this.dispatch(TestCaseStore, new ActionTestCaseClear('log'));
            this.dispatch(TestCaseStore, new ActionTestCaseClear('sequence-diagram'));
          }}
        >
          <span className="glyphicon glyphicon-erase" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderRealtimeInfo() {
    let className = 'glyphicon glyphicon-transfer';
    let realtimeConnectionStatus = '';
    if(0 === this.state.TestCaseStore.realtimeConnection){
      className += ' realtime_disconnected';
      realtimeConnectionStatus += 'down';
    }
    else if(1 === this.state.TestCaseStore.realtimeConnection) {
      className += ' realtime_connected';
      realtimeConnectionStatus += 'up';
    }
    return (
      <Popover placement="bottom" heading="Realtime Connection" content={realtimeConnectionStatus}>
        <span className={className} aria-hidden="true"></span>
      </Popover>
    );
  }
  
  renderStageButton() {
    return (
      <Button id="from_start_to_stage" placement="bottom" heading="Goto" content="Stage"
        onClick={(e) => {
          this.context.history(`/stage`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-star-empty" aria-hidden="true" style={{top: -4, left: -4, width: 0, transform: 'scale(0.9)'}}></span>
      </Button>
    );
  }
  
  renderLoginButton() {
    return (
      <Button id="from_start_to_login" placement="bottom" heading="Goto" content="User"
        onClick={(e) => {
          this.context.history(`/user`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderModalDialogAbstractionAdd() {
    if('none' === this.state.TestCasesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_cases_sut_add" ref={(c) => {this._modalDialogAbstractionAdd = c;}} type="sut" defaultMarkup={MiddleTestCasesToolbar.DEFAULT_MARKUP_SUT} result={'success'} capitalFirst={true} repos={this.state.SystemUnderTestStore.repos} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onLoad={() => {
            this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestRepoGet());
          }}
          onAdd={(name, description, repo) => {
            this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestAdd(repo, name, description));
          }}
          onTemplate={(templateName) => {
          }}
          onClear={() => {
          }}
        />
      );
    }
    else if('sut' === this.state.TestCasesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_cases_fut_add" ref={(c) => {this._modalDialogAbstractionAdd = c;}} type="fut" defaultMarkup={MiddleTestCasesToolbar.DEFAULT_MARKUP_FUT} result={'success'} capitalFirst={true} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onAdd={(name, description) => {
            this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestAdd(this.repo, this.props.sut, name, description));
          }}
          onClear={() => {
          }}
        />
      );
    }
    else if('fut' === this.state.TestCasesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_cases_tc_add" ref={(c) => {this._modalDialogAbstractionAdd = c;}} type="tc" wizardNames={this.state.TestCaseStore.wizard.stacks} templates={this.state.TestCaseStore.wizard.stackTemplates} defaultMarkup={MiddleTestCasesToolbar.DEFAULT_MARKUP_TC} result={'success'} capitalFirst={true} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onAdd={(name, description, stackName, templateName, testData) => {
            this.dispatch(TestCaseStore, new ActionTestCaseAdd(this.repo, this.props.sut, this.props.fut, name, description, stackName, templateName, testData));
          }}
          onClear={() => {
          }}
        />
      );
    }
  }
  
  renderModalDialogAbstractionProperties() {
    let currentName = '';
    let currentDescription = '';
    const tc = this.state.TestCasesStore.testCasesChecked.forEach((tc) => {
      currentName = tc.tc;
      currentDescription = tc.description;
    });
    return (
      <ModalDialogAbstractionProperties ref={(c) => this._modalDialogAbstractionProperties = c} name={currentName} description={currentDescription} heading="Test Case Properties" capitalFirst result={'result'}
        onAdd={(newName, description) => {
          this.dispatch(TestCaseStore, new ActionTestCaseRename(this.repo, this.props.sut, this.props.fut, currentName, newName, description));
        }}
        onClear={() => {
          this.dispatch(TestCasesStore, new ActionTestCasesTcClear());
        }}
      />
    );
  }
  
  render() {
    const sut = !this.props.sut ? undefined : this.state.SystemUnderTestStore.systemUnderTests.find((sut) => {
      return sut.name === this.props.sut;
    });
    this.repo = sut ? sut.repo : null;
    return (
      <div className="middle_toolbar">
        {this.renderModalDialogAbstractionAdd()}
        {this.renderModalDialogAbstractionProperties()}
        <div className="toolbar" role="toolbar" aria-label="...">
          <MiddleFilter sut fut disabled={'tc' === this.state.TestCasesStore.current.view}/>  
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderAddButton()}
            {this.renderRemoveButton()}
            {this.renderButtonProperties()}
            {this.renderButtonCopy()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
            {this.renderMarkupCopyButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('execution')}
            {this.renderClearButton('execution')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('log')}
            {this.renderClearButton('log')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('sequence-diagram')}
            {this.renderClearButton('sequence-diagram')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('debug', true)}
            <div className="btn-group btn-group-xs btn_abs_xs_groups_3" role="group" aria-label="...">
              <div className="btn_abs_xs_group_1">
                {this.renderPauseOnEnterRunButton()}
                {this.renderPauseOnLeaveRunButton()}
              </div>
              <div className="btn_abs_xs_group_2">
                {this.renderPauseOnIpEventButton()}
                {this.renderPauseOnGuiEventButton()}
              </div>
              <div className="btn_abs_xs_group_3">
                {this.renderDebugLocalButton()}
                {this.renderDebugRemoteButton()}
              </div>
            </div>
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartSlowMotionButton()}
            <div className="btn-group btn-group-xs btn_abs_xs_groups_1" role="group" aria-label="...">
              <div className="btn_abs_xs_group_1">
                {this.renderSlowOnIpEventButton()}
                {this.renderSlowOnGuiEventButton()}
              </div>
            </div>
            {this.renderStopButton()}
            {this.renderClearAllButton()}
            <div className="btn-group btn-group-xs btn_abs_xs_groups_1" role="group" aria-label="...">
              <div className="btn_abs_xs_group_1">
                {this.renderAsServiceButton()}
                {this.renderWithLogsButton()}
              </div>
            </div>
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderRealtimeInfo()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderStageButton()}
          </div>
        </div>
      </div>
    );
  }
  
  tabNavigation(tab) {
    if(tab !== this.props.tab) {
      this.context.history(`${this.props.sut}/${this.props.fut}/${this.props.tc}/${tab}`, {replace: true});
    }
  }
  
  _getAutoBreakeLines(breakOnLines, breakOnEnterRun, breakOnLeaveRun) {
    this.state.TestCaseStore.dbugger.actorFiles.forEach((actorFile) => {
      if(breakOnEnterRun && -1 !== actorFile.lineRunEnter) {
        breakOnLines.push({
          name: actorFile.path + '/' + actorFile.title,
          title: actorFile.title,
          pauseOnBreak: true,
          lineNumber: actorFile.lineRunEnter,
          url: ''
        });
      }
      if(breakOnLeaveRun && -1 !== actorFile.lineRunLeave) {
        breakOnLines.push({
          name: actorFile.path + '/' + actorFile.title,
          title: actorFile.title,
          pauseOnBreak: true,
          lineNumber: actorFile.lineRunLeave,
          url: ''
        });
      }
    });
  }
  
  _isRunning() {
    return this.state.TestCaseStore.realtimeConnection !== TestCaseStore.REALTIME_CONNECTION_OPEN || this.state.TestCaseStore.markup.definition || (0 !== this.state.TestCaseStore.execution);
  }
}

MiddleTestCasesToolbar.DEFAULT_MARKUP_SUT = {
  markup: `#### ***Add a new SUT***
  
***Set the SUT name and select the repo in which to put it.***`,
  markupStyle: {
    padding:'13px 8px 0px 11px'
  }
};

MiddleTestCasesToolbar.DEFAULT_MARKUP_FUT = {
  markup: `#### ***Add a new FUT***
  
***Set the FUT name.***`,
  markupStyle: {
    padding:'13px 0px 0px 18px'
  }
};

MiddleTestCasesToolbar.DEFAULT_MARKUP_TC = {
  markup: `#### ***Add a Test Case***
  
***Add Actors and TestData on your own.***`,
  markupStyle: {
    padding:'18px 8px 0px 14px'
  }
};


MiddleTestCasesToolbar.contextType = RouterContext;

