
'use strict';

import TestCaseStore from '../../../stores/test-case-store';
import TestCasesStore from '../../../stores/test-cases-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionTestCaseAdd } from '../../../actions/action-test-case/action-test-case';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestCasesNotFound extends ReactComponentStore {
  constructor(props) {
    super(props, [TestCasesStore, SystemUnderTestStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props.repo, nextProps.repo)
      || !this.shallowCompareObjectValues(this.props.sut, nextProps.sut)
      || !this.shallowCompareObjectValues(this.props.fut, nextProps.fut)
      || !this.shallowCompareObjectValues(this.props.tc, nextProps.tc)
      || this.state.SystemUnderTestStore.currentSut !== nextState.SystemUnderTestStore.currentSut
      || !this.shallowCompare(this.state.TestCasesStore.current, nextState.TestCasesStore.current);
  }
  
  render() {
    return (
      <div>
        <h1>
          Getting the Test Case failed.
        </h1>
        <p>
          <b>
            msg:
          </b>
          &nbsp;{this.props.error.msg}
        </p>
        <h3>
          If you want to create the Test Case press the button.
        </h3>
        <div className="btn-group" role="group" aria-label="...">
          <button type="button" className="btn btn-primary"
            onClick={(e) => {
              this.dispatch(TestCaseStore, new ActionTestCaseAdd(this.props.repo, this.props.sut, this.props.fut, this.props.tc, '', null, null, null, true));
            }}
          >Create Test Case</button>
        </div>
      </div>
    );
  }
}
