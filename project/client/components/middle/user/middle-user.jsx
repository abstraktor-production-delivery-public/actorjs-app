
'use strict';

import MiddleSidebar from '../middle_sidebar';
import MiddleUserToolbar from './middle-user-toolbar';
import MiddleUserView from './middle-user-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleUser extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'User'
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle">
        <MiddleUserToolbar />
        <MiddleSidebar />
        <div className="middle_view">
          <MiddleUserView />
        </div>
      </div>
    );
  }
}

