
'use strict';

import Button from 'z-abs-complayer-bootstrap-client/client/button';
//import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleUserToolbar extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="btn-toolbar" role="toolbar"  aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
          </div>
        </div>
      </div>
    );
  }
}


//MiddleUserToolbar.contextType = RouterContext;
