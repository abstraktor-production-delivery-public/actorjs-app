
'use strict';

import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleUserView extends ReactComponentStore {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="">
        <div className="col-xs-1" />
        <div className="col-xs-10">
          <h1>
            <strong>User</strong>
          </h1>
          <code style={{color:'Green', backgroundColor:'Honeydew', border:'1px solid Green'}}>Comming soon:</code>&nbsp;Here you will be able to register and handle Services.
        </div>
        <div className="col-xs-1" />
      </div>
    );
  }
}
