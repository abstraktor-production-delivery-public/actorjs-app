
'use strict';

import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleVideosView extends ReactComponentStore {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderHeading(videoHeading) {
    return (
      <p style={{fontSize:'0.8vw',fontWeight:'bolder',margin:0,padding:0}}>{videoHeading}</p>
    );
  }
  
  renderText(videoText) {
    return (
      <p style={{fontSize:'0.7vw',margin:0,padding:0}}>{videoText}</p>
    );
  }
  
  renderVideo(video, index) {
    return (
      <a className="videos_meny" key={index} href={`https://www.youtube.com/watch?v=${video.src}`} target="_blank">
        <div className="embed-responsive embed-responsive-16by9" style={{marginBottom:'4px',border:'solid black 1px'}}>
          <img id="start_page" src="/abs-images/svg/AbstraktorLogo.svg" alt="Abstraktor Logo" style={{
            display: 'block',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '0.6vw',
            marginBottom: '0.2vw',
            width: '100%'
          }} />
          <div style={{margin:0,padding:0,textAlign:'center'}}>
            {this.renderHeading(video.heading)}
            {this.renderText(video.text)}
          </div>
        </div>
      </a>
    );
  }
  
  renderOuterColumn(heading, videos) {
    const videosJsx = videos.map((video, index) => {
      return this.renderVideo(video, index);
    });
    return (
      <>
        <div className="videos_meny_header">
          <h2 className="videos_meny">{heading}</h2>
        </div>
        {videosJsx}
      </>
    );
  }
  
  renderVideos() {
    let key = 0;
    return (
      <div className="videos_menu_outer">
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Get Started', [
            {
              src:'4OUyEm7a-yQ&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '1. Define your first SUT'
            },
            {
              src:'dzZYmUQCWXI&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '2. Make your first Actors'
            },
            {
              src:'LWPE2N3THkM&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '3. Make your first Test Case'
            },
            {
              src:'pa9fm2QTM24&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '4. Make your first Test Suite'
            },
            {
              src:'pjVtuKKA8Q0&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '5. Add your first Addresses'
            },
            {
              src:'rRJoTBjbOkA&ab_channel=Abstraktor',
              heading: 'Get Started',
              text: '6. Share your work'
            }
          ])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('How to', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Abstractions', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Lessons', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Actor Api', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Stacks', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Tools', [])}
        </div>
        <div key={++key} className="videos_menu_inner">
          {this.renderOuterColumn('Markup', [])}
        </div>
      </div>
    );
  }
  
  render() {
    return (
      <div className="middle_view_full_full videos_menu">
        {this.renderVideos()}
      </div>
    );
  }
}
