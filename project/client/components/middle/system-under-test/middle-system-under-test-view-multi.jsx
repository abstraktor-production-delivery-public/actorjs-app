
'use strict';

import LoginStore from '../../../stores/login-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import FilterStore from '../../../stores/filter-store';
import ActorDefault from '../../../actor-default';
import { ActionSystemUnderTestChecked } from '../../../actions/action-system-under-test/action-system-under-test';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import React from 'react';


export default class MiddleSystemUnderTestViewMulti extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore, SystemUnderTestStore, FilterStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.login.systemUnderTest, nextState.LoginStore.systemUnderTest)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompareSetValues(this.state.TestSuitesStore.testSuitesChecked, nextState.TestSuitesStore.testSuitesChecked)
      || !this.shallowCompare(this.state.FilterStore.hide, nextState.FilterStore.hide);
  }
  
  renderCheckbox(sutRepo, sutName) {
    if(ActorDefault.NAME() !== sutName) {
      if(this.state.LoginStore.login.systemUnderTest === sutName) {
        return (
          <td>
            <input type="checkbox" id={`system_under_test_staged_${sutRepo}_${sutName}`} className="disabled" aria-label="..." autoComplete="off" checked={false} onChange={(e) => {}} />
          </td>
        )
      }
      else {
        return (
          <td>
            <input type="checkbox" id={`system_under_test_not_staged_${sutRepo}_${sutName}`} aria-label="..." autoComplete="off" checked={this.state.SystemUnderTestStore.systemUnderTestsChecked.has(`${sutRepo}_${sutName}`)}
              onChange={(e) => {
                this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestChecked(sutRepo, sutName, e.currentTarget.checked));
              }}
            />
          </td>
        );
      }
    }
    else {
      return (
        <td></td>
      );
    }
  }
  
  renderStatusHeading() {
    if(!this.state.FilterStore.hide) {
      return (
        <th>Status</th>
      );
    }
  }
  
  renderStatus(sutStatus) {
    if(!this.state.FilterStore.hide) {
      return (
        <td>{sutStatus}</td>
      );
    }
  }
  
  renderSystemUnderTestRow(sut, index) {
    if(!this.state.FilterStore.hide || (this.state.FilterStore.hide && sut.status !== SystemUnderTestStore.HIDDEN())) {
      return (
        <tr key={index}>
          <th scope="row">{index + 1}</th>
          {this.renderCheckbox(sut.repo, sut.name)}
          {this.renderStatus(sut.status)}
          <td>
            <Link href={`${sut.name}`}>
              {sut.name}
            </Link>
          </td>
          <td>{sut.repo}</td>
          <td>{sut.description}</td>
        </tr>
      );
    }
  }

  renderSystemUnderTestRows() {
    return this.state.SystemUnderTestStore.systemUnderTests.map((sut, index) => {
      return this.renderSystemUnderTestRow(sut, index);
    });
  }

  render() {
    return (
      <div className="middle_view middle_view_system_under_tests middle_system_under_test_multi_view">
        <div className="panel panel-default">
          <div className="panel-heading">System Under Test</div>
          <table id="system_under_test_table" className="table">
            <thead>
              <tr>
                <th>#</th>
                <th></th>
                {this.renderStatusHeading()}
                <th>Name</th>
                <th>Repo</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {this.renderSystemUnderTestRows()}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
