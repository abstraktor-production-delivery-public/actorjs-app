
'use strict';

import MiddleSystemUnderTestToolbar from './middle-system-under-test-toolbar';
import MiddleSidebar from '../middle_sidebar';
import FilterStore from '../../../stores/filter-store';
import MiddleSystemUnderTestViewSingle from './middle-system-under-test-view-single';
import MiddleSystemUnderTestViewMulti from './middle-system-under-test-view-multi';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleSystemUnderTest extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'System Under Test',
        prefix: 'SUT: '
      };
    });
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle middle_system_under_test">
        <MiddleSystemUnderTestToolbar />
        <MiddleSidebar />
        <Route switch="" handler={MiddleSystemUnderTestViewMulti} />
        <Route switch={["/:sut/:tab", "/:sut"]} handler={MiddleSystemUnderTestViewSingle} />
      </div>
    );
  }
}
