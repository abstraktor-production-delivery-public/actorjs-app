
'use strict';

import MiddleFilter from '../middle-sut-fut-filter';
import FilterStore from '../../../stores/filter-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionSystemUnderTestAdd, ActionSystemUnderTestDelete, ActionSystemUnderTestUpdate, ActionSystemUnderTestClearResult, ActionSystemUnderTestRepoAdd, ActionSystemUnderTestRepoGet } from '../../../actions/action-system-under-test/action-system-under-test';
import { ActionHideFilter } from '../../../actions/action-sut-fut-filter';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ModalDialogAbstractionAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-abstraction-add';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
//import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSystemUnderTestToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [SystemUnderTestStore, FilterStore]);
    this._modalDialogAbstractionRepoAdd = null;
    this._modalDialogAbstractionAdd = null;
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.FilterStore.hide, nextState.FilterStore.hide)
      || !this.shallowCompare(this.state.SystemUnderTestStore, nextState.SystemUnderTestStore);
  }  
  
  setStatus(status) {
    let systemUnderTests = this.state.SystemUnderTestStore.systemUnderTests.filter((sut) => {
      return this.state.SystemUnderTestStore.systemUnderTestsChecked.has(`${sut.repo}_${sut.name}`);
    });
    systemUnderTests.forEach((sut) => {
      sut.status = status;
    });
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestUpdate(systemUnderTests));
  }
  
  renderAddRepo() {
    return (
      <Button id="sut_add_repo" placement="bottom" heading="Add" content="Repo"
        onClick={(e) => {
          this._modalDialogAbstractionRepoAdd.show();
        }}
      >
        <span className="glyphicon glyphicon_project glyphicon-oil" aria-hidden="true" style={{left: -2}}></span>
        <span className="glyphicon glyphicon_project glyphicon-plus-sign" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderShowHiddenButton() {
    return (
      <Button id="sut_show_hidden" placement="bottom" heading="Show Hidden" content="System Under Test" active={!this.state.FilterStore.hide} //disabled={this.state.FilterStore.hide}
        onClick={(e) => {
          this.dispatch(FilterStore, new ActionHideFilter(!this.state.FilterStore.hide));
        }}
      >
        <span className="glyphicon glyphicon-filter" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderShowButton() {
    return (
      <Button id="sut_show" placement="bottom" heading="Show" content="System Under Test" disabled={!this.state.SystemUnderTestStore.checkedHiddens}
        onClick={(e) => {
          this.setStatus(SystemUnderTestStore.VISIBLE());
        }}
      >
        <span className="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderHideButton() {
    return (
      <Button id="sut_hidden" placement="bottom" heading="Hide" content="System Under Test" disabled={!this.state.SystemUnderTestStore.checkedVisibles}
        onClick={(e) => {
          this.setStatus(SystemUnderTestStore.HIDDEN());
        }}
      >
        <span className="glyphicon glyphicon-minus-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderAdd() {
    return (
      <Button id="sut_add" placement="bottom" heading="Add" content="System Under Test" disabled={0 !== this.state.SystemUnderTestStore.systemUnderTestsChecked.size}
        onClick={(e) => {
          this._modalDialogAbstractionAdd.show();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderRemove() {
    return (
      <Button id="sut_remove" placement="bottom" heading="Remove" content="System Under Test" disabled={0 === this.state.SystemUnderTestStore.systemUnderTestsChecked.size}
        onClick={(e) => {
          const suts = this.state.SystemUnderTestStore.systemUnderTests.filter((sut) => {
            return this.state.SystemUnderTestStore.systemUnderTestsChecked.has(`${sut.repo}_${sut.name}`);
          });
          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestDelete(suts));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderModalDialogRepoAdd() {
    return (
      <ModalDialogAbstractionAdd id="system_under_test_repo_add" ref={(c) => this._modalDialogAbstractionRepoAdd = c} type="repo" specialName="repo" defaultMarkup={MiddleSystemUnderTestToolbar.DEFAULT_MARKUP_REPO} result={'success'} capitalFirst={false} heading="Add a new Repo" nameplaceholder="The name of the Repo" descriptionplaceholder="A description of the Repo"
        onAdd={(name, description) => {
          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestRepoAdd(name, description));
        }}
        onClear={() => {
//          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestClearResult());
        }}
      />
    );
  }
  
  renderModalDialogAbstractionAdd() {
    return (
      <ModalDialogAbstractionAdd id="system_under_test_abstraction_add" ref={(c) => this._modalDialogAbstractionAdd = c} type="sut" specialName="sut" defaultMarkup={MiddleSystemUnderTestToolbar.DEFAULT_MARKUP_SUT} result={'success'} repos={this.state.SystemUnderTestStore.repos} heading="Add a new System Under Test" nameplaceholder="The name of the System Under Test" descriptionplaceholder="A description of the System Under Test"
        onLoad={() => {
          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestRepoGet());
        }}
        onAdd={(name, description, repo) => {
          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestAdd(repo, name, description));
        }}
        onClear={() => {
          this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestClearResult());
        }}
      />
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_system_under_test_toolbar">
        {this.renderModalDialogRepoAdd()}
        {this.renderModalDialogAbstractionAdd()}
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <MiddleFilter sut />
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderAddRepo()}
          </div>  
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderShowHiddenButton()}
            {this.renderShowButton()}
            {this.renderHideButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderAdd()}
            {this.renderRemove()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleSystemUnderTestToolbar.DEFAULT_MARKUP_SUT = {
  markup: `#### ***Add a new SUT***
  
***Set the SUT name and select the repo in which to put it.***`,
  markupStyle: {
    padding:'13px 8px 0px 11px'
  }
};

MiddleSystemUnderTestToolbar.DEFAULT_MARKUP_REPO = {
  markup: `#### ***Add a new Repo***
  
***Set the Repo name.***`,
  markupStyle: {
    padding:'13px 0px 0px 14px'
  }
};


//MiddleSystemUnderTestToolbar.contextType = RouterContext;
