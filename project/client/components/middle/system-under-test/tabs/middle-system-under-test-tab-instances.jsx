
'use strict';

import SystemUnderTestStore from '../../../../stores/system-under-test-store';
import { ActionSystemUnderTestInstancesMarkup, ActionSystemUnderTestInstancesMarkupChange, ActionSystemUnderTestInstancesMarkupCancel, ActionSystemUnderTestInstancesMarkupSave } from '../../../../actions/action-system-under-test/action-system-under-test';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataSystemUnderTestInstances from 'z-abs-complayer-markup-client/client/data/data-system-under-test/data-system-under-test-instances';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSystemUnderTestTabInstances extends ReactComponentStore {
  constructor(props) {
    super(props, [SystemUnderTestStore]);
    this.dataSystemUnderTestInstances = new DataSystemUnderTestInstances();
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.SystemUnderTestStore.instances, nextState.SystemUnderTestStore.instances);
  }
  
  _markupOpen() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestInstancesMarkup());
  }
  
  _markupSave() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestInstancesMarkupSave(this.state.SystemUnderTestStore.currentSut, this.state.SystemUnderTestStore.instances.content));
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestInstancesMarkupCancel());
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this.state.SystemUnderTestStore.instances.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = this.state.SystemUnderTestStore.instances.content === this.state.SystemUnderTestStore.instances.contentOriginal;
    return (
      <Button size="btn-xs" placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );    
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.SystemUnderTestStore.instances.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }

  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this.state.SystemUnderTestStore.instances.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkup() {
    return (
      <div className="markup_filter_horizontal_1">
        <div className="systems_under_test_group">
          <label htmlFor="system_under_test_instances_markup_textarea">Instances - Markup</label>
          <ComponentMarkedTextarea id="system_under_test_instances_markup_textarea" className="form-control content_definition same_size_as_parent" rows="10" value={this.state.SystemUnderTestStore.instances.content} results={this.state.SystemUnderTestStore.instances.rows}
            onChange={(value) => {
              this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestInstancesMarkupChange(value));
            }}
            />
        </div>
      </div>
    );
  }
  
  renderInstances() {
    if(this.state.SystemUnderTestStore.instances.definition) {
      return (
        <div className="systems_under_test_tab_view_inner">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="systems_under_test_tab_view_inner">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataSystemUnderTestInstances} values={this.state.SystemUnderTestStore.instances.instances}/>
        </div>
      );
    }
  }
  
  renderSpecificationInstancesDescription(sutName) {
    return (
      <div className="systems_under_test_specification_description">
        <h3 className="systems_under_test_specification">Instances:&#160;</h3>
        <h3 className="systems_under_test_specification_sut">{sutName}</h3>
      </div>
    );
  }

  renderSpecificationInstancesMarkup() {
    return (
      <div className="systems_under_test_specification_markup">
        <div className="systems_under_test_markup_panel">
          <div className="btn-toolbar" role="toolbar" aria-label="...">
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderMarkupOpenButton()}
              {this.renderMarkupSaveButton()}
              {this.renderMarkupHelpButton()}
              {this.renderMarkupCancelButton()}
            </div>
          </div>
        </div>
        {this.renderInstances()}
      </div>
    );
  }
  
  render() {
    const sut = this.state.SystemUnderTestStore.currentSut;
    if(sut) {
      return (
        <div className="systems_under_test_tab_view systems_under_test_tab_view_outer">
          {this.renderSpecificationInstancesDescription(sut.name)}
          {this.renderSpecificationInstancesMarkup()} 
        </div>
      );
    }
    else {
      return null;
    }
  }    
}

