
'use strict';

import SystemUnderTestStore from '../../../../stores/system-under-test-store';
import { ActionSystemUnderTestNodesMarkup, ActionSystemUnderTestNodesMarkupChange, ActionSystemUnderTestNodesMarkupCancel, ActionSystemUnderTestNodesMarkupSave } from '../../../../actions/action-system-under-test/action-system-under-test';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataSystemUnderTestNodes from 'z-abs-complayer-markup-client/client/data/data-system-under-test/data-system-under-test-nodes';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSystemUnderTestTabNodes extends ReactComponentStore {
  constructor(props) {
    super(props, [SystemUnderTestStore]);
    this.dataSystemUnderTestNodes = new DataSystemUnderTestNodes();
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.SystemUnderTestStore.nodes, nextState.SystemUnderTestStore.nodes);
  }
  
  _markupOpen() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestNodesMarkup());
  }
  
  _markupSave() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestNodesMarkupSave(this.state.SystemUnderTestStore.currentSut, this.state.SystemUnderTestStore.nodes.content));
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestNodesMarkupCancel());
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this.state.SystemUnderTestStore.nodes.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = this.state.SystemUnderTestStore.nodes.content === this.state.SystemUnderTestStore.nodes.contentOriginal;
    return (
      <Button size="btn-xs" placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );    
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.SystemUnderTestStore.nodes.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }

  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this.state.SystemUnderTestStore.nodes.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkup() {
    return (
      <div className="systems_under_test_group">
        <label htmlFor="system_under_test_nodes_markup_textarea">Nodes - Markup</label>
        <ComponentMarkedTextarea id="system_under_test_nodes_markup_textarea" className="form-control content_definition same_size_as_parent" rows="10" value={this.state.SystemUnderTestStore.nodes.content} results={this.state.SystemUnderTestStore.nodes.rows}
          onChange={(value) => {
            this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestNodesMarkupChange(value));
          }}
          />
      </div>
    );
  }
  
  renderNodes() {
    if(this.state.SystemUnderTestStore.nodes.definition) {
      return (
        <div className="systems_under_test_tab_view_inner">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="systems_under_test_tab_view">
          <ComponentTableDataTable showAlways classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataSystemUnderTestNodes} values={this.state.SystemUnderTestStore.nodes.nodes}/>
        </div>
      );
    }
  }
  
  renderSpecificationnodesDescription(sutName) {
    return (
      <div className="systems_under_test_specification_description">
        <h3 className="systems_under_test_specification">Nodes:&#160;</h3>
        <h3 className="systems_under_test_specification_sut">{sutName}</h3>
      </div>
    );
  }
  
  renderSpecificationNodesMarkup() {
    return (
      <div className="systems_under_test_specification_markup">
        <div className="systems_under_test_markup_panel">
          <div className="btn-toolbar" role="toolbar" aria-label="...">
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderMarkupOpenButton()}
              {this.renderMarkupSaveButton()}
              {this.renderMarkupHelpButton()}
              {this.renderMarkupCancelButton()}
            </div>
          </div>
        </div>
        {this.renderNodes()}
      </div>
    );
  }
  
  render() {
    const sut = this.state.SystemUnderTestStore.currentSut;
    if(sut) {
      return (
        <div className="systems_under_test_tab_view systems_under_test_tab_view_outer">
          {this.renderSpecificationnodesDescription(sut.name)}
          {this.renderSpecificationNodesMarkup()} 
        </div>
      );
    }
    else {
      return null;
    }
  }
}
