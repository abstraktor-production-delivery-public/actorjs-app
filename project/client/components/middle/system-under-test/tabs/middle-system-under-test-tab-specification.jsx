
'use strict';

import SystemUnderTestStore from '../../../../stores/system-under-test-store';
import { ActionSystemUnderTestSpecificationMarkup, ActionSystemUnderTestSpecificationMarkupChange, ActionSystemUnderTestSpecificationMarkupCancel, ActionSystemUnderTestSpecificationMarkupSave } from '../../../../actions/action-system-under-test/action-system-under-test';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import ScrollData from 'z-abs-complayer-markup-client/client/react-components/helper/scroll-data';
import ComponentDocument from 'z-abs-complayer-markup-client/client/react-components/markup/component-document';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSystemUnderTestTabSpecification extends ReactComponentStore {
  constructor(props) {
    super(props, [SystemUnderTestStore], {
      autoScroll: true,
      breaks: false,
      whiteSpaceClass: 'no_word_wrap',
      documentationOrder: true
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.boundKeyUp = this._keyUp.bind(this);
    this.ctrlKey = false;
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
    this.refDocumentDiv = React.createRef();
    this.refDocumentComponentDocument = React.createRef();
    this.refPreviewDiv = React.createRef();
    this.refPreviewComponentDocument = React.createRef();
    this.refMarkupComponentMarkedTextarea = React.createRef();
    this.scrollData = new ScrollData();
  }

  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
    window.addEventListener('keyup', this.boundKeyUp, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.autoScroll, nextState.autoScroll)
      || !this.shallowCompare(this.state.breaks, nextState.breaks)
      || !this.shallowCompare(this.state.whiteSpaceClass, nextState.whiteSpaceClass)
      || !this.shallowCompare(this.state.documentationOrder, nextState.documentationOrder)
      || !this.shallowCompare(this.state.SystemUnderTestStore.currentSut, nextState.SystemUnderTestStore.currentSut)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompare(this.state.SystemUnderTestStore.specification, nextState.SystemUnderTestStore.specification);
  }
  
  didUpdate(prevProps, prevState) { 
    if(this.state.SystemUnderTestStore.specification.definition) {
      if(!prevState.SystemUnderTestStore.specification.definition || this.state.documentationOrder !== prevState.documentationOrder) {
        const lines = this.state.SystemUnderTestStore.specification.contentLines;
        const lineHeight = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.scrollHeight / lines;
        const visibleLines = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.clientHeight / lineHeight;
        this.scrollData.setLength(Math.floor(lines - visibleLines));
        this.refPreviewComponentDocument.current.scroll(this.scrollData);
        this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
      }
      else {
        const lineDelta = this.state.SystemUnderTestStore.specification.contentLines - prevState.SystemUnderTestStore.specification.contentLines;
        if(0 !== lineDelta) {
          this.scrollData.stepLines(lineDelta);
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }
      }
    }
    else {
      if(null !== this.refDocumentComponentDocument.current) {
        this.refDocumentComponentDocument.current.scroll(this.scrollData);
      }
    }
  }
  
  willUnmount() {
    window.removeEventListener('keyup', this.boundKeyUp, true);
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSpecificationMarkup());
  }
  
  _markupSave() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSpecificationMarkupSave(this.state.SystemUnderTestStore.currentSut, this.state.SystemUnderTestStore.specification.content));
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSpecificationMarkupCancel());
  }
  
  _keyDown(e) {
    if(e.ctrlKey) {
      this.ctrlKey = true;
    }
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }

  _keyUp(e) {
    if(!e.ctrlKey) {
      this.ctrlKey = false;
    }
  }
  
  renderMarkupOpenButton() {
    this.markupDisabledOpen = this.state.SystemUnderTestStore.specification.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit testcase_specification_buton_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    this.markupDisabledSave = this.state.SystemUnderTestStore.specification.content === this.state.SystemUnderTestStore.specification.contentOriginal;
    return (
      <Button size="btn-xs" placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );    
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.SystemUnderTestStore.specification.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }

  renderMarkupCancelButton() {
    this.markupDisabledCancel = !this.state.SystemUnderTestStore.specification.definition;
    return (
      <Button size="btn-xs" placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle testcase_specification_button_color" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderHorizontalOrVerticalButton() {
    let toolTipInstruction;
    let image1Style;
    let image2Style;
    if(this.state.documentationOrder) {
      toolTipInstruction = 'Vertical';
      image1Style = {top:'7px',left:'0px',transform:'scale(0.91, 0.67)'};
      image2Style = {width:'0px',top:'-2px',left:'-11px',transform:'scale(0.91, 0.67)'};
    }
    else {
      toolTipInstruction = 'Horizontal';
      image1Style = {top:'2px',left:'-6px',transform:'scale(0.91, 0.67)'};
      image2Style = {width:'0px',top:'2px',left:'-6px',transform:'scale(0.91, 0.67)'};
    }
    return (
      <Button size="btn-xs" placement="bottom" heading={`${toolTipInstruction}`} content="Document Order" disabled={!this.state.SystemUnderTestStore.specification.definition}
        onClick={(e) => {
          this.updateState({
            documentationOrder: {$set: !this.state.documentationOrder}
          });
        }}
      >
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image1Style}></span>
        <span className="glyphicon glyphicon-modal-window" aria-hidden="true" style={image2Style}></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollFirstLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="First line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineFirst();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollPreviousBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'previous_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollPreviousLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.decrement();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollNextLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.increment();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollNextBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'next_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkupScrollEndLine() {
    const disabled = !this.state.autoScroll;
    return (
      <Button className="markup_button_right" type="transparent" placement="bottom" heading="Goto" content="End line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineLast();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderSpecificationMarkup() {
    return (
      <div className={`markup_filter_${this.state.documentationOrder ? 'horizontal' : 'vertical'}_2`}>
        <div className="systems_under_test_group">
          <div className="markup_heading">
            <p className="systems_under_test_specification">Specification - Markup</p>
            <p className="markup_checkbox">
              auto-scroll:
            </p>
            <input type="checkbox" id="sut_specification_markup_auto-scroll" className="markup_checkbox" aria-label="..." autoComplete="off" checked={this.state.autoScroll}
              onChange={(e) => {
                if(this.state.autoScroll) {
                  this.updateState({
                    autoScroll: {$set: false}
                  });
                }
                else {
                  this.updateState({
                    autoScroll: {$set: true}
                  });
                }
              }}
              />
            <p className="markup_checkbox">
              breaks:
            </p>
            <input type="checkbox" id="sut_specification_markup_breaks" className="markup_checkbox" aria-label="..." autoComplete="off" checked={this.state.breaks}
              onChange={(e) => {
                if(this.state.breaks) {
                  this.updateState({
                    breaks: {$set: false},
                    whiteSpaceClass: {$set: 'no_word_wrap'}
                  });
                }
                else {
                  this.updateState({
                    breaks: {$set: true},
                    whiteSpaceClass: {$set: 'word_wrap'}
                  });
                }
              }}
              />
            {this.renderSpecificationMarkupScrollEndLine()}
            {this.renderSpecificationMarkupScrollNextBlock()}
            {this.renderSpecificationMarkupScrollNextLine()}
            {this.renderSpecificationMarkupScrollPreviousLine()}
            {this.renderSpecificationMarkupScrollPreviousBlock()}
            {this.renderSpecificationMarkupScrollFirstLine()}
          </div>
          <ComponentMarkedTextarea ref={this.refMarkupComponentMarkedTextarea} id="system_under_test_specification_markup_textarea" className={this.state.whiteSpaceClass + " form-control content_definition"} rows="10" value={this.state.SystemUnderTestStore.specification.content}
            onScroll={(e) => {
              const lines = this.state.SystemUnderTestStore.specification.contentLines;
              const scrollLines = (e.target.scrollHeight - e.target.clientHeight) / e.target.scrollHeight * lines;
              const scrollLine = e.target.scrollTop / (e.target.scrollHeight - e.target.clientHeight) * scrollLines;
              const line = Math.floor(scrollLine);
              this.scrollData.setLine(line, scrollLine - line);
              if(this.state.autoScroll) {
                this.refPreviewComponentDocument.current.scroll(this.scrollData);
              }
            }}
            onChange={(value) => {
              this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSpecificationMarkupChange(value));
            }}
          />
        </div>
      </div>
    );
  }
  
  renderSpecificationPreview() {
    const storeState = this.state.SystemUnderTestStore.specification.documentationPreview;
    const lines = this.state.SystemUnderTestStore.specification.contentLines;
    return (
      <div className={`markup_filter_${this.state.documentationOrder ? 'horizontal' : 'vertical'}_2`}>
        <div className="systems_under_test_group">
          <div className="markup_heading">
            <p className="systems_under_test_specification">Specification - Preview</p>
          </div>
          <div ref={this.refPreviewDiv} id="sut_specification_markup_preview" className="systems_under_test_specification_preview"
            onScroll={(e) => {
              this.scrollData.setScroll(e.target.scrollTop);
            }}
          >
            <div className="doc_nav_preview_middle">
              <div className="doc_nav_preview_inner"
                onMouseUp={(e) => {
                  if(this.ctrlKey) {
                    this.scrollData.setScroll(e.target.offsetTop);
                    this.refPreviewComponentDocument.current.calculateTop(this.scrollData);
                    this.refPreviewComponentDocument.current.scroll( this.scrollData);
                    this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
                  }
                }}
                >
                <ComponentDocument ref={this.refPreviewComponentDocument} preview lines={lines} document={storeState.document}
                  onEditorScroll={(top) => {
                    this.refPreviewDiv.current.scrollTop = top;
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  renderSpecification() {
    if(this.state.SystemUnderTestStore.specification.definition) {
      return (
        <div className="systems_under_test_tab_view_inner">
          {this.renderSpecificationMarkup()}
          {this.renderSpecificationPreview()}
        </div>
      );
    }
    else {
      return (
        <div ref={this.refDocumentDiv} className="systems_under_test_tab_view_inner"
          onScroll={(e) => {
            this.scrollData.setScroll(e.target.scrollTop);
          }}
        >
        <ComponentDocument ref={this.refDocumentComponentDocument} document={this.state.SystemUnderTestStore.specification.document}
          onEditorScroll={(top) => {
            this.refDocumentDiv.current.scrollTop = top;
          }}
        />
        </div>
      );
    }
  }
  
  renderDescription(sutName, description) {
    return (
      <div className="systems_under_test_specification_description">
        <h3 className="systems_under_test_specification">Specification:&#160;</h3>
        <h3 className="systems_under_test_specification_sut">{sutName}</h3>
        <p className="systems_under_test_specification_data_last">Description:&#160;</p>
        <p>{description}</p>
      </div>
    );
  }
  
  renderMarkup() {
    return (
      <div className="systems_under_test_specification_markup">
        <div className="systems_under_test_markup_panel">
          <div className="btn-toolbar" role="toolbar" aria-label="...">
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderMarkupOpenButton()}
              {this.renderMarkupSaveButton()}
              {this.renderMarkupHelpButton()}
              {this.renderMarkupCancelButton()}
            </div>
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderHorizontalOrVerticalButton()}
            </div>
          </div>
        </div>
        {this.renderSpecification()}
      </div>
    );
  }
  
  render() {
    const sut = this.state.SystemUnderTestStore.currentSut;
    if(sut) {
      return (
        <div className="systems_under_test_tab_view systems_under_test_tab_view_outer">
          {this.renderDescription(sut.name, sut.description)}
          {this.renderMarkup()} 
        </div>
      );
    }
    else {
      return null;
    }
  }
}
