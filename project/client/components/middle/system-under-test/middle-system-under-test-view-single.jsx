
'use strict';

import SystemUnderTestStore from '../../../stores/system-under-test-store';
import StyleStore from 'z-abs-complayer-markup-client/client/stores/style-store';
import MiddleSystemUnderTestTabInstances from './tabs/middle-system-under-test-tab-instances';
import MiddleSystemUnderTestTabNodes from './tabs/middle-system-under-test-tab-nodes';
import MiddleSystemUnderTestTabSpecification from './tabs/middle-system-under-test-tab-specification';
import { ActionSystemUnderTestSet } from '../../../actions/action-system-under-test/action-system-under-test';
import ComponentBreadcrump from 'z-abs-complayer-bootstrap-client/client/breadcrump';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSystemUnderTestViewSingle extends ReactComponentStore {
  constructor(props) {
    super(props, [SystemUnderTestStore, StyleStore], {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['Specification', 'Instances', 'Nodes']);
    const key = this.helperTab.getIndex(props.tab);
    this.state.activeKey = undefined !== key ? key : 0;
  }
  
  didMount() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
      || !this.shallowCompare(this.state.StyleStore.stackStyles, nextState.StyleStore.stackStyles);
  }

  didUpdate(prevProps, prevState) {
    const key = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== key) {
      this.updateState({activeKey: {$set: key}});
    }
  }
  
  getTabPath(index) {
    return `${this.props.tab ? '' : this.props.sut + '/'}${this.helperTab.getRoute(index)}`;
  }
  
  render() {
    let index = 0;
    return (
      <div className="middle_view middle_view_system_under_tests middle_system_under_test_single_view">
        <div>
          <ComponentBreadcrump items={[
            {name: 'Systems Under Test', link: ''},
            {name: this.props.sut, link: `${this.props.sut}`},
          ]}/>
        </div>
        <div className="system_under_test">
          <Tabs id="test_case_tabs" className="same_size_as_parent" activeKey={this.state.activeKey} animation={false}
            onSelect={(key) => {
              if(undefined !== key) {
                this.context.history(`${this.props.sut}/${this.helperTab.getRoute(key)}`, {replace: true});
              }
            }}
          >
            <Tab id="sut_specification" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
              <MiddleSystemUnderTestTabSpecification sut={this.props.sut} />
            </Tab>
            <Tab id="sut_instances" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
              <MiddleSystemUnderTestTabInstances sut={this.props.sut} />
            </Tab>
            <Tab id="sut_nodes" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
              <MiddleSystemUnderTestTabNodes sut={this.props.sut} />
            </Tab>
          </Tabs>
        </div>
      </div>
    );
  }
}

MiddleSystemUnderTestViewSingle.contextType = RouterContext;
