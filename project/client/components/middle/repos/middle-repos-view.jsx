
'use strict';

import ReposStore from '../../../stores/repos-store';
import { ActionReposGet } from '../../../actions/action-repos/action-repos';
import DataReposContentLocal from 'z-abs-complayer-markup-client/client/data/data-repos/data-repos-content-local';
import DataReposDataLocal from 'z-abs-complayer-markup-client/client/data/data-repos/data-repos-data-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleReposView extends ReactComponentStore {
  constructor(props) {
    super(props, [ReposStore]);
    this.dataReposContentLocal = new DataReposContentLocal();
    this.dataReposDataLocal = new DataReposDataLocal();
  }
  
  didMount() {
    this.dispatch(ReposStore, new ActionReposGet());
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.ReposStore.markup, nextState.ReposStore.markup)
      || !this.shallowCompareArrayValues(this.state.ReposStore.actorjsContentLocalRepos, nextState.ReposStore.actorjsContentLocalRepos)
      || !this.shallowCompareArrayValues(this.state.ReposStore.actorjsDataLocalRepos, nextState.ReposStore.actorjsDataLocalRepos);
  }
  
  render() {
    if(this.state.ReposStore.markup.definition) {
      return (
        <div className="same_size_as_parent">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="repos_view same_size_as_parent">
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" dataTable={this.dataReposContentLocal} values={this.state.ReposStore.actorjsContentLocalRepos} showAlways/>
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" dataTable={this.dataReposDataLocal} values={this.state.ReposStore.actorjsDataLocalRepos} showAlways/>
        </div>
      );
    }
  }
}
