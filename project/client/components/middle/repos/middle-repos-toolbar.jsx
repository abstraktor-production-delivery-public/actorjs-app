
'use strict';

import Button from 'z-abs-complayer-bootstrap-client/client/button';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleReposToolbar extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
  //  this.dispatch(TestDataStore, new ActionTestDataMarkup());
  }
  
  _markupSave() {
  //  this.dispatch(TestDataStore, new ActionTestDataMarkupSave());
  }
  
  _markupHelp() {
  
  }
  
  _markupCancel() {
  //  this.dispatch(TestDataStore, new ActionTestDataMarkupCancel());
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  renderLoginButton() {
    return (
      <Button id="from_start_to_login" placement="bottom" heading="Goto" content="Login"
        onClick={(e) => {
          this.context.history(`/login`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDocumentationButton() {
    return (
      <Button id="from_start_to_documentation" heading="Goto" content="Documentation"
        onClick={(e) => {
          this.context.history(`/documentation`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderEducationButton() {
    return (
      <Button id="from_start_to_education" heading="Goto" content="Education"
        onClick={(e) => {
          this.context.history(`/education`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-education" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderWorkshopButton() {
    return (
      <Button id="from_start_to_workshop" heading="Goto" content="Workshop"
        onClick={(e) => {
          this.context.history(`/workshop`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-leaf" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderLocalDocumentationButton(repoName) {
    return (
      <Button id={`from_start_to_local_documentation_${repoName}`} heading="Goto" content={`Documentation ${repoName}`}
        onClick={(e) => {
          this.context.history(`/local-documentation-${repoName}`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-pencil" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderLocalDocumentationButtons() {
    if(this.serviceExists('z-abs-servicelayer-docs-client')) {
       return this.renderLocalDocumentationButton('Local');
     }
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="toolbar" role="toolbar"  aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderLoginButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderDocumentationButton()}
            {this.renderEducationButton()}
            {this.renderWorkshopButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderLocalDocumentationButtons()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleReposToolbar.contextType = RouterContext;
