
'use strict';

import MiddleReposToolbar from './middle-repos-toolbar';
import MiddleReposView from './middle-repos-view';
import MiddleSidebar from '../middle_sidebar';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleRepos extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Repos'
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle middle_repos">
        <MiddleReposToolbar />
        <MiddleSidebar />
        <MiddleReposView />
      </div>
    );
  }
}

