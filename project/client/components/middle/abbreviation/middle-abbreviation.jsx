
'use strict';

import MiddleAbbreviationToolbar from './middle-abbreviation-toolbar';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleAbbreviation extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Abbrevation'
      };
    });
  }
  
  render() {
    return (
      <div className="middle middle_code_editor">
        <MiddleAbbreviationToolbar />
      </div>
    );
  }
}
