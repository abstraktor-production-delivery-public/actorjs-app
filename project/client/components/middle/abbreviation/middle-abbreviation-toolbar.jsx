
'use strict';


import React from 'react';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import AbbreviationStore from '../../../stores/abbreviation-store';


export default class MiddleAbbreviationToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [AbbreviationStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
  }
  
  _markupSave() {
  }
  
  _markupHelp() {
  }
  
  _markupCancel() {
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
  }
  
  renderMarkupOpenButton() {
    return (
      <Button placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.state.AbbreviationStore.markup.definition}
        onClick={(e) => {
          this._markupOpen();
        }}
      >
        <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupSaveButton() {
    return (
      <Button placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.state.AbbreviationStore.markup.content === this.state.AbbreviationStore.markup.contentOriginal}
        onClick={(e) => {
          this._markupSave();
        }}
      >
        <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupHelpButton() {
    return (
      <Button placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.state.AbbreviationStore.markup.definition}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    return (
      <Button placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={!this.state.AbbreviationStore.markup.definition}
        onClick={(e) => {
          this._markupCancel();
        }}
      >
        <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar">
	      <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
          </div>
        </div>
      </div>
    );
  }
}
