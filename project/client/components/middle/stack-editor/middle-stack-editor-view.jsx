
'use strict';

import StackEditorStore from '../../../stores/stack-editor-store';
import { ActionStackEditorFileGet, ActionStackEditorFileSet, ActionStackEditorFileEdit, ActionStackEditorFileClose, ActionStackEditorFileMove } from '../../../actions/action-stack-editor/action-stack-editor-file';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/client/code-mirror-editor';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import LogType from 'z-abs-funclayer-engine-cs/clientServer/log/log-type';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStackEditorView extends ReactComponentStore {
  constructor(props) {
    super(props, [StackEditorStore]);
    this.close = false;
  }
  
  didMount() {
    if(this.props._uriPath && 0 < this.props._uriPath.length) {
      this.dispatch(StackEditorStore, new ActionStackEditorFileSet(`.${this.props._uriPath}`, this.props.location.search));
    }
    else {
      this.dispatch(StackEditorStore, new ActionStackEditorFileSet());
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath)
      || !this.shallowCompare(this.props.location.search, nextProps.location.search)
      || !this.shallowCompareMapValues(this.state.StackEditorStore.files, nextState.StackEditorStore.files)
      || !this.shallowCompare(this.state.StackEditorStore.current, nextState.StackEditorStore.current)
      || !this.shallowCompare(this.state.StackEditorStore.persistentData, nextState.StackEditorStore.persistentData)
      || !this.shallowCompare(this.state.StackEditorStore.build, nextState.StackEditorStore.build);
  }
    
  didUpdate(prevProps, prevState) {
    if(this.props._uriPath !== prevProps._uriPath) {
      this.dispatch(StackEditorStore, new ActionStackEditorFileSet(`.${this.props._uriPath}`, this.props.location.search));
    }
  }
  
  renderTab(options) {
    const result = [];
    const queryState = this.state.StackEditorStore.current.query;  
    const query = !queryState ? undefined : {
      url: queryState.url,
      line: queryState.line,
      typeCss: LogType.csses[Number.parseInt(this.state.StackEditorStore.current.query.type)]
    };
    this.state.StackEditorStore.files.forEach((file, key) => {
      if(null !== file.content) {
        result.push(
          <Tab eventKey={key} key={key} id={key} 
            title={
              <div>
                {file.title}
                <button type="button" className={this.theme(this.state.StackEditorStore.persistentData.darkMode, 'middle_code_editor_close')} aria-label="Close"
                  onClick={(e, a) => {
                    this.close = true;
                    this.dispatch(StackEditorStore, new ActionStackEditorFileClose(key));
                  }}
                >
                  ×
                </button>
              </div>
            }
            onMove={(fromKey, toKey) => {
              this.dispatch(StackEditorStore, new ActionStackEditorFileMove(fromKey, toKey));
            }}
          >
            <CodeMirrorEditor id={`stack_editor_view_${key}`} projectId={file.projectId} docKey={key} code={file.content} build={this.state.StackEditorStore.build} options={this.getOptions(file.type, key)} currentType={this.state.StackEditorStore.current.type} currentFile={this.state.StackEditorStore.current.file} currentQuery={query} darkMode={this.state.StackEditorStore.persistentData.darkMode} emptyLine={this.state.StackEditorStore.persistentData.emptyLine}
              onChanged={(projectId, docKey, code) => {
                this.dispatch(StackEditorStore, new ActionStackEditorFileEdit(projectId, docKey, code));
              }}
              onFocusChange={(focused, docKey) => {
                if(focused) {
                  const file = this.state.StackEditorStore.files.get(docKey);
                  if(file) {
                    this.dispatch(StackEditorStore, new ActionStackEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
                  }
                }
              }}
            />
          </Tab>
        );
      }
    });
    return result;
  }

  renderTabs(options) {
    if(0 !== this.state.StackEditorStore.files.size) {
      return (
        <Tabs id="stack_editor_tabs" draggable="true" activeKey={null !== this.state.StackEditorStore.current.file ? this.state.StackEditorStore.current.file.key : null} darkMode={this.state.StackEditorStore.persistentData.darkMode}
          onSelect={(selectedKey) => {
            if(!this.close) {
              const file = this.state.StackEditorStore.files.get(selectedKey);
              this.dispatch(StackEditorStore, new ActionStackEditorFileGet(file.projectId, file.key, file.title, file.path, file.type));
            }
            else {
              this.close = false;
            }
          }}
        >
          {this.renderTab(options)}
        </Tabs>
      );
    }
    else {
      return null;
    }
  }

  getOptions(type, key) {
    return {
      lineNumbers: true,
      highlightActiveLineGutter: true,
      highlightActiveLine: true,
      closeBrackets: true,
      bracketMatching: true,
      drawSelection: true,
      history: true,
      autocompletion: true,

      tabKeyAction: 'indent', 
      tabSize: 2,
      indentType: 'spaces',
      indentUnit: 2,
      
      darkMode: true,
      breakpointGutter: false,
      foldGutter: true,
      emptyLine: true,

      buildGutter: true,
      
      type
    };
  }
  
  render() {
    const darkMode = this.state.StackEditorStore.persistentData.darkMode;
    return (
      <div className={this.theme(darkMode, "view")}>
        {this.renderTabs()}
      </div>
    );
  }
}


MiddleStackEditorView.contextType = RouterContext;
MiddleStackEditorView.REMOVE_KEY_LENGTH = 'stack_editor_tabs-tab-'.length;
