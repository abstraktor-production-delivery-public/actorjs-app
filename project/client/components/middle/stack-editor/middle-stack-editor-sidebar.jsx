
'use strict';

import StackEditorStore from '../../../stores/stack-editor-store';
import { ActionStackEditorFileGet } from '../../../actions/action-stack-editor/action-stack-editor-file';
import { ActionStackEditorFolderGet } from '../../../actions/action-stack-editor/action-stack-editor-folder';
import { ActionStackEditorProjectToggle } from '../../../actions/action-stack-editor/action-stack-editor-project';
import FileIcons from 'z-abs-complayer-bootstrap-client/client/icons/file-icons';
import FolderIcons from 'z-abs-complayer-bootstrap-client/client/icons/folder-icons';
import Tree from 'z-abs-complayer-tree-client/client/react-components/tree';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStackEditorSidebar extends ReactComponentStore {
  constructor(props) {
    super(props, [StackEditorStore]);
    this.boundMouseUp = this._boundMouseUp.bind(this);
    this.boundMouseMove = this._boundMouseMove.bind(this);
    this.sidebarNode = null;
    this.sidebarWidthMin = 180;
    this.sidebarWidthMax = 720;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.StackEditorStore.projectLocal, nextState.StackEditorStore.projectLocal)
      || !this.shallowCompare(this.state.StackEditorStore.projectGlobal, nextState.StackEditorStore.projectGlobal)
      || !this.shallowCompare(this.state.StackEditorStore.current, nextState.StackEditorStore.current)
      || !this.shallowCompare(this.state.StackEditorStore.persistentData, nextState.StackEditorStore.persistentData);
  }
  
  _boundMouseUp(e) {
    window.removeEventListener('mouseup', this.boundMouseUp, true);
    window.removeEventListener('mousemove', this.boundMouseMove, true);
    this.sidebarNode = null;
  }
  
  _boundMouseMove(e) {
    let width = e.clientX + 2;
    if(width < this.sidebarWidthMin) {
      width = this.sidebarWidthMin;
    }
    else if(width > this.sidebarWidthMax) {
      width = this.sidebarWidthMax;
    }
    this.sidebarNode.style.width = `${width}px`;
    this.sidebarNode.nextSibling.style.left = `${width}px`;
  }
  
  render() {
    const stylePanel = {
      height:'100%'
    };
    const darkMode = this.state.StackEditorStore.persistentData.darkMode ;
    return (
      <div className={this.theme(darkMode, "sidebar", "middle_stack_editor_sidebar")}>
        <div className={this.theme(darkMode, "panel-default", "panel")} style={stylePanel}>
          <div className={this.theme(darkMode, "panel_body_sidebar")}>
            <div id="stack_editor_tree_root">
              <Tree key={this.state.StackEditorStore.projectLocal.projectId} name={'local'} id="stack_editor_tree_code_local" project={this.state.StackEditorStore.projectLocal} current={this.state.StackEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.StackEditorStore.persistentData.darkMode}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorFileGet(this.state.StackEditorStore.projectLocal.projectId, key, title, path, type));
                }}
                onClickFolder={(key, title, data) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorFolderGet(this.state.StackEditorStore.projectLocal.projectId, key, title, data));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorProjectToggle(this.state.StackEditorStore.projectLocal.projectId, key, expanded));
                }}
              />
              <Tree key={this.state.StackEditorStore.projectGlobal.projectId} name={'global'} id="stack_editor_tree_code_global" project={this.state.StackEditorStore.projectGlobal} current={this.state.StackEditorStore.current} folderIcons={FolderIcons} fileIcons={FileIcons} darkMode={this.state.StackEditorStore.persistentData.darkMode}
                onClickFile={(key, title, path, type) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorFileGet(this.state.StackEditorStore.projectGlobal.projectId, key, title, path, type));
                }}
                onClickFolder={(key, title, data) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorFolderGet(this.state.StackEditorStore.projectGlobal.projectId, key, title, data));
                }}
                onToggleFolder={(key, expanded) => {
                  this.dispatch(StackEditorStore, new ActionStackEditorProjectToggle(this.state.StackEditorStore.projectGlobal.projectId, key, expanded));
                }}
              />
            </div>
          </div>
        </div>
        <div className={this.theme(darkMode, "sidebar_mover")}
          onMouseDown={(e) => {
            if(0 === e.button) {
              this.sidebarNode = e.target.parentNode;
              window.addEventListener('mouseup', this.boundMouseUp, true);
              window.addEventListener('mousemove', this.boundMouseMove, true);
            }
          }}
        ></div>
      </div>
    );
  }
}


MiddleStackEditorSidebar.contextType = RouterContext;
