
'use strict';

import StackEditorStore from '../../../stores/stack-editor-store';
import { ActionStackEditorPersistentDataUpdate } from '../../../actions/action-stack-editor/action-stack-editor-persistent-data';
import { ActionStackEditorProjectUpdate, ActionStackEditorWizard } from '../../../actions/action-stack-editor/action-stack-editor-project';
import { ActionStackEditorFolderNew, ActionStackEditorFolderAdd, ActionStackEditorFolderUpdate, ActionStackEditorFolderRemove, ActionStackEditorFolderDelete } from '../../../actions/action-stack-editor/action-stack-editor-folder';
import { ActionStackEditorFileNew, ActionStackEditorFileAdd, ActionStackEditorFileRename, ActionStackEditorFileUpdate, ActionStackEditorFileUpdateAll, ActionStackEditorFileRemove, ActionStackEditorFileDelete, ActionStackEditorBuild } from '../../../actions/action-stack-editor/action-stack-editor-file';
import ModalDialogStore from 'z-abs-complayer-modaldialog-client/client/stores/modal-dialog-store';
import { ActionModalDialogShow, ActionModalDialogHide, ActionModalDialogPending, ActionModalDialogResult } from 'z-abs-complayer-modaldialog-client/client/actions/action-modal-dialog';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ModalDialogWizard from 'z-abs-complayer-modaldialog-client/client/modal-dialog-wizard';
import ModalDialogWizardList from 'z-abs-complayer-modaldialog-client/client/modal-dialog-wizard-list';
import ModalDialogFolderAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-add';
import ModalDialogFolderNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-new';
import ModalDialogFolderProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-properties';
import ModalDialogFolderRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-folder-remove';
import ModalDialogFileAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-add';
import ModalDialogFileNew from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-new';
import ModalDialogFileProperties from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-properties';
import ModalDialogFileRemove from 'z-abs-complayer-modaldialog-client/client/modal-dialog-file-remove';
import { ActionDialogFileGet, ActionDialogFileSet } from 'z-abs-corelayer-client/client/actions/action-dialog-file';
import DialogFileStore from 'z-abs-corelayer-client/client/stores/dialog-file-store';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';

 
export default class MiddleStackEditorToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [StackEditorStore, ModalDialogStore, DialogFileStore]);
    this._modalDialogStackWizard = null;
    this.boundKeyDown = this._keyDown.bind(this);
    this.buttonFileSaveDisabled = true;
    this.buttonFileSaveAllDisabled = true;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.StackEditorStore.projectLocal, nextState.StackEditorStore.projectLocal)
      || !this.shallowCompare(this.state.StackEditorStore.projectGlobal, nextState.StackEditorStore.projectGlobal)
      || !this.shallowCompare(this.state.StackEditorStore.current, nextState.StackEditorStore.current)
      || !this.shallowCompareMapValues(this.state.StackEditorStore.files, nextState.StackEditorStore.files)
      || !this.shallowCompare(this.state.StackEditorStore.persistentData, nextState.StackEditorStore.persistentData)
      || !this.shallowCompare(this.state.StackEditorStore.build, nextState.StackEditorStore.build)
      || !this.shallowCompareMapValues(this.state.ModalDialogStore.modalResults, nextState.ModalDialogStore.modalResults)
      || !this.shallowCompare(this.state.DialogFileStore.project, nextState.DialogFileStore.project)
      || !this.shallowCompare(this.state.DialogFileStore.current, nextState.DialogFileStore.current)
      || !this.shallowCompare(this.state.DialogFileStore.original, nextState.DialogFileStore.original);
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _fileSave() {
    this.dispatch(StackEditorStore, new ActionStackEditorFileUpdate(this.state.StackEditorStore.current.file.key));
  }

  _fileSaveAll() {
    const keys = [];
    this.state.StackEditorStore.files.forEach((file) => { 
      if(file.codeChanged) {
        keys.push(file.key);
      }
    });
    this.dispatch(StackEditorStore, new ActionStackEditorFileUpdateAll(keys));
  }
  
  _keyDown(e) {
    if(e.ctrlKey && e.shiftKey && 'S' === e.key) {
      if(!this.buttonFileSaveAllDisabled) {
        e.preventDefault();
        this._fileSaveAll();
      }
    }
    if(e.ctrlKey && 's' === e.key) {
      if(!this.buttonFileSaveDisabled) {
        e.preventDefault();
        this._fileSave();
      }
    }
  }
  
  renderButtonSaveProject() {
    const disabled = this.state.StackEditorStore.projectGlobal.isSaved() && this.state.StackEditorStore.projectLocal.isSaved();
    return (
      <Button id="stack_save_project" placement="bottom" heading="Save" content="Project" disabled={disabled}
        onClick={(e) => {
          if(!this.state.StackEditorStore.projectGlobal.isSaved()) {
            this.dispatch(StackEditorStore, new ActionStackEditorProjectUpdate(this.state.StackEditorStore.projectGlobal.projectId));
          }
          if(!this.state.StackEditorStore.projectLocal.isSaved()) {
            this.dispatch(StackEditorStore, new ActionStackEditorProjectUpdate(this.state.StackEditorStore.projectLocal.projectId));
          }
        }}
      >
        <span className="glyphicon glyphicon_project glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon_project glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }

  renderButtonFolderNew() {
    const disabled = 'folder' !== this.state.StackEditorStore.current.type || '.' === this.state.StackEditorStore.current.folder?.data.path;
    return (
      <Button id="stack_folder_new" placement="bottom" heading="New" content="Folder" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_Folder_New'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFolderAdd() {
    const disabled = 'folder' !== this.state.StackEditorStore.current.type;
    return (
      <Button id="stack_folder_add" placement="bottom" heading="Add" content="Folder" disabled={disabled}
        onClick={(e) => {
          const folder = this.state.StackEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = StackEditorStore.getProject(folder.projectId);
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'folder'));
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_Folder_Add'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonStackWizard() {
    return (
      <Button id="stack_wizart" placement="bottom" heading="Wizard" content="Stack" disabled={'folder' !== this.state.StackEditorStore.current.type || '.' !== this.state.StackEditorStore.current.folder.data.path}
        onClick={(e) => {
          this._modalDialogStackWizard.show(this.state.StackEditorStore.current.folder);
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-star-empty" aria-hidden="true" style={{top: -4, left: 1, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderProperties() {
    return (
      <Button id="stack_folder_properties" placement="bottom" heading="Properties" content="Folder" disabled={'folder' !== this.state.StackEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_Folder_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFolderDelete() {
    const disabled = 'folder' !== this.state.StackEditorStore.current.type || !(this.state.StackEditorStore.projectGlobal.isSaved() && this.state.StackEditorStore.projectLocal.isSaved());
    return (
      <Button id="stack_folder_delete" placement="bottom" heading="Remove or Delete" content="Folder" disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_Folder_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-folder-close" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileNew() {
    return (
      <Button id="stack_file_new" placement="bottom" heading="New" content="File" disabled={'folder' !== this.state.StackEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_File_New'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderButtonFileAdd() {
    return (
      <Button id="stack_file_add" placement="bottom" heading="Add" content="File" disabled={'folder' !== this.state.StackEditorStore.current.type}
        onClick={(e) => {
          const folder = this.state.StackEditorStore.current.folder;
          const dir = `${folder.data.path}/${folder.title}`;
          const project = StackEditorStore.getProject(folder.projectId);
          this.dispatch(DialogFileStore, new ActionDialogFileGet(dir, project.getRootName(), [dir], project.getFileNames(dir), 'file'));
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_File_Add'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-up" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSave() {
    this.buttonFileSaveDisabled = 'file' !== this.state.StackEditorStore.current.type || !this.state.StackEditorStore.current.file.codeChanged;
    return (
      <Button id="stack_file_save" placement="bottom" heading="Save" content="File" shortcut="Ctrl+S" disabled={this.buttonFileSaveDisabled}
        onClick={(e) => {
          this._fileSave();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileSaveAll() {
    this.buttonFileSaveAllDisabled = true;
    this.state.StackEditorStore.files.forEach((file) => { 
      this.buttonFileSaveAllDisabled = this.buttonFileSaveAllDisabled && !file.codeChanged;
    });
    return (
      <Button id="stack_file_save_all" placement="bottom" heading="Save" content="All Files" shortcut="Ctrl+Shift+S" disabled={this.buttonFileSaveAllDisabled}
        onClick={(e) => {
          this._fileSaveAll();
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: -1, left: -4, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: 3, left: -10, width: 0, transform: 'scale(0.9)'}}></span>
        <span className="glyphicon glyphicon-circle-arrow-down" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileProperties() {
    return (
      <Button id="stack_file_properties" placement="bottom" heading="Properties" content="File" disabled={'file' !== this.state.StackEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_File_Properties'));
        }}
      >
        <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-wrench" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderButtonFileDelete() {
    const disabled = ('file' !== this.state.StackEditorStore.current.type && 'invalid_file' !== this.state.StackEditorStore.current.type) || !(this.state.StackEditorStore.projectGlobal.isSaved() && this.state.StackEditorStore.projectLocal.isSaved());
    return (
      <Button id="stack_file_delete" placement="bottom" heading="Remove or Delete" content="File"  disabled={disabled}
        onClick={(e) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogShow('Stack_File_Remove'));
        }}
      >
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{left: 3}}></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{left: -17, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
    
  renderBuildButton() {
    const result = this.state.StackEditorStore.build.result;
    const success = this.state.StackEditorStore.build.success;
    let className;
    if(result) {
      if(success) {
        className = "stack_editor_success";
      }
      else {
        className = "stack_editor_error";
      }
    }
    return (
      <Button id="stack_check" className={className} placement="bottom" heading="Check" content="Code" disabled={'file' !== this.state.StackEditorStore.current.type}
        onClick={(e) => {
          this.dispatch(StackEditorStore, new ActionStackEditorBuild());
        }}
      >
        <span className="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderModalDialogFolderNew() {
    const name = 'Stack_Folder_New';
    return (
      <ModalDialogFolderNew name={name} folder={this.state.StackEditorStore.current.folder} heading="New Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderNew={(projectId, key, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFolderNew(projectId, key, title, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderAdd() {
    const name = 'Stack_Folder_Add';
    const folder = this.state.StackEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFolderAdd name={name} heading="Add Folder" folder={this.state.StackEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFolderAdd(projectId, title, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderProperties() {
    const name = 'Stack_Folder_Properties';
    return (
      <ModalDialogFolderProperties name={name} folder={this.state.StackEditorStore.current.folder} heading="Folder Properties" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderProperties={(projectId, title, newTitle, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFolderUpdate(projectId, title, newTitle, data), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFolderRemove() {
    const name = 'Stack_Folder_Remove';
    return (
      <ModalDialogFolderRemove name={name} folder={this.state.StackEditorStore.current.folder} heading="Remove or Delete Folder" modalResults={this.state.ModalDialogStore.modalResults}
        onFolderRemove={(projectId, title, key, data) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFolderRemove(projectId, title, key, data));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFolderDelete={(projectId, path, title, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFolderDelete(projectId, path, title, key), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileNew() {
    const name = 'Stack_File_New';
    return (
      <ModalDialogFileNew name={name} fileType heading="New Stack File" folder={this.state.StackEditorStore.current.folder} templateNames={MiddleStackEditorToolbar.TEMPLATE_DATAS} modalResults={this.state.ModalDialogStore.modalResults}
        onFileNew={(projectId, path, title, type, templateName) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFileNew(projectId, path, title, type, templateName), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogStackWizard() {
    let chosenTransportName = 'tcp';
    return (
      <ModalDialogWizard ref={(c) => this._modalDialogStackWizard = c} heading="Stack Wizard" namePlaceHolder="stack name" result={'result'} templateNames={MiddleStackEditorToolbar.TEMPLATE_STACK_DATA}
        onWizardNew={(projectId, path, title) => {
          this.dispatch(StackEditorStore, new ActionStackEditorWizard(projectId, path, title, MiddleStackEditorToolbar.TEMPLATE_TYPES_STACK, MiddleStackEditorToolbar.TEMPLATE_STACK_DATA, chosenTransportName));
        }}
        onClear={() => {}}
      >
        <ModalDialogWizardList listName={chosenTransportName} listNames={['tcp', 'udp']} 
          onChosenList={(listName) => {
            chosenTransportName = listName;
          }}
        />
      </ModalDialogWizard>
    );
  }
  
  renderModalDialogFileAdd() {
    const name = 'Stack_File_Add';
    const folder = this.state.StackEditorStore.current.folder;
    const projectId = folder !== null ? folder.projectId : null;
    return (
      <ModalDialogFileAdd name={name} heading="Add Stack File" folder={this.state.StackEditorStore.current.folder} project={this.state.DialogFileStore.project} current={this.state.DialogFileStore.current} original={this.state.DialogFileStore.original} modalResults={this.state.ModalDialogStore.modalResults}
        onAdd={(projectId, title, path, type) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFileAdd(projectId, title, path, type));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onChoose={(path) => {
          this.dispatch(DialogFileStore, new ActionDialogFileSet(path));
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileProperties() {
    const name = 'Stack_File_Properties';
    return (
      <ModalDialogFileProperties name={name} heading="File Properties" file={this.state.StackEditorStore.current.file} modalResults={this.state.ModalDialogStore.modalResults}
        onFileProperties={(projectId, path, title, newTitle) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFileRename(projectId, path, title, newTitle), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }
  
  renderModalDialogFileRemove() {
    const name = 'Stack_File_Remove';
    return (
      <ModalDialogFileRemove name={name} file={this.state.StackEditorStore.current.file} doDelete={'invalid_file' !== this.state.StackEditorStore.current.type} heading="Remove or Delete Stack File" modalResults={this.state.ModalDialogStore.modalResults}
        onFileRemove={(projectId, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFileRemove(projectId, key));
          this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, {
            code: 'success',
            error: null,
            msg: ''
          }));
        }}
        onFileDelete={(projectId, key) => {
          this.dispatch(ModalDialogStore, new ActionModalDialogPending(name));
          this.dispatch(StackEditorStore, new ActionStackEditorFileDelete(projectId, key), (response) => {
            this.dispatch(ModalDialogStore, new ActionModalDialogResult(name, response.result));
          });
        }}
        onHide={() => {
          this.dispatch(ModalDialogStore, new ActionModalDialogHide(name));
        }}
      />
    );
  }

  renderButtonDarkMode() {
    const darkMode = this.state.StackEditorStore.persistentData.darkMode;
    return (
      <Button active={darkMode} placement="bottom" heading="View" content="Dark mode"
        onClick={(e) => {
          this.dispatch(StackEditorStore, new ActionStackEditorPersistentDataUpdate('', 'darkMode', !darkMode));
        }}
      >
        <span className="glyphicon glyphicon-certificate" aria-hidden="true"></span>
      </Button>
    );
  }

  renderButtonEmptyLine() {
    const emptyLine = this.state.StackEditorStore.persistentData.emptyLine;
    return (
      <Button active={emptyLine} placement="bottom" heading="View" content="Empty Line"
        onClick={(e) => {
          this.dispatch(StackEditorStore, new ActionStackEditorPersistentDataUpdate('', 'emptyLine', !emptyLine));
        }}
      >
        _
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar middle_code_editor_toolbar">
        {this.renderModalDialogStackWizard()}
        {this.renderModalDialogFolderNew()}
        {this.renderModalDialogFolderAdd()}
        {this.renderModalDialogFolderProperties()}
        {this.renderModalDialogFolderRemove()}
        {this.renderModalDialogFileNew()}
        {this.renderModalDialogFileAdd()}
        {this.renderModalDialogFileProperties()}
        {this.renderModalDialogFileRemove()}
        <div className="btn-toolbar" role="toolbar" aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonSaveProject()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonStackWizard()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFolderNew()}
            {this.renderButtonFolderAdd()}
            {this.renderButtonFolderProperties()}
            {this.renderButtonFolderDelete()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderButtonFileNew()}
            {this.renderButtonFileAdd()}
            {this.renderButtonFileSave()}
            {this.renderButtonFileSaveAll()}
            {this.renderButtonFileProperties()}
            {this.renderButtonFileDelete()}
          </div>
          {/*<div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderBuildButton()}
          </div>*/}
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderButtonEmptyLine()}
            {this.renderButtonDarkMode()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleStackEditorToolbar.TEMPLATE_STACK_DATA = [
  {name: 'Client Connection', type:'js', icon: ''},
  {name: 'Server Connection', type:'js', icon: ''},
  {name: 'Encoder', type:'js', icon: ''},
  {name: 'Decoder', type:'js', icon: ''},
  {name: 'Msg', type:'js', icon: ''},
  {name: 'Const', type:'js', icon: ''},
  {name: 'Inner Log', type:'js', icon: ''},
  {name: 'Style', type:'js', icon: ''},
  {name: 'Api', type:'js', icon: ''},
  {name: 'Client Connection Options', type:'js', icon: ''},
  {name: 'Server Connection Options', type:'js', icon: ''}
];
MiddleStackEditorToolbar.TEMPLATE_TYPES_STACK = ['js', 'js', 'js', 'js', 'js', 'js', 'js', 'js', 'js', 'js', 'js'];
MiddleStackEditorToolbar.TEMPLATE_DATAS = MiddleStackEditorToolbar.TEMPLATE_STACK_DATA.concat([
  {name: 'Class [class js file]', type:'js', icon: ''},
  {name: 'None [empty js file]', type:'js', icon: ''}
]);
