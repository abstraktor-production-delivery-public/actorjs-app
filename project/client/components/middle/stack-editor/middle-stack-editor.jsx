
'use strict';

import StackEditorStore from '../../../stores/stack-editor-store';
import MiddleStackEditorToolbar from './middle-stack-editor-toolbar';
import MiddleStackEditorSidebar from './middle-stack-editor-sidebar';
import MiddleStackEditorView from './middle-stack-editor-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStackEditor extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Stacks',
        prefix: 'SE: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle middle_code_editor">
        <Route switch="*" handler={MiddleStackEditorToolbar} />
        <Route switch="*" handler={MiddleStackEditorSidebar} />
        <Route switch="*" handler={MiddleStackEditorView} />
      </div>
    );
  }
}
