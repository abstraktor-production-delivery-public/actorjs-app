
'use strict';

import FilterStore from '../../../stores/filter-store';
import TestSuiteStore from '../../../stores/test-suite-store';
import TestSuitesStore from '../../../stores/test-suites-store';
import { ActionTestSuitesTsChecked, ActionTestSuitesTsClear } from '../../../actions/action-test-suites';
import { ActionTestSuiteGet } from '../../../actions/action-test-suite/action-test-suite';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestSuitesViewTs extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, TestSuitesStore, TestSuiteStore]);
    this.refTs = React.createRef();
  }
  
  didMount() {
    if(this.props.repo) {
      this.dispatch(TestSuiteStore, new ActionTestSuiteGet(this.props.repo, this.props.sut, this.props.fut));
    }
    if(this.props.scroll) {
      this.refTs.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompareSetValues(this.state.TestSuitesStore.testSuitesChecked, nextState.TestSuitesStore.testSuitesChecked)
      || !this.shallowCompareArrayValues(this.state.TestSuiteStore.testSuites, nextState.TestSuiteStore.testSuites);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.repo && (this.props.repo !== prevProps.repo || this.props.sut !== prevProps.sut || this.props.fut !== prevProps.fut)) {
      this.dispatch(TestSuiteStore, new ActionTestSuiteGet(this.props.repo, this.props.sut, this.props.fut));
      this.dispatch(TestSuitesStore, new ActionTestSuitesTsClear());
    }
  }
  
  willUnmount() {
    this.dispatch(TestSuitesStore, new ActionTestSuitesTsClear());
    this.props.onRestoreData && this.props.onRestoreData(this.refTs.current.scrollTop);
  }
  
  renderCheckbox(tsName) {
    return (
      <input type="checkbox" id={`test_suites_input_checkbox_ts_${tsName}`} aria-label="..." autoComplete="off" checked={this.state.TestSuitesStore.testSuitesChecked.has(tsName)}
        onChange={(e) => {
          this.dispatch(TestSuitesStore, new ActionTestSuitesTsChecked(tsName, e.currentTarget.checked));
        }}
      />
    );
  }

  renderTestSuiteRows() {
    return this.state.TestSuiteStore.testSuites.map((ts, i) => {
      return (
        <tr key={`ts:ts_${this.props.sut}/${this.props.fut}/${ts.name}`}>
          <th scope="row">{i + 1}</th>
          <td>{this.renderCheckbox(ts.name)}</td>
          <td>
            <Link className="test_suite_link" href={`/${this.props.sut}/${this.props.fut}/${ts.name}/definition`}>
              {ts.name}
            </Link>
          </td>
          <td>{ts.description}</td>
        </tr>
      );
    });
  }
  
  render() {
    return (
      <div ref={this.refTs} className="test_suite_column">
        <div className="test_suites_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">
                [
                <Link className="test_suite_link" global href={`/system-under-tests/${this.props.sut}`}>
                  {this.props.sut}
                </Link>
                ] - [{this.props.fut}] - Ts
              </h3>
            </div>
            <table id="ts_ts_table" className="table test_suites_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th></th>
                  <th>Name</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                {this.renderTestSuiteRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
