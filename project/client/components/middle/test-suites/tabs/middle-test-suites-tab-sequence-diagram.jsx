
'use strict';

import TestSuiteStore from '../../../../stores/test-suite-store';
import LoginStore from '../../../../stores/login-store';
import { ActionTestSuiteButtonsUpdate } from '../../../../actions/action-test-suite/action-test-suite';
import StyleStore from 'z-abs-complayer-markup-client/client/stores/style-store';
import Const from 'z-abs-complayer-visualizationsequencediagram-client/client/logic/const';
import SeqDiaFilter from 'z-abs-complayer-visualizationsequencediagram-client/client/logic/seq-dia-filter';
import RealtimeZoom from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-zoom';
import RealtimeWidth from 'z-abs-complayer-bootstrap-client/client/realtime-components/realtime-width';
import ButtonSeqDiaScroll from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-scroll';
import ButtonSeqDiaExecutedStateEvents from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-executed-state-events';
import ButtonSeqDiaNotExecutedStateEvents from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-not-executed-state-events';
import ButtonSeqDiaProtocol from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol';
import ButtonSeqDiaProtocolInfo from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-info';
import ButtonSeqDiaProtocolAddressName from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-address-name';
import ButtonSeqDiaProtocolIp from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-ip';
import ButtonSeqDiaProtocolName from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-name';
import ButtonSeqDiaProtocolTransport from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-transport';
import ButtonSeqDiaProtocolInstance from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-protocol-instance';
import ButtonSeqDiaMessages from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-messages';
import ButtonSeqDiaMessageDetails from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-message-details';
import ButtonSeqDiaMessageContent from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-message-content';
import ButtonSeqDiaMessageSent from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-message-sent';
import ButtonSeqDiaMessageReceived from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-message-received';
import ButtonSeqDiaConnectionEvents from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-connection-events';
import ButtonSeqDiaServerEvents from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-server-events';
import ButtonSeqDiaConnectionDetail from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-connection-detail';
import ButtonSeqDiaStack from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-stack';
import ButtonSeqDiaStackInfo from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-stack-info';
import ButtonSeqDiaStackInstance from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-stack-instance';
import ButtonSeqDiaStackProtocol from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-stack-protocol';
import ButtonSeqDiaGui from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui';
import ButtonSeqDiaGuiInstance from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-instance';
import ButtonSeqDiaGuiProtocol from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-protocol';
import ButtonSeqDiaGuiObjects from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-objects';
import ButtonSeqDiaGuiActions from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-actions';
import ButtonSeqDiaGuiFunctions from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-functions';
import ButtonSeqDiaGuiInfo from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-gui-info';
import ButtonSeqDiaPhase from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/buttons/button-seq-dia-phase';
import SequenceDiagram from 'z-abs-complayer-visualizationsequencediagram-client/client/react-component/sequence-diagram';
import ActorPhaseConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-phase-const';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestSuitesTabSequenceDiagram extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestSuiteStore, LoginStore, StyleStore]);
    this.refZoomDisplay = React.createRef();
    this.refWidthDisplay = React.createRef();
    this.refSeqDia = React.createRef();
    this.refScrollButton = null;
    this.filter = new SeqDiaFilter();
    this.filter.init(this.state.TestSuiteStore.buttons.sequenceDiagram);
    this.cbIsRealtime = () => {
      return this.state.TestSuiteStore.execution !== TestSuiteStore.EXECUTION_NOT_RUNNING;
    };
  }
  
  didMount() {
    this.refSeqDia.current.active(this.props.active);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.state.TestSuiteStore.error, nextState.TestSuiteStore.error)
      || !this.shallowCompare(this.state.TestSuiteStore.testSuite, nextState.TestSuiteStore.testSuite)
      || !this.shallowCompare(this.state.TestSuiteStore.buttonsLoaded, nextState.TestSuiteStore.buttonsLoaded)
      || !this.shallowCompare(this.state.TestSuiteStore.buttons.sequenceDiagram, nextState.TestSuiteStore.buttons.sequenceDiagram)
      || !this.shallowCompare(this.state.LoginStore.login.systemUnderTest, nextState.LoginStore.login.systemUnderTest)
      || !this.shallowCompare(this.state.StyleStore.stackStyles, nextState.StyleStore.stackStyles);
  }
  
  didUpdate(prevProps, prevState) {
    this.refSeqDia.current.active(this.props.active);
  }
  
  realtimeUpdate(nextProps, nextState) {
    this.refSeqDia.current.active(nextProps.active);
  }
  
  _fireButtonAction(valueName, value, rt) {
    this.dispatch(TestSuiteStore, new ActionTestSuiteButtonsUpdate('sequenceDiagram', valueName, value));
    if(this.cbIsRealtime()) {
      this.renderRealtime(() => {
        rt();
      });
    }
  }
  
  render() {
    if(this.state.TestSuiteStore.error) {
      return (
        <div className="test_cases_tab_view">
          ERROR
        </div>
      );
    }
    else {
      const stateSequenceDiagram = this.state.TestSuiteStore.buttons.sequenceDiagram;
      return (
        <>
          <div className="seq_dia_panel">
            <div className="seq_dia_panel_inner btn-toolbar" role="toolbar" aria-label="...">
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <RealtimeZoom zoom={stateSequenceDiagram.zoom} colorMark="zoom_events" 
                  onAction={(zoom) => {
                    this.dispatch(TestSuiteStore, new ActionTestSuiteButtonsUpdate('sequenceDiagram', 'zoom', zoom));
                  }}
                />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <RealtimeWidth width={stateSequenceDiagram.width} colorMark="zoom_events" 
                  onAction={(width) => {
                    this.dispatch(TestSuiteStore, new ActionTestSuiteButtonsUpdate('sequenceDiagram', 'width', width));
                  }}
                />
              </div>  
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaScroll buttonValue={stateSequenceDiagram.scroll} 
                  onLoad={(e) => {
                    this.refScrollButton = e;
                  }}
                  onAction={this._fireButtonAction.bind(this, 'scroll')}
                />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaExecutedStateEvents buttonValue={stateSequenceDiagram.executedStateEvents} onAction={this._fireButtonAction.bind(this, 'executedStateEvents')} />
                <ButtonSeqDiaNotExecutedStateEvents buttonValue={stateSequenceDiagram.notExecutedStateEvents} onAction={this._fireButtonAction.bind(this, 'notExecutedStateEvents')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaServerEvents buttonValue={stateSequenceDiagram.serverEvents} onAction={this._fireButtonAction.bind(this, 'serverEvents')} />
                <ButtonSeqDiaConnectionEvents buttonValue={stateSequenceDiagram.connectionEvents} onAction={this._fireButtonAction.bind(this, 'connectionEvents')} />
                <ButtonSeqDiaMessages buttonValue={stateSequenceDiagram.messageEvents} onAction={this._fireButtonAction.bind(this, 'messageEvents')} />
                <ButtonSeqDiaMessageDetails buttonValue={stateSequenceDiagram.messageDetailEvents} onAction={this._fireButtonAction.bind(this, 'messageDetailEvents')} />
                {/*<ButtonSeqDiaMessageContent buttonValue={stateSequenceDiagram.showMessageContent} onAction={this._fireButtonAction.bind(this, 'showMessageContent')} />
                <ButtonSeqDiaMessageSent buttonValue={stateSequenceDiagram.showSentMessageNumbers} onAction={this._fireButtonAction.bind(this, 'showSentMessageNumbers')} />
                <ButtonSeqDiaMessageReceived buttonValue={stateSequenceDiagram.showReceivedMessageNumbers} onAction={this._fireButtonAction.bind(this, 'showReceivedMessageNumbers')} />*/}
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaProtocol buttonValue={stateSequenceDiagram.protocolData} onAction={this._fireButtonAction.bind(this, 'protocolData')} />
                <ButtonSeqDiaProtocolInfo buttonValue={stateSequenceDiagram.protocolInfoData} onAction={this._fireButtonAction.bind(this, 'protocolInfoData')} />
                <ButtonSeqDiaProtocolAddressName buttonValue={stateSequenceDiagram.protocolAddressNameData} onAction={this._fireButtonAction.bind(this, 'protocolAddressNameData')} />
                <ButtonSeqDiaProtocolIp buttonValue={stateSequenceDiagram.protocolIpData} onAction={this._fireButtonAction.bind(this, 'protocolIpData')} />
                <ButtonSeqDiaProtocolName buttonValue={stateSequenceDiagram.protocolNameData} onAction={this._fireButtonAction.bind(this, 'protocolNameData')} />
                <ButtonSeqDiaProtocolTransport buttonValue={stateSequenceDiagram.protocolTransportData} onAction={this._fireButtonAction.bind(this, 'protocolTransportData')} />
                <ButtonSeqDiaProtocolInstance buttonValue={stateSequenceDiagram.protocolInstanceData} onAction={this._fireButtonAction.bind(this, 'protocolInstanceData')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaStack buttonValue={stateSequenceDiagram.stackEvents} onAction={this._fireButtonAction.bind(this, 'stackEvents')} />
                <ButtonSeqDiaStackInfo buttonValue={stateSequenceDiagram.stackInfoEvents} onAction={this._fireButtonAction.bind(this, 'stackInfoEvents')} />
                <ButtonSeqDiaStackInstance buttonValue={stateSequenceDiagram.stackInstanceEvents} onAction={this._fireButtonAction.bind(this, 'stackInstanceEvents')} />
                <ButtonSeqDiaStackProtocol buttonValue={stateSequenceDiagram.stackProtocolEvents} onAction={this._fireButtonAction.bind(this, 'stackProtocolEvents')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaGui buttonValue={stateSequenceDiagram.guiEvents} onAction={this._fireButtonAction.bind(this, 'guiEvents')} />
                <ButtonSeqDiaGuiObjects buttonValue={stateSequenceDiagram.guiObjectEvents} onAction={this._fireButtonAction.bind(this, 'guiObjectEvents')} />
                <ButtonSeqDiaGuiActions buttonValue={stateSequenceDiagram.guiActionEvents} onAction={this._fireButtonAction.bind(this, 'guiActionEvents')} />
                <ButtonSeqDiaGuiFunctions buttonValue={stateSequenceDiagram.guiFunctionEvents} onAction={this._fireButtonAction.bind(this, 'guiFunctionEvents')} />
                <ButtonSeqDiaGuiInfo buttonValue={stateSequenceDiagram.guiInfoData} onAction={this._fireButtonAction.bind(this, 'guiInfoData')} />
                <ButtonSeqDiaGuiInstance buttonValue={stateSequenceDiagram.guiInstanceData} onAction={this._fireButtonAction.bind(this, 'guiInstanceData')} />
                <ButtonSeqDiaGuiProtocol buttonValue={stateSequenceDiagram.guiProtocolData} onAction={this._fireButtonAction.bind(this, 'guiProtocolData')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonSeqDiaPhase name="Data" buttonValue={stateSequenceDiagram.phases} buttonIndex={ActorPhaseConst.DATA} onAction={this._fireButtonAction.bind(this, 'phases')} />
                <ButtonSeqDiaPhase name="Precondition" buttonValue={stateSequenceDiagram.phases} buttonIndex={ActorPhaseConst.PRE} onAction={this._fireButtonAction.bind(this, 'phases')} />
                <ButtonSeqDiaPhase name="Execution" buttonValue={stateSequenceDiagram.phases} buttonIndex={ActorPhaseConst.EXEC} onAction={this._fireButtonAction.bind(this, 'phases')} />
                <ButtonSeqDiaPhase name="Postcondition" buttonValue={stateSequenceDiagram.phases} buttonIndex={ActorPhaseConst.POST} onAction={this._fireButtonAction.bind(this, 'phases')} />
                <ButtonSeqDiaPhase name="None" buttonValue={stateSequenceDiagram.phases} buttonIndex={ActorPhaseConst.NONE} onAction={this._fireButtonAction.bind(this, 'phases')} />
              </div>
            </div>
          </div>
          <SequenceDiagram ref={this.refSeqDia} filter={this.filter} name="test-suite-seq-dia" clearMsgPrefix="TestSuite" store={TestSuiteStore} storeName="TestSuiteStore" buttonsLoaded={this.state.TestSuiteStore.buttonsLoaded} stackStyles={this.state.StyleStore.stackStyles} testCase={this.state.TestSuiteStore.testCase} systemUnderTest={this.state.LoginStore.login.systemUnderTest}
            onOnit={() => {}}
            onAutoScroll={(autoScroll) => {
              this.refScrollButton.click();
            }}
          />
        </>
      );
    }
  }
}
