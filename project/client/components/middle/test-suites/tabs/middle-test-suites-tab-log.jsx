
'use strict';

import TestSuiteStore from '../../../../stores/test-suite-store';
import { ActionTestSuiteButtonsUpdate } from '../../../../actions/action-test-suite/action-test-suite';
import LogFilter from 'z-abs-complayer-visualizationlog-client/client/logic/log-filter';
import Log from 'z-abs-complayer-visualizationlog-client/client/react-component/log';
import ButtonLogFilter from 'z-abs-complayer-visualizationlog-client/client/react-component/buttons/button-log-filter';
import ButtonLogScroll from 'z-abs-complayer-visualizationlog-client/client/react-component/buttons/button-log-scroll';
import LogType from 'z-abs-funclayer-engine-cs/clientServer/log/log-type';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestSuitesTabLog extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestSuiteStore]);
    this.refLog = React.createRef();
    this.refScrollButton = null;
    this.logFilter = new LogFilter();
    this.logFilter.init(this.state.TestSuiteStore.buttons.log);
    this.cbIsRealtime = () => {
      return this.state.TestSuiteStore.execution !== TestSuiteStore.EXECUTION_NOT_RUNNING;
    };
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.props.location?.hash, nextProps.location?.hash)
      || !this.shallowCompare(this.state.TestSuiteStore.testCase, nextState.TestSuiteStore.testCase)
      || !this.shallowCompare(this.state.TestSuiteStore.buttonsLoaded, nextState.TestSuiteStore.buttonsLoaded)
      || !this.shallowCompare(this.state.TestSuiteStore.buttons.log, nextState.TestSuiteStore.buttons.log)
      || !this.shallowCompare(this.state.TestSuiteStore.iterationsTs, nextState.TestSuiteStore.iterationsTs)
  }
  
  didMount() {
    this.refLog.current.active(this.props.active);
  }
  
  didUpdate() {
    this.refLog.current.active(this.props.active);
    if(this.props.location) {
      const hash = 0 !== this.props.location.hash.length ? this.props.location.hash.substring(1) : this.props.location.hash;
      if(hash) {
        const element = document.getElementById(hash);
        //setTimeout(() => {
          element.parentNode.parentNode.scrollIntoView();
        //});
      }
    }
  }
  
  realtimeUpdate(nextProps, nextState) {
    this.refLog.current.active(nextProps.active);
  }
  
  _fireButtonAction(valueName, value, rt) {
    this.dispatch(TestSuiteStore, new ActionTestSuiteButtonsUpdate('log', valueName, value));
    if(this.cbIsRealtime()) {
      this.renderRealtime(() => {
        rt();
      });
    }
  }
  
  render() {
    if(this.state.TestSuiteStore.error) {
      return null;
    }
    else {
      const log = this.state.TestSuiteStore.buttons.log;
      return (
        <>
          <div className="log_panel">
            <div className="log_panel_inner btn-toolbar" role="toolbar" aria-label="...">
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogScroll buttonValue={log.scroll}
                  onLoad={(e) => {
                    this.refScrollButton = e;
                  }}
                  onAction={this._fireButtonAction.bind(this, 'scroll')}
                />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.ERROR]} buttonValue={log.filterError} colorMark="log_error" onAction={this._fireButtonAction.bind(this, 'filterError')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.WARNING]} buttonValue={log.filterWarning} colorMark="log_warning" onAction={this._fireButtonAction.bind(this, 'filterWarning')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.SUCCESS]} buttonValue={log.filterSuccess} colorMark="log_verify_success" onAction={this._fireButtonAction.bind(this, 'filterSuccess')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.FAILURE]} buttonValue={log.filterFailure} colorMark="log_verify_failure" onAction={this._fireButtonAction.bind(this, 'filterFailure')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.ENGINE]} buttonValue={log.filterEngine} colorMark="log_engine" onAction={this._fireButtonAction.bind(this, 'filterEngine')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.DEBUG]} buttonValue={log.filterDebug} colorMark="log_debug" onAction={this._fireButtonAction.bind(this, 'filterDebug')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.TEST_DATA]} buttonValue={log.filterTestData} colorMark="log_test_data" onAction={this._fireButtonAction.bind(this, 'filterTestData')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.IP]} buttonValue={log.filterIP} colorMark="log_ip" onAction={this._fireButtonAction.bind(this, 'filterIP')} />
              </div>
              <div className="btn-group btn-group-xs" role="group" aria-label="...">
                <ButtonLogFilter name={LogType.pureNames[LogType.GUI]} buttonValue={log.filterGUI} colorMark="log_gui" onAction={this._fireButtonAction.bind(this, 'filterGUI')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.BROWSER_ERR]} buttonValue={log.filterBrowserErr} colorMark="log_browser_err" onAction={this._fireButtonAction.bind(this, 'filterBrowserErr')} />
                <ButtonLogFilter name={LogType.pureNames[LogType.BROWSER_LOG]} buttonValue={log.filterBrowserLog} colorMark="log_browser_log" onAction={this._fireButtonAction.bind(this, 'filterBrowserLog')} />
              </div>
            </div>
          </div>
          <Log ref={this.refLog} filter={this.logFilter} name="test-suite-log" clearMsgPrefix="TestSuite" store={TestSuiteStore} storeName="TestSuiteStore" buttonsLoaded={this.state.TestSuiteStore.buttonsLoaded}
            onOnit={() => {}}
            onAutoScroll={(autoScroll) => {
              this.refScrollButton.click();
            }}
          />
        </>
      );
    }
  }
}
