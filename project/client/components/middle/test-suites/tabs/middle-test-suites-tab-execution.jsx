
'use strict';

import LoginStore from '../../../../stores/login-store';
import TestSuiteStore from '../../../../stores/test-suite-store';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import HighResolutionDate from 'z-abs-corelayer-cs/clientServer/time/high-resolution-date';
import HighResolutionDuration from 'z-abs-corelayer-cs/clientServer/time/high-resolution-duration';
import React from 'react';


export default class MiddleTestSuitesTabExecution extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestSuiteStore, LoginStore]);
    this.sumResultValue = ActorResultConst.NONE;
    this.index = null;
    this.tsIndex = 0;
    this.stageData = null;
    this.successes = 0;
    this.failures = 0;
    this.nones = 0;
    this.nas = 0;
    this.emptyResultNodes = [];
    this.emptyDurationTestCaseNode = null;
    this.refTableSum = React.createRef();
    this.refTbody = React.createRef();
    this.groupData = null;
  }
  
  didMount() {
    this.groupData = this.addRealtimeFrameGroup();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.active, nextProps.active)
      || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login)
      || !this.shallowCompare(this.state.TestSuiteStore.testSuite, nextState.TestSuiteStore.testSuite)
      || !this.shallowCompare(this.state.TestSuiteStore.iterationsTs, nextState.TestSuiteStore.iterationsTs)
      || !this.shallowCompare(this.state.TestSuiteStore.run, nextState.TestSuiteStore.run);
  }
  
  didUpdate(prevProps, prevState) {
    const sumRowNode = this.refTableSum.current.firstChild.firstChild
    this.addRealtimeFrame('SUM_RESULT', this.groupData, sumRowNode.childNodes[1]);
    this.addRealtimeFrame('SUM_DURATION', this.groupData, sumRowNode.childNodes[3].firstChild);
    this.addRealtimeFrame('SUM_RESULT_AMOUNT', this.groupData, sumRowNode.childNodes[5].firstChild);
    this.addRealtimeFrame('SUM_RESULT_SUCCESS', this.groupData, sumRowNode.childNodes[7].firstChild);
    this.addRealtimeFrame('SUM_RESULT_FAILURE', this.groupData, sumRowNode.childNodes[9].firstChild);
    this.addRealtimeFrame('SUM_RESULT_NONE', this.groupData, sumRowNode.childNodes[11].firstChild);
    this.addRealtimeFrame('SUM_RESULT_NA', this.groupData, sumRowNode.childNodes[13].firstChild);
    this.addRealtimeFrame('SUM_START', this.groupData, sumRowNode.childNodes[15].firstChild);
    this.addRealtimeFrame('SUM_STOP', this.groupData, sumRowNode.childNodes[17].firstChild);
    
    if(!this.shallowCompare(this.state.TestSuiteStore.run.testSuite, prevState.TestSuiteStore.run.testSuite)) {
      const testCaseTableRowNodes = this.refTbody.current.rows;
      this.emptyDurationTestCaseNode = 0 < testCaseTableRowNodes.length ? testCaseTableRowNodes[0].cells[4].cloneNode(true) : null;
      this.emptyDurationStageNode = 0 < testCaseTableRowNodes.length ? testCaseTableRowNodes[0].cells[2].cloneNode(true) : null;
      for(let i = 0; i < testCaseTableRowNodes.length; ++i) {
        const type = testCaseTableRowNodes[i].getAttribute('data-type');
        const key = testCaseTableRowNodes[i].getAttribute('data-key');
        if('tc' === type) {
          this.emptyResultNodes.push(testCaseTableRowNodes[i].cells[3].cloneNode(true));
          this.addRealtimeFrame(`SUM_TC_${key}_RESULT`, this.groupData, testCaseTableRowNodes[i].cells[3]);
          this.addRealtimeFrame(`SUM_TC_${key}_DURATION`, this.groupData, testCaseTableRowNodes[i].cells[4]);
          this.addRealtimeFrame(`SUM_TC_${key}_TIME`, this.groupData, testCaseTableRowNodes[i].cells[7].firstChild);
        }
        else if('stage' === type) {
          this.emptyResultNodes.push(testCaseTableRowNodes[i].cells[1].cloneNode(true));
          this.addRealtimeFrame(`SUM_TC_${key}_RESULT`, this.groupData, testCaseTableRowNodes[i].cells[1]);
          this.addRealtimeFrame(`SUM_TC_${key}_DURATION`, this.groupData, testCaseTableRowNodes[i].cells[2]);
          this.addRealtimeFrame(`SUM_TC_${key}_TIME`, this.groupData, testCaseTableRowNodes[i].cells[5].firstChild);
        }
      }
    }
  }
  
  willUnmount() {
    this.removeRealtimeFrameGroup(this.groupData);
  }
  
  _clean() {
    this.sumResultValue = ActorResultConst.NONE;
    this.replaceRealtimeFrameObject('SUM_RESULT', this.groupData, resultFactoryTestSuite.resultTemplates[ActorResultConst.NONE].cloneNode(true));
    this.replaceRealtimeFrameText('SUM_DURATION', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_RESULT_AMOUNT', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_RESULT_SUCCESS', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_RESULT_FAILURE', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_RESULT_NONE', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_RESULT_NA', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_START', this.groupData, '');
    this.replaceRealtimeFrameText('SUM_STOP', this.groupData, '');
    
    const testCaseTableRowNodes = this.refTbody.current.rows;
    for(let i = 0; i < testCaseTableRowNodes.length; ++i) {
      const type = testCaseTableRowNodes[i].getAttribute('data-type');
      const key = testCaseTableRowNodes[i].getAttribute('data-key');
      if('tc' === type) {
        this.replaceRealtimeFrameObject(`SUM_TC_${key}_RESULT`, this.groupData, this.emptyResultNodes[i]);
        this.replaceRealtimeFrameObject(`SUM_TC_${key}_DURATION`, this.groupData, this.emptyDurationTestCaseNode.cloneNode());
        this.replaceRealtimeFrameText(`SUM_TC_${key}_TIME`, this.groupData, '\u00A0');
      }
      else if('stage' === type) {
        this.replaceRealtimeFrameObject(`SUM_TC_${key}_RESULT`, this.groupData, this.emptyResultNodes[i]);
        this.replaceRealtimeFrameObject(`SUM_TC_${key}_DURATION`, this.groupData, this.emptyDurationStageNode.cloneNode());
        this.replaceRealtimeFrameText(`SUM_TC_${key}_TIME`, this.groupData, '\u00A0');
      }
    }
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.TEST_CASE_STARTED === msg.msgId) {
      this.replaceRealtimeFrameText(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_TIME`, this.groupData, new HighResolutionDate(msg.timestamp).getDateMilliSeconds());
    }
    else if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId) {
      this.sumResultValue = Math.max(this.sumResultValue, msg.resultId);
      if(ActorResultConst.SUCCESS === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_SUCCESS', this.groupData, `${++this.successes}`);
      }
      else if(ActorResultConst.NONE === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_NONE', this.groupData, `${++this.nones}`);
      }
      else if(ActorResultConst.NA === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_NA', this.groupData, `${++this.nas}`);
      }
      else {
        this.replaceRealtimeFrameText('SUM_RESULT_FAILURE', this.groupData, `${++this.failures}`);
      }
      const resultTd = resultFactoryTestSuite.resultTemplates[msg.resultId].cloneNode(true);
      resultTd.firstChild.href = `log#tc_log_anchor_${this.state.TestSuiteStore.executionGuid}_${this.tsIndex + 1}_${msg.index + 1}_${msg.iterationTs}`;
      this.replaceRealtimeFrameObject(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_RESULT`, this.groupData, resultTd);
      const log2 = Math.round(Math.log2(Number(msg.duration / 100000n)));
      const className = `duration_${log2}`;
      const td = this.emptyDurationTestCaseNode.cloneNode();
      td.appendChild(document.createTextNode(new HighResolutionDuration(msg.duration).getDuration()));
      this.replaceRealtimeFrameObject(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_DURATION`, this.groupData, td, className);
    }
    else if(AppProtocolConst.TEST_STAGE_STARTED === msg.msgId) {
      this.replaceRealtimeFrameText(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_TIME`, this.groupData, new HighResolutionDate(msg.timestamp).getDateMilliSeconds());
    }
    else if(AppProtocolConst.TEST_STAGE_STOPPED === msg.msgId) {
      if(ActorResultConst.SUCCESS === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_SUCCESS', this.groupData, `${++this.successes}`);
      }
      else if(ActorResultConst.NONE === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_NONE', this.groupData, `${++this.nones}`);
      }
      else if(ActorResultConst.NA === msg.resultId) {
        this.replaceRealtimeFrameText('SUM_RESULT_NA', this.groupData, `${++this.nas}`);
      }
      else {
        this.replaceRealtimeFrameText('SUM_RESULT_FAILURE', this.groupData, `${++this.failures}`);
      }
      this.replaceRealtimeFrameObject(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_RESULT`, this.groupData, resultFactoryTestSuite.resultTemplates[msg.resultId].cloneNode(true));
      const log2 = Math.round(Math.log2(Number(msg.duration / 100000n)));
      const className = `duration_${log2}`;
      const td = this.emptyDurationTestCaseNode.cloneNode();
      td.appendChild(document.createTextNode(new HighResolutionDuration(msg.duration).getDuration()));
      this.replaceRealtimeFrameObject(`SUM_TC_${msg.index + 1}:${msg.iterationTs}_DURATION`, this.groupData, td, className);
    }
    else if(AppProtocolConst.TEST_SUITE_STARTED === msg.msgId) {
      this._clean();
      this.tsIndex = msg.index;
      this.successes = 0;
      this.failures = 0;
      this.nones = 0;
      this.nas = 0;
      this.replaceRealtimeFrameObject('SUM_RESULT', this.groupData, resultFactoryTestSuite.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      this.replaceRealtimeFrameText('SUM_DURATION', this.groupData, '-s ---.---ms');
      this.replaceRealtimeFrameText('SUM_RESULT_AMOUNT', this.groupData, '' + this.index.i);
      this.replaceRealtimeFrameText('SUM_RESULT_SUCCESS', this.groupData, '0');
      this.replaceRealtimeFrameText('SUM_RESULT_FAILURE', this.groupData, '0');
      this.replaceRealtimeFrameText('SUM_RESULT_NONE', this.groupData, '0');
      this.replaceRealtimeFrameText('SUM_RESULT_NA', this.groupData, '0');
      this.replaceRealtimeFrameText('SUM_START', this.groupData, new HighResolutionDate(msg.timestamp).getDateMilliSeconds());
      this.replaceRealtimeFrameText('SUM_STOP', this.groupData, '---, -- --- ---- --:--:-- GMT : ---.---');
    }
    else if(AppProtocolConst.TEST_SUITE_STOPPED === msg.msgId) {
      this.replaceRealtimeFrameObject('SUM_RESULT', this.groupData, resultFactoryTestSuite.resultTemplates[this.sumResultValue].cloneNode(true));
      this.replaceRealtimeFrameText('SUM_DURATION', this.groupData, new HighResolutionDuration(msg.duration).getDuration());
      this.replaceRealtimeFrameText('SUM_STOP', this.groupData, new HighResolutionDate(msg.timestamp).getDateMilliSeconds());
    }
    else if(AppProtocolConst.TEST_SUITE_CLEAR === msg.msgId) {
      if('execution' === msg.tab) {
        this._clean();
        this.replaceRealtimeFrameObject('SUM_RESULT', this.groupData, resultFactoryTestSuite.resultTemplates[ActorResultConst.NONE].cloneNode(true));
      }
    }
  }
  
  _getStage(tcName) {
    try {
      return JSON.parse(tcName.substring(5));
    }
    catch(err) {
      return null;
    }
  }
  
  renderTableRowStageIterationError(index, stageType, stageData, stateDepth) {
    const key = `${index.stage + index.tc}:${index.iter}`;
    return (
      <tr key={key} className="test_suite_table test_suite_row_stage_failure" data-type="stage" data-key={key}>
        <td colSpan="3">{index.i}</td>
        <td> </td>
        <td> </td>
        <td  >
          Stage parse Error.
        </td>
        <td>
          <Link className="test_case_link" global href={this.nameToLink(stageData.tsName)}>{stageData.tsName}</Link>
        </td>
        <td> </td>
      </tr>
    );
  }
  
  renderTableRowStageIterationSuccess(index, stageType, stageData, stateDepth) {
    const key = `${index.stage + index.tc}:${index.iter}`;
    let stageDirection = '';
    let stage = '';
    let depth = stateDepth.depth;
    if(STATE_PUSH === stageType) {
      stageDirection = 'test_suite_row_stage_push';
      stage = ` - '${stageData.labId}', '${stageData.userId}', '${stageData.sut}', '${stageData.sutInstance}', ${stageData.forcedStage}`;
    }
    else if(STATE_POP === stageType) {
      stageDirection = 'test_suite_row_stage_pop';
      stage = ` - '${stageData.labId}', '${stageData.userId}', '${stageData.sut}', '${stageData.sutInstance}', ${stageData.forcedStage}`;
    }
    else if(STATE_CHANGE === stageType) {
      stageDirection = 'test_suite_row_stage_change';
      stage = ` - '${stageData.labId}', '${stageData.userId}', '${stageData.sut}', '${stageData.sutInstance}', ${stageData.forcedStage}`;
    }
    else if(STATE_RESTORE === stageType) {
      stageDirection = 'test_suite_row_stage_restore';
      ++depth;
    }
    const depthSign = '|'.repeat(depth);
    return (
      <tr key={key} className={`test_suite_table ${stageData.commentOut ? 'test_suite_comment_out' : 'test_suite_row_stage_success'}`} data-type="stage" data-key={key}>
        <td colSpan="3">{index.i}</td>
        <td> </td>
        <td> </td>
        <td >
          <span className="test_suite_row_stage">
            {depthSign}
          </span>
          <span className={`test_suite_row_stage ${stageDirection}`}>
            &#x279C;
          </span>
          <span className="test_suite_row_stage">
            Stage {stage}
          </span>
        </td>
        <td>
          <Link className="test_case_link" global href={this.nameToLink(stageData.tsName)}>{stageData.tsName}</Link>
        </td>
        <td> </td>
      </tr>
    );
  }
  
  renderTableRowStageIteration(index, stageType, stageData, stateDepth) {
    if(stageData.success) {
      return this.renderTableRowStageIterationSuccess(index, stageType, stageData, stateDepth);
    }
    else {
      return this.renderTableRowStageIterationError(index, stageType, stageData, stateDepth);
    }
  }
  
  renderTableRowTestCaseIteration(index, tsName, tcName, commentOut) {
    const key = `${index.stage + index.tc}:${index.iter}`;
    return (
      <tr key={key} className={`test_suite_table${commentOut ? ' test_suite_comment_out' : ''}`} data-type="tc" data-key={key}>
        <td>{index.i}</td>
        <td>{index.tc}</td>
        <td>{index.iter}</td>
        <td> </td>
        <td> </td>
        <td>
          <Link className="test_case_link" global href={this.nameToLink(tcName)}>{tcName}</Link>
        </td>
        <td>
          <Link className="test_case_link" global href={this.nameToLink(tsName)}>{tsName}</Link>
        </td>
        <td> </td>
      </tr>
    );
  }

  renderTableRowTestCase(index, iterationsTs, tsName, tcName, commentOut, isTc, stateDepth) {
    const rows = [];
    if(isTc) {
      ++index.tc;
      index.iter = 0;
      for(let i = 0; i < iterationsTs; ++i) {
        ++index.i;
        ++index.iter;
        rows.push(this.renderTableRowTestCaseIteration(index, tsName, tcName, commentOut));
      }
    }
    else {
      ++index.stage;
      ++index.i;
      index.iter = 1;
      if(this.stageData) {
        stateDepth.stages.pop();
        this.stageData = stateDepth.stages[stateDepth.stages.length - 1];
        stateDepth.previousKey = stateDepth.keys[stateDepth.keys.length - 1];
        stateDepth.keys.pop();
        --stateDepth.depth;
        rows.push(this.renderTableRowStageIterationSuccess(index, stateDepth.previousKey !== stateDepth.key ? STATE_POP : STATE_RESTORE, this.stageData, stateDepth));
        ++index.stage;
        ++index.i;
      }
      const parsedStage = this._getStage(tcName);
      if(parsedStage) {
        this.stageData = {
          success: true,
          tsName: tsName,
          commentOut: commentOut,
          labId: parsedStage[0],
          userId: parsedStage[1],
          sut: parsedStage[2],
          sutInstance: parsedStage[3] ? parsedStage[3] : '',
          forcedStage: parsedStage[4] ? parsedStage[4] : 'false'
        };
      }
      else {
        this.stageData = {
          success: false,
          tsName: tsName,
          commentOut: commentOut,
          labId: '',
          userId: '',
          sut: '',
          sutInstance: '',
          forcedStage: ''
        };
      }
      stateDepth.keys.push(stateDepth.key);
      stateDepth.stages.push(this.stageData);
      ++stateDepth.depth;
      rows.push(this.renderTableRowStageIteration(index, stateDepth.previousKey !== stateDepth.key ? STATE_PUSH : STATE_CHANGE, this.stageData, stateDepth));
    }
    return rows;
  }
  
  renderTableRows() {
    if(0 !== this.state.TestSuiteStore.run.testSuite.size) {
      const rows = [];
      this.index = {
        i: 0,
        tc: 0,
        stage: 0,
        iter: 0
      };
      const stateDepth = {
        stages: [
          {
            success: true,
            tsName: '',
            commentOut: '',
            labId: this.state.LoginStore.login.labId,
            userId: this.state.LoginStore.login.userId,
            sut: this.state.LoginStore.login.systemUnderTest,
            sutInstance: this.state.LoginStore.login.systemUnderTestInstance,
            forcedStage: this.state.LoginStore.login.forcedLocalhost
          }
        ],
        keys: ['BASE_'],
        key: '',
        previousKey: 'BASE_',
        depth: 0
      };
      this.state.TestSuiteStore.run.testSuite.forEach((abstractions, key) => {
        stateDepth.key = key;
        const newRows = abstractions.map((abstraction) => {
          return this.renderTableRowTestCase(this.index, abstraction.iterationsTs, abstraction.tsName, abstraction.tcName, abstraction.commentOut, abstraction.tcName.startsWith('tc.'), stateDepth);
        });
        if(1 <= newRows.length) {
          rows.splice(rows.length, 0, ...newRows);
        }
      });
      if(this.stageData) {
        this.index.iter = 1;
        ++this.index.stage;
        ++this.index.i;
        stateDepth.stages.pop();
        this.stageData = stateDepth.stages[stateDepth.stages.length - 1];
        stateDepth.previousKey = stateDepth.keys[stateDepth.keys.length - 1];
        stateDepth.keys.pop();
        --stateDepth.depth;
        rows.push(this.renderTableRowStageIterationSuccess(this.index, STATE_POP, this.stageData, stateDepth));
        this.stageData = null;
      }
      return (
        <tbody ref={this.refTbody}>
          {rows}
        </tbody>
      );
    }
  }
  
  render() {
    return (
      <div className="same_as_parent">
        <div className="execution_panel">
          <table ref={this.refTableSum} className="test_suite_table test_suite_table_heading_execution table table-bordered table-striped table-condensed table-hover"> 
            <tbody>
              <tr key="asdf">
                <th className="test_suite_header" scope="row">result</th>
                <td className="test_none<">
                  {ActorResultConst.results[ActorResultConst.NONE]}
                </td>
                <th className="test_suite_header" scope="row">duration</th>
                <td className="test_suite_header_duration"> </td>
                <th className="test_suite_header" scope="row">#</th>
                <td className="test_suite_header_result"> </td>
                <th className="test_suite_header" scope="row">success</th>
                <td className="test_suite_header_result"> </td>
                <th className="test_suite_header" scope="row">failure</th>
                <td className="test_suite_header_result"> </td>
                <th className="test_suite_header" scope="row">none</th>
                <td className="test_suite_header_result"> </td>
                <th className="test_suite_header" scope="row">n/a</th>
                <td className="test_suite_header_result"> </td>
                <th className="test_suite_header" scope="row">start</th>
                <td className="test_suite_header_start"> </td>
                <th className="test_suite_header" scope="row">stop</th>
                <td className="test_suite_header_stop"> </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="test_case_tab_view_with_toolbar">
          <div className="test_suite_table_log_row_execution test_suite_table_fit_content">
            <table className="test_suite_table table table-bordered table-striped table-condensed table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>tc</th>
                  <th>it</th>
                  <th>result</th>
                  <th>duration</th>
                  <th>test case</th>
                  <th>test suite</th>
                  <th>time</th>
                </tr>
              </thead>
             {this.renderTableRows()}
            </table>
          </div>
        </div>
      </div>
    );
  }
  
  nameToLink(name) {
    const paths = name.split('.');
    if(0 === paths.length) {
      return '/';
    }
    let link = '';
    if('tc' === paths[0]) {
      link = '/test-cases';
    }
    else if('ts' === paths[0]) {
      link = '/test-suites';
    }
    if(1 === paths.length) {
      return link;
    }
    paths.splice(0, 1);
    link += '/' + paths.join('/');
    return link;
  }
}

class ResultFactoryTestSuite {
  constructor() {
    this.resultTemplates = [];
    ActorResultConst.resultIds.forEach((resultId) => {
      this.resultTemplates.push(this._createTemplate(resultId));
    });
  }
  
  _createTemplate(resultId) {
    const td = document.createElement('td');
    td.classList.add(ActorResultConst.classes[resultId]);
    const a = td.appendChild(document.createElement('a'));
    a.classList.add(ActorResultConst.classes[resultId]);
    a.appendChild(document.createTextNode(ActorResultConst.results[resultId]));
    return td;
  }
}


const resultFactoryTestSuite = new ResultFactoryTestSuite();

const STATE_PUSH = 0;
const STATE_POP = 1;
const STATE_CHANGE = 2;
const STATE_RESTORE = 3;

