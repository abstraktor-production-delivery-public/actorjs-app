
'use strict';

import TestSuiteStore from '../../../../stores/test-suite-store';
import { ActionTestSuiteMarkupChange } from '../../../../actions/action-test-suite/action-test-suite';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataTestSuiteAbstraction from 'z-abs-complayer-markup-client/client/data/data-test-suite/data-test-suite-abstraction';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestSuitesTabDefinition extends ReactComponentStore {
  constructor(props) {
    super(props, [TestSuiteStore]);
    this.dataTestSuiteAbstraction = new DataTestSuiteAbstraction();
  }

  shouldUpdate(nextProps, nextState) {
    return nextProps.active
      && (
        !this.shallowCompare(this.props.active, nextProps.active)
        || !this.shallowCompare(this.state.TestSuiteStore.testSuite, nextState.TestSuiteStore.testSuite)
        || !this.shallowCompare(this.state.TestSuiteStore.markup, nextState.TestSuiteStore.markup)
      );
  }
    
  renderTables() {
    let key = 0;
    if(undefined !== this.state.TestSuiteStore.testSuite) {
      return (
        <div className="same_size_as_parent" >
          <ComponentTableDataTable classTable="test_suite_table_fit_content" classHeading="test_suite_table_heading" classRow="test_suite_table" key={++key} dataTable={this.dataTestSuiteAbstraction} values={this.state.TestSuiteStore.testSuite.ts.abstractions}/>
        </div>
      );
    }
  }
  
  renderMarkup() {
    if(undefined !== this.state.TestSuiteStore.testSuite) {
      return (
        <div className="test_suite_form_group">
          <label htmlFor="test_suites_definition_markup_textarea">Test Suite - Markup</label>
          <ComponentMarkedTextarea id="test_suites_definition_markup_textarea" className="form-control test_suite_definition same_size_as_parent" rows="10" value={this.state.TestSuiteStore.markup.content} results={this.state.TestSuiteStore.markup.rows}
            onChange={(value) => {
              this.dispatch(TestSuiteStore, new ActionTestSuiteMarkupChange(value));
            }}
          />
        </div>
      );
    }
  }
  
  render() {
    if(!this.props.active) {
      return null;
    }
    if(this.state.TestSuiteStore.markup.definition) {
      return (
        <div className="test_suite_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="test_suite_tab_view" style={{overflow:'auto'}}>
          {this.renderTables()}
        </div>
      );
    }
  }
}
