
'use strict';

import MiddleTestSuitesToolbar from './middle-test-suites-toolbar';
import MiddleSidebar from '../middle_sidebar';
import TestSuiteStore from '../../../stores/test-suite-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import MiddleTestSuitesViewColumns from './middle-test-suites-view-columns';
import { ActionTestSuiteMounted, } from '../../../actions/action-test-suite/action-test-suite';
import { ActionSystemUnderTestClear } from '../../../actions/action-system-under-test/action-system-under-test';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestSuites extends ReactComponentStore {
  constructor(props) {
    super(props, [], {
      name: null
    });
    this.setTitle(() => {
      return {
        isPath: false,
        text: this.state.name ? this.state.name : 'Test Suite',
        prefix: this.state.name ? 'TS: ' : 'ActorJs'
      };
    });
  }
  
  didMount() {
    this.dispatch(TestSuiteStore, new ActionTestSuiteMounted());
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.name, nextState.name);
  }
  
  willUnmount() {
    this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestClear());
  }
  
  render() {
    return (
      <div className="middle middle_test_suites">
        <Route switch={["/:sut/:fut/:ts/:tab*", "/:sut/:fut/:ts", "/:sut/:fut", "/:sut", ""]} handler={MiddleTestSuitesToolbar} />
        <MiddleSidebar />
        <div className="middle_view middle_view_test_suites">
          <Route switch={["/:sut/:fut/:ts*", "/:sut/:fut", "/:sut", ""]} handler={MiddleTestSuitesViewColumns} onTsName={(name) => {
            this.updateState({name: {$set: name}});
          }}/>
        </div>
      </div>
    );
  }
}
