
'use strict';

import SystemUnderTestStore from '../../../stores/system-under-test-store';
import TestSuiteStore from '../../../stores/test-suite-store';
import TestSuitesStore from '../../../stores/test-suites-store';
import MiddleTestSuitesViewRecent from './middle-test-suites-view-recent';
import MiddleTestSuitesViewSut from './middle-test-suites-view-sut';
import MiddleTestSuitesViewFut from './middle-test-suites-view-fut';
import MiddleTestSuitesViewTs from './middle-test-suites-view-ts';
import MiddleTestSuitesViewTestSuite from './middle-test-suites-view-test-suite';
import { ActionSystemUnderTestSet } from '../../../actions/action-system-under-test/action-system-under-test';
import { ActionTestSuitesSetView } from '../../../actions/action-test-suites';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
//import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ComponentBreadcrump from 'z-abs-complayer-bootstrap-client/client/breadcrump';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestSuitesViewColumns extends ReactComponentStore {
  constructor(props) {
    super(props, [TestSuiteStore, SystemUnderTestStore]);
    this.boundKeyDown = this._keyDown.bind(this);
    this.scrollRecent = 0;
    this.scrollFut = 0;
    this.scrollSut = 0;
    this.scrollTc = 0;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
    this.setView();
    if(this.props.onTsName && this.props.ts) {
      this.props.onTsName(this.props.ts);
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.ts, nextProps.ts)
      || this.state.TestSuiteStore.markup.definition !== nextState.TestSuiteStore.markup.definition
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests);
  }
  
  didUpdate(prevProps, prevState) {
    this.setView();
    if(this.props.ts !== prevProps.ts) {
      this.props.onTsName(this.props.ts);
    }
  }
  
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _keyDown(e) {
    if(e.altKey && 'c' === e.key && !this._disabledCopy()) {
      e.preventDefault();
      this._copyMarkupName(e);      
    }
  }
  
  setView() {
    if(undefined === this.props.sut) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(null));
      this.dispatch(TestSuitesStore, new ActionTestSuitesSetView('none'));
    }
    else if(undefined === this.props.fut) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestSuitesStore, new ActionTestSuitesSetView('sut'));
    }
    else if(undefined === this.props.ts) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestSuitesStore, new ActionTestSuitesSetView('fut'));
    }
    else {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestSet(this.props.sut));
      this.dispatch(TestSuitesStore, new ActionTestSuitesSetView('ts'));
    }
  }
  
  renderTestSuites() {
    return (
      <div className="test_suite_columns">
        <MiddleTestSuitesViewRecent />
        <MiddleTestSuitesViewSut />
      </div>
    );
  }
  
  renderTestSuitesSut(repo) {
    return (
      <div className="test_suite_columns">
        <MiddleTestSuitesViewRecent />
        <MiddleTestSuitesViewSut />
        <MiddleTestSuitesViewFut sut={this.props.sut} repo={repo} />
      </div>
    );
  }
  
  renderTestSuitesFut(repo) {
    return (
      <div className="test_suite_columns">
        <MiddleTestSuitesViewRecent scroll={this.scrollRecent}
          onRestoreData={(scrollTop) => {
            this.scrollRecent = scrollTop;
          }}
        />
        <MiddleTestSuitesViewSut scroll={this.scrollSut}
          onRestoreData={(scrollTop) => {
            this.scrollSut = scrollTop;
          }}
        />
        <MiddleTestSuitesViewFut repo={repo} sut={this.props.sut} scroll={this.scrollFut}
          onRestoreData={(scrollTop) => {
            this.scrollFut = scrollTop;
          }}
        />
        <MiddleTestSuitesViewTs repo={repo} sut={this.props.sut} fut={this.props.fut} scroll={this.scrollTc}
          onRestoreData={(scrollTop) => {
            this.scrollTc = scrollTop;
          }}
        />
      </div>
    );
  }
  
  renderTestSuitesTs(repo) {
    return (
      <Route switch={["/:tab*", ""]} repo={repo} sut={this.props.sut} fut={this.props.fut} ts={this.props.ts} handler={MiddleTestSuitesViewTestSuite} />
    );
  }
  
  renderColumns() {
    const sut = !this.props.sut ? undefined : this.state.SystemUnderTestStore.systemUnderTests.find((sut) => {
      return sut.name === this.props.sut;
    });
    const repo = sut ? sut.repo : undefined;
    if(undefined !== this.props.ts) {
      return this.renderTestSuitesTs(repo);
    }
    else if(undefined !== this.props.fut) {
      return this.renderTestSuitesFut(repo);
    }
    else if(undefined !== this.props.sut) {
      return this.renderTestSuitesSut(repo);
    }
    else {
      return this.renderTestSuites();
    }
  }
  
  _getTestSuiteFullName() {
    let testSuiteFullName = 'ts';
    if(undefined !== this.props.sut) {
      testSuiteFullName += `.${this.props.sut}`;
    }
    if(undefined !== this.props.fut) {
      testSuiteFullName += `.${this.props.fut}`;
    }
    if(undefined !== this.props.ts) {
      testSuiteFullName += `.${this.props.ts}`;
    }
    return testSuiteFullName;
  }
  
  renderTestSuiteFullName() {
    return (
      <div className="bootstrap_breadcrump_full_name">
        {'Markup name: ' + this._getTestSuiteFullName()}   
      </div>
    );
  }
  
  _copyMarkupName(e) {
    navigator.clipboard.writeText(this._getTestSuiteFullName()).then(() => {
      /* clipboard successfully set */
    }, () => {
      /* clipboard write failed */
    });
  }
  
  _disabledCopy() {
    return this.state.TestSuiteStore.markup.definition;
  }
  
  renderCopyButton() {
    return (
      <Button placement="bottom" heading="Copy" content="Markup name" shortcut="Alt+c" disabled={this._disabledCopy()}
        onClick={(e) => {
          this._copyMarkupName(e);
        }}
      >
        <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
      </Button>
    );
  }

  render() {
    return (
      <div className="same_size_as_parent">
        <div className="test_suite_breadcrump">
          <ComponentBreadcrump items={[
            {name: 'Test Suites', link: ''},
            {name: this.props.sut, link: `${this.props.sut}`},
            {name: this.props.fut, link: `${this.props.sut}/${this.props.fut}`},
            {name: this.props.ts, link: `${this.props.sut}/${this.props.fut}/${this.props.ts}`}
          ]}/>
          <div className="bootstrap_breadcrump_full_name">
            {'-'}   
          </div>
          <div className="btn-group btn-group-xs" role="group" aria-label="..." style={{marginLeft:'8px'}}>
            {this.renderCopyButton()}
          </div>
          {this.renderTestSuiteFullName()}
        </div>
        {this.renderColumns()}
      </div>
    ); 
  }
}


//MiddleTestSuitesViewColumns.contextType = RouterContext;
