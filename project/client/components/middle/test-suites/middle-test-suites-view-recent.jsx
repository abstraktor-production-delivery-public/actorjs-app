
'use strict';

import FilterStore from '../../../stores/filter-store';
import TestSuiteStore from '../../../stores/test-suite-store';
import { ActionTestSuiteRecentGet } from '../../../actions/action-test-suite/action-test-suite';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleTestSuitesViewRecent extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, TestSuiteStore]);
    this.refRecent = React.createRef();
  }
  
  didMount() {
    this.dispatch(TestSuiteStore, new ActionTestSuiteRecentGet());
    if(this.props.scroll) {
      this.refRecent.current.scrollTop = this.props.scroll;
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareArrayValues(this.state.TestSuiteStore.recentTestSuites, nextState.TestSuiteStore.recentTestSuites);
  }
    
  willUnmount() {
    this.props.onRestoreData && this.props.onRestoreData(this.refRecent.current.scrollTop);
  }
  
  renderRecentTestSuitesRows() {
    let recentTestSuitesRows = this.state.TestSuiteStore.recentTestSuites.map((recentTs, i) => {
    return (
      <tr key={`ts:recent_ts.${recentTs.sut}.${recentTs.fut}.${recentTs.ts}`}>
        <th scope="row">{i + 1}</th>    
        <td>
          <Link className="test_suite_link" href={`/${recentTs.sut}/${recentTs.fut}/${recentTs.ts}`}>
            {`ts.${recentTs.sut}.${recentTs.fut}.${recentTs.ts}`}
          </Link>
        </td>
        <td>{recentTs.repo}</td>
      </tr>)
    })
  	return recentTestSuitesRows;
  }
  
  render() {
    return (
      <div ref={this.refRecent} className="test_suite_column">
        <div className="test_suites_table">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Recent</h3>
            </div>
            <table id="ts_recent_table" className="table test_suites_table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Repo</th>
                </tr>
              </thead>
              <tbody>
                {this.renderRecentTestSuitesRows()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
