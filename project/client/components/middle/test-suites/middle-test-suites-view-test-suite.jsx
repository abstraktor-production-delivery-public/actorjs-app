
'use strict';

import TestSuiteStore from '../../../stores/test-suite-store';
import MiddleTestSuitesTabDefinition from './tabs/middle-test-suites-tab-definition';
import MiddleTestSuitesTabExecution from './tabs/middle-test-suites-tab-execution';
import MiddleTestSuitesTabLog from './tabs/middle-test-suites-tab-log';
import MiddleTestSuitesTabSequenceDiagram from './tabs/middle-test-suites-tab-sequence-diagram';
import { ActionTestSuiteGet } from '../../../actions/action-test-suite/action-test-suite';
import Arc from 'z-abs-complayer-documentation-client/client/calculations/arc';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import AppProtocolConst from 'z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const';
import ActorResultConst from 'z-abs-funclayer-engine-cs/clientServer/execution/actor-result-const';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestSuitesViewTestSuite extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestSuiteStore], {
      activeKey: 0,
      nbrOfTestSuitesAbstractions: 0
    });
    this.helperTab = new HelperTab(['Definition', 'Execution', 'Log', 'Sequence Diagram', 'Specification'/*, 'Analyze'*/]);
    if(undefined !== props.tab) {
      this.state.activeKey = this.helperTab.getIndex(props.tab);
    }
    this.templateArcsTcs = null;
    this.templateArcsIterations = null;
    this.sumResultTcValue = ActorResultConst.NONE;
    this.sumResultIterValue = ActorResultConst.NONE;
    this.tc = 0;
    this.iterationTs = 0;
    this.stop = false;
    this.refSvgTcStatus = React.createRef();
    this.refIterationsTs = React.createRef();
    this.groupData = null;
  }
  
  didMount() {
    this.groupData = this.addRealtimeFrameGroup();
    if(this.props.repo) {
      this.dispatch(TestSuiteStore, new ActionTestSuiteGet(this.props.repo, this.props.sut, this.props.fut, this.props.ts));
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.repo, nextProps.repo)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.ts, nextProps.ts)
      || !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
      || !this.shallowCompare(this.state.nbrOfTestSuitesAbstractions, nextState.nbrOfTestSuitesAbstractions)
      || !this.shallowCompare(this.state.TestSuiteStore.run, nextState.TestSuiteStore.run)
      || !this.shallowCompare(this.state.TestSuiteStore.iterationsTs, nextState.TestSuiteStore.iterationsTs);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.repo !== prevProps.repo || this.props.sut !== prevProps.sut || this.props.fut !== prevProps.fut || this.props.ts !== prevProps.ts) {
      this.dispatch(TestSuiteStore, new ActionTestSuiteGet(this.props.repo, this.props.sut, this.props.fut, this.props.ts));
    }
    const nextKey = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== nextKey) {
      this.updateState({activeKey: {$set: nextKey}});
    }
    if(this.state.TestSuiteStore.run.testSuite !== prevState.TestSuiteStore.run.testSuite) {
      this._preapairTc();
      this._prepairIterationsTs();
    }
    else if(this.state.TestSuiteStore.iterationsTs !== prevState.TestSuiteStore.iterationsTs) {
      this._prepairIterationsTs();
    }
  }
  
  willUnmount() {
    this.removeRealtimeFrameGroup(this.groupData);
  }
  
  onRealtimeMessage(msg) {
    if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId) {
      const key = this.templateArcsTcs[msg.index].key;
      const arcResults = this.templateArcsTcs[msg.index].classes;
      this.replaceRealtimeFrameText('tc_text', this.groupData, (++this.tc).toString());
      this.replaceRealtimeFrameObject(key, this.groupData, arcResults[msg.resultId]);
      this.sumResultTcValue = Math.max(this.sumResultTcValue, msg.resultId);
    }
    else if(AppProtocolConst.TEST_STAGE_STOPPED === msg.msgId) {
      const key = this.templateArcsTcs[msg.index].key;
      const arcResults = this.templateArcsTcs[msg.index].classes;
      this.replaceRealtimeFrameText('tc_text', this.groupData, (++this.tc).toString());
      this.replaceRealtimeFrameObject(key, this.groupData, arcResults[msg.resultId]);
      this.sumResultTcValue = Math.max(this.sumResultTcValue, msg.resultId);
    }
    else if(AppProtocolConst.TEST_SUITE_STARTED === msg.msgId) {
      this.sumResultTcValue = ActorResultConst.NONE;
      this.tc = 0;
      if(this.stop) {
        this.stop = false;
        this.iterationTs = 0;
        this._clean(1 < this.state.TestSuiteStore.iterationsTs);
      }
      if(0 !== this.iterationTs) {
        this.tc = 0;
        if(++this.iterationTs > this.state.TestSuiteStore.iterationsTs) {
          this.replaceRealtimeFrameText('tc_text', this.groupData, 0);
          if(1 < this.state.TestSuiteStore.iterationsTs) {
            this.replaceRealtimeFrameText('iter_text', this.groupData, 0);
          }
          this.iterationTs = 1;
          this._clean(1 < this.state.TestSuiteStore.iterationsTs);
        }
        else {
          this._clean();
        }
      }
      else {
        this.replaceRealtimeFrameText('tc_text', this.groupData, 0);
        if(1 < this.state.TestSuiteStore.iterationsTs) {
          this.replaceRealtimeFrameText('iter_text', this.groupData, 0);
        }
        this.iterationTs = 1;
        this._clean(1 < this.state.TestSuiteStore.iterationsTs);
      }
    }
    else if(AppProtocolConst.TEST_SUITE_STOP === msg.msgId) {
      this.stop = true;
    }
    else if(AppProtocolConst.TEST_SUITE_STOPPED === msg.msgId) {
      if(1 !== this.state.TestSuiteStore.iterationsTs) {
        if(1 < this.state.TestSuiteStore.iterationsTs) {
          const key = this.templateArcsIterations[this.iterationTs-1].key;
          const arcResults = this.templateArcsIterations[this.iterationTs-1].classes;
          this.replaceRealtimeFrameText('iter_text', this.groupData, this.iterationTs.toString());
          this.replaceRealtimeFrameObject(key, this.groupData, arcResults[this.sumResultTcValue]);
          this.sumResultIterValue = Math.max(this.sumResultIterValue, this.sumResultTcValue);
        }
      }
    }
    else if(AppProtocolConst.TEST_SUITE_CLEAR === msg.msgId) {
      this.tc = 0;
      this.iterationTs = 0;
      this._clean(true);
    }
  }
  
  _addResultArch(key, svg, resultArcs, currentAngle, nextAngle) {
    const arcNode = pathNode.cloneNode();
    arcNode.setAttribute('d', Arc.describeArc(37, 37, 34, -19, currentAngle, nextAngle));
    const classes = [];
    resultArcs.push({
      key,
      classes
    });
    //resultArcs.set(key, classes);
    ActorResultConst.classes.forEach((className) => {
      const arc = arcNode.cloneNode(true);
      arc.classList.add(className);
      classes.push(arc);
    });
    svg.appendChild(arcNode);
    arcNode.classList.add('test_none');
    this.addRealtimeFrame(key, this.groupData, arcNode);
  }
  
  _preapairTc() {
    this.tc = 0;
    this.templateArcsTcs = [];
    if(this.refSvgTcStatus.current) {
      const svg = this.refSvgTcStatus.current;
      const text = svg.childNodes[2];
      this.addRealtimeFrame('tc_text', this.groupData, text.firstChild);
      let nbrOfTestSuitesAbstractions = 0 ;
      this.state.TestSuiteStore.run.testSuite.forEach((abstractions) => {
        abstractions.forEach((abstraction) => {
          for(let i = 0; i < abstraction.iterationsTs; ++i) {
            ++nbrOfTestSuitesAbstractions;
            nbrOfTestSuitesAbstractions += abstraction.type;
          }
        });
      });
      this.updateState({nbrOfTestSuitesAbstractions: {$set: nbrOfTestSuitesAbstractions}});
      const arcs = [];
      const angle = 360.0 / nbrOfTestSuitesAbstractions;
      let currentAngle = 0;
      let nextAngle = angle;
      let index = 0;
      let hasState = false;
      this.state.TestSuiteStore.run.testSuite.forEach((abstractions) => {
        abstractions.forEach((abstraction) => {
          ++index;
          if(0 === abstraction.type) {
            for(let i = 0; i < abstraction.iterationsTs; ++i) {
              const key = `arc_${index}_${i + 1}`;
              this._addResultArch(key, svg, this.templateArcsTcs, currentAngle, nextAngle);
              currentAngle = nextAngle;
              nextAngle += angle;
            }
          }
          else {
            if(hasState) {
              const key = `arc_${index}_1`;
              this._addResultArch(key, svg, this.templateArcsTcs, currentAngle, nextAngle);
              currentAngle = nextAngle;
              nextAngle += angle;
              ++index;
            }
            else {
              hasState = true;
            }
            const key = `arc_${index}_1`;
            this._addResultArch(key, svg, this.templateArcsTcs, currentAngle, nextAngle);
            currentAngle = nextAngle;
            nextAngle += angle;
          }
        });
      });
      if(hasState) {
        const key = `arc_${++index}_1`;
        this._addResultArch(key, svg, this.templateArcsTcs, currentAngle, nextAngle);
      }
    }
  }
  
  _prepairIterationsTs() {
    this.iterationTs = 0;
    this.templateArcsIterations = [];
    if(this.refIterationsTs.current) {
      const svg = this.refIterationsTs.current;
      const text = svg.childNodes[2];
      this.addRealtimeFrame('iter_text', this.groupData, text.firstChild);
      if(1 !== this.state.TestSuiteStore.iterationsTs) {
        const arcs = [];
        const angle = 360.0 / this.state.TestSuiteStore.iterationsTs;
        let currentAngle = 0;
        let nextAngle = angle;
        let index = 0;
        for(let i = 0; i < this.state.TestSuiteStore.iterationsTs; ++i) {
          const key = `arc_iter_${i + 1}`;
           this._addResultArch(key, svg, this.templateArcsIterations, currentAngle, nextAngle);
           currentAngle = nextAngle;
           nextAngle += angle;
        }
      }
    }
  }
  
  _clean(iter = false) {
    this._clearTcStatus();
    if(iter) {
      this._clearIterStatus();
    }
  }
  
  _clearTcStatus() {
    this.sumResultTcValue = ActorResultConst.NONE;
    if(this.templateArcsTcs) {
      this.templateArcsTcs.forEach((arc) => {
        const key = arc.key;
        const arcResults = arc.classes;
        this.replaceRealtimeFrameObject(key, this.groupData, arcResults[0]);
      });
    }
  }
  
  _clearIterStatus() {
    this.sumResultIterValue = ActorResultConst.NONE;
    if(1 !== this.state.TestSuiteStore.iterationsTs) {
      for(let i = 0; i < this.state.TestSuiteStore.iterationsTs; ++i) {
        const arc = this.templateArcsIterations[i];
        const key = arc.key;
        const arcResults = arc.classes;
        this.replaceRealtimeFrameObject(key, this.groupData, arcResults[0]);
      }
    }
  }
  
  getTabPath(index) {
    return `${this.props.tab ? '' : this.props.ts + '/'}${this.helperTab.getRoute(index)}`;
  }
  
  renderTcStatus() {
    if(0 !== this.state.TestSuiteStore.run.testSuite.size) {
      return (
        <div className="test_suite_result_view_tcs">
          <svg ref={this.refSvgTcStatus} className="test_suite_result_view" viewBox="0 0 74 74" xmlns="http://www.w3.org/2000/svg">
            <circle cx="37" cy="37" r="35" strokeWidth="1" fill="whitesmoke" />
            <circle cx="37" cy="37" r="14" strokeWidth="1" fill="white" />
            <text className="test_suite_result_view_tcs" x="37" y="37" dy="5">{this.state.nbrOfTestSuitesAbstractions}</text>
          </svg>
        </div>
      );
    }
  }
  
  renderIterationStatus() {
    if(0 !== this.state.TestSuiteStore.run.testSuite.size && 1 !== this.state.TestSuiteStore.iterationsTs) {
      return (
        <div className="test_suite_result_view_iterations">
          <svg ref={this.refIterationsTs} className="test_suite_result_view" viewBox="0 0 74 74" xmlns="http://www.w3.org/2000/svg">
            <circle cx="37" cy="37" r="35" strokeWidth="1" fill="whitesmoke" />
            <circle cx="37" cy="37" r="14" strokeWidth="1" fill="white" />
            <text className="test_suite_result_view_iterations" x="37" y="37" dy="5">{this.state.TestSuiteStore.iterationsTs}</text>
          </svg>
        </div>
      );
    }
  }
  
  render() {
    let index = 0;
    return (
      <div className="test_suite_tabs">
        {this.renderIterationStatus()}
        {this.renderTcStatus()}
        <Tabs id="test_suite_tabs" activeKey={this.state.activeKey}
          onSelect={(key) => {
            if(undefined !== key) {
              this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
            }
          }}
        >
          <Tab id="test_suite_definition" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleTestSuitesTabDefinition active={index - 1 === this.state.activeKey} />
          </Tab>
          <Tab id="test_suite_execution" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="execution">
            <Route handler={MiddleTestSuitesTabExecution} switch="*" active={index - 1 === this.state.activeKey} catchClick />
          </Tab>
          <Tab id="test_suite_log" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="log">
            {/*<Route handler={MiddleTestSuitesTabLog} switch="*" catchClick active={index - 1 === this.state.activeKey} />*/}
            <MiddleTestSuitesTabLog active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} ts={this.props.ts} repo={this.props.repo} />
          </Tab>
          <Tab id="test_suite_sequence_diagram" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)} colorMark="sequence-diagram">
            <Route handler={MiddleTestSuitesTabSequenceDiagram} switch="*" catchClick active={index - 1 === this.state.activeKey} />
            {/*<MiddleTestSuitesTabSequenceDiagram active={index - 1 === this.state.activeKey} sut={this.props.sut} fut={this.props.fut} ts={this.props.ts} repo={this.props.repo} />*/}
          </Tab>
          <Tab id="test_suite_specification" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <h1>Specification</h1>
          </Tab>
          {/*<Tab id="test_suite_analyze" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <h1>Analyze</h1>
          </Tab>*/}
        </Tabs>
      </div>
    );
  }
}

const pathNode = document.createElementNS('http://www.w3.org/2000/svg', 'path');

MiddleTestSuitesViewTestSuite.contextType = RouterContext;
