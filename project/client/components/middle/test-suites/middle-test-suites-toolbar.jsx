
'use strict';

import MiddleFilter from '../middle-sut-fut-filter';
import LoginStore from '../../../stores/login-store';
import TestSuitesStore from '../../../stores/test-suites-store';
import TestSuiteStore from '../../../stores/test-suite-store';
import FunctionUnderTestStore from '../../../stores/function-under-test-store';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionSystemUnderTestAdd, ActionSystemUnderTestDelete, ActionSystemUnderTestRepoGet } from '../../../actions/action-system-under-test/action-system-under-test';
import { ActionFunctionUnderTestAdd, ActionFunctionUnderTestDelete } from '../../../actions/action-function-under-test';
import { ActionTestSuiteAdd, ActionTestSuiteDelete, ActionTestSuiteMarkup, ActionTestSuiteMarkupSave, ActionTestSuiteMarkupCancel, ActionTestSuiteExecutionStart, ActionTestSuiteExecutionStop, ActionTestSuiteClear, ActionTestSuiteIterations } from '../../../actions/action-test-suite/action-test-suite';
import { ActionTestSuitesSutClear, ActionTestSuitesFutClear, ActionTestSuitesTsClear } from '../../../actions/action-test-suites';
import ModalDialogAbstractionAdd from 'z-abs-complayer-modaldialog-client/client/modal-dialog-abstraction-add';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentRealtime from 'z-abs-corelayer-client/client/react-component/react-component-realtime';
import React from 'react';


export default class MiddleTestSuitesToolbar extends ReactComponentRealtime {
  constructor(props) {
    super(props, [TestSuitesStore, TestSuiteStore, SystemUnderTestStore, LoginStore]);
    this._modalDialogAbstractionAdd = null;
    this.boundKeyDown = this._keyDown.bind(this);
    this.markupDisabledOpen = false;
    this.markupDisabledSave = true;
    this.markupDisabledHelp = false;
    this.markupDisabledCancel = true;
    this.repo = null;
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.ts, nextProps.ts)
      || !this.shallowCompare(this.state.TestSuiteStore.markup, nextState.TestSuiteStore.markup)
      || !this.shallowCompare(this.state.TestSuiteStore.iterationsTs, nextState.TestSuiteStore.iterationsTs)
      || !this.shallowCompare(this.state.TestSuiteStore.execution, nextState.TestSuiteStore.execution)
      || !this.shallowCompare(this.state.TestSuiteStore.realtimeConnection, nextState.TestSuiteStore.realtimeConnection)
      || !this.shallowCompare(this.state.TestSuitesStore, nextState.TestSuitesStore)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompareArrayValues(this.state.SystemUnderTestStore.repos, nextState.SystemUnderTestStore.repos)
      || !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login);
  }
      
  willUnmount() {
    window.removeEventListener('keydown', this.boundKeyDown, true);
  }
  
  _markupOpen() {
    this.dispatch(TestSuiteStore, new ActionTestSuiteMarkup());
    this.tabNavigation('definition');
  }
  
  _markupSave() {
    this.dispatch(TestSuiteStore, new ActionTestSuiteMarkupSave(this.repo, this.props.sut, this.props.fut, this.props.ts));
    this.tabNavigation('definition');
  }
  
  _markupHelp() {
    this.context.history('../documentation/markup/markup-test-suite');
  }
  
  _markupCancel() {
    this.dispatch(TestSuiteStore, new ActionTestSuiteMarkupCancel());
    this.tabNavigation('definition');
  }
  
  _keyDown(e) {
    if(e.ctrlKey && 'o' === e.key) {
      if(!this.markupDisabledOpen) {
        e.preventDefault();
        this._markupOpen();
      }
    }
    else if(e.ctrlKey && e.shiftKey && '?' === e.key) {
      if(!this.markupDisabledHelp) {
        e.preventDefault();
        this._markupHelp();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'C' === e.key) {
      if(!this.markupDisabledCancel) {
        e.preventDefault();
        this._markupCancel();
      }
    }
    else if(e.ctrlKey && 's' === e.key) {
      if(!this.markupDisabledSave) {
        e.preventDefault();
        this._markupSave();
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'A' === e.key) {
      const disabled = 'ts' === this.state.TestSuitesStore.current.view;
      if(!disabled) {
        e.preventDefault();
        this._add(e);
      }
    }
    else if(e.ctrlKey && e.shiftKey && 'D' === e.key) {
      const disabled = 0 === this.state.TestSuitesStore.systemUnderTestsChecked.size && 0 === this.state.TestSuitesStore.functionUnderTestsChecked.size && 0 === this.state.TestSuitesStore.testSuitesChecked.size;
      if(!disabled) {
        e.preventDefault();
        this._delete(e);
      }
    }
  }
  
  getText() {
    if('none' === this.state.TestSuitesStore.current.view) {
      return 'System Under Test';
    }
    else if('sut' === this.state.TestSuitesStore.current.view) {
      return 'Function Under Test';
    }
    else if('fut' === this.state.TestSuitesStore.current.view) {
      return 'Test Suite';
    }     
  }
  
  _add(e) {
    this._modalDialogAbstractionAdd.show();  
  }
  
  _delete(e) {
    const suts = [];
    this.state.TestSuitesStore.systemUnderTestsChecked.forEach((sut) => {
      suts.push(sut);
    });
    const futs = [];
    this.state.TestSuitesStore.functionUnderTestsChecked.forEach((fut) => {
      const found = suts.find((sut) => {
        return sut.repo === fut.repo && sut.name === fut.sut;
      });
      if(!found) {
        futs.push(fut);
      }
    });
    const tses = [];
    this.state.TestSuitesStore.testSuitesChecked.forEach((ts) => {
      const found = suts.find((sut) => {
        return sut.name === ts.sut;
      });
      if(!found) {
        const foundFut = futs.find((fut) => {
          return fut.repo === ts.repo && fut.sut === ts.sut && fut.fut === ts.fut;
        });
        if(!foundFut) {
          tses.push(ts);
        }
      }
    });
    const isCurrentSut = suts.some((sut) => {
      return sut.name === this.props.sut;
    });
    const isCurrentFut = futs.some((fut) => {
      return fut.repo === this.repo && fut.sut === this.props.sut && fut.fut === this.props.fut;
    });
    this.dispatch(TestSuitesStore, new ActionTestSuitesSutClear());
    this.dispatch(TestSuitesStore, new ActionTestSuitesFutClear());
    this.dispatch(TestSuitesStore, new ActionTestSuitesTsClear());
    if(0 !== suts.length) {
      this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestDelete(suts));
      if(isCurrentSut) {
        return this.context.history('');
      }
    }
    if(0 !== futs.length) {
      this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestDelete(this.repo, this.props.sut, futs));
      if(isCurrentFut) {
        return this.context.history(`${this.props.sut}`);
      }
    }
    if(0 !== tses.length) {
      this.dispatch(TestSuiteStore, new ActionTestSuiteDelete(this.repo, this.props.sut, this.props.fut, tses));
    }
  }
  
  renderAddButton() {
    if('ts' !== this.state.TestSuitesStore.current.view) {
      return (
        <Button id="test_suite_add" placement="bottom" heading="Add" content={this.getText()} shortcut="Ctrl+Shift+A" disabled={'ts' === this.state.TestSuitesStore.current.view}
          onClick={(e) => {
            this._add(e);
          }}
        >
          <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderRemoveButton() {
    if('ts' !== this.state.TestSuitesStore.current.view) {
      return (
        <Button id="test_suite_remove" placement="bottom" heading="Delete" content="Checked" shortcut="Ctrl+Shift+D" disabled={0 === this.state.TestSuitesStore.systemUnderTestsChecked.size && 0 === this.state.TestSuitesStore.functionUnderTestsChecked.size && 0 === this.state.TestSuitesStore.testSuitesChecked.size}
          onClick={(e) => {
            this._delete(e);
          }}
        >
          <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupOpenButton() {
    if('ts' === this.state.TestSuitesStore.current.view) {
      this.markupDisabledOpen = this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (0 !== this.state.TestSuiteStore.execution);
      return (
        <Button placement="bottom" heading="Open" content="Markup" shortcut="Ctrl+O" disabled={this.markupDisabledOpen}
          onClick={(e) => {
            this._markupOpen();
          }}
        >
          <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupSaveButton() {
    if('ts' === this.state.TestSuitesStore.current.view) {
      this.markupDisabledSave = this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.content === this.state.TestSuiteStore.markup.contentOriginal;
      return (
        <Button placement="bottom" heading="Save" content="Markup" shortcut="Ctrl+S" disabled={this.markupDisabledSave}
          onClick={(e) => {
            this._markupSave();
          }}
        >
          <span className="glyphicon glyphicon-save-file" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderMarkupHelpButton() {
    this.markupDisabledHelp = this.state.TestSuiteStore.markup.definition;
    return (
      <Button placement="bottom" heading="Help" content="Markup" shortcut="Ctrl+?" disabled={this.markupDisabledHelp}
        onClick={(e) => {
          this._markupHelp();
        }}
      >
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderMarkupCancelButton() {
    if('ts' === this.state.TestSuitesStore.current.view) {
      this.markupDisabledCancel = !this.state.TestSuiteStore.markup.definition;
      return (
        <Button placement="bottom" heading="Cancel" content="Markup" shortcut="Ctrl+Shift+C" disabled={this.markupDisabledCancel}
          onClick={(e) => {
            this._markupCancel();
          }}
        >
          <span className="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderStartButton(tab) {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <Button  id={`test_suite_start_${tab}`} placement="bottom" heading="Run" content={`in the ${tab} tab`} disabled={this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (TestSuiteStore.EXECUTION_NOT_RUNNING !== this.state.TestSuiteStore.execution) || this.state.LoginStore.login.systemUnderTest !== this.props.sut}
          onClick={(e) => {
            if(!(this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (TestSuiteStore.EXECUTION_NOT_RUNNING !== this.state.TestSuiteStore.execution))) {
              this.dispatch(TestSuiteStore, new ActionTestSuiteExecutionStart(this.repo, this.props.sut, this.props.fut, this.props.ts, this.state.LoginStore.login.systemUnderTest, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:false}));
              this.tabNavigation(tab);
            }
          }}
        >
          <span className={`glyphicon glyphicon-play test_suite_${tab}`} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderStartSlowMotionButton(tab) {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <Button id="test_suite_start_slow_motion" placement="bottom" heading="Run Slow Motion" content="in current tab" disabled={this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (TestSuiteStore.EXECUTION_NOT_RUNNING !== this.state.TestSuiteStore.execution)  || this.state.LoginStore.login.systemUnderTest !== this.props.sut}
          onClick={(e) => {
            this.dispatch(TestSuiteStore, new ActionTestSuiteExecutionStart(this.repo, this.props.sut, this.props.fut, this.props.ts, this.state.LoginStore.login.systemUnderTest, this.state.LoginStore.login.systemUnderTestInstance, this.state.LoginStore.login.nodes, {slow:true}));
          }}
        >
          <span className="glyphicon glyphicon-eject" aria-hidden="true" style={{transform: 'rotate(90deg)'}}></span>
        </Button>
      );
    }
  }
  
  renderStopButton(tab) {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <Button id="test_suite_stop" placement="bottom" heading="Stop" content="in current tab" disabled={this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (TestSuiteStore.EXECUTION_RUNNING !== this.state.TestSuiteStore.execution)}
          onClick={(e) => {
            this.dispatch(TestSuiteStore, new ActionTestSuiteExecutionStop());
          }}
        >
          <span className="glyphicon glyphicon-stop" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderClearButton(tab) {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <Button id={`test_suite_clear_${tab}`} placement="bottom" heading="Clear" content={`In the ${tab} tab`} disabled={this.state.TestSuiteStore.markup.definition || (0 !== this.state.TestSuiteStore.execution)}
          onClick={(e) => {
            this.dispatch(TestSuiteStore, new ActionTestSuiteClear(tab));
            this.tabNavigation(tab);
          }}
        >
          <span className={`glyphicon glyphicon-erase test_suite_${tab}`} aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderClearAllButton() {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <Button id="test_suite_clear_all" placement="bottom" heading="Clear" content="all" disabled={this.state.TestSuiteStore.markup.definition || (0 !== this.state.TestSuiteStore.execution)}
          onClick={(e) => {
            this.dispatch(TestSuiteStore, new ActionTestSuiteClear('execution'));
            this.dispatch(TestSuiteStore, new ActionTestSuiteClear('log'));
            this.dispatch(TestSuiteStore, new ActionTestSuiteClear('sequence-diagram'));
          }}
        >
          <span className="glyphicon glyphicon-erase" aria-hidden="true"></span>
        </Button>
      );
    }
  }
  
  renderRealtimeInfo() {
    let className = 'glyphicon glyphicon-transfer';
    let realtimeConnectionStatus = '';
    if(0 === this.state.TestSuiteStore.realtimeConnection){
      className += ' realtime_disconnected';
      realtimeConnectionStatus += 'down';
    }
    else if(1 === this.state.TestSuiteStore.realtimeConnection) {
      className += ' realtime_connected';
      realtimeConnectionStatus += 'up';
    }
    return (
      <Popover className="pull-right" placement="bottom" heading="Realtime Connection" content={realtimeConnectionStatus}>
        <span className={className} aria-hidden="true"></span>
      </Popover>
    );
  }
  
  renderStageButton() {
    return (
      <Button id="from_start_to_stage" placement="bottom" heading="Goto" content="Stage"
        onClick={(e) => {
          this.context.history(`/stage`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-star-empty" aria-hidden="true" style={{top: -4, left: -4, width: 0, transform: 'scale(0.9)'}}></span>
      </Button>
    );
  }
  
  renderLoginButton() {
    return (
      <Button id="from_start_to_login" placement="bottom" heading="Goto" content="User"
        onClick={(e) => {
          this.context.history(`/user`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderModalDialogAbstractionAdd() {
    if('none' === this.state.TestSuitesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_suites_sut_add" ref={(c) => this._modalDialogAbstractionAdd = c} type="sut" defaultMarkup={MiddleTestSuitesToolbar.DEFAULT_MARKUP_SUT} result={'success'} capitalFirst={true} repos={this.state.SystemUnderTestStore.repos} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onLoad={() => {
            this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestRepoGet());
          }}
          onAdd={(name, description, repo) => {
            this.dispatch(SystemUnderTestStore, new ActionSystemUnderTestAdd(repo, name, description));
          }}
          onClear={() => {
          }}
        />
      );
    }
    else if('sut' === this.state.TestSuitesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_suites_fut_add" ref={(c) => this._modalDialogAbstractionAdd = c} type="fut" defaultMarkup={MiddleTestSuitesToolbar.DEFAULT_MARKUP_FUT} result={'success'} capitalFirst={true} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onAdd={(name, description) => {
            this.dispatch(FunctionUnderTestStore, new ActionFunctionUnderTestAdd(this.repo, this.props.sut, name, description));
          }}
          onClear={() => {
          }}
        />
      );
    }
    else if('fut' === this.state.TestSuitesStore.current.view) {
      return (
        <ModalDialogAbstractionAdd id="test_suites_ts_add" ref={(c) => this._modalDialogAbstractionAdd = c} type="ts" defaultMarkup={MiddleTestSuitesToolbar.DEFAULT_MARKUP_TS} result={'success'} capitalFirst={true} heading={`Add a new ${this.getText()}`} nameplaceholder={`The name of the ${this.getText()}`} descriptionplaceholder={`A description of the ${this.getText()}`}
          onAdd={(name, description) => {
            this.dispatch(TestSuiteStore, new ActionTestSuiteAdd(this.repo, this.props.sut, this.props.fut, name, description));
          }}
          onClear={() => {
          }}
        />
      );
    }
  }
  
  getIterationOptions() {
    let key = 0;
    const values = [1, 2, 5, 10, 25, 50, 250, 1000];
    return values.map((value) => {
      return (<option value={value} key={key++}>{value}</option>);
    });
  }
  
  renderIterationOptions() {
    if('ts' === this.state.TestSuitesStore.current.view) {
      return (
        <>
          <label className="middle_login_toolbar_component" htmlFor="middle_test_suite_iterations">Iterations:</label>
          <select id="middle_test_suite_iterations" className="input-sm" value={this.state.TestSuiteStore.iterationsTs} disabled={this.state.TestSuiteStore.realtimeConnection !== TestSuiteStore.REALTIME_CONNECTION_OPEN || this.state.TestSuiteStore.markup.definition || (TestSuiteStore.EXECUTION_NOT_RUNNING !== this.state.TestSuiteStore.execution)}
            onChange={(e) => {
              this.dispatch(TestSuiteStore, new ActionTestSuiteIterations(Number.parseInt(e.target.value)));
            }}
          >
          {this.getIterationOptions()}
          </select>
        </>
      );
    }
  }
  
  render() {
    const sut = !this.props.sut ? undefined : this.state.SystemUnderTestStore.systemUnderTests.find((sut) => {
      return sut.name === this.props.sut;
    });
    this.repo = sut ? sut.repo : null;
    return (
      <div className="middle_toolbar">
        {this.renderModalDialogAbstractionAdd()}
        <div className="toolbar" role="toolbar" aria-label="...">
          <MiddleFilter sut fut/>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderAddButton()}
            {this.renderRemoveButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderMarkupOpenButton()}
            {this.renderMarkupSaveButton()}
            {this.renderMarkupHelpButton()}
            {this.renderMarkupCancelButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('execution')}
            {this.renderClearButton('execution')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('log')}
            {this.renderClearButton('log')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartButton('sequence-diagram')}
            {this.renderClearButton('sequence-diagram')}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderStartSlowMotionButton()}
            {this.renderStopButton()}
            {this.renderClearAllButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderIterationOptions()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderRealtimeInfo()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderStageButton()}
          </div>
        </div>
      </div>
    );
  }
  
  tabNavigation(tab) {
    if(tab !== this.props.tab) {
      this.context.history(`${this.props.sut}/${this.props.fut}/${this.props.ts}/${tab}`, {replace: true});
    }
  }
}

MiddleTestSuitesToolbar.DEFAULT_MARKUP_SUT = {
  markup: `#### ***Add a new SUT***
  
***Set the SUT name and select the repo in which to put it.***`,
  markupStyle: {
    padding:'13px 8px 0px 11px'
  }
};

MiddleTestSuitesToolbar.DEFAULT_MARKUP_FUT = {
  markup: `#### ***Add a new FUT***
  
***Set the FUT name.***`,
  markupStyle: {
    padding:'13px 0px 0px 18px'
  }
};

MiddleTestSuitesToolbar.DEFAULT_MARKUP_TS = {
  markup: `#### ***Add a Test Suite***
  
***Add Test Suites and Test Cases on your own.***`,
  markupStyle: {
    padding:'5px 8px 0px 3px',
    width:'102%'
  }
};


MiddleTestSuitesToolbar.contextType = RouterContext;
