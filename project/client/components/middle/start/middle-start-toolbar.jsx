
'use strict';

import Button from 'z-abs-complayer-bootstrap-client/client/button';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStartToolbar extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderStageButton() {
    return (
      <Button id="from_start_to_stage" placement="bottom" heading="Goto" content="Stage" aria-label="Stage"
        onClick={(e) => {
          this.context.history(`/stage`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-star-empty" aria-hidden="true" style={{top: -4, left: -4, width: 0, transform: 'scale(0.9)'}}></span>
      </Button>
    );
  }
  
  renderLoginButton() {
    return (
      <Button id="from_start_to_login" placement="bottom" heading="Goto" content="User" aria-label="Login"
        onClick={(e) => {
          this.context.history(`/user`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderDocumentationButton() {
    return (
      <Button id="from_start_to_documentation" heading="Goto" content="Documentation" aria-label="Documentation"
        onClick={(e) => {
          this.context.history(`/documentation`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderEducationButton() {
    return (
      <Button id="from_start_to_education" heading="Goto" content="Education" aria-label="Education"
        onClick={(e) => {
          this.context.history(`/education`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-education" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderWorkshopButton() {
    return (
      <Button id="from_start_to_workshop" heading="Goto" content="Workshop" aria-label="Workshop"
        onClick={(e) => {
          this.context.history(`/workshop`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-leaf" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderVideosButton() {
    return (
      <Button id="from_start_to_video" heading="Goto" content="Videos" aria-label="Videos"
        onClick={(e) => {
          this.context.history(`/videos`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-film" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderPlusServices() {
    return null;
   /* return (
      <Button id="from_start_to_plus_services" heading="Goto" content="Plus Services"
        onClick={(e) => {
          this.context.history('/plus-services', {global: true});
        }}
      >
        <span className="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-cog" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );*/
  }
  
  renderPlusServiceCodeEditor() {
    return null;
    /*return (
      <Button id="from_start_to_plus_services_code_editor" heading="Goto" content="Plus Service - Code Editor"
        onClick={(e) => {
          this.context.history('/code-editor', {global: true});
        }}
      >
        <span className="glyphicon glyphicon-blackboard" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-pencil" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );*/
  }
  
  renderLocalDocumentationButton(repoName) {
    return (
      <Button id={`from_start_to_local_documentation_${repoName}`} heading="Goto" content="Plus Service - Docs" aria-label="Plus Service - Docs"
        onClick={(e) => {
          this.context.history(`/local-documentation-${repoName}`, {global: true});
        }}
      >
        <span className="glyphicon glyphicon-book" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-pencil" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)'}}></span>
      </Button>
    );
  }
  
  renderLocalDocumentationButtons() {
    if(this.serviceExists('z-abs-servicelayer-docs-client')) {
       return this.renderLocalDocumentationButton('Local');
     }
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="toolbar" role="toolbar"  aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderDocumentationButton()}
            {this.renderEducationButton()}
            {this.renderWorkshopButton()}
            {this.renderVideosButton()}
          </div>
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderPlusServices()}
            {this.renderPlusServiceCodeEditor()}
            {this.renderLocalDocumentationButtons()}
          </div>
          <div className="btn-group btn-group-sm pull-right" role="group" aria-label="...">
            {this.renderStageButton()}
            {this.renderLoginButton()}
          </div>
        </div>
      </div>
    );
  }
}


MiddleStartToolbar.contextType = RouterContext;
