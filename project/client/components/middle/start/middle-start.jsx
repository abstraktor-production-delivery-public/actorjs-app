
'use strict';

import MiddleSidebar from '../middle_sidebar';
import MiddleStartToolbar from './middle-start-toolbar';
import MiddleStartView from './middle-start-view';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStart extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Start'
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle middle_start">
        <MiddleStartToolbar />
        <MiddleSidebar />
        <MiddleStartView />
      </div>
    );
  }
}

