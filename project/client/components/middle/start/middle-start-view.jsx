
'use strict';


import { ActionReposAnalyzeLocalReposGet, ActionReposClone, ActionReposVerifyGit } from '../../../actions/action-repos/action-repos';
import ToolVersionsStore from 'z-abs-complayer-toolversions-client/client/stores/tool-versions-store';
import ReposStore from '../../../stores/repos-store';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStartView extends ReactComponentStore {
  constructor(props) {
    super(props, [ToolVersionsStore, ReposStore], {
      useRepoSetup: false,
      localDataRepo: '',
      localContentRepo: '',
      newDataRepo: false,
      newContentRepo: false
    });
    this.inputDataRepoRef = React.createRef();
    this.inputContentRepoRef = React.createRef();
  }
  
  didMount() {
    this.dispatch(ReposStore, new ActionReposAnalyzeLocalReposGet());
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.useRepoSetup, nextState.useRepoSetup)
      || !this.shallowCompare(this.state.localDataRepo, nextState.localDataRepo)
      || !this.shallowCompare(this.state.localContentRepo, nextState.localContentRepo)
      || !this.shallowCompare(this.state.newDataRepo, nextState.newDataRepo)
      || !this.shallowCompare(this.state.newContentRepo, nextState.newContentRepo)
      || !this.shallowCompare(this.state.ToolVersionsStore.tools.git, nextState.ToolVersionsStore.tools.git)
      || !this.shallowCompare(this.state.ReposStore, nextState.ReposStore);
  }
  
  renderInputRepo(type, inputRef, valueState, url, exists) {
    const inputClass = `col-sm-6 input_column ${exists ? 'has-success' : 'has-warning'} has-feedback`;
    const inputId = `middle_start_${type}_repo`;
    const inputSpanId = `${inputId}_status`;
    const feedbackSpanClass = `glyphicon ${exists ? 'glyphicon-ok' : 'glyphicon-warning-sign'} form-control-feedback`;
    const stateObject = {};
    return (
      <div className="form-group">
        <div className="row" style={{textAlign:'right',paddingTop:'12px'}}>
          <div className="col-sm-4 input_column">
            <label htmlFor={inputId} className="control-label">{`${type}, repo url:`}</label>
          </div>
          <div className={inputClass}>
            <input ref={inputRef} type="text" id={inputId} className="form-control input-sm" placeholder="git remote url" value={url ? url : valueState} aria-describedby={inputSpanId} disabled={exists}
              onChange={(e) => {
                if('actorjs-data-local' === type) {
                  this.updateState({localDataRepo: {$set: e.target.value}});
                }
                else if('actorjs-content-local' === type) {
                  this.updateState({localContentRepo: {$set: e.target.value}});
                }
              }}
            />
            <span className={feedbackSpanClass} aria-hidden="true" style={{right:'4px'}}></span>
            <span id={inputSpanId} className="sr-only">(success)</span>
          </div>
        </div>
        <div className="row" style={{textAlign:'right',paddingTop:'12px'}}>
          <div className="col-sm-4 input_column"></div>
          {this.renderRadio(type, exists, true)}
          {this.renderRadio(type, exists, false)}
        </div>
      </div>
    );
  }
  
  renderRadio(type, exists, existingOrNew) {
    if(exists) {
      return null;
    }
    const wantNewOrExisting = 'actorjs-data-local' === type ? this.state.newDataRepo : this.state.newContentRepo;
    const radioId = `radio_${type}`;
    return (
      <>
        <div className="col-sm-2 input_column" style={{textAlign:'left',paddingTop:'5px'}}>
           <input type="radio" id={radioId} style={{position:'relative',top:'1px'}} aria-label="..." checked={existingOrNew ? !wantNewOrExisting : wantNewOrExisting} onChange={(e) => {
             if('actorjs-data-local' === type) {
               this.updateState({newDataRepo: {$set: existingOrNew ? !e.target.checked : e.target.checked}});
             }
             else if('actorjs-content-local' === type) {
               this.updateState({newContentRepo: {$set: existingOrNew ? !e.target.checked : e.target.checked}});
             }
           }}/>
           <label htmlFor={radioId} style={{paddingLeft:'4px'}}>{existingOrNew ? 'Existing Repo' : 'New Repo'}</label>
        </div>
      </>
    );
  }
  
  renderDataRepo(dataExists) {
    return (
      <div style={{textAlign:'left',padding:'0px 8px'}}>
        {this.renderInputRepo('actorjs-data-local', this.inputDataRepoRef, this.state.localDataRepo, this.state.ReposStore.localDataUrl, dataExists)}
      </div>
    );
  }
    
  renderContentRepo(dataExists, contentExists) {
    if(!dataExists) {
      return null;
    }
    if(!contentExists) {
      return (
        <div style={{textAlign:'left',padding:'0px 8px'}}>
          {this.renderInputRepo('actorjs-content-local', this.inputContentRepoRef, this.state.localContentRepo, this.state.ReposStore.localContentUrl, contentExists)}
        </div>
      );
    }
  }
  
  renderHandleRepos() {
    const button = !this.state.ReposStore.actorjsDataLocalExists ? (this.state.newDataRepo ? 'Push' : 'Clone') : (!this.state.ReposStore.actorjsContentLocalExists ? (this.state.newContentRepo ? 'Push' : 'Clone') : '');
    return (
      <>
        <p className="start_welcome">With this wizard, you can connect either to an existing repo or a new, empty repo. Using ssh is preferable to using https. The repo must have its default branch set to main.
The wizard is not guaranteed to work in all setups, especially in the case of older Git versions.</p>
        <p className="start_welcome"><strong>Existing Repo:</strong>&nbsp;Set the repo url to the url of the existing repo. This will overwrite all local  data and content and will replace it with the data/content in the existing repo. If you want to keep your changes, you must make a copy manually through the file system.</p>
        <p className="start_welcome"><strong>New Repo:</strong>&nbsp;Create a new repo and make sure it is 100 % empty. Set the repo url to the url of the new repo. This will make the first commit with auto-generated data plus data you have added so far.</p>
        {this.renderDataRepo(this.state.ReposStore.actorjsDataLocalExists)}
        {this.renderContentRepo(this.state.ReposStore.actorjsDataLocalExists, this.state.ReposStore.actorjsContentLocalExists)}
        <div className="middle_start_clone">
          <Popover placement="bottom" heading={button} content="local repo" shortcut="Enter" style={{display:'inline-block'}}>
            <button id="middle_start_clone_button" type="button" className="btn btn-primary" disabled={(!this.state.ReposStore.localDataUrl && !this.state.localDataRepo) || this.state.ReposStore.progress.clone}
              onClick={(e) => {
                if(!this.state.ReposStore.actorjsDataLocalExists) {
                  this.dispatch(ReposStore, new ActionReposClone('actorjs-data-local', this.state.localDataRepo, this.state.newDataRepo));
                }
                else if(!this.state.ReposStore.actorjsContentLocalExists) {
                  this.dispatch(ReposStore, new ActionReposClone('actorjs-content-local', this.state.localContentRepo ? this.state.localContentRepo : this.state.ReposStore.localContentUrl, this.state.newContentRepo));
                }
              }}
            >{button}</button>
          </Popover>
        </div>
      </>
    );
  }
  
  renderError() {
    if(this.state.ReposStore.error.error) {
      const errorDivStyle = {
        float: 'left',
        textAlign: 'left',
        marginLeft: '-10px'
      };
      const labelStyle = {
        position: 'relative'
      };
      return (
        <>
          <div className="col-sm-4 input_column" />
          <div className="col-sm-6 input_column" style={errorDivStyle}>
            <label className="text-danger control-label" style={labelStyle}>{this.state.ReposStore.error.text}</label>
          </div>
        </>
      );
    }
    else {
      return null;
    }
  }
  
  renderVerifyGitConfigValue(gitConfig, index) {
    const inputClass = gitConfig.expected ? 'alert-success' : 'alert-warning';
    const style= {
      textAlign: 'left',
      padding: '0px 0px 0px 5px',
      marginBottom: '5px',
      margin: '0px 5px 0px 5px'
    };
    return (
      <div key={`git-config-value-${index}`} className="row" style={{paddingTop:'0px'}}>
        <div className="col-sm-3 input_column" role="alert" style={{textAlign:'right'}}>
          <label className="control-label">{gitConfig.name}</label>
        </div>
        <div className={`col-sm-4 input_column alert ${inputClass}`} role="alert" style={style}>
          <p className="control-label">{gitConfig.value}</p>
        </div>
        <div className="col-sm-3 input_column alert alert-info" role="alert" style={style}>
          <p className="control-label">{gitConfig.expectedValue}</p>
        </div>
      </div>
    );
  }
  
  renderVerifyGitConfigValues() {
    if(0 === this.state.ReposStore.gitConfigs.length) {
      return null;
    }
    const rows = this.state.ReposStore.gitConfigs.map((gitConfig, index) => {
      return this.renderVerifyGitConfigValue(gitConfig, index);
    });
    return (
      <>
        <div key={`git-config-header`} className="row" style={{paddingTop:'0px'}}>
          <div className="col-sm-3 input_column" role="alert" style={{textAlign:'right'}}>
            <label className="control-label">name</label>
          </div>
          <div className="col-sm-4 input_column" role="alert" style={{textAlign:'left',marginLeft:'4px'}}>
            <label className="control-label">value</label>
          </div>
          <div className="col-sm-3 input_column" role="alert" style={{textAlign:'left',marginLeft:'8px'}}>
            <label className="control-label">expected value</label>
          </div>
        </div>
        {rows}
      </>
    );
  }
  
  renderVerifyGitConfigs() {
    return (
      <div>
        <h3 className="start_welcome"><strong>Verify git config</strong></h3>
        <p className="start_welcome">ActorJs needs certain git configs to work properly. To determine whether you have set these configs, click the Verify button to display recommended settings (you decide whether you want to use these settings).</p>
        {this.renderVerifyGitConfigValues()}
        <div className="middle_start_clone">
          <Popover placement="bottom" heading="Verify" content="git config" shortcut="Enter" style={{display:'inline-block'}}>
            <button id="middle_start_clone_button" type="button" className="btn btn-primary"
              onClick={(e) => {
                this.dispatch(ReposStore, new ActionReposVerifyGit());
              }}
            >Verify</button>
          </Popover>
        </div>
      </div>
    );
  }
  
  renderRepoSetup() {
    return (
      <>
        <h3 className="start_welcome"><strong>Set up your private repos</strong></h3>
        <p className="start_welcome">When you use ActorJs, your work will be placed in your own private git repos. It is a good idea to set up the repos from the start, so you do not have to do this manually later. Read more about the ActorJs repo structure in&#160; 
          <a href="/documentation/abstractions/repo">Repo Abstraction</a>.
        </p>
        <p className="start_welcome">There are two mandatory repos:</p>
        <ul className="repo_list">
          <li><strong>actorjs-data-local:</strong> actorjs-data-local: A text repo to store Repo configurations, Systems Under Test configurations, Test Suites, Test Cases, Stacks, Actors, Test Data, and Content configurations.</li>
          <li><strong>actorjs-content-local:</strong> A binary repo to store content used by Actors in Test Cases.</li>
        </ul>
      </>
    );
  }
  
  renderWithGit() {
    if(4 !== this.state.ToolVersionsStore.tools.git.status) {
      return (
        <>
          {this.renderVerifyGitConfigs()}
          {this.renderRepoSetup()}
          {this.renderHandleRepos()}
          {this.renderError()}
        </>
      );
    }
  }
  
  renderWithoutGit() {
    if(4 === this.state.ToolVersionsStore.tools.git.status) {
      return (
        <>
          <h3 className="start_welcome"><strong>Install git</strong></h3>
          <p className="start_welcome">When you use ActorJs, your work will be placed in your own, private git repos. It is a good idea to install git and set up the repos from the start, so you don't have to do this manually later. Read more about the ActorJs repo structure in&#160; 
          <a href="/documentation/abstractions/repo">Repo Abstraction</a>.
          </p>
        </>
      );
    }
  }
  
  renderRepoWizard() {
    let key = 0;
    return (
      <div className="start_info">
        <div className="start_menu_outer">
          <div key={++key} className="start_menu_inner">
            <h3 className="start_info_heading">Repo wizard</h3>
            <p className="start_info_text">Getting help to set up your own repos.</p>
            <button type="button" className="btn btn-primary start_info_button"
              onClick={(e) => {
                this.updateState({useRepoSetup: {$set: true}});
              }}
            >Wizard</button>
          </div>
        </div>      
      </div>
    );
  }
  
  renderContent() {
    if(this.state.ReposStore.handleRepos || this.state.ReposStore.error.error) {
      if(this.state.useRepoSetup) {
        return (
          <>
            <div className="middle_start_repo_setup_outer">
              <div className="middle_start_repo_setup_inner">
                <h1 className="start_welcome"><strong>Welcome to ActorJs</strong></h1>
                  {this.renderWithGit()}
                  {this.renderWithoutGit()}
              </div>
            </div>
          </>
        );
      }
      else {
        return (
          <>
            {this.renderRepoWizard()}
          </>
        );
      }
    }
    else {
      return null;
    }
  }
  
  render() {
    return (
      <div className="middle_view start_menu">
        <img className="start_page" src="/abs-images/svg/AbstraktorLogo.svg" alt="Abstraktor Logo"></img>
        {this.renderContent()}
      </div>
    );
  }
}
