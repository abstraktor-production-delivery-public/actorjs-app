
'use strict';

import React from 'react';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import FilterStore from '../../stores/filter-store';
import SystemUnderTestStore from '../../stores/system-under-test-store';
import { ActionSutFilter } from '../../actions/action-sut-fut-filter';


export default class MiddleFilter extends ReactComponentStore {
  constructor(props) {
    super(props, [FilterStore, SystemUnderTestStore]);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.sut, nextProps.sut)
      || !this.shallowCompare(this.props.fut, nextProps.fut)
      || !this.shallowCompare(this.props.disabled, nextProps.disabled)
      || !this.shallowCompare(this.state.FilterStore, nextState.FilterStore)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests);
  }  
  
  setSut(sut) {
    this.dispatch(FilterStore, new ActionSutFilter(sut));
  }
  
  /*renderSutMenuItem() {
    let systemUnderTestRows = [];
    systemUnderTestRows = this.state.SystemUnderTestStore.systemUnderTests.map((sut, i) => {
      if(!this.state.FilterStore.hide || (this.state.FilterStore.hide && sut.status !== 'hidden')) {
        return (
          <MenuItem key={i + 1} eventKey={sut.name}
            onSelect={(key, e) => {
              this.setSut(key);
            }}
          >{sut.name}</MenuItem>
        );
      }
    });
    return systemUnderTestRows;
  }*/
  
  renderSutDropdownButton() {
    /*if(this.props.sut) {
      return (
        <OverlayTrigger placement="bottom" overlay={this.getTooltip('Filter:', 'System Under Test')}>
          <SplitButton id="sut-filter" bsStyle="default" bsSize="small" title={`sut: ${this.state.FilterStore.sut}`}>
            <MenuItem key="0" eventKey="*"
              onSelect={(key, e) => {
                this.setSut(key);
              }}
            >*</MenuItem>
            <MenuItem divider />
            {this.renderSutMenuItem()}
          </SplitButton>
        </OverlayTrigger>
      );
    }*/
  }
  
  renderFutDropdownButton() {
    /*if(this.props.fut && this.state.FilterStore.sut !== '*') {
      return (
        <OverlayTrigger placement="bottom" overlay={this.getTooltip('Filter:', 'Function Under Test')}>
          <SplitButton id="fut-filter" bsStyle="default" bsSize="small" title={`fut: ${this.state.FilterStore.fut}`}>
            <MenuItem eventKey="1">*</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey="2">Actor</MenuItem>
            <MenuItem eventKey="4">Demo</MenuItem>
          </SplitButton>
        </OverlayTrigger>
      );
    }*/
  }
  
  render() {
    if(!this.props.disabled) {
      return (
        <div className="btn-groupbtn-group-sm" role="group" aria-label="...">
          {this.renderSutDropdownButton()}
          {this.renderFutDropdownButton()}
        </div>
      );
    }
    else {
      return null;
    }
  }
}
