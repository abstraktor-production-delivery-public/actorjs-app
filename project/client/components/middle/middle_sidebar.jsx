
'use strict';

import PluginsStore from '../../stores/plugins-store';
import PlusServicesStore from '../../stores/plus-services-store';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import React from 'react';


export default class MiddleSidebar extends ReactComponentStore {
  constructor(props) {
    super(props, [PluginsStore, PlusServicesStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.PluginsStore, nextState.PluginsStore)
      || !this.shallowCompare(this.state.PlusServicesStore.plusServicesSubscriptions, nextState.PlusServicesStore.plusServicesSubscriptions);
  }
  
  renderPlugin(plugin, index) {
    if(0 === index % 2) {
      return (
        <div key={plugin.guid} style={{float:'left',display:'inline-block'}}>
          <Link className="middle_sidebar" global href={`/plugins${plugin.path}`}>{plugin.name}</Link>
        </div>
      );
    }
    else {
      if(-1 !== index) {
        return (
          <div key={plugin.guid} style={{marginLeft:'85px'}}>
            <Link className="middle_sidebar" global href={`/plugins${plugin.path}`}>{plugin.name}</Link>
          </div>
        );
      }
      else {
        return (
          <div key="4208ec27-682f-4796-acfe-85d4ffad7f22" style={{marginLeft:'85px'}}>&nbsp;</div>
        );
      }
    }
  }
  
  renderPlugins() {
    const pluginsStore = this.state.PluginsStore;
    if(pluginsStore.error || !pluginsStore.loaded) {
      return null;
    }
    else {
      const plugins = [];
      let index = -1;
      pluginsStore.plugins.forEach((plugin) => {
        plugins.push(this.renderPlugin(plugin, ++index));
      });
      if(0 !== pluginsStore.plugins.length % 2) {
        plugins.push(this.renderPlugin(null, -1));
      }
      return (
        <div>
          <h4 className="middle_sidebar">Plugins</h4>
          {plugins}
        </div>
      );
    }
  }
  
  renderPlusServices() {
    if(0 === this.state.PlusServicesStore.plusServicesSubscriptions.length) {
      return null;
    }
    else {
      return (
        <div>
          <h4 className="middle_sidebar">Plus Services</h4>
          <div style={{float:'left',display:'inline-block'}}>
            <Link className="middle_sidebar" global href="/code-editor">Chat</Link>
          </div>
          <div style={{marginLeft:'85px'}}>
            <Link className="middle_sidebar" global href="/code-editor">Versioning</Link>
          </div>
        </div>
      );
    }
  }
  
  render() {
    return (
      <div className="middle_sidebar">
        <div className="panel panel-default middle_sidebar_panel">
          <div className="panel-body middle_sidebar_panel_body">
            <div>
              <h4 className="middle_sidebar">Core tools</h4>
              <div style={{position:'relative',float:'left'}}>
                <Link id="middle_repo_link" className="middle_sidebar" href="/repos" replaceStay>Repos</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link id="middle_sut_link" className="middle_sidebar" href="/systems-under-test" replaceStay>SUTs</Link>
              </div>
              <div style={{position:'relative',float:'left'}}>
                <Link id="middle_ts_link" className="middle_sidebar" href="/test-suites" replaceStay>Test Suites</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link id="middle_tc_link" className="middle_sidebar" href="/test-cases" replaceStay>Test Cases</Link>
              </div>
              <div style={{float:'left',display:'inline-block'}}>
                <Link id="middle_stacks_link" className="middle_sidebar" href="/stack-editor" replaceStay>Stacks</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link id="middle_actors_link" className="middle_sidebar" href="/actor-editor" replaceStay>Actors</Link>
              </div>
            </div>
            <div>
              <h4 className="middle_sidebar">Address tool</h4>
              <table>
                <tbody>
                  <tr>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/src" replaceStay>Src</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/dst" replaceStay>Dst</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/srv" replaceStay>Srv</Link>
                    </td>
                  </tr>
                  <tr>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/client-address" replaceStay>Client</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/sut-address" replaceStay>Sut</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/server-address" replaceStay>Server</Link>
                    </td>
                  </tr>
                  <tr>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/client-interface" replaceStay>iClient</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/sut-interface" replaceStay>iSut</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/server-interface" replaceStay>iServer</Link>
                    </td>
                  </tr>
                  <tr>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/network-interface" replaceStay>iNetwork</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/ports" replaceStay>Ports</Link>
                    </td>
                    <td className="sidbar_3_col">
                      <Link className="middle_sidebar" href="/addresses/dns" replaceStay>Dns</Link>
                    </td>
                    <td>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div>
              <h4 className="middle_sidebar">Test Data tool</h4>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/test-data/general" replaceStay>General</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/test-data/system" replaceStay>System</Link>
              </div>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/test-data/output" replaceStay>Output</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/test-data/environment" replaceStay>Environment</Link>
              </div>
            </div>
            <div>
              <h4 className="middle_sidebar">Content tool</h4>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/content/text" replaceStay>Text</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/content/documents" replaceStay>Documents</Link>
              </div>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/content/video" replaceStay>Video</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/content/audio" replaceStay>Audio</Link>
              </div>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/content/image" replaceStay>Image</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/content/other" replaceStay>Other</Link>
              </div>
            </div>
            {/*<div>
              <h4 className="middle_sidebar">Load tool</h4>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/load/local" replaceStay>Local</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/load/distributed" replaceStay>Distributed</Link>
                <span className="glyphicon glyphicon-plus-sign middle_plus_service" aria-hidden="true"></span>
              </div>
            </div> 
            <div>
              <h4 className="middle_sidebar">Visualization tools</h4>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/visualization/log" replaceStay>Log</Link>
                <span className="glyphicon glyphicon-plus-sign middle_plus_service" aria-hidden="true"></span>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/visualization/seq-dia" replaceStay>Seq Dia</Link>
                <span className="glyphicon glyphicon-plus-sign middle_plus_service" aria-hidden="true"></span>
              </div>
            </div>*/}
            {this.renderPlugins()}
            {this.renderPlusServices()}
            <div>
              <h4 className="middle_sidebar">Support</h4>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/support/feedback" replaceStay>Feedback</Link>
              </div>
              <div style={{marginLeft:'85px'}}>
                <Link className="middle_sidebar" href="/support/issue" replaceStay>Issue</Link>
              </div>
              <div style={{float:'left',display:'inline-block'}}>
                <Link className="middle_sidebar" href="/support/question" replaceStay>Question</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
