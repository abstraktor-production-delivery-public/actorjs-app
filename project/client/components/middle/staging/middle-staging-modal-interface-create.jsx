
'use strict';

import Helper from './helper';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStagingModalInterfaceCreate extends ReactComponentBase {
  constructor(props) {
    super(props, {
      chosenVirtualInterfaces: new Map(),
      chosenNetworks: new Map(),
      networks: [],
      virtualNetworks: [],
      password: ''
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.show, nextProps.show)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompareArrayValues(this.props.osNetworks, nextProps.osNetworks)
      || !this.shallowCompare(this.props.platform, nextProps.platform)
      || !this.shallowCompare(this.props.chosenAddresses, nextProps.chosenAddresses)
      || !this.shallowCompareMapValues(this.state.chosenVirtualInterfaces, nextState.chosenVirtualInterfaces)
      || !this.shallowCompareMapValues(this.state.chosenNetworks, nextState.chosenNetworks)
      || !this.shallowCompareArrayValues(this.state.networks, nextState.networks)
      || !this.shallowCompareArrayValues(this.state.virtualNetworks, nextState.virtualNetworks)
      || !this.shallowCompare(this.state.password, nextState.password);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.props.osNetworks !== prevProps.osNetworks) {
      const networks = [];
      const virtualNetworks = [];
      if('linux' === this.props.platform) {
        this.props.osNetworks.forEach((osNetwork, index) => {
          if(!osNetwork.internal) {
            const colon = osNetwork.name.indexOf(':');
            if(-1 === colon) {
              networks.push(osNetwork);
            }
            else {
              if(osNetwork.name.substring(colon + 1).startsWith('aj')) {
                virtualNetworks.push(osNetwork);
              }
            }
          }
        });
      }
      else if('win32' === this.props.platform) {
        this.props.osNetworks.forEach((osNetwork) => {
          if(!osNetwork.internal) {
            networks.push(osNetwork);
          }
        });
      }
      this.updateState({networks: {$set: networks}});
      virtualNetworks.sort();
      this.updateState({virtualNetworks: {$set: virtualNetworks}});
      this.updateState({chosenNetworks: (chosenNetworks) => {
        chosenNetworks.clear();
        if(undefined !== this.props.chosenAddresses.missingAddresses) {
          this.props.chosenAddresses.missingAddresses.forEach((virtualInterface) => {
            chosenNetworks.set(virtualInterface.interfaceName, networks[0]);
          });
        }
      }});
    }
  }
  
  renderErrorMessage() {
    
  }
  
  renderAddressHeadings() {
    return (
      <tr key="modal_dialog_create_interface_address_headings">
        <th>#</th>
        <th>Name</th>
        <th>Address</th>
        <th>Netmask</th>
        <th>Family</th>
        <th>Subnet</th>
        <th>Interface</th>
        <th>Create</th>
      </tr>
    );
  }
  
  renderNetworkOptions() {
    return this.state.networks.map((network, index) => {
      return (
        <option key={index} value={network.name}>{network.name}</option>
      );
    });
  }
  
  renderAddress(virtualInterface, index) {
    const value = this.state.chosenNetworks.get(virtualInterface.interfaceName);
    const createEnabled = virtualInterface.validAddress && virtualInterface.validNetmask && virtualInterface.validFamily && virtualInterface.validSubnet;
    return (
      <tr key={`${virtualInterface.interfaceName}_${virtualInterface.address}`}>
        <td className="modal_interface">{index + 1}</td>
        <td className="modal_interface">{virtualInterface.interfaceName}</td>
        <td className={'modal_interface ' + (virtualInterface.validAddress ? 'login_valid_success' : 'login_valid_failure')}>{virtualInterface.address}</td>
        <td className={'modal_interface ' + (virtualInterface.validNetmask ? 'login_valid_success' : 'login_valid_failure')}>{virtualInterface.netmask}</td>
        <td className={'modal_interface ' + (virtualInterface.validFamily ? 'login_valid_success' : 'login_valid_failure')}>{virtualInterface.family}</td>
        <td className={'modal_interface ' + (virtualInterface.validSubnet ? 'login_valid_success' : 'login_valid_failure')}>{virtualInterface.subnet}</td>
        <td className="modal_interface">
          <select className="form-control input-sm" id="modal_dialog_create_interface_networks" value={value} defaultValue={value}
            onChange={(e) => {
              this.updateState({chosenNetworks: (chosenNetworks) => {
                chosenNetworks.set(virtualInterface.interfaceName, e.target.value);
              }});
            }}
          >
            {this.renderNetworkOptions()}
          </select>
        </td>
        <td className="modal_interface">
          <input type="checkbox" id="modal_dialog_create_interface_checkbox" aria-label="..." autoComplete="off" disabled={!createEnabled} checked={undefined !== this.state.chosenVirtualInterfaces.get(virtualInterface.interfaceName)} style={{marginTop:'9px', marginLeft:'14px'}}
            onChange={(e) => {
              this.updateState({chosenVirtualInterfaces: (chosenVirtualInterfaces) => {
                if(e.currentTarget.checked) {
                  chosenVirtualInterfaces.set(virtualInterface.interfaceName, virtualInterface);
                }
                else {
                  chosenVirtualInterfaces.delete(virtualInterface.interfaceName);
                }
              }});
            }}
          />
        </td>
      </tr>
    ); 
  }
  
  renderAddresses(virtualInterfaces) {
    if(undefined !== virtualInterfaces) {
      const addedAddresses = new Set();
      return virtualInterfaces.map((virtualInterface, index) => {
        if(!addedAddresses.has(virtualInterface.address) && !virtualInterface.multicast) {
          addedAddresses.add(virtualInterface.address);
          return this.renderAddress(virtualInterface, index);
        }
      });
    }
    return null;
  }
  
  renderPassword() {
    if('win32' === this.props.platform) {
      return null;
    }
    else {
      return (
        <div className="row" style={{textAlign: 'right'}}>
          <div className="col-sm-2">
            <label htmlFor="modal_dialog_create_interface_password" className="control-label">Password</label>
          </div>
          <div className="col-sm-9">
            <input type="new-password" id="modal_dialog_create_interface_password" className="form-control input-sm" placeholder="modal_dialog_create_interface_password" value={this.state.password}
              onChange={(e) => {
                this.updateState({password: {$set: e.target.value}});
              }}
              />
          </div>
        </div>
      );
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    return (
      <Modal className="modal_dialog" aria-labelledby="contained-modal-title-sm" show={this.props.show}
        onHide={(e) => {
          this.props.onClose && this.props.onClose();
          this.updateState({chosenVirtualInterfaces: {$set: new Map()}});
          this.updateState({chosenNetworks: {$set: new Map()}});
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <table className="table table-bordered table-striped table-condensed table-hover">
            <thead>
              {this.renderAddressHeadings()}
            </thead>
            <tbody>
              {this.renderAddresses(this.props.chosenAddresses.missingAddresses)}
            </tbody>
          </table>
          {this.renderPassword()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button id="interface_create_create" type="button" className="btn btn-primary" disabled={0 === this.state.chosenVirtualInterfaces.size}
            onClick={(e) => {
              const interfaceParameters = [];
              let virtualNetworkName;
              this.state.chosenVirtualInterfaces.forEach((virtualInterface) => {
                if('linux' === this.props.platform) {
                  virtualNetworkName = Helper.calculateVirtualNetworkName(virtualNetworkName, this.state.virtualNetworks);
                }
                const network = this.state.chosenNetworks.get(virtualInterface.interfaceName);
                interfaceParameters.push({
                  address: virtualInterface.address,
                  netmask: virtualInterface.netmask,
                  network: network.name,
                  dhcpEnabled: network.dhcpEnabled,
                  virtualNetwork: virtualNetworkName
                });
              });
              this.props.onCreateInterfaces && this.props.onCreateInterfaces(interfaceParameters, this.state.password);
            }}
          >Create</button>
          <button id="interface_create_close" type="button" className="btn btn-default"
            onClick={(e) => {
              this.props.onClose && this.props.onClose();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
