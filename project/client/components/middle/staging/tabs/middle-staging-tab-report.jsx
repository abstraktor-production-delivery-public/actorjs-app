
'use strict';

import LoginStore from '../../../../stores/login-store';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStagingTabReport extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.loginReport, nextState.LoginStore.loginReport);
  }
  
  renderLog(log, index) {
    return (
      <tr key={index}>
        <td>{index + 1}</td>
        <td>{log.reportType}</td>
        <td>{log.action}</td>
        <td>{log.status}</td>
        <td>{log.log}</td>
      </tr>
    );
  }
  
  renderLogs() {
    const loginReport = this.state.LoginStore.loginReport;
    const rows = loginReport.map((log, index) => {
      return this.renderLog(log, index);
    });
    return (
      <>
        {rows}
      </>
    );
  }
  
  render() {
    return (
      <>
        <table className="table table-bordered table-striped table-condensed table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>type</th>
              <th>action</th>
              <th>status</th>
              <th>log</th>
            </tr>
          </thead>
          <tbody>
            {this.renderLogs()}
          </tbody>
        </table>
      </>
    );
  }
}
