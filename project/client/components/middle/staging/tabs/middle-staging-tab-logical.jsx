
'use strict';

import LoginStore from '../../../../stores/login-store';
import ComponentImageLab from 'z-abs-complayer-markup-client/client/react-components/markup/component-image-lab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStagingTabLogical extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.virtualNetworks, nextState.LoginStore.virtualNetworks)
      || !this.shallowCompare(this.state.LoginStore.pingResults, nextState.LoginStore.pingResults);
  }
  
  renderLoginReport() {
    return (
      <>
        <span className="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
      </>
    );
  }
  
  render() {
    return (
      <>
        <ComponentImageLab width="98%" height="98%" virtualNetworks={this.state.LoginStore.virtualNetworks} pingResults={this.state.LoginStore.pingResults} />
        {this.renderLoginReport()}
      </>
    );
  }
}
