
'use strict';

import LoginStore from '../../../../stores/login-store';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import React from 'react';


export default class MiddleStagingTabSetup extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore]);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.osNetworks, nextState.LoginStore.osNetworks)
      || !this.shallowCompare(this.state.LoginStore.cpus, nextState.LoginStore.cpus);
  }
 
  
  renderOs() {
    return this.state.LoginStore.cpus.map((cpu, index) => {
      return (
        <div key={index} className="test_machine_os_network_interface">
          {`${cpu.model}`}
        </div>
      );
    });
  }
  
  renderInterfaces(addresses) {
    return addresses.map((interface_, connectionIndex) => {
      return (
        <div key={interface_.address} className="test_machine_os_network_interface">
          {`${interface_.family} - ${interface_.internal ? 'internal' : 'global'} - ${interface_.address}`}
        </div>
      );
    });
  }
  
  renderNetwork(osNetwork) {
    return (
      <React.Fragment key={osNetwork.name}>
        <div className="test_machine_os_network_name">
          {osNetwork.name}
        </div>
        {this.renderInterfaces(osNetwork.addresses)}
      </React.Fragment>
    );
  }
  
  renderNetworks() {
    const networks = [];
    this.state.LoginStore.osNetworks.forEach((osNetwork) => {
      networks.push(this.renderNetwork(osNetwork));
    });
    return networks;
  }
  
  render() {
    return (
      <>
        <div className="test_machine_os_networks">
          {this.renderNetworks()}
        </div>
        <div className="test_machine_cpus">
          {this.renderOs()}
        </div>
      </>
    );
  }
}

