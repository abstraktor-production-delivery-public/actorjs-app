
'use strict';

import LoginStore from '../../../../stores/login-store';
import { ActionLoginReposUpdate, ActionLoginReposClear} from '../../../../actions/action-login/action-login';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStagingTabRepos extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderReposButton() {
    return (
      <Button placement="bottom" heading="Repos" content="Run"
        onClick={(e) => {
          this.dispatch(LoginStore, new ActionLoginReposUpdate());
        }}
      >
        <span className="glyphicon glyphicon-eye-open test_case_debug" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderReposClearButton() {
    return (
      <Button placement="bottom" heading="Repos" content="Clear"
        onClick={(e) => {
          this.dispatch(LoginStore, new ActionLoginReposClear());
        }}
      >
        <span className="glyphicon glyphicon-erase test_case_debug" aria-hidden="true"></span>
      </Button>
    )
  }
  
  renderReposTable() {
    const repoRows = [];//this.state.LoginStore.repo.repos;
 //   const dependencyRows = dependencies.map((dependency, index) => {
 //     return this.renderDependenciesRows(dependency, index);
 //   });
    return (
      <table className="test_case_table">
        <thead className="">
          <tr>
            <th className="login_dependecies_column">#</th>
            <th className="login_dependecies_column">Name</th>
            <th className="login_dependecies_column">Type</th>
            <th className="login_dependecies_column">Changed</th>
            <th className="login_dependecies_column">New</th>
          </tr>
        </thead>  
        <tbody className="">
          {repoRows}
        </tbody>
      </table>
    );
  }
  
  renderRepos() {
    return (
      <div>
        <h3>ActorJs - Repos</h3>
        {this.renderReposTable()}
      </div>
    );
  }
  
  render() {
    return (
      <div>
        <div className="analyze_panel">
          <div className="btn-toolbar" role="toolbar" aria-label="...">
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {this.renderReposButton()}
              {this.renderReposClearButton()}
            </div>
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {/*this.renderFilterParentButton()*/}
              {/*this.renderFilterDependenciesButton()*/}
            </div>
            <div className="btn-group btn-group-xs" role="group" aria-label="...">
              {/*this.renderDependenciesTime()*/}
            </div>
          </div>
        </div>
        <div className="test_case_tab_view_with_toolbar">
          {this.renderRepos()}
        </div>
      </div>
    );
  }
}
