
'use strict';

import MiddleStagingModalInterfaceCreate from './middle-staging-modal-interface-create';
import MiddleStagingModalInterfaceDelete from './middle-staging-modal-interface-delete';
import LoginStore from '../../../stores/login-store';
import ActorDefault from '../../../actor-default';
import SystemUnderTestStore from '../../../stores/system-under-test-store';
import { ActionLoginUpdate, ActionLoginPing, ActionLoginCreateInterfaces, ActionLoginDeleteInterfaces, ActionLoginButtonsUpdate  } from '../../../actions/action-login/action-login';
import ToolVersionsStore from 'z-abs-complayer-toolversions-client/client/stores/tool-versions-store';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import React from 'react';


export default class MiddleStagingToolbar extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore, ToolVersionsStore, SystemUnderTestStore], {
      labId: '',
      userId: '',
      systemUnderTest: '',
      systemUnderTestInstance: '',
      systemUnderTestInstances: [],
      showModalInterfaceCreate: false,
      showModalInterfaceDelete: false,
      loggedIn: false
    });
    this.state.labId = this.state.LoginStore.login.labId;
    this.state.userId = this.state.LoginStore.login.userId;
    this.state.systemUnderTest = this.state.LoginStore.login.systemUnderTest;
    this.state.systemUnderTestInstance = this.state.LoginStore.login.systemUnderTestInstance;
    const foundSut = this._getSut(this.state.systemUnderTest);
    this.state.systemUnderTestInstances = foundSut ? foundSut.instances : [];
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login)
      || !this.shallowCompare(this.state.LoginStore.buttons, nextState.LoginStore.buttons)
      || !this.shallowCompare(this.state.ToolVersionsStore.devEnvironment, nextState.ToolVersionsStore.devEnvironment)
      || !this.shallowCompareArrayValues(this.state.LoginStore.osNetworks, nextState.LoginStore.osNetworks)
      || !this.shallowCompare(this.state.LoginStore.loggedIn, nextState.LoginStore.loggedIn)
      || !this.shallowCompare(this.state.LoginStore.working, nextState.LoginStore.working)
      || !this.deepCompare(this.state.SystemUnderTestStore.systemUnderTests, nextState.SystemUnderTestStore.systemUnderTests)
      || !this.shallowCompare(this.state.labId, nextState.labId)
      || !this.shallowCompare(this.state.userId, nextState.userId)
      || !this.shallowCompare(this.state.systemUnderTest, nextState.systemUnderTest)
      || !this.shallowCompare(this.state.systemUnderTestInstance, nextState.systemUnderTestInstance)
      || !this.shallowCompare(this.state.systemUnderTestInstances, nextState.systemUnderTestInstances)
      || !this.shallowCompare(this.state.showModalInterfaceCreate, nextState.showModalInterfaceCreate)
      || !this.shallowCompare(this.state.showModalInterfaceDelete, nextState.showModalInterfaceDelete)
      || !this.shallowCompare(this.state.loggedIn, nextState.loggedIn);
  }
  
  didUpdate(prevProps, prevState) {
    if(this.state.LoginStore.login.labId !== prevState.LoginStore.login.labId || this.state.LoginStore.login.userId !== prevState.LoginStore.login.userId || this.state.LoginStore.login.systemUnderTest !== prevState.LoginStore.login.systemUnderTest || this.state.LoginStore.login.systemUnderTestInstance !== prevState.LoginStore.login.systemUnderTestInstance) {
      this.updateState({labId: {$set: this.state.LoginStore.login.labId}});
      this.updateState({userId: {$set: this.state.LoginStore.login.userId}});
      this.updateState({systemUnderTest: {$set: this.state.LoginStore.login.systemUnderTest}});
      this.updateState({systemUnderTestInstance: {$set: this.state.LoginStore.login.systemUnderTestInstance}});
      const foundSut = this._getSut(this.state.LoginStore.login.systemUnderTest);
      this.updateState({systemUnderTestInstances: {$set: foundSut ? foundSut.instances : []}});
    }
  }
  
  getSystemUnderTestOptions() {
    let key = 0;
    return this.state.SystemUnderTestStore.systemUnderTests.map((systemUnderTest) => {
      if(SystemUnderTestStore.VISIBLE() === systemUnderTest.status || SystemUnderTestStore.STATIC() === systemUnderTest.status) {
        return (<option value={systemUnderTest.name} key={key++}>{systemUnderTest.name}</option>);
      }
    });
  }
  
  renderForceLocalHostButton() {
    return (
      <Button id="login_force_local" active={this.state.LoginStore.buttons.toolbar.forceLocalhost} placement="bottom" heading="Generated addresses as" content="localhost"
        onClick={(e) => {
          this.dispatch(LoginStore, new ActionLoginButtonsUpdate('toolbar', 'forceLocalhost', !this.state.LoginStore.buttons.toolbar.forceLocalhost));
        }}
      >
        <span className="glyphicon glyphicon-globe" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderPingButton() {
    const disabled = this.state.LoginStore.working || !this.state.LoginStore.loggedIn || this.state.LoginStore.loginDataChanged;
    return (
      <Button id="login_ping" placement="bottom" heading="Ping" content="Interfaces" disabled={disabled}
        onClick={(e) => {
          this.dispatch(LoginStore, new ActionLoginPing());
        }}
      >
        <span className="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderCreateVirtualNetworksButton() {
    const disabled = this.state.LoginStore.working || !this.state.LoginStore.loggedIn || this.state.LoginStore.loginDataChanged;
    return (
      <Button id="login_create_network" placement="bottom" heading="Create" content="Static Ip-addresses" disabled={disabled}
        onClick={(e) => {
          this.updateState({showModalInterfaceCreate: {$set: true}});
        }}
      >
        <span className="glyphicon glyphicon-save" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-file" aria-hidden="true" style={{top: 8, left: 0, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderDeleteVirtualNetworksButton() {
    const disabled = this.state.LoginStore.working || !this.state.LoginStore.loggedIn || this.state.LoginStore.loginDataChanged;
    return (
      <Button id="login_delete_network" placement="bottom" heading="Delete" content="Static Ip-addresses" disabled={disabled}
        onClick={(e) => {
          this.updateState({showModalInterfaceDelete: {$set: true}});
        }}
      >
        <span className="glyphicon glyphicon-save" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-trash" aria-hidden="true" style={{top: 8, left: 0, width: 0, transform: 'scale(0.6)'}}></span>
      </Button>
    );
  }
  
  renderModalDialogInterfaceCreate() {
    return (
      <MiddleStagingModalInterfaceCreate heading="Create Virtual Interfaces" result={'result'} show={this.state.showModalInterfaceCreate} chosenAddresses={this.state.LoginStore.chosenAddresses} osNetworks={this.state.LoginStore.osNetworks} platform={this.state.ToolVersionsStore.devEnvironment.platform}
        onCreateInterfaces={(interfaceParameters, password) => {
          this.dispatch(LoginStore, new ActionLoginCreateInterfaces(interfaceParameters, password));
          this.updateState({showModalInterfaceCreate: {$set: false}});
          this.updateState({loggedIn: {$set: false}});
        }}
        onClose={() => {
          this.updateState({showModalInterfaceCreate: {$set: false}});
        }}
      />
    );
  }
  
  renderModalDialogInterfaceDelete() {
    return (
      <MiddleStagingModalInterfaceDelete heading="Delete Virtual Interfaces" result={'result'} show={this.state.showModalInterfaceDelete} chosenAddresses={this.state.LoginStore.chosenAddresses} osNetworks={this.state.LoginStore.osNetworks} platform={this.state.ToolVersionsStore.devEnvironment.platform}
        onDeleteInterfaces={(interfaceParameters, password) => {
          this.dispatch(LoginStore, new ActionLoginDeleteInterfaces(interfaceParameters, password));
          this.updateState({showModalInterfaceDelete: {$set: false}});
          this.updateState({loggedIn: {$set: false}});
        }}
        onClose={() => {
          this.updateState({showModalInterfaceDelete: {$set: false}});
        }}
      />
    );
  }
  
  getSystemUnderInstanceTestOptions() {
    let key = 0;
    return this.state.systemUnderTestInstances.map((systemUnderTestInstance) => {
      return (<option value={systemUnderTestInstance.name} key={key++}>{systemUnderTestInstance.name}</option>);
    });
  }
  
  renderInstances() {
    if(0 !== this.state.systemUnderTestInstances.length) {
      return (
        <div className="form-group form-group-sm middle_login_toolbar_component">
          <label htmlFor="login_system_under_test_instance" className="middle_login_toolbar_component">Instance:</label>
          <select id="login_system_under_test_instance" className="form-control input-sm" value={this.state.systemUnderTestInstance}
            onChange={(e) => {
              this.updateState({systemUnderTestInstance: {$set: e.target.value}});
            }}
          >
            {this.getSystemUnderInstanceTestOptions()}
          </select>
        </div>
      );
    }
    else {
      return null;
    }
  }
  
  _getSut(systemUnderTest) {
    return this.state.SystemUnderTestStore.systemUnderTests.find((sut) => {
      return sut.name === systemUnderTest;
    });
  }
  
  render() {
    return (
      <div className="middle_login_toolbar middle_toolbar">
        {this.renderModalDialogInterfaceCreate()}
        {this.renderModalDialogInterfaceDelete()}
        <form id="login_form" className="form-inline"
          onSubmit={(e) => {
            e.preventDefault();
            const foundSut = this._getSut(this.state.systemUnderTest);
            if(foundSut) {
              this.dispatch(LoginStore, new ActionLoginUpdate(this.state.labId, this.state.userId, foundSut.repo, this.state.systemUnderTest, this.state.systemUnderTestInstance, foundSut.nodes, this.state.LoginStore.buttons.toolbar.forceLocalhost));
              this.updateState({loggedIn: {$set: true}});
            }
          }}
        >
          <div className="form-group form-group-sm middle_login_toolbar_component">
            <label className="middle_login_toolbar_component" htmlFor="login_user_id">User Id:</label>
            <input type="text" id="login_user_id" className="form-control input-sm" style={{width:'100px'}} placeholder="UserId" value={this.state.userId}
              onChange={(e) => {
                this.updateState({userId: {$set: e.target.value}});
              }}
            />
          </div>
          <div className="form-group form-group-sm middle_login_toolbar_component">
            <label className="middle_login_toolbar_component" htmlFor="login_lab_id">Lab Id:</label>
            <input type="text" id="login_lab_id" className="form-control input-sm" style={{width:'100px'}} placeholder="LabId" value={this.state.labId}
              onChange={(e) => {
                this.updateState({labId: {$set: e.target.value}});
              }}
            />
          </div>
          <div className="form-group form-group-sm middle_login_toolbar_component">
            <label className="middle_login_toolbar_component" htmlFor="login_system_under_test_sut">System Under Test:</label>
            <select id="login_system_under_test_sut" className="form-control input-sm" value={this.state.systemUnderTest}
              onChange={(e) => {
                const value = e.target.value;
                this.updateState({systemUnderTest: {$set: value}});
                const foundSut = this._getSut(value);
                if(foundSut) {
                  this.updateState({systemUnderTestInstances: {$set: foundSut.instances}});
                  if(0 !== foundSut.instances.length) {
                    if(value === this.state.LoginStore.login.systemUnderTest) {
                      this.updateState({systemUnderTestInstance: {$set: this.state.LoginStore.login.systemUnderTestInstance}});
                    }
                    else {
                      this.updateState({systemUnderTestInstance: {$set: foundSut.instances[0].name}});
                    }
                  }
                  else {
                    this.updateState({systemUnderTestInstance: {$set: ''}});
                    this.updateState({systemUnderTestInstances: {$set: []}});
                  }
                }
              }}
            >
              {this.getSystemUnderTestOptions()}
            </select>
          </div>
          {this.renderInstances()}
          <div className="btn-group btn-group-sm middle_login_toolbar_component" role="group" aria-label="...">
            <button id="login_login" type="submit" className="btn btn-default" disabled={this.state.LoginStore.working}>Stage</button>
            {this.renderForceLocalHostButton()}
          </div>
          <div className="btn-group btn-group-sm middle_login_toolbar_component" role="group" aria-label="...">
            {this.renderPingButton()}
          </div>
          <div className="btn-group btn-group-sm middle_login_toolbar_component" role="group" aria-label="...">
            {this.renderCreateVirtualNetworksButton()}
            {this.renderDeleteVirtualNetworksButton()}
          </div>
        </form>
      </div>
    );
  }
}
