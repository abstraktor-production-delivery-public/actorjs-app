
'use strict';

import Helper from './helper';
import Modal from 'z-abs-complayer-bootstrap-client/client/modal';
import ModalHeader from 'z-abs-complayer-bootstrap-client/client/modal-header';
import ModalBody from 'z-abs-complayer-bootstrap-client/client/modal-body';
import ModalFooter from 'z-abs-complayer-bootstrap-client/client/modal-footer';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStagingModalInterfaceDelete extends ReactComponentBase {
  constructor(props) {
    super(props, {
      chosenAddresses: new Map(),
      password: ''
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.show, nextProps.show)
      || !this.shallowCompare(this.props.heading, nextProps.heading)
      || !this.shallowCompareArrayValues(this.props.osNetworks, nextProps.osNetworks)
      || !this.shallowCompare(this.props.platform, nextProps.platform)
      || !this.shallowCompare(this.props.chosenAddresses, nextProps.chosenAddresses)
      || !this.shallowCompare(this.state.chosenAddresses, nextState.chosenAddresses)
      || !this.shallowCompare(this.state.password, nextState.password);
  }
  
  renderErrorMessage() {
    
  }
  
  renderAddressHeadings() {
    return (
      <tr key="modal_dialog_create_interface_address_headings">
        <th>#</th>
        <th>Address</th>
        <th>Netmask</th>
        <th>Family</th>
        <th>Subnet</th>
        <th>Interface</th>
        <th>Delete</th>
      </tr>
    );
  }
  
  renderAddress(osNetwork, address, index) {
    return (
      <tr key={`${osNetwork.name}_${address.address}_${index}`}>
        <td className="modal_interface">{index}</td>
        <td className="modal_interface">{address.address}</td>
        <td className="modal_interface">{address.netmask}</td>
        <td className="modal_interface">{address.family}</td>
        <td className="modal_interface" className="modal_interface">{Helper.calculateSubnet(address.family, address.address, address.netmask)}</td>
        <td className="modal_interface">{osNetwork.name}</td>
        <td className="modal_interface">
          <input type="checkbox" id="modal_dialog_deleye_interface_checkbox" aria-label="..." autoComplete="off" checked={undefined !== this.state.chosenAddresses.get(address.address)} style={{marginTop:'9px', marginLeft:'14px'}}
            onChange={(e) => {
              this.updateState({chosenAddresses: (chosenAddresses) => {
                if(e.currentTarget.checked) {
                  chosenAddresses.set(address.address, {
                    address:  address,
                    interfaceName: osNetwork.name
                  });
                }
                else {
                  chosenAddresses.delete(address.address);
                }
              }});
            }}
          />
        </td>
      </tr>
    ); 
  }
  
  renderAddresses() {
    this.addresses = [];
    let index = 0;
    this.props.osNetworks.forEach((osNetwork) => {
      if(!osNetwork.internal) {
        osNetwork.addresses.forEach((address) => {
          if(!address.dhcp) {
            if(undefined !== this.props.chosenAddresses.chosenAddresses) {
              const foundAddress = this.props.chosenAddresses.chosenAddresses.find((chosenAddress) => {
                return chosenAddress.address === address.address;
              });
              if(undefined !== foundAddress && foundAddress.static) {
                this.addresses.push(this.renderAddress(osNetwork, address, ++index));
              }
            }
          }
        });
      }
    });
    return this.addresses;
  }
  
  renderPassword() {
    if('win32' === this.props.platform) {
      return null;
    }
    else {
      return (
        <div className="row" style={{textAlign: 'right'}}>
          <div className="col-sm-2">
            <label htmlFor="modal_dialog_delete_interface_password" className="control-label">Password</label>
          </div>
          <div className="col-sm-9">
            <input type="new-password" id="modal_dialog_delete_interface_password" className="form-control input-sm" placeholder="modal_dialog_create_delete_password" value={this.state.password}
              onChange={(e) => {
                this.updateState({password: {$set: e.target.value}});
              }}
              />
          </div>
        </div>
      );
    }
  }
  
  render() {
    const iconStyle = {
      width: 24,
      height: 24,
      border: 0
    };
    const textStyle = {
      marginLeft: 64,
      marginTop: 5,
      marginBottom: -2
    };
    return (
      <Modal className="modal_dialog" aria-labelledby="contained-modal-title-sm" show={this.props.show}
        onHide={(e) => {
          this.props.onClose && this.props.onClose();
          this.updateState({chosenAddresses: {$set: new Map()}});
          this.updateState({chosenNetworks: {$set: new Map()}});
        }}
      >
        <ModalHeader closeButton>
          <img className="pull-left" src="/abs-images/svg/AbstraktorA.svg" alt="Abstraktor Icon" style={iconStyle}></img>
          <h4 id="modal-sm" style={textStyle}>{this.props.heading}</h4>
        </ModalHeader>
        <ModalBody>
          <table className="table table-bordered table-striped table-condensed table-hover">
            <thead>
              {this.renderAddressHeadings()}
            </thead>
            <tbody>
              {this.renderAddresses()}
            </tbody>
          </table>
          {this.renderPassword()}
        </ModalBody>
        <ModalFooter>
          {this.renderErrorMessage()}
          <button id="interface_delete_delete" type="button" className="btn btn-danger" disabled={0 === this.state.chosenAddresses.size}
            onClick={(e) => {
              const interfaceParameters = [];
              let virtualNetworkName;
              this.state.chosenAddresses.forEach((address) => {
                if('linux' === this.props.platform) {
                  virtualNetworkName = Helper.calculateVirtualNetworkName(address.interfaceName);
                }
                interfaceParameters.push({
                  address: address.address.address,
                  netmask: address.address.netmask,
                  network: address.interfaceName,
                  virtualNetwork: virtualNetworkName
                });
              });
              this.props.onDeleteInterfaces && this.props.onDeleteInterfaces(interfaceParameters, this.state.password);
            }}
          >Delete</button>
          <button id="interface_delete_close" type="button" className="btn btn-default"
            onClick={(e) => {
              this.props.onClose && this.props.onClose();
            }}
          >Close</button>
        </ModalFooter>
      </Modal>
    );
  }
}
