
'use strict';

import LoginStore from '../../../stores/login-store';
import { ActionLoginMounted, } from '../../../actions/action-login/action-login';
import MiddleStagingToolbar from './middle-staging-toolbar';
import MiddleStagingView from './middle-staging-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleStaging extends ReactComponentStore {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'ActorJs - Staging'
      };
    });
  }
  
  didMount() {
    this.dispatch(LoginStore, new ActionLoginMounted());
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle middle_login">
        <MiddleStagingToolbar />
        <Route switch={["/:tab", ""]} handler={MiddleStagingView} />
      </div>
    );
  }
}
