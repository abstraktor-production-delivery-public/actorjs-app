
'use strict';

import MiddleStagingTabLogical from './tabs/middle-staging-tab-logical';
import MiddleStagingTabReport from './tabs/middle-staging-tab-report';
import MiddleStagingTabSetup from './tabs/middle-staging-tab-setup';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStagingView extends ReactComponentBase {
  constructor(props) {
    super(props, {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['Logical', 'Setup', 'Report']);
    this.state.activeKey = this.helperTab.getIndex(props.tab);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey);
  }
  
  didUpdate(prevProps, prevState) {
    const key = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== key) {
      this.updateState({activeKey: {$set: key}});
    }
  }
  
  getTabPath(index) {
    return `${this.props.tab ? '' : 'login/'}${this.helperTab.getRoute(index)}`;
  }
  
  render() {
    let index = 0;
    return (
      <div className="middle_view_full">
        <Tabs id="login_tabs" className="same_size_as_parent" activeKey={this.state.activeKey}
          onSelect={(key) => {
            if(undefined !== key) {
              this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
            }
          }}
        >
          <Tab id="stage_logical_tab" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleStagingTabLogical />
          </Tab>
          <Tab id="stage_setup_tab" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleStagingTabSetup />
          </Tab>
          <Tab id="stage_report_tab" tab={this.getTabPath(index)} title={this.helperTab.getTabName(index++)}>
            <MiddleStagingTabReport />
          </Tab>
        </Tabs>
      </div>
    );
  }
}


MiddleStagingView.contextType = RouterContext;
