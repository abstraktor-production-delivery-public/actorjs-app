

class Helper {

  static calculateSubnet(family, address, netmask) {
    if('IPv4' === family) {
      return Helper._subnet(address.split('.'), netmask.split('.'), 10);
    }
    else if('IPv6' === family) {
      return Helper._subnet(Helper._formatIPv6(address), Helper._formatIPv6(netmask), 16);
    }
  }
  
  static _subnet(addressParts, netmaskParts, base) {
    let subnet = '';
    let i = 0
    for(; i < addressParts.length - 1; ++i) {
      subnet += (Number.parseInt(addressParts[i], base) & Number.parseInt(netmaskParts[i], base)) + (10 === base ? '.' : ':');
    }
    subnet += (Number.parseInt(addressParts[i], base) & Number.parseInt(netmaskParts[i], base));
    return `${subnet}/${Helper._netmaskBits(netmaskParts, base)}`;
  }
  
  static _netmaskBits(netmaskParts, base) {
    if(10 === base) {
      let sum = 0;
      for(let i = 0; i < netmaskParts.length; ++i) {
        const binary = Number.parseInt(netmaskParts[i]).toString(2).padStart(8, '0');
        const firstZero = binary.indexOf('0');
        if(-1 === firstZero) {
          sum += 8;
        }
        else {
          sum += firstZero;
          return sum;
        }
      }
    }
    else {
      return 'ToBeImpl';
    }
  }
  
  static calculateVirtualNetworkName(previousVirtualNetworkName, virtualNetworks) {
    if(undefined === previousVirtualNetworkName) {
      if(0 === virtualNetworks.length) {
        return 'aj001';
      }
      else {
        const name = virtualNetworks[virtualNetworks.length - 1].name;
        previousVirtualNetworkName = name.substring(name.indexOf(':') + 1);
      }
    }
    return `aj${(Number.parseInt(previousVirtualNetworkName.substring(2)) + 1).toString().padStart(3, '0')}`;
  }
}

module.exports = Helper;
