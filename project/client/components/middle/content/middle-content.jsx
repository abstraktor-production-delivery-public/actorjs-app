
'use strict';

import React from 'react';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import MiddleContentToolbar from './middle-content-toolbar';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import MiddleSidebar from '../middle_sidebar';
import MiddleContentView from './middle-content-view';


export default class MiddleContent extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Content',
        prefix: 'CONT: '
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props._uriPath, nextProps._uriPath);
  }
  
  render() {
    return (
      <div className="middle">
        <MiddleContentToolbar />
        <MiddleSidebar />
        <div className="middle_view middle_view_content">
          <Route switch="/:tab" handler={MiddleContentView} />
        </div>
      </div>
    );
  }
}
