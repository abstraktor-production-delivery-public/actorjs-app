
'use strict';

import ContentStore from '../../../stores/content-store';
import { ActionContentMarkupChange } from '../../../actions/action-content/action-content';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/client/marked-textarea';
import DataContentDocumentsGlobal from 'z-abs-complayer-markup-client/client/data/data-content/data-content-documents-global';
import DataContentDocumentsLocal from 'z-abs-complayer-markup-client/client/data/data-content/data-content-documents-local';
import ComponentTableDataTable from 'z-abs-complayer-markup-client/client/react-components/markup/component-table-data-table';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleContentViewDocuments extends ReactComponentStore {
  constructor(props) {
    super(props, [ContentStore]);
    this.dataContentDocumentsGlobal = new DataContentDocumentsGlobal();
    this.dataContentDocumentsLocal = new DataContentDocumentsLocal();
  }
  
  shouldUpdate(nextProps, nextState) {
     return 'documents' === nextState.ContentStore.currentView
      && (
        !this.shallowCompare(this.state.ContentStore.markup, nextState.ContentStore.markup)
        || !this.shallowCompareArrayValues(this.state.ContentStore.contentDocumentsLocals, nextState.ContentStore.contentDocumentsLocals)
        || !this.shallowCompareArrayValues(this.state.ContentStore.contentDocumentsGlobals, nextState.ContentStore.contentDocumentsGlobals)
      );
  }
  
  renderMarkup() {
    return (
      <div className="content_group">
        <label htmlFor="content_documents_markup_textarea">Content - Markup</label>
        <ComponentMarkedTextarea id="content_documents_markup_textarea" className="form-control content_definition same_size_as_parent" rows="10" value={this.state.ContentStore.markup.content} results={this.state.ContentStore.markup.rows}
          onChange={(value) => {
            this.dispatch(ContentStore, new ActionContentMarkupChange(value));
          }}
          />
      </div>
    );
  }

  render() {
    if(this.state.ContentStore.markup.definition) {
      return (
        <div className="content_tab_view">
          {this.renderMarkup()}
        </div>
      );
    }
    else {
      return (
        <div className="content_tab_view">
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_local" classHeading="markup_table_local_table_heading" classRow="test_data_table" showAlways dataTable={this.dataContentDocumentsLocal} values={this.state.ContentStore.contentDocumentsLocals}/>
          <ComponentTableDataTable classTable="markup_table_fit_content markup_table_global" classHeading="markup_table_global_table_heading" classRow="test_data_table" showAlways dataTable={this.dataContentDocumentsGlobal} values={this.state.ContentStore.contentDocumentsGlobals}/>
        </div>
      );
    }
  }
}
