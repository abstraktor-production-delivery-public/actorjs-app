
'use strict';

import MiddleContentViewText from './middle-content-view-text';
import MiddleContentViewDocuments from './middle-content-view-documents';
import MiddleContentViewImage from './middle-content-view-image';
import MiddleContentViewVideo from './middle-content-view-video';
import MiddleContentViewAudio from './middle-content-view-audio';
import MiddleContentViewOther from './middle-content-view-other';
import ContentStore from '../../../stores/content-store';
import { ActionContentTextGet, ActionContentDocumentsGet, ActionContentImageGet, ActionContentVideoGet, ActionContentAudioGet, ActionContentOtherGet, ActionContentMarkupCancel } from '../../../actions/action-content/action-content';
import Tabs from 'z-abs-complayer-bootstrap-client/client/tabs';
import Tab from 'z-abs-complayer-bootstrap-client/client/tab';
import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import HelperTab from 'z-abs-corelayer-client/client/components/helper-tab';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleContentViewContent extends ReactComponentStore {
  constructor(props) {
    super(props, [ContentStore], {
      activeKey: 0
    });
    this.helperTab = new HelperTab(['Text', 'Documents', 'Image', 'Video', 'Audio', 'Other']);
    this.state.activeKey = this.helperTab.getIndex(props.tab);
  }
  
  didMount() {
    this._getData(this.state.activeKey);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.tab, nextProps.tab)
      || !this.shallowCompare(this.state.activeKey, nextState.activeKey)
      || !this.shallowCompare(this.state.ContentStore.markup.definition, nextState.ContentStore.markup.definition);
  }
  
  didUpdate(prevProps) {
    const nextKey = this.helperTab.getIndex(this.props.tab);
    if(this.state.activeKey !== nextKey) {
      this.updateState({activeKey: {$set: nextKey}});
      this._getData(nextKey);
    }
  }
  
  render() {
    let index = 0;
    return (
      <div className="same_size_as_parent">
        <div className="content_tabs">
          <Tabs id="content_tabs" className="content_tabs_view" activeKey={this.state.activeKey}
            onSelect={(key) => {
              if(undefined !== key && this.state.activeKey !== key) {
                if(this.state.ContentStore.markup.definition) {
                  // TODO: Ask if we really want to leve markup.
                  this.dispatch(ContentStore, new ActionContentMarkupCancel());
                }
                this.context.history(`${this.helperTab.getRoute(key)}`, {replace: true});
              }
            }}  
            >
            <Tab id="content_text_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewText />
            </Tab>
            <Tab id="content_documents_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewDocuments />
            </Tab>
            <Tab id="content_image_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewImage />
            </Tab>
            <Tab id="content_video_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewVideo />
            </Tab>
            <Tab id="content_audio_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewAudio />
            </Tab>
            <Tab id="content_other_tab" tab={`${this.helperTab.getRoute(index)}`} title={this.helperTab.getTabName(index++)}>
              <MiddleContentViewOther />
            </Tab>
          </Tabs>
        </div>
      </div>
    );
  }
  
  _getData(key) {
    let tabName = this.helperTab.getTabName(key);
    if('Text' === tabName) {
      this.dispatch(ContentStore, new ActionContentTextGet());
    }
    else if('Documents' === tabName) {
      this.dispatch(ContentStore, new ActionContentDocumentsGet());
    }
    else if('Image' === tabName) {
      this.dispatch(ContentStore, new ActionContentImageGet());
    }
    else if('Video' === tabName) {
      this.dispatch(ContentStore, new ActionContentVideoGet());
    }
    else if('Audio' === tabName) {
      this.dispatch(ContentStore, new ActionContentAudioGet());
    }
    else if('Other' === tabName) {
      this.dispatch(ContentStore, new ActionContentOtherGet());
    }
  }
}


MiddleContentViewContent.contextType = RouterContext;
