
'use strict';

import MiddleSidebar from '../middle_sidebar';
import MiddleSupportToolbar from './middle-support-toolbar';
import MiddleSupportView from './middle-support-view';
import Route from 'z-abs-complayer-router-client/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleSupport extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Support'
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle">
        <MiddleSupportToolbar />
        <MiddleSidebar />
        <div className="middle_view">
          <Route switch="/:type" handler={MiddleSupportView} />
        </div>
      </div>
    );
  }
}

