
'use strict';

import Button from 'z-abs-complayer-bootstrap-client/client/button';
//import {RouterContext} from 'z-abs-complayer-router-client/client/react-component/router-context';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleSupportToolbar extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderSendFeedbackButton() {
    return (
      <Button id="send_feedback_button" placement="bottom" heading="Send" content="Feedback"
        onClick={(e) => {
          document.location.href = 'mailto:feedback@actorjs.com?subject=Feedback:&body=Hi,%20I%20have%20some%20feedback.%0D%0A%0D%0A=>)%20';
        }}
      >
        <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-send" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)', color:'Green'}}></span>
        
      </Button>
    );
  }
  
  renderSendQuestionButton() {
    return (
      <Button id="send_question_button" placement="bottom" heading="Send" content="Question"
        onClick={(e) => {
          document.location.href = 'mailto:question@actorjs.com?subject=Question:&body=Hi,%20I%20have%20a%20question.%0D%0A%0D%0A?)%20';
        }}
      >
        <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-question-sign" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)', color:'RoyalBlue'}}></span>
      </Button>
    );
  }
  
  renderSendIssueButton() {
    return (
      <Button id="send_issue_button" placement="bottom" heading="Send" content="Issue"
        onClick={(e) => {
          document.location.href = 'mailto:issue@actorjs.com?subject=Issue:&body=Hi,%20I%20found%20an%20issue.%0D%0A%0D%0A!)%20';
        }}
      >
        <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" style={{top: 6, left: -3, width: 0, transform: 'scale(0.8)', color:'Tomato'}}></span>
        
      </Button>
    );
  }
  
  render() {
    return (
      <div className="middle_toolbar">
        <div className="toolbar" role="toolbar"  aria-label="...">
          <div className="btn-group btn-group-sm" role="group" aria-label="...">
            {this.renderSendFeedbackButton()}
            {this.renderSendIssueButton()}
            {this.renderSendQuestionButton()}
          </div>
        </div>
      </div>
    );
  }
}


//MiddleSupportToolbar.contextType = RouterContext;
