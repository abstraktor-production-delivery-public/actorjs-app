
'use strict';

import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddleSupportView extends ReactComponentStore {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="">
        <div className="col-xs-1" />
        <div className="col-xs-10">
          <h1>
            <strong>Support</strong>
          </h1>
          During the Beta period, you can send feedback, issues, and ask questions by email.
        </div>
        <div className="col-xs-1" />
      </div>
    );
  }
}
