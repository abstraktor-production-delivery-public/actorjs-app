
'use strict';

import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class MiddlePlusServicesView extends ReactComponentStore {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
    
  render() {
    return (
      <div className="middle_view start_menu">
        <div className="col-xs-1" />
        <div className="col-xs-10">
          <h1>
            <strong>Plus Services</strong>
          </h1>
          <code style={{color:'Green', backgroundColor:'Honeydew', border:'1px solid Green'}}>Comming Soon:</code>&nbsp;Services adding value for your deveolpment environment.
        </div>
        <div className="col-xs-1" />
      </div>
    );
  }
}
