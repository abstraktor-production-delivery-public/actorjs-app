
'use strict';

import MiddleSidebar from '../middle_sidebar';
import MiddlePlusServicesToolbar from './middle-plus-services-toolbar';
import MiddlePlusServicesView from './middle-plus-services-view';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class MiddleStart extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.setTitle(() => {
      return {
        isPath: false,
        text: 'Plus Services'
      };
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="middle middle_start">
        <MiddlePlusServicesToolbar />
        <MiddleSidebar />
        <MiddlePlusServicesView />
      </div>
    );
  }
}

