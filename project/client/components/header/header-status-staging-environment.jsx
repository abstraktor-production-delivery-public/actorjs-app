
'use strict';

import LoginStore from '../../stores/login-store';
import SystemUnderTestStore from '../../stores/system-under-test-store';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class HeaderStatusLogin extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore, SystemUnderTestStore]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login)
      || !this.shallowCompare(this.state.LoginStore.loggedIn, nextState.LoginStore.loggedIn)
      || !this.shallowCompare(this.state.LoginStore.loginDataChanged, nextState.LoginStore.loginDataChanged)
      || !this.shallowCompare(this.state.SystemUnderTestStore.currentSut, nextState.SystemUnderTestStore.currentSut);
  }
  
  renderInstanceHeader() {
    if(this.state.LoginStore.login.systemUnderTestInstance) {
      return (<th className="table_header_status header_table_heading">instance</th>);
    }
    else {
      return null;
    }
  }
  
  renderInstanceValue() {
    if(this.state.LoginStore.login.systemUnderTestInstance) {
      return (<td className={'table_header_status header_table_heading'}>{this.state.LoginStore.login.systemUnderTestInstance}</td>);
    }
    else {
      return null;
    }
  }
  
  verify() {
    if(this.state.LoginStore.loggedIn) {
      if(this.state.SystemUnderTestStore.currentSut) {
        if(this.state.LoginStore.login.systemUnderTest === this.state.SystemUnderTestStore.currentSut.name) {
          if(this.state.LoginStore.login.forcedLocalhost) {
            return 'staged_to_localhost';
          }
          else {
            return 'staged';
          }
        }
        else {
          return 'staged_not_current';
        }
      }
      else {
        if(this.state.LoginStore.login.forcedLocalhost) {
          return 'staged_to_localhost';
        }
        else {
          return 'staged';
        }
      }
    }
    else {
      return 'not_staged';
    }
  }
  
  renderColorMark(verify) {
    return (
      <div className={`status_popup stage_${verify}`}>
        <Popover placement="bottom" heading="Staging" content="" innerContent={() => {
          return `<div class="status_popup_help_outer_group">
<div class="status_popup_help_outer">
  <div class="status_popup_help stage_staged"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Staged</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help stage_staged_to_localhost"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Staged - localhost</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help stage_staged_not_current"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Staged - not current SUT</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help stage_not_staged"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Not staged</p></div>
</div>
</div>
`;
        }} shortcut="" showDelay="0" hideDelay="1000" autoHideDelay="120000" style={{widht:'100%',height:'100%'}}>
        </Popover>
      </div>
    );
  }
  
  render() {
    const login = this.state.LoginStore.login;
    const colSpan = login.systemUnderTestInstance ? 5 : 4;
    return (
      <div className="header_status_login">
        <table className="table_header_status">
          <thead className="table_header_status">
            <tr className="table_header_status">
              <th className="table_header_status" colSpan={colSpan}>
                {this.renderColorMark(this.verify())}
                staging</th>
            </tr>
            <tr>
              <th className="table_header_status header_table_heading">lab id</th>
              <th className="table_header_status header_table_heading">user id</th>
              <th className="table_header_status header_table_heading">repo</th>
              <th className="table_header_status header_table_heading">sut</th>
              {this.renderInstanceHeader()}
            </tr>
          </thead>
          <tbody className="table_header_status">
            <tr>
              <td className={'table_header_status header_table_heading'}>{login.labId}</td>
              <td className={'table_header_status header_table_heading'}>{login.userId}</td>
              <td className={'table_header_status header_table_heading'}>{login.repo}</td>
              <td className={'table_header_status header_table_heading'}>{login.systemUnderTest}</td>
              {this.renderInstanceValue()}
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
