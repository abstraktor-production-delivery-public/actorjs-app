
'use strict';

import Image from 'z-abs-complayer-bootstrap-client/client/image';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import Link from 'z-abs-complayer-router-client/client/react-component/link';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class HeaderIcon extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    const styleImage = {
      position: 'absolute',
      height: 54.4,
      border: 0
    };
    const styleText = {
      position: 'absolute',
      color: 'black',
      textShadow: '2px 2px 4px LightGray',
      fontSize: 48,
      margin: 0,
      padding: 0,
      top: 7,
      left: 73,
      height: 56
    };
    const s = {
      marginLeft:'60px',
      marginTop:'12.1px'
    };
    return (
      <div className="header_icon">
        <Link href="/start" global>
          <Image style={styleImage} src="/abs-images/svg/AbstraktorA.svg" placement="bottom" heading="Goto" content="Home">
          </Image>
          <div style={s}>
            <Popover placement="bottom" heading="Goto" content="Home">
              <img src="/abs-images/svg/ActorJs.svg" alt="ActorJs" width="204.6642668109053px" height="38px" />
            </Popover>
          </div>
        </Link>
      </div>
    );
  }
}
