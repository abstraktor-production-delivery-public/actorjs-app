
'use strict';

import HeaderIcon from './header_icon';
import HeaderStatus from './header_status';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class Header extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="header">
        <HeaderIcon />
        <HeaderStatus />
      </div>
    );
  }
}
