
'use strict';

import React from 'react';
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import HeaderStatusLogin from './header-status-login';
import HeaderStatusStagingEnvironment from './header-status-staging-environment';
import StatusEnvironmentAbstraktorTools from 'z-abs-complayer-toolversions-client/client/components/status-environment-abstraktor-tools';
import StatusEnvironmentDevelopment from 'z-abs-complayer-toolversions-client/client/components/status-environment-development';
import StatusEnvironmentExternalTools from 'z-abs-complayer-toolversions-client/client/components/status-environment-external-tools';


export default class HeaderStatus extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  render() {
    return (
      <div className="header_status">
        <HeaderStatusLogin />
        <HeaderStatusStagingEnvironment />
        <StatusEnvironmentDevelopment />
        <StatusEnvironmentExternalTools />
        <StatusEnvironmentAbstraktorTools />
      </div>
    );
  }
}
