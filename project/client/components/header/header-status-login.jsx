
'use strict';

import LoginStore from '../../stores/login-store';
import Popover from 'z-abs-complayer-bootstrap-client/client/popover';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class HeaderStatusLogin extends ReactComponentStore {
  constructor(props) {
    super(props, [LoginStore]);
    this.verificationClasses = ['logged_in', 'not_logged_in', 'offline', 'not_registered'];
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state.LoginStore.login, nextState.LoginStore.login);
  }
  
  renderColorMark(verify) {
    return (
      <div className={`status_popup login_${verify}`}>
        <Popover placement="bottom" heading="Login" content="" innerContent={() => {
          return `<div class="status_popup_help_outer_group">
<div class="status_popup_help_outer">
  <div class="status_popup_help login_logged_in"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Logged in</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help login_not_logged_in"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Not logged in</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help login_offline"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Offline</p></div>
</div>
<div class="status_popup_help_outer">
  <div class="status_popup_help login_not_registered"></div>
  <div class="status_popup_help_inner"><p class="status_popup_help_inner">Not registered</p></div>
</div>
</div>
`;
        }} shortcut="" showDelay="0" hideDelay="1000" autoHideDelay="120000" style={{widht:'100%',height:'100%'}}>
        </Popover>
      </div>
    );
  }
  
  verify() {
  	return 'not_registered';
  }
  
  render() {
    return (
      <div className="header_status_login">
        <table className="table_header_status">
          <thead className="table_header_status">
            <tr className="table_header_status">
              <th className="table_header_status" colSpan="2">
                {this.renderColorMark(this.verify())}
                login</th>
            </tr>
            <tr>
              <th className="table_header_status header_table_heading">user</th>
              <th className="table_header_status header_table_heading">organization</th>
            </tr>
          </thead>
          <tbody className="table_header_status">
            <tr>
              <td className={'table_header_status header_table_heading'}>-</td>
              <td className={'table_header_status header_table_heading'}>-</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
