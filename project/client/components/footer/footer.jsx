
'use strict';

import ConsoleStore from '../../stores/console-store';
import { ActionConsoleClear } from '../../actions/action-console/action-console';
import Button from 'z-abs-complayer-bootstrap-client/client/button';
import ReactComponentStore from 'z-abs-corelayer-client/client/react-component/react-component-store';
import React from 'react';


export default class Footer extends ReactComponentStore {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return false;
  }
  
  renderClearConsoleButton(style) {
    return (
      <Button id="console_clear" className="footer_button" placement="top" heading="Clear" content="Console" style={{fontSize:'11px',float:'right'}} aria-label="Clear Console"
        onClick={(e) => {
          this.dispatch(ConsoleStore, new ActionConsoleClear());
        }}
      >
        <span className="glyphicon glyphicon-erase" aria-hidden="true"></span>
      </Button>
    );
  }
  
  renderBuildDiv() {
    return null;
    /*return (
      <div className="build_info" />
    );*/
  }
  
  renderTestExecutiondDiv() {
    return null;
    /*return (
      <div className="test_execution_info" />
    );*/
  }
  
  renderRefButton(heading, content, link, ariaLabel) {
    return (
      <Button id={`${heading}_${content}`} className="footer_button" placement="top" heading={heading} content={content} aria-label={ariaLabel}
        onClick={(e) => {
          const width = 1200;
          const height = 600;
          const left = 100;
          const top = 100;
          window.open(link, '_blank', `width=${width},innerWidth=${width},height=${height},innerHeight=${height},left=${left},screenX=${left},top=${top},screenY=${top}`);
        }}
      >
        <span className="glyphicon glyphicon-link" aria-hidden="true" style={{fontSize:'10px'}}>{content}</span>
      </Button>
    );
  }
  
  render() {
    const styleImage = {
      position: 'relative',
      height: 24,
      border: 0,
      float: 'right'
    };
    return (
      <div className="footer">
        <div className="btn-toolbar" role="toolbar" aria-label="..." style={{float:'left'}}>
          <div className="btn-group btn-group-xs" role="group" aria-label="...">
            {this.renderRefButton('MDN', 'String', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String', 'MDN String')}
            {this.renderRefButton('MDN', 'Number', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number', 'MDN Number')}
            {this.renderRefButton('MDN', 'Array', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array', 'MDN Array')}
            {this.renderRefButton('MDN', 'Map', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map', 'MDN Map')}
            {this.renderRefButton('MDN', 'Set', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set', 'MDN Set')}
            {this.renderRefButton('MDN', 'Proxy', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy', 'MDN Proxy')}
            {this.renderRefButton('MDN', 'Reflect', 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect', 'MDN Reflect')}
          </div>
          <div className="btn-group btn-group-xs" role="group" aria-label="...">
            {this.renderRefButton('Nodejs', 'Nodejs', 'https://nodejs.org/en/', 'Nodejs')}
            {this.renderRefButton('Nodejs', 'Buffer', 'https://nodejs.org/dist/latest-v16.x/docs/api/buffer.html', 'Nodejs Buffer')}
          </div>
          <div className="btn-group btn-group-xs" role="group" aria-label="...">
            {this.renderRefButton('Puppeteer', 'Puppeteer', 'https://pptr.dev/api/', 'Puppeteer')}
            {this.renderRefButton('Puppeteer', 'Page', 'https://pptr.dev/api/puppeteer.page', 'Puppeteer Page')}
            {this.renderRefButton('Puppeteer', 'ElementHandle', 'https://pptr.dev/api/puppeteer.elementhandle', 'Puppeteer Element Handle')}
          </div>
          <div className="btn-group btn-group-xs footer_right" role="group" aria-label="...">
            <img id="start_page" src="/abs-images/svg/AbstraktorLogo.svg" alt="Abstraktor Logo" style={styleImage}></img>
            {this.renderClearConsoleButton()}
            {this.renderBuildDiv()}
            {this.renderTestExecutiondDiv()}
          </div>
        </div>
      </div>
    );
  }
}
