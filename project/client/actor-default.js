
'use strict';

export default class ActorDefault {
  static NAME() {
    return 'Actor';
  }
  
  static SYSTEM_UNDER_TEST() {
    return {
      name: ActorDefault.NAME(),
      description: 'Default System Under Test. The tool it self acts as the system under test.',
      instances: [],
      nodes: [],
      status: 'static'
    }
  }
}
