
'use strict';

const Os = require('os');
const getos = require('getos');


let pendings = 2;
let version = '';
let versionQueue = [];
async function loadESModule() {
  try {
    version = (await import('os-name')).default();
  }
  catch(e) {
    ddb.error(`Could not load module 'os-name'.`, e);
    version = 'Unknown';
  }
  if(0 === --pendings) {
    versionQueue.forEach((cb) => {
      cb();
    });
    versionQueue = [];
  }
}
loadESModule();

let distributionData = null;
getos((err, osData) =>  {
  if(err) {
    ddb.error('Could not get os distribution data.');
  }
  else {
    distributionData = osData;
    if(0 === --pendings) {
      versionQueue.forEach((cb) => {
        cb();
      });
      versionQueue = [];
    }
  }
});

class OsHardware {
  constructor() {
    let cpus = Os.cpus();
    this.cpus = [];
    cpus.forEach((cpu) => {
      this.cpus.push({
        model: cpu.model,
        speed: cpu.speed
      });
    });
  }
  
  getCpus() {
   return this.cpus; 
  }
  
  getDevEnvironment(cbValue) {
    const devEnvirionment = {
      type: Os.type(),
      platform: Os.platform(),
      arch: Os.arch(),
      release: Os.release(),
      version: version,
      versionfromNode: Os.version(),
      distributionData: distributionData
    };
    if(version) {
      cbValue(devEnvirionment);
    }
    else {
      versionQueue.push(() => {
        devEnvirionment.version = version;
        devEnvirionment.distributionData = distributionData;
        cbValue(devEnvirionment);
      });
    }
  }
}

module.exports = new OsHardware();
