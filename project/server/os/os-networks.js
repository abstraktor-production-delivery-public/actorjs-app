
'use strict';

const Os = require('os');


class OsNetworks {
  static getArray() {
    const networksArray = [];
    const interfaces = Os.networkInterfaces();
    for(let k in interfaces) {
      let internal = false;
      if(1 <= interfaces[k].length) {
        internal = interfaces[k][0].internal;
      }
      let families = new Set();
      if('win32' === Os.platform() && k.startsWith('vEthernet')) {
        // Filter out virtual Ethernet adapters on windows.
        continue;
      }
      interfaces[k].forEach((address) => {
        if('IPv4' === address.family) {
          families.add('IPv4');
        }
        else if('IPv6' === address.family && 0 === address.scopeid) {
          families.add('IPv6');
        }
      });
      const network = {
        name: k,
        families: families,
        dhcpEnabled: false,
        internal: internal,
        addresses: []
      };
      networksArray.push(network);
      interfaces[k].forEach((address) => {
        if('IPv6' === address.family && 0 !== address.scopeid) {
          // Filter out Link-local IPv6 Address
          return;
        }
        network.addresses.push(address);
      });
    }
    return networksArray;
  }
}


module.exports = OsNetworks;
