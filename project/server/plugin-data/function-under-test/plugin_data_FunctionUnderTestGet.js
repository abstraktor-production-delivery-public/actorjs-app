
'use strict';

const Fs = require('fs');
const Path = require('path');
const Logger = require('z-abs-corelayer-server/server/log/logger');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class FunctionUnderTestGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repoName, sutName, futName) {
    if(futName) {
      return this.responsePartError(`Could not get the Function Under Test ${futName}. NOT IMPLEMENTED`);
    }
    else {
      let responseDone = false;
      this.getFunctionUnderTests(repoName, sutName, (data, err) => {
        if(!responseDone) {
          responseDone = true;
          if(err) {
            LOG_ERROR(Logger.ERROR.CATCH, 'FunctionUnderTestGet', err);
            return this.responsePartError('Could not get the Functions Under Test.');
          }
          data.sort((a, b) => {
            if(a.name < b.name) {
              return -1;
            }
            else if(b.name < a.name) {
              return 1;
            }
            else {
              return 0;
            }
          });
          return this.responsePartSuccess({
            functionUnderTests: data
          });
        }
      });
    }
  }
  
  getFunctionUnderTests(repoName, sutName, done) {
    let functionUnderTests = [];
    let dir = ActorPathData.getSystemUnderTestFolder(repoName, sutName);
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(functionUnderTests, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(functionUnderTests);
      }
      for(let i = 0; i < files.length; ++i) {
        let file = files[i];
        let resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(functionUnderTests, err);
          }
          if(stat && stat.isDirectory()) {
            let prefix = `Fut${sutName}`;
            if(file.startsWith(prefix) && file.length > prefix.length) {
              Fs.readFile(ActorPathData.getFunctionUnderTestFile(repoName, sutName, file.substr(prefix.length)), (err, data) => {
                if(err) {
                  return done(functionUnderTests, err);
                }	  
                functionUnderTests.push(JSON.parse(data));
                if(0 === --pendings) {
                  return done(functionUnderTests);
                }
              });
            }
            else if(0 === --pendings) {
              return done(functionUnderTests);
            }
          }
          else if(0 === --pendings) {
            return done(functionUnderTests);
          }
        });
      }
    });
  }
}

module.exports = FunctionUnderTestGet;
