
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class FunctionUnderTestDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(futs) {
    futs.forEach((fut) => {
      this.asynchRmdirResponse(ActorPathData.getFunctionUnderTestFolder(fut.repo, fut.sut, fut.fut), true);
    });
  }
}

module.exports = FunctionUnderTestDelete;
