
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class FunctionUnderTestAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repoName, sutName, futName, description) {
    let functionUnderTest = {
      name: futName,
      description: description,
      status: 'visible'
    }
    let responsePart = this.verifyInput(functionUnderTest);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.mkdir(ActorPathData.getFunctionUnderTestFolder(repoName, sutName, futName), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the Function Under Test Folder: ' ${ActorPathData.getFunctionUnderTestFolder(repoName, sutName, futName)} '.`);
      }
      Fs.writeFile(ActorPathData.getFunctionUnderTestFile(repoName, sutName, futName), JSON.stringify(functionUnderTest, null, 2), (err) => {
        if(err) {
          return this.responsePartError(`Could not create the Function Under Test File: ' ${ActorPathData.getFunctionUnderTestFile(repoName, sutName, futName)} '.`);
        }
        Fs.mkdir(ActorPathData.getTestCasesFolder(repoName, sutName, futName), (err) => {
          if(err) {
            return this.responsePartError(`Could not create the TestCases Folder: ' ${ActorPathData.getTestCasesFolder(repoName, sutName, futName)} '.`);
          }
          Fs.mkdir(ActorPathData.getTestSuitesFolder(repoName, sutName, futName), (err) => {
            if(err) {
              return this.responsePartError(`Could not create the TestSuites Folder: ' ${ActorPathData.getTestSuitesFolder(repoName, sutName, futName)} '.`);
            }
          });   
        });    
      });
    });
    return this.responsePartSuccess();
  }

  verifyInput(functionUnderTest) {
    let name = functionUnderTest.name;
    if(0 === name.length) {
      return this.responsePartError(`The Function Under Test name: '' must not be empty.`);
    }
    let first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A Function Under Test name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
}

module.exports = FunctionUnderTestAdd;
