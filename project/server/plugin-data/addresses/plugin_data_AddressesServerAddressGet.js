
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesServerAddressGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalAddressesServerFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalAddressesServerFile());
  }
}

module.exports = AddressesServerAddressGet;
