
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSrcGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    // BACKWARD COMPATIBILITY
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalSrcFile(), (err, srcs) => {
      if(srcs && Array.isArray(srcs)) { // BACKWARD COMPATIBILITY - 0.0.0-aj-beta.2
        srcs.forEach((src) => {
          if(src.browser && ! src.page) {
            src.page = src.browser;
            src.browser = undefined;
          }
        });
      } 
    });
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalSrcFile(), (err, srcs) => {
      if(srcs && Array.isArray(srcs)) { // BACKWARD COMPATIBILITY - 0.0.0-aj-beta.2
        srcs.forEach((src) => {
          if(src.browser && ! src.page) {
            src.page = src.browser;
            src.browser = undefined;
          }
        });
      } 
    });
  }
}

module.exports = AddressesSrcGet;
