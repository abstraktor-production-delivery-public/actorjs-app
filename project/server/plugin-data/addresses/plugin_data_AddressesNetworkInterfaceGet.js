
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesNetworkInterfaceGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalNetworksFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalNetworksFile());
  }
}

module.exports = AddressesNetworkInterfaceGet;
