
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSrvGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalSrvFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalSrvFile());
  }
}

module.exports = AddressesSrvGet;
