
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesClientAddressGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalAddressesClientFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalAddressesClientFile());
  }
}

module.exports = AddressesClientAddressGet;