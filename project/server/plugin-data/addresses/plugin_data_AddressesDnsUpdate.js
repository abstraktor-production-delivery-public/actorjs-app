
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesDnsUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalDnsFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalDnsFile(), locals, false);
  }
}

module.exports = AddressesDnsUpdate;
