
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesServerInterfaceUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalInterfacesServerFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalInterfacesServerFile(), locals, false);
  }
}

module.exports = AddressesServerInterfaceUpdate;
