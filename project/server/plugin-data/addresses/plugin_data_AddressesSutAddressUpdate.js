
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSutAddressUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalAddressesSutFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalAddressesSutFile(), locals, false);
  }
}

module.exports = AddressesSutAddressUpdate;
