
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSrvUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalSrvFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalSrvFile(), locals, false);
  }
}

module.exports = AddressesSrvUpdate;
