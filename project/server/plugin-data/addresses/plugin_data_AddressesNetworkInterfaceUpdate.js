
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesNetworkInterfaceUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalNetworksFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalNetworksFile(), locals, false);
  }
}

module.exports = AddressesNetworkInterfaceUpdate;
