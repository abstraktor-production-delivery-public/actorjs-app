
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesClientInterfaceGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalInterfacesClientFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalInterfacesClientFile());
  }
}

module.exports = AddressesClientInterfaceGet;
