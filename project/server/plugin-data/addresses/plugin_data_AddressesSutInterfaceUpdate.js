
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSutInterfaceUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalInterfacesSutFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalInterfacesSutFile(), locals, false);
  }
}

module.exports = AddressesSutInterfaceUpdate;
