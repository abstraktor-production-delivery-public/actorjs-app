
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesClientInterfaceUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalInterfacesClientFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalInterfacesClientFile(), locals, false);
  }
}

module.exports = AddressesClientInterfaceUpdate;
