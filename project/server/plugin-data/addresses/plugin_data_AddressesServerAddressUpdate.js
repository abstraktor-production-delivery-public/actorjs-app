
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesServerAddressUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalAddressesServerFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalAddressesServerFile(), locals, false);
  }
}

module.exports = AddressesServerAddressUpdate;
