
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSutInterfaceGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalInterfacesSutFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalInterfacesSutFile());
  }
}

module.exports = AddressesSutInterfaceGet;
