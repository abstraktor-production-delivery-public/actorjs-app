
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesPortsUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalPortsFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalPortsFile(), locals, false);
  }
}

module.exports = AddressesPortsUpdate;
