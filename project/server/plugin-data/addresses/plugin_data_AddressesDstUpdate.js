
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesDstUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getAddressesGlobalDstFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getAddressesLocalDstFile(), locals, false);
  }
}

module.exports = AddressesDstUpdate;
