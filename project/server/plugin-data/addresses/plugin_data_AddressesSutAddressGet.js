
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class AddressesSutAddressGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getAddressesGlobalAddressesSutFile());
    this.asynchReadFileResponse(ActorPathData.getAddressesLocalAddressesSutFile());
  }
}

module.exports = AddressesSutAddressGet;
