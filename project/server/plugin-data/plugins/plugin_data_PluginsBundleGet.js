
'use strict';

const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class PluginsBundleGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.production = 'production' === process.env.NODE_ENV;
  }
  
  onRequest(bundle, type) {
    const file = 'js' === type ? `${ActorPathDist.getScriptsPath()}${Path.sep}${bundle}${this.production ? '.gzip' : ''}` : `${ActorPathDist.getCssPath()}${Path.sep}${bundle}${this.production ? '.gzip' : ''}`;
    const indexProduction = this.expectAsynchResponse();
    const indexfile = this.expectAsynchResponse();
    this.asynchReadTextFile(file, (err, data) => {
      if(!err) {
        this.asynchResponseSuccess(indexProduction, this.production);
        this.asynchResponseSuccess(indexProduction, data);
      }
      else {
        this.asynchResponseSuccess(indexProduction, this.production);
        this.asynchResponseError(err.message, indexfile);
      }
    });
  }
}


module.exports = PluginsBundleGet;
