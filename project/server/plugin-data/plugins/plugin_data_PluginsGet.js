
'use strict';

const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class PluginsGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFile(ActorPath.getSettingsFile(), (err, data) => {
      if(!err) {
        this.expectAsynchResponseSuccess(data.plugins);
      }
      else {
        this.expectAsynchResponseError(`Could not settings.json`);
      }
    });
  }
}


module.exports = PluginsGet;
