
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class SystemUnderTestSpecificationUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(sut, specification) {
    if(undefined !== specification) {
      sut.specification = specification;
    }
    this.asynchWriteFileResponse(ActorPathData.getSystemUnderTestFile(sut.repo, sut.name), sut, true);
  }
}

module.exports = SystemUnderTestSpecificationUpdate;
