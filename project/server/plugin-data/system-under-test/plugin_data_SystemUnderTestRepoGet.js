
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class SystemUnderTestRepoGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.getRepos(ActorPathData.getReposLocalFolder(), (repos, err) => {
      if(err) {
        return this.responsePartError('Could not get the System Under Test Repos.');
      }
      if(ActorPathData.getReposGlobalFolder() !== ActorPathData.getReposLocalFolder()) {
        repos.push(ActorPathData.getGlobalDataRepoName());
      }
      repos.sort();
      return this.responsePartSuccess(repos);
    });
  }
  
  getRepos(dir, done) {
    const repos = [];
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(repos, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(repos);
      }
      for(let i = 0; i < files.length; ++i) {
        const file = files[i];
        const resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(repos, err);
          }
          if(stat && stat.isDirectory()) {
            repos.push(file);
          }
          if(0 === --pendings) {
            return done(repos);
          }
        });
      }
    });
  }
}

module.exports = SystemUnderTestRepoGet;
