
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class SystemUnderTestAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.parsedData = {};
  }
  
  onRequest(repoName, sutName, description) {
    this.addSystemUnderTest(repoName, sutName, description);
  }
  
  addSystemUnderTest(repoName, sutName, description) {
    const systemUnderTest = {
      name: sutName,
      description: description,
      status: 'visible'
    }
    const responsePart = this.verifyInput(systemUnderTest);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.mkdir(ActorPathData.getSystemUnderTestFolder(repoName, sutName), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the System Under Test: ' ${ActorPathData.getSystemUnderTestFolder(repoName, sutName)} '.`);
      }
      Fs.writeFile(ActorPathData.getSystemUnderTestFile(repoName, sutName), JSON.stringify(systemUnderTest, null, 2), (err) => {
        if(err) {
          return this.responsePartError(`Could not create the System Under Test File: ' ${ActorPathData.getSystemUnderTestFile(repoName, sutName)} '.`);
        }
      });
    });
    return this.responsePartSuccess();
  }
  
  verifyInput(systemUnderTest) {
    const name = systemUnderTest.name;
    if(0 === name.length) {
      return this.responsePartError(`The System Under Test name: '' must not be empty.`);
    }
    if('Actor' === name) {
      return this.responsePartError(`The System Under Test name: '${name}' is reserved.`);
    }
    let first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A System Under Test name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
  
  failureResponse(systemUnderTest, msg) {
    const status = {
      name: {
        text: systemUnderTest.name,
	      status: false
      },
      description: {
        text: systemUnderTest.description,
        status: true
      }
    }
    return this.responsePartError(status);
  }
}

module.exports = SystemUnderTestAdd;
