
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class SystemUnderTestNodesUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(sut, nodes) {
    if(undefined !== nodes) {
      sut.nodes = nodes;
    }
    this.asynchWriteFileResponse(ActorPathData.getSystemUnderTestFile(sut.repo, sut.name), sut, true);
  }
}

module.exports = SystemUnderTestNodesUpdate;
