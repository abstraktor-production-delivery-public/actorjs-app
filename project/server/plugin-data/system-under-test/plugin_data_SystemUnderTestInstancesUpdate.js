
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class SystemUnderTestInstancesUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(sut, instances) {
    if(undefined !== instances) {
      sut.instances = instances;
    }
    this.asynchWriteFileResponse(ActorPathData.getSystemUnderTestFile(sut.repo, sut.name), sut, true);
  }
}

module.exports = SystemUnderTestInstancesUpdate;
