
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');


class SystemUnderTestRepoAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repoName) {
    const responsePart = this.verifyInput(repoName);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.mkdir(ActorPathData.getReposRepoFolder(repoName), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the Repo Folder: ' ${ActorPathData.getReposRepoFolder(repoName)} '.`);
      }
      Fs.mkdir(ActorPathData.getTestAbstractionFolder(repoName), (err) => {
        if(err) {
          return this.responsePartError(`Could not create the Repo TestAbstraction Folder: ' ${ActorPathData.getTestAbstractionFolder(repoName)} '.`);
        }
        return this.responsePartSuccess();
      });
    });      
  }
  
  verifyInput(repoName) {
    if(0 === repoName.length) {
      return this.responsePartError(`The Repo name: '' must not be empty.`);
    }
    if('actorjs-data-global' === repoName) {
       return this.responsePartError(`A Repo name must not be 'actorjs-data-global'. It is already used by ActorJs.`);
    }
    if('actorjs-data-local' === repoName) {
       return this.responsePartError(`A Repo name must not be 'actorjs-data-local'. It is already used by by ActorJs.`);
    }
    return true;
  }
}

module.exports = SystemUnderTestRepoAdd;
