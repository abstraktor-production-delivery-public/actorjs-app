
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class SystemUnderTestDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(suts) {
    suts.forEach((sut) => {
      this.asynchRmdirResponse(ActorPathData.getSystemUnderTestFolder(sut.repo, sut.name), true);
    });
  }
}

module.exports = SystemUnderTestDelete;
