
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class SystemUnderTestUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(suts) {
    this.updateSystemsUnderTest(suts);
  }
  
  updateSystemsUnderTest(suts) {
    suts.forEach((sut) => {
      this.asynchWriteFileResponse(ActorPathData.getSystemUnderTestFile(sut.repo, sut.name), sut, false);
    });
  }
}

module.exports = SystemUnderTestUpdate;
