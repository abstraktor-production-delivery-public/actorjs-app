
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class SystemUnderTestGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.getRepos(ActorPathData.getReposLocalFolder(), (repos, err) => {
      if(err) {
        return this.responsePartError('Could not get the repos.');
      }
      if(ActorPathData.getReposGlobalFolder() !== ActorPathData.getReposLocalFolder()) {
        repos.push(ActorPathData.getGlobalDataRepoName());
      }
      repos.sort();
      let errorResponse = false;
      let pendings = 0;
      let systemUnderTests = [];
      repos.forEach((repoName) => {
        ++pendings;
        this.getSystemUnderTests(ActorPathData.getTestAbstractionFolder(repoName), repoName, (data, err) => {
          if(!errorResponse) {
            if(err) {
              errorResponse = true;
              return this.responsePartError(`Could not get the Systems Under Test in the repo: '${repoName}'.`);
            }
            systemUnderTests = systemUnderTests.concat(data);
            systemUnderTests.forEach((systemUnderTest) => {
              if(!systemUnderTest.instances) {
                systemUnderTest.instances = [];
              }
              if(!systemUnderTest.nodes) {
                systemUnderTest.nodes = [];
              }
            });
            if(0 === --pendings && !errorResponse) {
               systemUnderTests.sort((a, b) => {
                if(a.name < b.name) {
                  return -1;
                }
                else if(b.name < a.name) {
                  return 1;
                }
                else {
                  return 0;
                }
              });
              return this.responsePartSuccess({
                systemUnderTests: systemUnderTests
              });
            }
          }
        });
      });
    });
  }
    
  getRepos(dir, done) {
    const repos = [];
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(repos, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(repos);
      }
      for(let i = 0; i < files.length; ++i) {
        const file = files[i];
        const resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(repos, err);
          }
          if(stat && stat.isDirectory()) {
            repos.push(file);
          }
          if(0 === --pendings) {
            return done(repos);
          }
        });
      }
    });
  }
  
  getSystemUnderTests(dir, repoName, done) {
    const systemUnderTests = [];
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(systemUnderTests, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(systemUnderTests);
      }
      for(let i = 0; i < files.length; ++i) {
        const file = files[i];
        const resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(systemUnderTests, err);
          }
          if(stat && stat.isDirectory()) {
            if(file.startsWith('Sut') && file.length > 'Sut'.length) {
              Fs.readFile(ActorPathData.getSystemUnderTestFile(repoName, file.substr('Sut'.length)), (err, data) => {
                if(err) {
                  return done(systemUnderTests, err);
                }	  
                const ts = JSON.parse(data);
                ts.repo = repoName;
                systemUnderTests.push(ts);
                if(0 === --pendings) {
                  return done(systemUnderTests);
                }
              });
            }
            else if(0 === --pendings) {
              return done(systemUnderTests);
            }
          }
          else if(0 === --pendings) {
            return done(systemUnderTests);
          }
        });
      }
    });
  }
}

module.exports = SystemUnderTestGet;
