
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ConsoleClear extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest() {
    console.clear();
    process.stdout.write("\u001b[3J\u001b[2J\u001b[1J");
    this.expectAsynchResponseSuccess();
  }
}


module.exports = ConsoleClear;
