
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Os = require('os');


class LoginDeleteInterfaces extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
    this.pendings = 0;
    this.results = [];
  }
  
  onRequest(interfaceParameters, password) {
    interfaceParameters.forEach((interfaceParameter) => {
      ++this.pendings;
      this.asynchSudoProcess(this._getDeleteInterfacesCommand(interfaceParameter), password, (err, std) => {
        this.results.push(err);
        if(0 === --this.pendings) {
          return this.expectAsynchResponseSuccess(this.results);
        }
      });
    });
  }
  
  _getDeleteInterfacesCommand(interfaceParameter) {
    if('win32' === Os.platform()) {
      return `netsh interface ip delete address "${interfaceParameter.network}" ${interfaceParameter.address} ${interfaceParameter.netmask}`;
    }
    else {
      return `ifconfig ${interfaceParameter.network} down`;
    }
  }
}


module.exports = LoginDeleteInterfaces;
