
'use strict';

const DataResponse = require('z-abs-corelayer-server/server/data-response');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class LoginWsGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
    
  onRequest() {
    const versioningApp = this.getService('versioning-app-fs');
    const wsServers = versioningApp?.getWsServers();
    if(wsServers) {
      this.expectAsynchResponseSuccess(wsServers);
    }
    else {
      const serverDatas = DataResponse._.serverDatas.wsWebServers;
      const resultingServerDatas = [];
      serverDatas.forEach((serverData) => {
        resultingServerDatas.push({
          name: serverData.name,
          type: serverData.type,
          host: serverData.host,
          port: serverData.port
        });
      });
      this.expectAsynchResponseSuccess(resultingServerDatas);
    }
  }
}


module.exports = LoginWsGet;
