
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Os = require('os');


class LoginCreateInterfaces extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.pendings = 0;
    this.results = [];
    this.interfaceDhcpEnabledCache = new Set();
  }
  
  onRequest(interfaceParameters, password) {
    interfaceParameters.forEach((interfaceParameter) => {
      ++this.pendings;
      this.setDhcpEnabled(interfaceParameter, password, () => {
        if(0 === --this.pendings) {
          interfaceParameters.forEach((interfaceParameter) => {
            ++this.pendings;
            this.createInterface(interfaceParameter, password, () => {
              if(0 === --this.pendings) {
                return this.expectAsynchResponseSuccess(this.results);     
              }
            });
          });
        }
      });
    });
  }
  
  _getCreateInterfacesCommand(interfaceParameter) {
    if('win32' === Os.platform()) {
      //netsh interface ip set address "Wi-Fi" dhcp
      return `netsh interface ip add address "${interfaceParameter.network}" ${interfaceParameter.address} ${interfaceParameter.netmask}`;
    }
    else {
      return `ifconfig ${interfaceParameter.network}:${interfaceParameter.virtualNetwork} ${interfaceParameter.address} netmask ${interfaceParameter.netmask}`;
    }
  }
  
  setDhcpEnabled(interfaceParameter, password, cb) {
    if(this.interfaceDhcpEnabledCache.has(interfaceParameter.network) || !interfaceParameter.dhcpEnabled) {
      process.nextTick(() => {
        cb();
      });
    }
    else {
      this.interfaceDhcpEnabledCache.add(interfaceParameter.network);
      this.asynchChildProcess(`netsh interface ip show interfaces "${interfaceParameter.network}"`, (err, std) => {
        if(-1 === std.indexOf('DHCP/Static IP coexistence         : enabled')) {
          this.asynchSudoProcess(`netsh interface ip set interface interface="${interfaceParameter.network}" dhcpstaticipcoexistence=enabled`, password, (err, std) => {
            cb();
          });
        }
        else {
          cb();
        }
      });
    }
  }
  
  createInterface(interfaceParameter, password, cb) {
    ++this.pendings;
    this.asynchSudoProcess(this._getCreateInterfacesCommand(interfaceParameter), password, (err, std) => {
      ++this.pendings;
      this.results.push(err);
      cb();
    });
  }
}

module.exports = LoginCreateInterfaces;
