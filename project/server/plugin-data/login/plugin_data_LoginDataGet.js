
'use strict';

const OsNetworks = require('../../os/os-networks');
const OsHardware = require('../../os/os-hardware');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Os = require('os');


class LoginDataGet extends PluginBaseMulti {
  static DHCP_ENABLED = 'DHCP enabled:';
  static DHCP_ENABLED_LENGTH = LoginDataGet.DHCP_ENABLED.length;
  static SHOW_IP_ADDRESSES_ADDRESS = 'Address ';
  static SHOW_IP_ADDRESSES_ADDRESS_LENGTH = LoginDataGet.SHOW_IP_ADDRESSES_ADDRESS.length;
  
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.expectAsynchResponseSuccess(OsHardware.getCpus());
    const index = this.expectAsynchResponse();
    this.getOsNetworks((command, cb) => {
      this.asynchChildProcess(command, (err, stdout) => {
        cb(err, stdout);
      });
    }, (osNetworks) => {
      this.asynchResponseSuccess(index, osNetworks);
    });
  }
  
  getOsNetworks(cbExecute, cb) {
    const osNetworks = OsNetworks.getArray();
    if('win32' === Os.platform()) {
      let pendings = 0;
      osNetworks.forEach((osNetwork) => {
        ++pendings;
        cbExecute(`netsh interface ip show addresses "${osNetwork.name}"`, (err, stdout) => {
          if(!err) {
            const lines = stdout.split('\r\n');
            for(let i = 0; i < lines.length; ++i) {
              const index = lines[i].indexOf(LoginDataGet.DHCP_ENABLED);
              if(-1 !== index) {
                osNetwork.dhcpEnabled = 'Yes' === lines[i].substring(index + LoginDataGet.DHCP_ENABLED_LENGTH).trim();
                break;
              }
            }
          }
          if(0 === --pendings) {
            cb(osNetworks);
          }
        });
        ++pendings;
        cbExecute(`netsh interface ip show ipaddresses "${osNetwork.name}"`, (err, stdout) => {
          if(!err) {
            const addresses = stdout.split('\r\n\r\n');
            addresses.pop();
            addresses.forEach((address) => {
              const addressStartIndex = address.indexOf(LoginDataGet.SHOW_IP_ADDRESSES_ADDRESS) + LoginDataGet.SHOW_IP_ADDRESSES_ADDRESS_LENGTH;
              if(-1 !== addressStartIndex) {
                const addressStopIndex = address.indexOf(' ', addressStartIndex);
                if(-1 !== addressStopIndex) {
                  const ipAddress = address.substring(addressStartIndex, addressStopIndex);
                  const dhcpAddress = -1 !== address.indexOf('Address Type       : Dhcp', addressStopIndex);
                  const foundAddress = osNetwork.addresses.find((osAddress) => {
                    return osAddress.address === ipAddress;
                  });
                  if(undefined !== foundAddress) {
                    foundAddress.dhcp = dhcpAddress;
                  }
                }
              }
            });
          }
          if(0 === --pendings) {
            cb(osNetworks);
          }
        });
      });
    }
    else { 
      process.nextTick(() => {
        cb(osNetworks);
      });
    }
  }
}


module.exports = LoginDataGet;
