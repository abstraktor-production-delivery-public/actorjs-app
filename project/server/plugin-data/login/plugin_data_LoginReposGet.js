
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class LoginReposGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const index = this.expectAsynchResponse();
    return this.asynchResponseSuccess(index, []);
  }
}

module.exports = PluginBaseMulti;
