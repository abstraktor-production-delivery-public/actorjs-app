
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const os = require('os');


class LoginPing extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.pendings = 0;
    this.results = [];
  }
  
  onRequest(chosenAddresses) {
    this._pingAddresses(chosenAddresses.realAddresses);
    this._pingAddresses(chosenAddresses.externalAddresses);
    this._pingAddresses(chosenAddresses.missingAddresses);
  }
  
  _pingAddresses(addresses) {
    addresses.forEach((address) => {
      ++this.pendings;
      this.asynchChildProcess(this._getPingCommand(address.address), (err, stdout) => {
        this.results.push([address.address, 
          {
            address: address,
            err: err,
            result: this._parseAnswer(stdout)
          }
        ]);
        if(0 === --this.pendings) {
          return this.expectAsynchResponseSuccess(this.results);          
        }
      });
    });
  }
  
  _getPingCommand(address) {
    if('win32' === os.platform()) {
      return `ping ${address}  -n 1 -w 1`;
    }
    else {
      return `ping ${address} -c 1 -W 1`;
    }
  }
  
  _parseAnswer(stdout) {
    if('win32' === os.platform()) {
      const resp = this._getLine(6, stdout).trim();
      const expectedResult = 'Packets: Sent = 1, Received = 1';
      return resp.startsWith(expectedResult);
    }
    else {
      const expectedResult = '1 packets transmitted, 1 received';
      return this._getLine(5, stdout).startsWith(expectedResult);
    }
  }
  
  _getLine(line, response) {
    let currentLine = 0;
    let indexStart = 0;
    let indexStop = response.indexOf('\n');
    while(++currentLine < line && indexStart < response.length) {
      indexStart = indexStop + 1;
      indexStop = response.indexOf('\n', indexStart);
    }
    return response.substring(indexStart, indexStop);
  }
}

module.exports = LoginPing;
