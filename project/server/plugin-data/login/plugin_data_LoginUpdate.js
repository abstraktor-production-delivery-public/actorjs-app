
'use strict';

const AddressCalculator = require('z-abs-funclayer-engine-server/server/address/address-calculator');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class LoginUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(labId, userId, repo, systemUnderTest, systemUnderTestInstance, nodes, forceLocalhost) {
    new AddressCalculator().calculateAndSave(labId, userId, systemUnderTest, systemUnderTestInstance, forceLocalhost, (loginReport, chosenAddresses) => {
      this.asynchWriteFileResponse(ActorPathGenerated.getStagingFile(), {
        labId,
        userId,
        repo,
        systemUnderTest,
        systemUnderTestInstance,
        nodes,
        forcedLocalhost: forceLocalhost
      }, true);
      this.asynchReadFileResponse(ActorPathGenerated.getNetworksFile());
      this.asynchWriteFileResponse(ActorPathGenerated.getStagingReport(), loginReport, true);
      this.asynchWriteFileResponse(ActorPathGenerated.getChosenAddresses(), chosenAddresses, true);
    });
  }
  
  _getOsNetworks() {
    const index = this.expectAsynchResponse();
    LoginHelper.getOsNetworks((command, cb) => {
      this.asynchChildProcess(command, (err, stdout) => {
        cb(err, stdout);
      });
    }, (osNetworks) => {
      return this.asynchResponseSuccess(index, osNetworks);    
    });
  }
}


module.exports = LoginUpdate;
