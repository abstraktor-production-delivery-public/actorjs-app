
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class LoginGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathGenerated.getStagingFile());
    this.asynchReadFileResponse(ActorPathGenerated.getNetworksFile());
    this.asynchReadFileResponse(ActorPathGenerated.getStagingReport());
    this.asynchReadFileResponse(ActorPathGenerated.getChosenAddresses());
  }
}


module.exports = LoginGet;
