
'use strict';

const Repos = require('./repos');
const ActorPathContent = require('z-abs-corelayer-server/server/path/actor-path-content');
const ActorContentPaths = require('z-abs-corelayer-server/server/path/paths/actor-content-paths');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorDataPaths = require('z-abs-corelayer-server/server/path/paths/actor-data-paths');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const GitSimple = require('simple-git');
const Fs = require('fs');
const Os = require('os');


class ReposClone  extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(type, url, newRepo) {
    const actorDataLocal = 'actorjs-data-local' === type;
    const dir = actorDataLocal ? ActorPathData.getActorDataLocalFolder() : ActorPathContent.getContentLocalFolder();
    if(!newRepo) {
      const dirParent = actorDataLocal ? ActorPathData.getReposLocalFolder() : ActorPathContent.getContentFolder();
      Fs.rm(dir, {recursive:true, force: true}, (err) => {
        if(err) {
          ddb.error('rm', dir, err);
          return this.expectAsynchResponseError(`Could not remove folder: '${dir}'.`);
        }
        const cloneOptions = 'win32' === Os.platform() ? ['--config','core.autocrlf=true'] : ['--config','core.autocrlf=input'];
        GitSimple(dirParent).clone(url, type, cloneOptions, (err, result) => {
          if(err) {
            ddb.error('git clone', err);
            this.expectAsynchResponseError(`Could not clone repo: '${url}'.`);
          }
          else {
            if(actorDataLocal) {
              const platformService = this.getService('platform');
              if(platformService) {
                platformService.verifyOrCreateFiles(() => {
                  this._updateReposFile(url, type);
                }, ['Server:ActorLayer/server-local', 'Server:StackLayer/server-local']);
              }
              else {
                this._updateReposFile(url, type);
              }
            }
            else {
              this._updateReposFile(url, type);
            }
          }
        });
      });
    }
    else {
      this._verifyOrCreateRepoFolder(dir, actorDataLocal, (err) => {
        if(err) {
          return this.expectAsynchResponseError(`'${dir}' must be a folder or none existing.`);
        }
        GitSimple(dir).init(['--initial-branch', 'main'], (err, result) => {
          if(err) {
            ddb.error('git init', err);
            return this.expectAsynchResponseError(`Could not git initialize folder: '${dir}'.`);
          }
          GitSimple(dir).addRemote('origin', url, (err, result) => {
            if(err) {
              ddb.error('git remote', err);
              return this.expectAsynchResponseError(`Could not set git remote folder: '${dir}'.`);
            }
            GitSimple(dir).add('.', (err) => {
              if(err) {
                ddb.error('git add', err);
                return this.expectAsynchResponseError(`Could not add pull folder: '${dir}'.`);
              }
              GitSimple(dir).commit('Initial Commit', (err) => {
                if(err) {
                  ddb.error('git commit', err);
                  this.expectAsynchResponseError(`Could not git commit folder: '${dir}'.`);
                }
                GitSimple(dir).push(['--set-upstream', 'origin', 'main'], (err) => {
                  if(err) {
                    ddb.error('git push', err);
                    this.expectAsynchResponseError(`Could not git push folder: '${dir}'.`);
                  }
                  else {
                    if(actorDataLocal) {
                      const platformService = this.getService('platform');
                      if(platformService) {
                        platformService.verifyOrCreateFiles(() => {
                          this._updateReposFile(url, type);
                        }, ['Server:ActorLayer/server-local', 'Server:StackLayer/server-local']);
                      }
                      else {
                        this._updateReposFile(url, type);
                      }
                    }
                    else {
                      this._updateReposFile(url, type);
                    }
                  }
                });
              });
            });
          });
        });
      });
    }
  }
  
  _updateReposFile(url, type) {
    const index = this.expectAsynchResponse();
    Repos.getReposFile(this.asynchReadFile.bind(this), (err, data) => {
      if(!err) {
        Repos.update('actorjs-content-local' === type ? url : null, 'actorjs-data-local' === type ? url : null, data, this.asynchWriteFile.bind(this), (err, data) => {
          if(err) {
            ddb.error('repos update', err);
            this.asynchResponseError(`Could not update repos.json file.`, index);
          }
          else {
            this.asynchResponseSuccess(index, data);
          }
        });
      }
      else {
        if('ENOENT' === err.code) {
          Repos.writeNew(null, url, this.asynchWriteFile.bind(this), (err, data) => {
            if(!err) {
              this.asynchResponseSuccess(index, data);
            }
            else {
              ddb.error('repos write', err);
              this.asynchResponseError(`Could not create repos.json file.`, index);
            }
          });
        }
        else {
          ddb.error('repos write', err);
          this.asynchResponseError(`Could not read repos.json file.`, index);
        }
      }
    });
  }
  
  _verifyOrCreateRepoFolder(dir, actorDataLocal, done) {
    if(actorDataLocal) {
      new ActorDataPaths().verifyOrCreate(done);
    }
    else {
      new ActorContentPaths().verifyOrCreate(done);
    }
  }
}

module.exports = ReposClone;
