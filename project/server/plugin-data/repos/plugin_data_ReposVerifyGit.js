
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Os = require('os');


class ReposVerifyGit extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.results = [];
  }
  
  onRequest() {
    const index = this.expectAsynchResponse();
    this.asynchChildProcess('git config --list', (err, stdout) => {
      if(!err) {
        const configs = stdout.split('\n');
        const userEmail = this._getConfig(configs, 'user.email');
        userEmail.expected = userEmail.value.length >= 1;
        userEmail.expectedValue = '! empty()';
        this.results.push(userEmail);
        const userName = this._getConfig(configs, 'user.name');
        userName.expected = userName.value.length >= 1;
        userName.expectedValue = '! empty()';
        this.results.push(userName);
        if('win32' === Os.platform()) {
          const coreAutocrlf = this._getConfig(configs, 'core.autocrlf');
          coreAutocrlf.expected = coreAutocrlf.value === 'true';
          coreAutocrlf.expectedValue = 'true';
          this.results.push(coreAutocrlf);
          const coreLongpaths = this._getConfig(configs, 'core.longpaths');
          coreLongpaths.expected = coreLongpaths.value === 'true';
          coreLongpaths.expectedValue = 'true';
          this.results.push(coreLongpaths);
        }
        else if('linux' === Os.platform()) {
          const coreAutocrlf = this._getConfig(configs, 'core.autocrlf');
          coreAutocrlf.expected = coreAutocrlf.value === 'input';
          coreAutocrlf.expectedValue = 'input';
          this.results.push(coreAutocrlf);
          const credentialHelper = this._getConfig(configs, 'credential.helper');
          credentialHelper.expected = credentialHelper.value.length >= 1;
          credentialHelper.expectedValue = '! empty(), only for https git repos.';
          this.results.push(credentialHelper);
        }
        else if('darwin' === Os.platform()) {
          const coreAutocrlf = this._getConfig(configs, 'core.autocrlf');
          coreAutocrlf.expected = coreAutocrlf.value === 'input';
          coreAutocrlf.expectedValue = 'input';
          this.results.push(coreAutocrlf);
          const credentialHelper = this._getConfig(configs, 'credential.helper');
          credentialHelper.expected = credentialHelper.value.length >= 1;
          credentialHelper.expectedValue = '! empty(), only for https git repos.';
          this.results.push(credentialHelper);
        }
        return this.asynchResponseSuccess(index, this.results);
      }
      else {
        this.asynchResponseError(stdout, index);
      }
    });
  }
  
  _getConfig(configs, findConfig) {
    const foundConfig = configs.find((config) => {
      return config.startsWith(findConfig);
    });
    return {
      name: findConfig,
      value: foundConfig ? foundConfig.substring(findConfig.length + 1) : '-',
      found: !!foundConfig,
      expected: false,
      expectedValue: ''
    };
  }
}

module.exports = ReposVerifyGit;
