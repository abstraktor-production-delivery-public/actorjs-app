
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Fs = require('fs');


class Repos {
  static writeNew(contentUrl, dataUrl, asynchWriteFile, done) {
    Fs.access(ActorPathData.getLocalReposFolder(), Fs.R_OK | Fs.W_OK, (err) => {
      if(err) {
        this._createFolder((err) => {
          if(err) {
            done(err);
          }
          else {
            this._createFile(contentUrl, dataUrl, asynchWriteFile, done);
          }
        });
      }
      else {
        this._createFile(contentUrl, dataUrl, asynchWriteFile, done);
      }
    });
  }
  
  static _update(repos, name, url) {
    const found = repos.find((repo) => {
      return name === repo.name;
    });
    if(found) {
      found.url = url;
    }
    else {
      repos.push({
        name: name,
        url: url
      });
    }
  }
  
  static update(contentUrl, dataUrl, repos, asynchWriteFile, done) {
    if(contentUrl) {
      this._update(repos.actorjsContentRepos, 'actorjs-content-local', contentUrl);
    }
    if(dataUrl) {
      this._update(repos.actorjsDataRepos, 'actorjs-data-local', dataUrl);
    }
    asynchWriteFile(ActorPathData.getLocalReposFile(), repos, done);
  }
  
  static _createFolder(done) {
    Fs.mkdir(ActorPathData.getLocalReposFolder(), done);
  }
  
  static _createFile(contentUrl, dataUrl, asynchWriteFile, done) {
    const repos = {
      actorjsContentRepos: [],
      actorjsDataRepos: []
    };
    if(contentUrl) {
      repos.actorjsContentRepos.push({
        name: 'actorjs-content-local',
        url: contentUrl
      });
    }
    if(dataUrl) {
      repos.actorjsDataRepos.push({
        name: 'actorjs-data-local',
        url: dataUrl
      });
    }
    asynchWriteFile(ActorPathData.getLocalReposFile(), repos, done);
  }
  
  static getReposFile(asynchReadFile, done) {
    Fs.access(ActorPathData.getLocalReposFile(), Fs.R_OK | Fs.W_OK, (err) => {
      if(err) {
        if('ENOENT' === err.code) {
          done(null);
        }
        else {
          done(err);
        }
      }
      else {
        asynchReadFile(ActorPathData.getLocalReposFile(), done);
      }
    });
  }
}


module.exports = Repos;
