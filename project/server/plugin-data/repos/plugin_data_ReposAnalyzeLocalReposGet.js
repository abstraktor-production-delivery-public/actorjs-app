
'use strict';

const Repos = require('./repos');
const ActorPathContent = require('z-abs-corelayer-server/server/path/actor-path-content');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const GitSimple = require('simple-git');
const Fs = require('fs');


class ReposAnalyzeLocalReposGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const index1 = this.expectAsynchResponse();
    const index2 = this.expectAsynchResponse();
    const index3 = this.expectAsynchResponse();
    let pendings = 3;
    let content = null;
    let data = null;
    let repos = null;
    this._analyzeRepo(ActorPathContent.getContentLocalFolder(), false, (err, isRepo, remote) => {
      if(!err) {
        content = {
          isRepo: isRepo,
          url: remote
        };
        if(0 === --pendings) {
          this._writeRepos(content, data, repos, index3);
        }
        this.asynchResponseSuccess(index1, content);
      }
      else {
        this.asynchResponseError(`Could not find out if ${ActorPathContent.getContentLocalFolder()} is a repo.`, index1);
      }
    });
    this._analyzeRepo(ActorPathData.getActorDataLocalFolder(), true, (err, isRepo, remote) => {
      if(!err) {
        data = {
          isRepo: isRepo,
          url: remote
        };
        if(0 === --pendings) {
          this._writeRepos(content, data, repos, index3);
        }
        this.asynchResponseSuccess(index2, data);
      }
      else {
        this.asynchResponseError(`Could not find out if ${ActorPathData.getActorDataLocalFolder()} is a repo.`, index2);
      }
    });
    Repos.getReposFile(this.asynchReadFile.bind(this), (err, data) => {
      if(!err) {
        repos = data;
        if(!data) {
          if(0 === --pendings) {
            this._writeRepos(content, data, repos, index3);
          }
        }
        else {
          this.asynchResponseSuccess(index3, data);
        }
      }
      else {
        this.asynchResponseError('Could not get repos.json.', index3);
      }
    });
  }
  
  _analyzeRepo(dir, dataRepo, done) {
    Fs.lstat(dir, (err, stat) => {
      if(err) {
        if('ENOENT' === err.code) {
          done(null, false);
        }
        else {
          done(err);
        }
      }
      else {
        if(!stat.isDirectory()) {
          return done(new Error('Is file.'));
        }
        GitSimple(dir).checkIsRepo((err, result) => {
          if(err) {
            done(err);
          }
          else {
            if(result) {
              GitSimple(dir).remote(['--verbose'], (err, resultRemote) => {
                if(!err) {
                  const results = resultRemote.split(/[\s\n]+/);
                  done(null, true, results[1]);
                }
                else {
                  done(err);
                }
              });
            }
            else {
              done(null, false);
            }
          }
        });
      }
    });
  }
  
  _writeRepos(content, data, repos, index3) {
    if(!repos) {
      if(data && data.isRepo) {
        Repos.writeNew(content.url, data.url, this.asynchWriteFile.bind(this), (err, data) => {
          if(!err) {
            this.asynchResponseSuccess(index3, data);
          }
          else {
            this.asynchResponseError(`Could not create repos.json file.`, index3);
          }
        });
      }
      else {
        this.asynchResponseSuccess(index3);
      }
    }
    else {
      Repos.update(content.url, data.url, repos, this.asynchWriteFile.bind(this), (err, data) => {
        if(!err) {
          this.asynchResponseSuccess(index3, data);
        }
        else {
          this.asynchResponseError(`Could not create repos.json file.`, index3);
        }
      });
    }
  }
}


module.exports = ReposAnalyzeLocalReposGet;
