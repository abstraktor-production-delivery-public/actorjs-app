
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ContentAudioUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getContentAudioGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getContentAudioLocalFile(), locals, false);
  }
}

module.exports = ContentAudioUpdate;
