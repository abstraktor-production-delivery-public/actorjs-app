
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ContentTextGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getContentTextGlobalFile());
    this.asynchReadFileResponse(ActorPathData.getContentTextLocalFile());
  }
}

module.exports = ContentTextGet;
