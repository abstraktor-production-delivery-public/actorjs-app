
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ContentVideoUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getContentVideoGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getContentVideoLocalFile(), locals, false);
  }
}

module.exports = ContentVideoUpdate;
