
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ContentOtherUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getContentOtherGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getContentOtherLocalFile(), locals, false);
  }
}

module.exports = ContentOtherUpdate;
