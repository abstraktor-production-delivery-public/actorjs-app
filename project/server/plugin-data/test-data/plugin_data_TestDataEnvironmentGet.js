
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestDataEnvironmentGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.statics = [
      {
        name: 'undefined',
        value: undefined,
        description: 'The value undefined. Cannot be part of a formatting.'
      },
      {
        name: 'null',
        value: null,
        description: 'The value null. Cannot be part of a formatting.'
      },
      {
        name: 'pipe',
        value: '|',
        description: `The value '|', pipe.`
      },
      {
        name: 'space',
        value: ' ',
        description: `The value ' ', space.`
      },
      {
        name: 'preActorNames',
        value: undefined,
        description: 'An Array containing the names of the Actors in the current Test Case´s pre phase.'
      },
      {
        name: 'preActorInstanceIndices',
        value: undefined,
        description: 'An Array containing the instance indices of the Actors in the current Test Case´s pre phase.'
      },
      {
        name: 'execActorNames',
        value: undefined,
        description: 'An Array containing the names of the Actors in the current Test Case´s exec phase.'
      },
      {
        name: 'execActorInstanceIndices',
        value: undefined,
        description: 'An Array containing the instance indices of the Actors in the current Test Case´s exec phase.'
      },
      {
        name: 'postActorNames',
        value: undefined,
        description: 'An Array containing the names of the Actors in the current Test Case´s post phase.'
      },
      {
        name: 'postActorInstanceIndices',
        value: undefined,
        description: 'An Array containing the instance indices of the Actors in the current Test Case´s post phase.'
      }
    ];
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getTestDataEnvironmentGlobalFile());
    this.asynchReadFileResponse(ActorPathData.getTestDataEnvironmentLocalFile());
    this.expectAsynchResponseSuccess(this.statics);
  }
}

module.exports = TestDataEnvironmentGet;
