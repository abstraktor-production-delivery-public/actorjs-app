
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestDataGeneralUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getTestDataGeneralGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getTestDataGeneralLocalFile(), locals, false);
  }
}

module.exports = TestDataGeneralUpdate;
