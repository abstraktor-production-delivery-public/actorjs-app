
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestDataGeneralGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathData.getTestDataGeneralGlobalFile());
    this.asynchReadFileResponse(ActorPathData.getTestDataGeneralLocalFile());
  }
}

module.exports = TestDataGeneralGet;
