
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestDataSystemUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getTestDataSystemGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getTestDataSystemLocalFile(), locals, false);
  }
}

module.exports = TestDataSystemUpdate;
