
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestDataEnvironmentUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(globals, locals) {
    this.asynchWriteFileResponse(ActorPathData.getTestDataEnvironmentGlobalFile(), globals, false);
    this.asynchWriteFileResponse(ActorPathData.getTestDataEnvironmentLocalFile(), locals, false);
  }
}

module.exports = TestDataEnvironmentUpdate;
