
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class ActorEditorFileInlineUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(url, content) {
    const params = url.split('/');
    if(7 !== params.length) {
      return this.expectAsynchResponseError(`Wrong format of the url: '${url}'.`);
    }
    else {
      params.splice(0, 2);
      const actorName = params.pop();
      this._getTestCase(...params, (testCase, err) => {
        if(err) {
          return this.responsePartError('Could not get the Test Case from file.');
        }
        else {
          const actor = testCase.tc.actors.find((actor) => {
            const name = url.replaceAll('/', '.');
            return name === `..${actor.name}.js`;
          });
          if(actor) {
            actor.inlineCode = content;
            this._saveTestCase(...params, testCase);
          }
          else {
            return this.responsePartError('Could not get the Actor in the Test Case file.');
          }
        }
      });
    }
  }
  
  _getTestCase(repoName, sutName, futName, tcName, done) {
    Fs.readFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), (err, data) => {
      if(err) {
        return done(undefined, err);
      }
      else {
        try {
          return done(JSON.parse(data));
        }
        catch(e) {
          return done(undefined, e);
        }
      }
    });
  }
  
  _saveTestCase(repoName, sutName, futName, tcName, testCase) {
    Fs.writeFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), JSON.stringify(testCase, null, 2), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the TestCase File: ' ${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)} '.`);
      }
      return this.responsePartSuccess();
    });
  }
}

module.exports = ActorEditorFileInlineUpdate;
