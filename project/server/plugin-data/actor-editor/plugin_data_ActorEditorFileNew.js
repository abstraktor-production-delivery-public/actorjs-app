
'use strict';

const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const StackComponentsHelpers = require('z-abs-funclayer-engine-server/server/stack/factory/stack-components-helpers');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Os = require('os');


class ActorEditorFileNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(path, name, type, templateName, dynamicTemplateName, dynamicTemplateName2) {
    let formatFunction = null;
    if(!dynamicTemplateName) {
      formatFunction = ActorEditorFileNew.templates.get(templateName);
      this._create(path, name, type, templateName, formatFunction);
    }
    else {
      StackComponentsFactory.getStacks((stacks) => {
        const stackTemplateFactory = StackComponentsFactory.createTemplatesActors(dynamicTemplateName);
        if(stackTemplateFactory) {
          const actorsTypes = new stackTemplateFactory().templates;
          const actorTemplate = actorsTypes.find((actorType) => {
            return actorType.name === templateName;
          });
          if(actorTemplate) {
            const formatIndex = actorTemplate.templateNames.findIndex((templateName) => {
              return templateName === dynamicTemplateName2;
            });
            if(-1 !== formatIndex) {
              formatFunction = actorTemplate.templates[formatIndex];
            }
            else {
              ddb.error('NO formatObject');
              return; //TODO: Error response
            }
          }
          else {
            ddb.error('NO actorTemplate');
            return; //TODO: Error response
          }
        }
        this._create(path, name, type, templateName, formatFunction);
      });
    }
  }
  
  _create(path, name, type, templateName, formatFunction) {
    const fileName = this._fileName(path, name, type, templateName);
    const actorEnding = ActorEditorFileNew.actorEndings.get(templateName);
    const text = formatFunction({
      actorEnding: actorEnding,
      name: this._formatName(name),
      actorType: templateName,
      ups: this._getUps(path)
    });
    const file = ActorPathData.getActorFile(`${path}/${fileName}`);
    this.asynchWriteTextFileResponse(file, text, fileName);
  }
  
  _formatNamePart(namePart) {
    if(0 === namePart.length) {
      return '';
    }
    else if(1 === namePart.length) {
      return `${namePart[0].toUpperCase()}`;
    }
    else {
      return `${namePart[0].toUpperCase()}${namePart.slice(1)}`;
    }
  }
  
  _formatName(name) {
    const nameParts = name.split('-');
    const newNameParts = nameParts.map((namePart) => {
      return this._formatNamePart(namePart);
    });
    return newNameParts.join('');
  }
  
  _fileName(path, name, type, templateName) {
    return `${name}${ActorEditorFileNew.actorEndings.get(templateName)}.${type}`;
  }
  
  _getUps(path) {
    return '../'.repeat(path.split('/').length - 1);
  }
}

ActorEditorFileNew.actorEndings = new Map([
  ['Originating', 'Orig'],
  ['Terminating', 'Term'],
  ['Intercepting', 'Inter'],
  ['Proxy', 'Proxy'],
  ['Condition', 'Cond'],
  ['Local', 'Local'],
  ['Sut', 'Sut'],
  ['ActorPartPre', 'Pre'],
  ['ActorPartPost', 'Post'],
  ['Msg', ''],
  ['Class [class js file]', ''],
  ['None [empty js file]', '']
]);

ActorEditorFileNew.templates = new Map([
  [
    'Originating',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.connection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.connection = this.createConnection('__stack_name__');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.connection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Terminating',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.connection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.connection = this.createServer('__stack_name__');
  }
  
  *run() {
    this.connection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.connection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Intercepting',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');
const __MessageSelector_object__ = require('__path_to_ MessageSelector_object__');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.connection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.connection = this.createServer('__stack_name__', {
      messageSelector: new __MessageSelector_object__(__message_selector_parameters__)
    });
  }
  
  *run() {
    this.connection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.connection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Proxy',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.clientConnection = null;
    this.serverConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.serverConnection = this.createServer('__stack_name__');
  }
  
  *initClient() {
    this.clientConnection = this.createConnection('__stack_name__');
  }
  
  *run() {
    this.serverConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.serverConnection);
    this.closeConnection(this.clientConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Condition',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
  }
  
  *data() {
  }
  
  *initClientPre() {
  }
  
  *runPre() {
  }
  
  *exitPre() {
  }
  
  *initClientPost() {
  }
  
  *runPost() {
  }
  
  *exitPost() {
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Local',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
  }
  
  *data() {
  }
  
  *run() {
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Sut',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.clientConnection = null;
    this.serverConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.serverConnection = this.createServer('__stack_name__');
  }
  
  *initClient() {
    this.clientConnection = this.createConnection('__stack_name__');
  }
  
  *run() {
    this.serverConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.serverConnection);
    this.closeConnection(this.clientConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'ActorPartPre',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.ActorPartPre {
  constructor() {
    super();
  }
  
  *run() {
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'ActorPartPost',
    StackComponentsHelpers._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.ActorPartPost {
  constructor() {
    super();
  }
  
  *run() {
  }
}

module.exports = ${'name'}${'actorEnding'};
`
  ],
  [
    'Msg',
    StackComponentsHelpers._template``
  ],
  [
    'Class [class js file]',
    StackComponentsHelpers._template`

class ${'name'} {
  constructor() {
    
  }
}

module.exports = ${'name'};
`
  ],
  [
    'None [empty js file]',
    StackComponentsHelpers._template``
  ]
]);


module.exports = ActorEditorFileNew;
