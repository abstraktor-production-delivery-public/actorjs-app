
'use strict';

const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorStacksGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const templatesMap = [];
    StackComponentsFactory.getStacks((stacks) => {
      stacks.forEach((stack) => {
        let foundTemplates = false;
        const actorStackTemplateFactory = StackComponentsFactory.createTemplatesActors(stack.name);
        if(actorStackTemplateFactory) {
          try {
            const actorsTypesMap = [];
            const actorsTypes = new actorStackTemplateFactory().templates;
            if(actorsTypes) {
              actorsTypes.forEach((actorsType) => {
                foundTemplates = foundTemplates || 0 !== actorsType.templateNames.length;
                actorsTypesMap.push([
                  actorsType.name,
                  {
                    names: actorsType.templateNames,
                    markupNodes: actorsType.markupNodes,
                    markupStyles: actorsType.markupStyles,
                    markups: actorsType.markups
                  }
                ]);
              });
              if(foundTemplates) {
                templatesMap.push([
                  stack.name,
                  actorsTypesMap
                ]);
              }
            }
          }
          catch(e) {
            ddb.error(`Failing to load stack '${stack.name}' for Stack Templates.`, e);
          }
        }
      });
      this.expectAsynchResponseSuccess(templatesMap);
    });
  }
}

module.exports = ActorEditorStacksGet;
