
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
//const Browserify = require('browserify');
//const { JSHINT } = require('jshint');
const Vm = require('vm');
const Fs = require('fs');
const Path = require('path');


class ActorEditorBuild extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(content, path, file) {
    const indexVmScript = this.expectAsynchResponse();
    const indexBundle = this.expectAsynchResponse();
    const indexJsHint = this.expectAsynchResponse();
    try {
      const script = new Vm.Script(content, {filename:file});
      this.asynchResponseSuccess(indexVmScript, {check:'success'});
    }
    catch(err) {
      this.asynchResponseSuccess(indexVmScript, {check: 'failure', err: this._parseVmError(err)});
    }
    this.asynchResponseSuccess(indexBundle, {check:'success'});
    this.asynchResponseSuccess(indexJsHint, {check:'success'});
    /*let bundleResponseSent = false;
    const newContent = this._updateRequire(content, path, file);
    this._createTempBundleFile(newContent, file, (err, tempFileName) => {
      if(err) {
        this.asynchResponseError(err.message, indexBundle);
      }
      else {
        const browserify = Browserify(tempFileName);
        const x = browserify.bundle((errB, buf) => {
          if(errB) {
            if(!bundleResponseSent) {
              const bundleError = this._parseBundle(errB.message, newContent, file);
              if(null !== bundleError) {
                this.asynchResponseSuccess(indexBundle, {check:'failure', err: bundleError});
              }
              else {
                this.asynchResponseSuccess(indexBundle, {check:'success'});
              }
              bundleResponseSent = true;
            }
          }
          else {
            if(!bundleResponseSent) {
              this.asynchResponseSuccess(indexBundle, {check:'success'});
              bundleResponseSent = true;
            }
          }
        });
      }
    }); 
    JSHINT(content, {node: true, esversion: 11, noyield: true});
    if(0 === JSHINT.errors.length) {
      this.asynchResponseSuccess(indexJsHint, {check:'success'});
    }
    else {
      const jsHintErrors = this._parseJsHint(JSHINT.errors, file);
      this.asynchResponseSuccess(indexJsHint, {check:'failure', errors: jsHintErrors});
    }*/
  }
  
  _createTempBundleFile(content, file, cb) {
    const id = GuidGenerator.create();
    const tempFileName = `${ActorPath.getActorTmpPath()}${Path.sep}${id}_${file}`;
    Fs.writeFile(tempFileName, content, (err) => {
      if(err) {
        cb(err);
      }
      else {
        cb(undefined, tempFileName);
      }
    });
  }
  
  _updateRequire(content, path, file) {
    let startIndex = content.indexOf("require('");
    let stopIndex = content.indexOf("');", startIndex);
    const foundStackNames = [];
    const reqLen = "require('".length;
    while(-1 !== startIndex && -1 !== stopIndex) {
      const stackName = content.substring(startIndex + reqLen, stopIndex);
      foundStackNames.push({name: stackName, start: startIndex + reqLen, stop: stopIndex});
      startIndex = content.indexOf("require('", stopIndex);
      stopIndex = content.indexOf("');", startIndex);
    }
    startIndex = 0;
    let newContent = '';
    for(let i = 0; i < foundStackNames.length; ++i) {
      const stackObject = foundStackNames[i];
      newContent += content.substring(startIndex, stackObject.start);
      if(stackObject.name.endsWith('-stack-api')) {
        newContent += stackObject.name;
      }
      else if(stackObject.name.endsWith('actor-api') || stackObject.name.endsWith('stack-api')) {
        newContent += stackObject.name;
      }
      else if(stackObject.name.startsWith('.')) {
        const newPath = Path.normalize(`${ActorPathData.getActorFile(path)}/${stackObject.name}`).replace(new RegExp('[/\\\\]', 'g'), '/');
        newContent += newPath;
      }
      else {
        newContent += stackObject.name;
      }
      startIndex = stackObject.stop;
    }
    newContent += content.substring(startIndex, content.length - 1);
    return newContent;
  }
  
  _parseVmError(err) {
    const msg = err.stack;
    const lineBrIndex = msg.indexOf('\n');
    if(-1 !== lineBrIndex) {
      const firstLine = msg.substring(0, lineBrIndex);
      const parts = firstLine.split(':');
      return {
        file: parts[0].trim(),
        msg: err.message,
        line: Number.parseInt(parts[1].trim())
      };
    }
    return {
      file: 'unknown',
      msg: err.message,
      line: -1
    };
  }
  
  _parseBundle(err, content, file) {
    if(err.startsWith('Cannot find module')) {
      const first = err.indexOf("'");
      if(-1 !== first) {
        const second = err.indexOf("'", first + 1);
        if(-1 !== second) {
          let current = 0;
          let lineNbr = 1;
          const searchIndex = content.indexOf(err.substring(first + 1, second));
          if(-1 !== searchIndex) {
            while(true) {
              const lineBr = content.indexOf('\n', current);
              if(searchIndex <= lineBr) {
                break;
              }
              else {
                current = lineBr + 1;
                ++lineNbr;
              }
            }
            return {
              file: file,
              msg: err.substring(0, second + 1),
              line: lineNbr
            };
          }
        }
      }
    }
    return null;
  }
  
  _parseJsHint(errors, file) {
    const responseErrors = [];
    errors.forEach((error) => {
      responseErrors.push({
        file: file,
        msg: error.reason,
        line: error.line
      });
    });
    return responseErrors;
  }
}

module.exports = ActorEditorBuild;
