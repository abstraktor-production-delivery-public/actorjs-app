
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorProjectToggle extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(content, path) {
    this.asynchWriteFileResponse(ActorPathGenerated.getActorsProjectFile(path), content, false);
  }
}

module.exports = ActorEditorProjectToggle;
