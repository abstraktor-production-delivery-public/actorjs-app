
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorFolderUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(oldPath, newPath) {
    this.asynchRenameResponse(ActorPathData.getActorFile(oldPath), ActorPathData.getActorFile(newPath));
  }
}


module.exports = ActorEditorFolderUpdate;
