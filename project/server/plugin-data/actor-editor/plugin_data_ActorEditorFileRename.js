
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class ActorEditorFileRename extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(oldPath, newPath) {
    Fs.access(ActorPathData.getActorFile(newPath), Fs.F_OK, (err) => {
      if(err && 'ENOENT' === err.code) {
        this.asynchRenameResponse(ActorPathData.getActorFile(oldPath), ActorPathData.getActorFile(newPath));
      }
      else {
        this.expectAsynchResponseError(`Path '${newPath}' does already exist.`);
      }
    });
  }
}



module.exports = ActorEditorFileRename;
