
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorFileUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.Update);
  }
  
  onRequest(file, data) {
    this.asynchWriteTextFileResponse(ActorPathData.getActorFile(file), data);
  }
}

module.exports = ActorEditorFileUpdate;
