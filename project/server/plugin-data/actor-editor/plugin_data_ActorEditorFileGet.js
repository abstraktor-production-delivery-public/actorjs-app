
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(file) {
    this.asynchReadTextFileResponse(ActorPathData.getActorFile(file));
  }
}

module.exports = ActorEditorFileGet;
