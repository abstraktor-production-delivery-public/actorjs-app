
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class ActorEditorFolderDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(file) {
    this.asynchRmdirResponse(ActorPathData.getActorFile(file), true, true);
    this.asynchRmdirResponse(ActorPathDist.getActorFile(file), true, true);
  }
}


module.exports = ActorEditorFolderDelete;
