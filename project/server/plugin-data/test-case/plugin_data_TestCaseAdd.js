
'use strict';

const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestCaseAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repoName, sutName, futName, tcName, description, dataTestCase, stackName, templateName, testData) {
    const testCase = {
      name: tcName,
      description: description,
      status: 'visible',
      tc: dataTestCase
    }
    const responsePart = this._verifyInput(testCase);
    if(!responsePart) {
      return responsePart; // ???
    }
    if(stackName && 'none' !== stackName) {
      StackComponentsFactory.getStacks((stacks) => {
        const testCaseTemplateFactory = StackComponentsFactory.createTemplatesTestCases(stackName);
        if(testCaseTemplateFactory) {
          const templates = new testCaseTemplateFactory().templates;
          const template = templates.get(templateName);
          if(template) {
            template.createTestCase(testCase.tc, testData, repoName, sutName, futName, tcName);
            return this._saveTestCase(repoName, sutName, futName, tcName, testCase);
          }
        }
        else {
          this._saveTestCase(repoName, sutName, futName, tcName, testCase);
        }
      });
    }
    else {
      this._saveTestCase(repoName, sutName, futName, tcName, testCase);
    }
  }
  
  _saveTestCase(repoName, sutName, futName, tcName, testCase) {
    Fs.mkdir(ActorPathData.getTestCaseFolder(repoName, sutName, futName, tcName), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the Test Case Folder: ' ${ActorPathData.getTestCaseFolder(repoName, sutName, futName, tcName)} '.`);
      }
      Fs.writeFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), JSON.stringify(testCase, null, 2), (err) => {
        if(err) {
          return this.responsePartError(`Could not create the TestCase File: ' ${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)} '.`);
        }
        return this.responsePartSuccess();
      });
    });
  }
  
  _verifyInput(testCase) {
    const name = testCase.name;
    if(0 === name.length) {
      return this.responsePartError(`The Test Casename: '' must not be empty.`);
    }
    const first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A Test Case name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
}

module.exports = TestCaseAdd;
