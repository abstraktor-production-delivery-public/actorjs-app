
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseActorsFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(actorFiles) {
    actorFiles.forEach((file) => {
      if(file.startsWith('./Actors-inline')) {
        this.expectAsynchResponseSuccess(null);
      }
      else {
        this.asynchReadTextFileResponse(ActorPathData.getActorFile(file));
      }
    });
  }
}


module.exports = TestCaseActorsFileGet;
