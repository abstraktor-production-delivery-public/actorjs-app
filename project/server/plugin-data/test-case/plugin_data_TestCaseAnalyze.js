
'use strict';

const TestCaseLoader = require('z-abs-funclayer-engine-server/server/engine/test-case-loader');
const Logger = require('z-abs-corelayer-server/server/log/logger');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseAnalyze extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.analyzeActor = [];
    this.analyze = null;
  }
  
  onRequest(repoName, sutName, futName, tcName, sutInstance) {
    TestCaseLoader.load(repoName, sutName, futName, tcName, sutInstance, [], false, {slow:false,asService:false}, this.cbMessage, (err, testCase) => {
      if(err) {
        LOG_ERROR(Logger.ERROR.CATCH, this.constructor.name, err);
        return this.responsePartError(`Could not find the Test Case: '${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)}'`);
      }
      else {
        let mocked = false;
        let analyzedTestDatas = [];
        mocked = this.analyzeTestData(analyzedTestDatas, 0, testCase.actors.preActors, mocked);
        mocked = this.analyzeTestData(analyzedTestDatas, 1, testCase.actors.execActors, mocked);
        this.analyzeTestData(analyzedTestDatas, 2, testCase.actors.postActors, mocked);
        
        let analyzedDependencies = [];
        this.analyzeDependencies(analyzedDependencies, 0, testCase.actors.preActors);
        this.analyzeDependencies(analyzedDependencies, 1, testCase.actors.execActors);
        this.analyzeDependencies(analyzedDependencies, 2, testCase.actors.postActors);
        
        
        this.expectAsynchResponseTemp();
        this.expectAsynchResponseSuccess(analyzedTestDatas);
        this.expectAsynchResponseSuccess(analyzedDependencies);
        this.expectAsynchResponseSuccess(new Date(Date.now()).toUTCString());
        this.unExpectAsynchResponseTemp();
      }
    });
  }
  
  fakeFunctions(actor) {
    actor.logEngine = () => {};
    actor.logDebug = () => {};
    actor.logError = () => {};
    actor.logWarning = () => {};
    actor.logIp = () => {};
    actor.logGui = () => {};
    actor.logVerifySuccess = () => {};
    actor.logVerifyFailure = () => {};
    actor.logTestData = () => {};
    actor.logBrowserLog = () => {};
    actor.logBrowserErr = () => {};
          
    actor.getContent = function* gen() {};
    actor.getContentByName = function* gen() {};
    
    actor.setSharedData = function* gen() {};
    actor.setSharedDataSync = () => {};
    actor.getSharedData = () => {};
    actor.waitForSharedData = function* gen() {};
    actor.setSharedDataActor = function* gen() {};
    actor.setSharedDataActorSync = () => {};
    actor.getSharedDataActor = () => {};
    actor.waitForSharedDataActor = function* gen() {};
  }
  
  mockTestDataActorApi(actor) {
    this.getTestDataStringOriginal = actor.getTestDataString.bind(actor);
    this.getTestDataStringsOriginal = actor.getTestDataStrings.bind(actor);
    this.getTestDataArrayStringsOriginal = actor.getTestDataArrayStrings.bind(actor);
    this.getTestDataNumberOriginal = actor.getTestDataNumber.bind(actor);
    this.getTestDataNumbersOriginal = actor.getTestDataNumbers.bind(actor);
    this.getTestDataArrayNumbersOriginal = actor.getTestDataArrayNumbers.bind(actor);
    this.getTestDataBooleanOriginal = actor.getTestDataBoolean.bind(actor);
    this.getTestDataBooleansOriginal = actor.getTestDataBooleans.bind(actor);
    this.getTestDataArrayBooleansOriginal = actor.getTestDataArrayBooleans.bind(actor);
    this.getTestDataObjectOriginal = actor.getTestDataObject.bind(actor);
  
    this._getTestDataOriginal = actor._getTestData.bind(actor);
    this.testDataActorGetOriginal = actor.testDataActor.get.bind(actor.testDataActor);
    this.testDataIterationGetOriginal = actor._getTestDataIteration.bind(actor);
    
    actor.getTestDataString = this.getTestDataString.bind(this);
    actor.getTestDataStrings = this.getTestDataStrings.bind(this);
    actor.getTestDataArrayStrings = this.getTestDataArrayStrings.bind(this);
    actor.getTestDataNumber = this.getTestDataNumber.bind(this);
    actor.getTestDataNumbers = this.getTestDataNumbers.bind(this);
    actor.getTestDataArrayNumbers = this.getTestDataArrayNumbers.bind(this);
    actor.getTestDataBoolean = this.getTestDataBoolean.bind(this);
    actor.getTestDataBooleans = this.getTestDataBooleans.bind(this);
    actor.getTestDataArrayBooleans = this.getTestDataArrayBooleans.bind(this);
    actor.getTestDataObject = this.getTestDataObject.bind(this);

    actor._getTestData = this._getTestData.bind(this);
    actor.testDataActor.get = this.testDataActorGet.bind(this);
    actor._getTestDataIteration = this.testDataIterationGet.bind(this);
  }
      
  mockTestDataSystem(actor) {
    this.testDataTestCasesGetOriginal = actor.tcData.testDatasTestCase.get.bind(actor.tcData.testDatasTestCase);
  
    this.testDataGeneralGlobalGetOriginal = actor.executionContext.testData.generalTestDataGlobal.get.bind(actor.executionContext.testData.generalTestDataGlobal);
    this.testDataGeneralLocalGetOriginal = actor.executionContext.testData.generalTestDataLocal.get.bind(actor.executionContext.testData.generalTestDataLocal);
    this.testDataLogGlobalGetOriginal = actor.executionContext.testData.logTestDataGlobal.get.bind(actor.executionContext.testData.logTestDataGlobal);
    this.testDataLogLocalGetOriginal = actor.executionContext.testData.logTestDataLocal.get.bind(actor.executionContext.testData.logTestDataLocal);
    this.testDataSystemGlobalGetOriginal = actor.executionContext.testData.systemTestDataGlobal.get.bind(actor.executionContext.testData.systemTestDataGlobal);
    this.testDataSystemLocalGetOriginal = actor.executionContext.testData.systemTestDataLocal.get.bind(actor.executionContext.testData.systemTestDataLocal);
    this.testDataEnvironmentGlobalGetOriginal = actor.executionContext.testData.environmentTestDataGlobal.get.bind(actor.executionContext.testData.environmentTestDataGlobal);
    this.testDataEnvironmentLocalGetOriginal = actor.executionContext.testData.environmentTestDataLocal.get.bind(actor.executionContext.testData.environmentTestDataLocal);
    this.testDataEnvironmentStaticGetOriginal = actor.executionContext.testData.environmentTestDataStatic.get.bind(actor.executionContext.testData.environmentTestDataStatic);
    
    actor.tcData.testDatasTestCase.get = this.testDataTestCasesGet.bind(this);

    actor.executionContext.testData.generalTestDataGlobal.get = this.testDataGeneralGlobalGet.bind(this);
    actor.executionContext.testData.generalTestDataLocal.get = this.testDataGeneralLocalGet.bind(this);
    actor.executionContext.testData.logTestDataGlobal.get = this.testDataLogGlobalGet.bind(this);
    actor.executionContext.testData.logTestDataLocal.get = this.testDataLogLocalGet.bind(this);
    actor.executionContext.testData.systemTestDataGlobal.get = this.testDataSystemGlobalGet.bind(this);
    actor.executionContext.testData.systemTestDataLocal.get = this.testDataSystemLocalGet.bind(this);
    actor.executionContext.testData.environmentTestDataGlobal.get = this.testDataEnvironmentGlobalGet.bind(this);
    actor.executionContext.testData.environmentTestDataLocal.get = this.testDataEnvironmentLocalGet.bind(this);      
    actor.executionContext.testData.environmentTestDataStatic.get = this.testDataEnvironmentStaticGet.bind(this);
  }
  
  
  analyzeTestData(analyzedTestDatas, phaseId, phaseActors, mocked) {
    phaseActors.actors.forEach((actor) => {
      if(actor.__mocked) {
        return;
      }
      actor.__mocked = true;
      
      this.analyzeActor = [];
      analyzedTestDatas.push([actor.name, this.analyzeActor]);

      this.fakeFunctions(actor);
      this.mockTestDataActorApi(actor);
      if(!mocked) {
        this.mockTestDataSystem(actor);
         mocked = true;
      }
      
      if(undefined !== actor.data) {
        const generator = actor.data();
        let result = {
          value: undefined,
          done: false
        };
        while(!result.done) {
          result = generator.next();
        }
      }
    });
    return mocked;
  }

  analyzeDependencies(analyzedDependencies, phaseId, phaseActors) {
    phaseActors.actors.forEach((actor) => {
      const index = actor.getIndexFromPhase(phaseId);
      analyzedDependencies.push([
        `${actor.name}_${index}`,
        {
          index: index,
          phaseId: phaseId,
          dependencies: actor.getDependenciesFromPhase(phaseId)
        }
      ]);
    });
  }
    
  getTestDataString(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, 'string');
    try {
      return this.analyze.value = this.getTestDataStringOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataStrings(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[string]');
    try {
      let value = this.analyze.value = this.getTestDataStringsOriginal(key, defaultValue, formatter);
      if(!Array.isArray(value)) {
        this.analyze.result = 'The value is not a [string]';
      }
      return value;
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataArrayStrings(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[[string]]');
    try {
      let value = this.analyze.value = this.getTestDataArrayStringsOriginal(key, defaultValue, formatter);
      if(!Array.isArray(value)) {
        this.analyze.result = 'The value is not a [[string]]';
      }
      else {
        for(let i = 0; i < value.length; ++i) {
          if(!Array.isArray(value[i])) {
            this.analyze.result = 'The value is not a [[string]]';
            return value;
          }
        }
      }
      //this.analyze.raw
      return value;
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataNumber(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, 'number');
    try {
      return this.analyze.value = this.getTestDataNumberOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataNumbers(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[number]');
    try {
      return this.analyze.value = this.getTestDataNumbersOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataArrayNumbers(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[[number]]');
    try {
      return this.analyze.value = this.getTestDataArrayNumbersOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataBoolean(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, 'boolean');
    try {
      return this.analyze.value = this.getTestDataBooleanOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataBooleans(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[boolean]');
    try {
      return this.analyze.value = this.getTestDataBooleansOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataArrayBooleans(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, '[[boolean]]');
    try {
      return this.analyze.value = this.getTestDataArrayBooleansOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }
  
  getTestDataObject(key, defaultValue, formatter) {
    this._createTestDataAnalyze(key, defaultValue, 'object');
    try {
      return this.analyze.value = this.getTestDataObjectOriginal(key, defaultValue, formatter);
    }
    catch(err) {
      this.analyze.result = err.message;
    }
  }

  _getTestData(key, defaultValue) {
    if(1 === ++this.analyze.depth) {
      return this._getTestDataOriginal(key, defaultValue);
    }
    else {
      this._createTestDataAnalyze(key, defaultValue, 'format', this.analyze.depth);
      let value = this._getTestDataOriginal(key, defaultValue);
      this.analyze = this.analyze.parent;
      return value;
    }
  }
  
  _createTestDataAnalyze(key, defaultValue, type, depth = 0) {
    let analyze = {
      result: 'ok',
      name: key,
      type: type,
      value: undefined,
      layer: undefined,
      default: defaultValue,
      raw: undefined,
      children: [],
      parent: null,
      depth: depth,
      toJSON() {
        return {
          result: this.result,
          name: this.name,
          type: this.type,
          value: this.value,
          layer: this.layer,
          default: this.default,
          raw: this.raw,
          children: this.children
        };
      }
    };
    if(0 === depth) {
      this.analyze = analyze;
      this.analyzeActor.push(analyze);
    }
    else if(1 === depth) {}
    else {
      analyze.parent = this.analyze;
      this.analyze.children.push(analyze);
      this.analyze = analyze;
    }
  }
  
  testDataActorGet(key) {
    let value = this.testDataActorGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'actor';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataIterationGet(key) {
    let value = this.testDataIterationGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'actors';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataTestCasesGet(key) {
    let value = this.testDataTestCasesGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'test case';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataGeneralGlobalGet(key) {
    let value = this.testDataGeneralGlobalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'general global';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataGeneralLocalGet(key) {
    let value = this.testDataGeneralLocalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'general local';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataLogGlobalGet(key) {
    let value = this.testDataLogGlobalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'log global';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataLogLocalGet(key) {
    let value = this.testDataLogLocalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'log local';
      this.analyze.raw = value.value;
    }
    return value;
  }

  testDataSystemGlobalGet(key) {
    let value = this.testDataSystemGlobalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'system global';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataSystemLocalGet(key) {
    let value = this.testDataSystemLocalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'system local';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
 
  testDataEnvironmentGlobalGet(key) {
    let value = this.testDataEnvironmentGlobalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'environment global';
      this.analyze.raw = value.value;
    }
    return value;
  }
  
  testDataEnvironmentLocalGet(key) {
    let value = this.testDataEnvironmentLocalGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'environment local';
      this.analyze.raw = value.value;
    }
    return value;
  }

  testDataEnvironmentStaticGet(key) {
    let value = this.testDataEnvironmentStaticGetOriginal(key);
    if(undefined !== value) {
      this.analyze.layer = 'system static';
      this.analyze.raw = value.value;
    }
    return value;
  }
}

module.exports = TestCaseAnalyze;
