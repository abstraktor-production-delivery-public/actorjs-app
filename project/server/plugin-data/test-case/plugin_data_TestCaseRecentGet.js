
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class TestCaseRecentGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    this.asynchReadFileResponse(ActorPathGenerated.getTestCasesRecentFile());
  }
}

module.exports = TestCaseRecentGet;
