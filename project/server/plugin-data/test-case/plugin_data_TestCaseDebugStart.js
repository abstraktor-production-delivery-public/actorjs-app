
'use strict';

const Debug = require('./debug');
const MessageTestCaseDebugActorIndexOk = require('z-abs-funclayer-engine-server/server/communication/messages/messages-s-to-w/message-test-case-debug-actor-index-ok');
const MessageTestCaseDebugPaused = require('z-abs-funclayer-engine-server/server/communication/messages/messages-s-to-c/message-test-case-debug-paused');
const Debugger = require('z-abs-funclayer-engine-server/server/debug/debugger');
const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const ActionRequest = require('z-abs-corelayer-cs/clientServer/communication/action-request');
const Path = require('path');
const chromeRemoteInterface = require('chrome-remote-interface');


class TestCaseDebugStart extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.actorIndex = -1;
    this.client = null;
    this.dbugger = new Debugger();
    this.processStarted = false;
    this.logFileLoaded = false;
    this.testCaseDebugRunStarted = false;
    this.breakOnEnterRun = false;
    this.breakOnLeaveRun = false;
    this.debugType = 0; // Local
    this.runBreakpoints = [];
    this.resume = true;
    this.stepOver = false;
    this.stepOverBreakpoints = [];
    this.stepInto = false;
    this.stepOut = false;
    this.stepOutBreakpoints = [];
    this.pause = false;
    this.currentStack = [];
    this.previousStack = [];
    this.previousLocation = {};
  }
  
  onRequest(repoName, sutName, futName, tcName, sutInstance, nodes, config) {
    this.breakOnEnterRun = config.breakOnEnterRun;
    this.breakOnLeaveRun = config.breakOnLeaveRun;
    this.runBreakpoints = config.breakOnLines;
    this.debugType = config.debugType;
    this.runBreakpoints.forEach((runBreakpoint) => {
      runBreakpoint.url = Debugger.calculateBreakpointUrl(sutName, futName, tcName, runBreakpoint);
    });
    this.setSessionData('debugger', this.dbugger);
    this._startWorkerProcess(repoName, sutName, futName, tcName, sutInstance, config, (port) => {
      this._readDebuggerFile(sutName, futName, tcName, port);
    });
  }
  
  onMessageFromClient(msg) {
    this._onMessageFromClient(msg);
  }
  
  async _onMessageFromClient(msg) {
    if(AppProtocolConst.TEST_CASE_DEBUG_CONTINUE === msg.msgId) {
      this.resume = true;
      await this.client.Debugger.resume();
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_STEP_OVER === msg.msgId) {
      this.stepOverBreakpoints = await this._getPossibleBreakpointsStepOver();
      this.stepOver = true;
      await this.client.Debugger.stepOver();
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_STEP_IN === msg.msgId) {
      this.stepInto = true;
      await this.client.Debugger.stepInto();
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_STEP_OUT === msg.msgId) {
      this.stepOutBreakpoints = await this._getPossibleBreakpointsStepOut();
      this.stepOut = true;
      await this.client.Debugger.stepOut();
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_PAUSE === msg.msgId) {
      this.pause = true;
      await this.client.Debugger.pause();
    }
  }
  
  onMessageFromWorker(msg, dataBuffers, debug, cbMessage) {
    if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId && msg.last) {
      this.onClose(() => {
        cbMessage(msg, dataBuffers, debug);
        this.clearSession(() => {});
      });
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_ACTOR_INDEX === msg.msgId) {
      this.actorIndex = msg.actorIndex;
      this.sendMessageToWorkerProcess(new MessageTestCaseDebugActorIndexOk(this.requestData.sessionId, this.actorIndex));
    }
    else {
      cbMessage(msg, dataBuffers, debug);
    }
  }
  
  async close(done) {
    if(this.client) {
      await this.client.close();
      done();
    }
    else {
      done();
    }
  }
  
  onClose(done) {
    this.close(done);
  }
  
  _startWorkerProcess(repoName, sutName, futName, tcName, sutInstance, config, done) {
    this.expectAsynchResponseTemp();
    this.forkWorkerProcess('server-test-case', [`test-case${Path.sep}plugin_data_TestCaseDebugLoad.js`, `test-case${Path.sep}plugin_data_TestCaseDebugRun.js`], true, ['127.0.0.1', false, this.debugType], (pid, port) => {
      this._loadTestCase(repoName, sutName, futName, tcName, sutInstance, config, port);
      this.unExpectAsynchResponseTemp();
      done(port);
    });
  }
  
  _loadTestCase(repoName, sutName, futName, tcName, sutInstance, config, port) {
    const indexDebugLoad = this.expectAsynchResponse();
    const testCaseDebugLoadRequest = new ActionRequest();
    testCaseDebugLoadRequest.add('TestCaseDebugLoad', 0, repoName, sutName, futName, tcName, sutInstance, config);
    this.sendRequestToWorkerProcess(testCaseDebugLoadRequest, (response) => {
      const result = response.responses[0].result;
      if('TestCaseDebugLoad' === response.responses[0].name && 'success' === result.code) {
        this.processStarted = true;
        this.startDebugger(port);
        this.asynchResponseSuccess(indexDebugLoad);
      }
      else {
        this.asynchResponseError(result.msg, indexDebugLoad);
        this.clearSession();
      }
    });
  }
  
  _readDebuggerFile(sutName, futName, tcName, port) {
    const file = ActorPathGenerated.getTestCaseDebuggerFile(sutName, futName, tcName);
    this.asynchReadFile(file, (err, dbuggerData) => {
      if(!err) {
        this.dbugger.setBreakpoints(dbuggerData.breakpoints);
        this.logFileLoaded = true;
        this.startDebugger(port);
      }
      else if('ENOENT' === err.code) {
        const parentPath = file.substring(0, file.lastIndexOf(Path.sep));
        this.asynchMkdir(parentPath, (err) => {
          if(!err) {
            this.logFileLoaded = true;
            this.startDebugger(port);
          }
          else {
            this.expectAsynchResponseError(`Could not read dbugger file '${file}'.`);
          }
        }, true);
      }
      else {
        this.expectAsynchResponseError(`Could not read dbugger file '${file}'.`);
      }
    });
  }
  
  async _enable() {
    try {
      await this.client.Debugger.enable();
      const breakpoints = this.dbugger.getBreakpoints();
      for(let i = 0; i < breakpoints.length; ++i) {
        const breakpoint = breakpoints[i];
        if(breakpoint.pauseOnBreak) {
          await Debug._getPossibleBreakpoints(this.client, this.dbugger, breakpoint);
          const result = await this.client.Debugger.setBreakpointByUrl(breakpoint);
          breakpoint.breakpointId = result.breakpointId;
        }
      }
      if(this.breakOnEnterRun || this.breakOnLeaveRun) {
        for(let i = 0; i < this.runBreakpoints.length; ++i) {
          const runBreakpoint = this.runBreakpoints[i];
          if(!this.dbugger.findbreakpointInBreakpoints(runBreakpoint)) {
            await Debug._getPossibleBreakpoints(this.client, this.dbugger, runBreakpoint);
            const result = await this.client.Debugger.setBreakpointByUrl(runBreakpoint);
            this.dbugger.setRunBreakpoint(runBreakpoint);
          }
        }
      }
    }
    catch(err) {
      ddb.error('FAILURE[_enable]:', err);
    };
  }
  
  async _paused(parameters) {
    const stackData = await this._pausedCollectStack(parameters);
    this.previousStack = this.currentStack;
    this.currentStack = stackData;
    if(this.resume) {
      this.resume = false;
    }
    else if(this.stepOver) {
      return await this._pausedStepOver(stackData, parameters.hitBreakpoints);
    }
    else if(this.stepInto) {
      this.stepInto = false;
    }
    else if(this.stepOut) {
      return await this._pausedStepOut(stackData, parameters.hitBreakpoints);
    }
    else if(this.pause) {
      this.pause = false;
    }
    else {
      return;
    }
    return await this._getScript(stackData);
  }
  
  async _pausedStepOver(stackData, hitBreakpoints) {
    let found = null;
    if(0 !== hitBreakpoints.length) {
      this.stepOver = false;
      return await this._getScript(stackData);
    }
    else if(0 !== this.stepOverBreakpoints.length) {
      for(let i = 0; i < this.stepOverBreakpoints.length; ++i) {
      const bp = this.stepOverBreakpoints[i];
      for(let j = 0; j < bp.locations.length; ++j) {
        const b = bp.locations[j];
        if(stackData.stack[0].location.scriptId === b.scriptId && stackData.stack[0].location.lineNumber === b.lineNumber && stackData.stack[0].location.columnNumber === b.columnNumber) {
          found = b;
          break;
        }
      }
    }
      if(found) {
        this.stepOver = false;
        this.previousLocation = found;
        return await this._getScript(stackData);
      }
      else {
        if('return' === this.previousLocation.type) {
          this.stepOver = false;
          return await this._getScript(stackData);
        }
        if('call' === this.previousLocation.type) {
          await this.client.Debugger.stepOver();
          return;
        }
        else {
          await this.client.Debugger.stepOver();
          return;
        }
      }
    }
  }
  
  async _pausedStepOut(stackData, hitBreakpoints) {
    let found = null;
    if(0 !== hitBreakpoints.length) {
      this.stepOver = false;
      return await this._getScript(stackData);
    }
    else if(0 !== this.stepOutBreakpoints.length) {
      const bp = this.stepOutBreakpoints[0];
      for(let i = 0; i < bp.locations.length; ++i) {
        const b = bp.locations[i];
        if(stackData.stack[0].location.scriptId === b.scriptId && stackData.stack[0].location.lineNumber === b.lineNumber && stackData.stack[0].location.columnNumber === b.columnNumber) {
          found = b;
          break;
        }
      }
      if(found) {
        this.stepOut = false;
        this.previousLocation = found;
        return await this._getScript(stackData);
      }
      else {
        if('return' === this.previousLocation.type) {
          this.stepOut = false;
          return await this._getScript(stackData);
        }
        if('call' === this.previousLocation.type) {
          await this.client.Debugger.stepOut();
          return;
        }
        else {
          await this.client.Debugger.stepOut();
          return;
        }
      }
    }
  }
  
  async _pausedCollectStack(parameters) {
    const stack = [];
    for(let i = 0; i < parameters.callFrames.length; ++i) {
      const callFrame = parameters.callFrames[i];
      const url = this.dbugger.getScriptParameters(callFrame.location.scriptId).url;
      let thisMembers = [];
      if(callFrame.this && callFrame.this.objectId) {
        try {
          thisMembers = await this.client.Runtime.getProperties({objectId: callFrame.this.objectId});
        }
        catch(err) {
          console.log('Runtime.getProperties[callFrame]:', err);
        }
      }
      const scopes = [];
      for(let j = 0; j < callFrame.scopeChain.length; ++j) {
        const scope = callFrame.scopeChain[j];
        if(scope.object && scope.object.objectId) {
          try {
            const scopeMembers = await this.client.Runtime.getProperties({objectId: scope.object.objectId});
            scopes.push({
              type: scope.type,
              name: scope.name,
              object: scopeMembers.result,
              startLocation: scope.startLocation,
              endLocation: scope.endLocation
            });
          }
          catch(err) {
            console.log('Runtime.getProperties[scope]:', err);
          }
        }
      }
      stack.push({
        callFrameId: callFrame.callFrameId,
        functionName: callFrame.functionName,
        objectName: callFrame.this.className,
        location: callFrame.location,
        scriptName: Path.parse(url).base,
        url: url,
        this: callFrame.this,
        scopes: scopes
      });
    }
    return {
      stack
    };
  }
  
  async _getScript(stack) {
    const scriptId = stack.stack[0].location.scriptId;
    const source = await this.client.Debugger.getScriptSource({scriptId: scriptId});
    return {
      stack: stack.stack,
      scriptId: scriptId,
      script: source.scriptSource
    };
  }
    
  async _getPossibleBreakpointsStepOver() {
    const results = [];
    for(let i = 0; i < this.currentStack.stack.length; ++i) {
      const stack = this.currentStack.stack[i];
      for(let i = 0; i < stack.scopes.length; ++i) {
        const scope = stack.scopes[i];
        if(scope.startLocation) {
          try {
            const result = await this.client.Debugger.getPossibleBreakpoints({
              start: scope.startLocation,
              end: scope.endLocation
            });
            const res = {
              scriptName: stack.scriptName,
              location: stack.location,
              locations: result.locations
            };
            results.push(res);
          }
          catch(err) {}
        }
      }
    }
    return results;
  }
  
  async _getPossibleBreakpointsStepOut() {
     const results = [];
    for(let i = 1; i < this.currentStack.stack.length; ++i) {
      const stack = this.currentStack.stack[i];
      const scope = stack.scopes[0];
      if(scope.startLocation) {
        try {
          const result = await this.client.Debugger.getPossibleBreakpoints({
            start: scope.startLocation,
            end: scope.endLocation
          });
          const res = {
            scriptName: stack.scriptName,
            location: stack.location,
            locations: result.locations
          };
          results.push(res);
        }
        catch(err) {}
      }
    }
    return results;
  }
  
  createDebugger(port, done) {
    chromeRemoteInterface({
      host: '127.0.0.1',
      port: port
    }, (client) => {
    }).on('error', (err) => {
      ddb.err(`chromeRemoteInterface ERROR. ${JSON.stringify(err, null, 2)}`);
    }).on('connect', async (client) => {
      this.client = client;
      await this.client.Console.disable();
      this._enable().then(() => {
        done(null);
      }).catch((err) => {
        done(err);
      });
      await client.Debugger.scriptParsed((parameters) => {
        this.dbugger.scriptParsed(parameters);
      });
      await client.Debugger.scriptFailedToParse((parameters) => {});
      await this.client.Debugger.breakpointResolved((parameters) => {});
      await this.client.Debugger.paused(async (parameters) => {
        try {
          const responseData = await this._paused(parameters);
          if(responseData) {
            this.sendMessage(new MessageTestCaseDebugPaused(responseData.stack, responseData.scriptId, responseData.script, this.actorIndex), null, true);
          }
        }
        catch(err) {
          console.log(err);
        }
      });
      await this.client.Debugger.resumed((parameters) => {});
      //await this.client.Debugger.setAsyncCallStackDepth({maxDepth: 1230});
    });
  }
  
  _startDebugger(port) {
    this.setSessionData('client', this.client);
    const indexDebugRun = this.expectAsynchResponse();
    const testCaseDebugRunRequest = new ActionRequest();
    testCaseDebugRunRequest.add('TestCaseDebugRun');
    this.sendRequestToWorkerProcess(testCaseDebugRunRequest, (response) => {
      this.asynchResponseSuccess(indexDebugRun);
      this.testCaseDebugRunStarted = true;
    });  
  }
  
  startDebugger(port) {
    if(this.processStarted && this.logFileLoaded) {
      const indexDebugger = this.expectAsynchResponse();
      if(0 !== port) {
        this.createDebugger(port, (err) => {
          if(!err) {
            this._startDebugger(port);
            this.asynchResponseSuccess(indexDebugger);
          }
          else {
            this.asynchResponseError('Could not create debugger.', indexDebugger);
          }
        });
      }
      else {
        this._startDebugger(port);
        this.asynchResponseSuccess(indexDebugger);
      }
    }
  }
}

module.exports = TestCaseDebugStart;
