
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class TestCaseGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repoName, sutName, futName, tcName) {
    if(tcName) {
      this.getTestCase(repoName, sutName, futName, tcName, (data, err) => {
        if(err) {
          return this.responsePartError('Could not get the Test Case from file.');
        }
        else {
          return this.responsePartSuccess({
            testCase: data
          });
        }
      });
    }
    else {
      let responseDone = false;
      this.getTestCases(repoName, sutName, futName, (data, err) => {
        if(!responseDone) {
          responseDone = true;
          if(err) {
            return this.responsePartError('Could not get the Test Cases.');
          }
          data.sort((a, b) => {
            if(a.name < b.name) {
              return -1;
            }
            else if(b.name < a.name) {
              return 1;
            }
            else {
              return 0;
            }
          });
          return this.responsePartSuccess({
            testCases: data
          });
        }
      });
    }
  }
  
  getTestCase(repoName, sutName, futName, tcName, done) {
    Fs.readFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), (err, data) => {
      if(err) {
        return done(undefined, err);
      }
      else {
        return done(JSON.parse(data));
      }
    });
  }
  
  getTestCases(repoName, sutName, futName, done, previusError=false) {
    const testCases = [];
    const dir = ActorPathData.getTestCasesFolder(repoName, sutName, futName);
    Fs.readdir(dir, (err, files) => {
      if(err) {
        if(!previusError && 'ENOENT' === err.code) {
          return this.asynchMkdir(dir, (mkdirErr) => {
            if(mkdirErr) {
              return done(testCases, mkdirErr);
            }
            this.getTestCases(repoName, sutName, futName, done, true);
          });
        }
        return done(testCases, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(testCases);
      }
      for(let i = 0; i < files.length; ++i) {
        const file = files[i];
        const resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(testCases, err);
          }
          if(stat && stat.isDirectory()) {
            const prefix = `Tc`;//${sutName}${futName}
            if(file.startsWith(prefix) && file.length > prefix.length) {
              Fs.readFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, file.substr(prefix.length)), (err, data) => {
                if(err) {
                  return done(testCases, err);
                }	  
                testCases.push(JSON.parse(data));
                if(0 === --pendings) {
                  return done(testCases);
                }
              });
            }
            else if(0 === --pendings) {
              return done(testCases);
            }
          }
          else if(0 === --pendings) {
            return done(testCases);
          }
        });
      }
    });
  }
}

module.exports = TestCaseGet;
