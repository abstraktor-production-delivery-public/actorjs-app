
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class TestCaseRename extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repo, sut, fut, currentTcName, newTcName, description) {
    const currentFolder = ActorPathData.getTestCaseFolder(repo, sut, fut, currentTcName);
    const newFolder = ActorPathData.getTestCaseFolder(repo, sut, fut, newTcName);
    const newFile = newFolder + Path.sep + ActorPathData.getTestCaseFileName(newTcName);
    this.asynchRename(currentFolder, newFolder, (err) => {
      if(!err) {
        this.asynchRename(newFolder + Path.sep + ActorPathData.getTestCaseFileName(currentTcName), newFile, (err) => {
          if(!err) {
            this.asynchReadFile(newFile, (err, data) => {
              if(err) {
                consle.log(err);
              }
              data.name = newTcName;
              data.description = description;
              this.asynchWriteFileResponse(newFile, data);
            });
          }
          else {
            this.responsePartError(`Could rename Test Case '${currentTcName}' to '${newTcName}'. Files might be corrupt. You might have to use 'git restore'`);
          }
        });
      }
      else {
        this.responsePartError(`Could rename Test Case '${currentTcName}' to '${newTcName}'.`);
      }
    });
  }
}

module.exports = TestCaseRename;
