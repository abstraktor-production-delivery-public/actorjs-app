
'use strict';

const Debug = require('./debug');
const Debugger = require('z-abs-funclayer-engine-server/server/debug/debugger');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class TestCaseDebugBreakpointSet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repo, sut, fut, tc, breakpoint) {
    this.setBreakpoint(repo, sut, fut, tc, breakpoint);
  }
  
  async setBreakpoint(repo, sut, fut, tc, breakpoint) {
    const file = ActorPathGenerated.getTestCaseDebuggerFile(sut, fut, tc);
    if(breakpoint.name.startsWith('./')) {
      breakpoint.url = Debugger.calculateBreakpointUrl(sut, fut, tc, breakpoint);
    }
    else {
      breakpoint.url = breakpoint.name;
    }
    try {
      const dbuggerData = await this.asynchReadFilePromise(file);
      const client = this.getSessionData('client');
      const dbugger = this.getSessionData('debugger');
      if(client && dbugger) {
        await Debug._getPossibleBreakpoints(client, dbugger, breakpoint);
      }
      const foundIndex = dbuggerData.breakpoints.findIndex((data) => {
        return data.url === breakpoint.url && data.lineNumber === breakpoint.lineNumber
      });
      if(-1 !== foundIndex) {
        const index = this.expectAsynchResponse();
        this.asynchResponseSuccess(index, dbuggerData);
        return;
      }
      dbuggerData.breakpoints.push(breakpoint);
      dbuggerData.breakpoints.sort((a, b) => {
        if(a.title !== b.title) {
          return a.title >= b.title ? 1 : -1;
        }
        else if(a.lineNumber != b.lineNumber) {
          return a.lineNumber >= b.lineNumber ? 1 : -1;
        }
        else {
          return a.columnNumber >= b.columnNumber ? 1 : -1;
        }
      });
      const index = this.expectAsynchResponse();
      this.asynchWriteFile(file, dbuggerData,(err) => {
        if(!err) {
          if(client && dbugger) {
            if(!dbugger.findbreakpointInRunBreakpoints(breakpoint)) {
              this._setBreakpointByUrl(client, dbugger, breakpoint, (err) => {
                if(!err) {
                  this.asynchResponseSuccess(index, dbuggerData);
                }
                else {
                  this.asynchResponseError(err.message);
                }
              });
            }
            else {
              this.asynchResponseSuccess(index, dbuggerData);
            }
          }
          else {
            this.asynchResponseSuccess(index, dbuggerData);
          };
        }
        else {
          this.asynchResponseError(err.message);
        }
      });
    }
    catch(err) {
      if('ENOENT' === err.code) {
        const parentPath = file.substring(0, file.lastIndexOf(Path.sep));
        this.asynchMkdir(parentPath, (err) => {
          if(!err) {
            let dbuggerData = {
              "breakpoints": []
            };
            dbuggerData.breakpoints.push(breakpoint);
            this.asynchWriteFileResponse(file, dbuggerData, true);
          }
          else {
            this.responsePartError(`Could not create dbugger file '${file}'.`);
          }
        }, true);
      }
      else {
        this.responsePartError(`Could not read dbugger file '${file}'.`);
      }
    }
  }
  
  async _setBreakpointByUrl(client, dbugger, breakpoint, done) {
    try {
      const result = await client.Debugger.setBreakpointByUrl(breakpoint);
      breakpoint.breakpointId = result.breakpointId;
      dbugger.setBreakpoint(breakpoint);
    }
    catch(err) {
      ddb.error('FAILURE: ' + err);
      return done(err);
    };
    done();
  }
}

module.exports = TestCaseDebugBreakpointSet;
