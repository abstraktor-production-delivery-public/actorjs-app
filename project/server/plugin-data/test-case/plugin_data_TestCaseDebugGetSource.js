
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseDebugGetSource extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(scriptId) {
    this.getSource(scriptId);
  }
  
  async getSource(scriptId) {
    const client = this.getSessionData('client');
    try {
      const source = await client.Debugger.getScriptSource({scriptId: scriptId});
      this.expectAsynchResponseSuccess(source.scriptSource);
    }
    catch(err) {
      this.expectAsynchResponseError(`Could not get the debug script source of script with scriptId: '${scriptId}'.`);
    }    
  }
}

module.exports = TestCaseDebugGetSource;
