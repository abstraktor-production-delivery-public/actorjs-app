
'use strict';

const Debugger = require('z-abs-funclayer-engine-server/server/debug/debugger');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class TestCaseDebugBreakpointUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(repo, sut, fut, tc, breakpoint) {
    const file = ActorPathGenerated.getTestCaseDebuggerFile(sut, fut, tc);
    if(breakpoint.name.startsWith('./')) {
      breakpoint.url = Debugger.calculateBreakpointUrl(sut, fut, tc, breakpoint);
    }
    else {
      breakpoint.url = breakpoint.name;
    }
    this.asynchReadFile(file, (err, dbuggerData) => {
      if(!err) {
        const breakpointCurrent = dbuggerData.breakpoints.find((foundBreakpoint) => {
          return foundBreakpoint.name === breakpoint.name
            && foundBreakpoint.lineNumber === breakpoint.lineNumber;
        });
        const pauseOnBreakCurrent = breakpointCurrent.pauseOnBreak;
        breakpointCurrent.pauseOnBreak = breakpoint.pauseOnBreak;
        this.asynchWriteFileResponse(file, dbuggerData, true, (err) => {
          if(!err) {
            let client = this.getSessionData('client');
            if(undefined !== client) {
              this._updateBreakpointByUrl(client, pauseOnBreakCurrent, breakpoint);
            }
          }
        });
      }
      else if('ENOENT' === err.code) {
        let parentPath = file.substring(0, file.lastIndexOf(Path.sep));
        this.asynchMkdir(parentPath, (err) => {
          if(!err) {
            let dbuggerData = {
              "breakpoints": []
            };
            dbuggerData.breakpoints.push(breakpoint);
            this.asynchWriteFileResponse(file, dbuggerData, true);
          }
          else {
            this.responsePartError(`Could not create dbugger file '${file}'.`);
          }
        }, true);
      }
      else {
        this.responsePartError(`Could not read dbugger file '${file}'.`);
      }
    });
  }
  
  async _updateBreakpointByUrl(client, pauseOnBreakCurrent, breakpointNext) {
    try {
      if(pauseOnBreakCurrent !== breakpointNext.pauseOnBreak) {
        let dbugger = this.getSessionData('debugger');
        if(breakpointNext.pauseOnBreak) {
          let result = await client.Debugger.setBreakpointByUrl(breakpointNext);
          breakpointNext.breakpointId = result.breakpointId;
        }
        else {
          let breakpoint = dbugger.getBreakpoint(breakpointNext);
          await client.Debugger.removeBreakpoint(breakpoint);
          dbugger.removeBreakpoint(breakpointNext);    
          breakpointNext.breakpointId = '';
          
        }
        dbugger.setBreakpoint(breakpointNext);
      }
    }
    catch(err) {
      ddb.error('FAILURE: ' + err);
    };
  }
}

module.exports = TestCaseDebugBreakpointUpdate;
