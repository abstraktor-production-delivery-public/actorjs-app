
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class TestCaseRecentDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repo, suts, futs, tcs) {
    this.writeLock(ActorPathGenerated.getTestCasesRecentFile(), (unlock) => {
      Fs.readFile(ActorPathGenerated.getTestCasesRecentFile(), (err, data) => {
        if(err) {
          unlock();
          return this.responsePartError(`Could not delete the TestCases Recent Files: ' ${ActorPathGenerated.getTestCasesRecentFile()} '.`);
        }
        const parsedData = JSON.parse(data);
        const filteredData = this.deleteRecentFiles(parsedData, repo, suts, futs, tcs);
        Fs.writeFile(ActorPathGenerated.getTestCasesRecentFile(), JSON.stringify(filteredData, null, 2), (err) => {
          unlock();
          if(err) {
            return this.responsePartError(`Could not delete the TestCases Receant Files: ' ${ActorPathGenerated.getTestCasesRecentFile()} '.`);
          }
          return this.responsePartSuccess();
        });
      });
    });
  }
  
  deleteRecentFiles(recentFiles, repo, suts, futs, tcs) {
    if(null !== tcs) {
      for(let j = 0; j < tcs.length; ++j) {
        recentFiles = recentFiles.filter((recentFile) => {
          return !(tcs[j].repo === recentFile.repo && tcs[j].sut === recentFile.sut && tcs[j].fut === recentFile.fut && tcs[j].tc === recentFile.tc);
        });
      }
    }
    else if(null !== futs) {
      recentFiles = recentFiles.filter((recentFile) => {
        return !(repo === recentFile.repo && suts === recentFile.sut && futs === recentFile.fut);
      });
    }
    else if(null !== suts) {
      recentFiles = recentFiles.filter((recentFile) => {
        return !(repo === recentFile.repo && suts === recentFile.sut);
      });
    }
    return recentFiles;
  }
}

module.exports = TestCaseRecentDelete;
