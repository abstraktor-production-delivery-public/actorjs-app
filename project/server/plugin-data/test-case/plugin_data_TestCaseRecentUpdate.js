
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestCaseRecentUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.maxTestCases = 10;
  }
  
  onRequest(repoName, sutName, futName, tcName) {
    this.writeLock(ActorPathGenerated.getTestCasesRecentFile(), (unlock) => {
      Fs.readFile(ActorPathGenerated.getTestCasesRecentFile(), (err, data) => {
        if(err) {
          unlock();
          return this.responsePartError(`Could not get the TestCases Recent File: ' ${ActorPathGenerated.getTestCasesRecentFile()} '.`);
        }
        const parsedData = this._safeJsonParse(data, (e) => {
          return this.responsePartError(`Could not JSON.parse the TestCases Recent File: ' ${ActorPathGenerated.getTestCasesRecentFile()} '.`);
        });
        Fs.writeFile(ActorPathGenerated.getTestCasesRecentFile(), JSON.stringify(this.updateRecentFiles(parsedData, repoName, sutName, futName, tcName), null, 2), (err) => {
          unlock();
          if(err) {
            return this.responsePartError(`Could not update the TestCases Recent File: ' ${ActorPathGenerated.getTestCasesRecentFile()} '.`);
          }
          return this.responsePartSuccess();
        });
      });
    });
  }
  
  updateRecentFiles(recentFiles, repoName, sutName, futName, tcName) {
    for(let i = 0; i < recentFiles.length; ++i) {
      if(sutName === recentFiles[i].sut && futName === recentFiles[i].fut && tcName === recentFiles[i].tc) {
        recentFiles.splice(i, 1);
        recentFiles.unshift({
          repo: repoName,
          sut: sutName,
          fut: futName,
          tc: tcName
        });
        return recentFiles;
      }
    }
    if(this.maxTestCases < recentFiles.length) {
      recentFiles.pop();
    }
    recentFiles.unshift({
      repo: repoName,
      sut: sutName,
      fut: futName,
      tc: tcName
    });
    return recentFiles;
  }
}

module.exports = TestCaseRecentUpdate;
