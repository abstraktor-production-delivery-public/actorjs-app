
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseButtonsGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(key) {
    this.asynchReadFileResponse(ActorPathGenerated.getTestCaseButtonsFile(key));
  }
}


module.exports = TestCaseButtonsGet;
