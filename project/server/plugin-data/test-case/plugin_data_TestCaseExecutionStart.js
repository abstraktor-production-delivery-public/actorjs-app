
'use strict';

const TestCaseLoader = require('z-abs-funclayer-engine-server/server/engine/test-case-loader');
const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBase = require('z-abs-corelayer-server/server/plugin-base');
const Logger = require('z-abs-corelayer-server/server/log/logger');
const Fs = require('fs');


class TestCaseExecutionStart extends PluginBase {
  constructor() {
    super(PluginBase.GET);
    this.testCase = null;
  }
  
  onRequest(repoName, sutName, futName, tcName, sutInstance, stagedNodes, config) {
    TestCaseLoader.load(repoName, sutName, futName, tcName, sutInstance, stagedNodes, false, config, this.cbMessage, (err, testCase) => {
      if(err) {
        if('N/A' === err.message) {
          return this.responsePartError('N/A');
        }
        else {
          const errorLog = `Could not find the Test Case: '${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)}'`;
          LOG_ERROR(Logger.ERROR.CATCH, errorLog, err);
          return this.responsePartError(errorLog);
        }
      }
      else {
        this.testCase = testCase;
        process.nextTick(() => {
          testCase.executionContext.initTestCase(testCase);
          testCase.run((done) => {
            testCase.executionContext.exitTestCase(done);
          }, (sessionDataName, SessionData) => {});
        });
        return this.responsePartSuccess();
      }
    });
  }
  
  onMessageToClient(msg, dataBuffers, cbMessage) {
    if(AppProtocolConst.TEST_CASE_STOPPED === msg.msgId && msg.last) {
      this.clearSession(() => {
        cbMessage(msg, dataBuffers);
      });
    }
    else {
      cbMessage(msg, dataBuffers);
    }
  }
  
  onMessageFromClient(msg) {
    if(AppProtocolConst.TEST_CASE_STOP === msg.msgId) {
      if(null !== this.testCase) {
        this.testCase.stop(() => {});
      }
    }
  }
  
  onClose(done) {
    if(null !== this.testCase) {
      this.testCase.stop(done);
    }
    else {
      done();
    }
  }
}

module.exports = TestCaseExecutionStart;
