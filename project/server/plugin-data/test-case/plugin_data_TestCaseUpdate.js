
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestCaseUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repoName, sutName, futName, tcName, description, tc) {
    const responsePart = this.verifyInput(tc);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.writeFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), JSON.stringify(tc, null, 2), (err) => {
      if(err) {
        return this.responsePartError(`Could not update the TestCase File: ' ${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)} '.`);
      }
      else {
        return this.responsePartSuccess();
      }
    });
  }
  
  verifyInput(testCase) {
    const name = testCase.name;
    if(0 === name.length) {
      return this.responsePartError(`The Test Case name: '' must not be empty.`);
    }
    const first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A Test Case name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
}

module.exports = TestCaseUpdate;
