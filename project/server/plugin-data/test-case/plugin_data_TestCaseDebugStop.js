
'use strict';

const PluginBase = require('z-abs-corelayer-server/server/plugin-base');


class TestCaseDebugStop extends PluginBase {
  constructor() {
    super(PluginBase.GET);
  }
  
  onRequest() {
    //let client = this.getSessionData('debugger');
    setTimeout(() => {
      ddb.info('CLOSE:::::::::::::::::');
      this.killWorkerProcess(() => {
        this.responsePartSuccess();
      });
    }, 0);
  }
}

module.exports = TestCaseDebugStop;
