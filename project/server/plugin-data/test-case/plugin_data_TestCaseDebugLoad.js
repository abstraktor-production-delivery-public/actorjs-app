
'use strict';

const TestCaseLoader = require('z-abs-funclayer-engine-server/server/engine/test-case-loader');
const Logger = require('z-abs-corelayer-server/server/log/logger');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBase = require('z-abs-corelayer-server/server/plugin-base');
const Fs = require('fs');


class TestCaseDebugLoad extends PluginBase {
  constructor() {
    super(PluginBase.GET);
  }
  
  onRequest(repoName, sutName, futName, tcName, sutInstance, config) {
    TestCaseLoader.load(repoName, sutName, futName, tcName, sutInstance, [], true, config, this.cbMessage, (err, testCase) => {
      if(err) {
        if('N/A' === err.message) {
          return this.responsePartError('N/A');
        }
        else {
          LOG_ERROR(Logger.ERROR.CATCH, this.constructor.name, err);
          return this.responsePartError(`Could not find the Test Case: '${ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName)}'`);
        }
      }
      else {
        this.setSessionData('TEST_CASE', testCase);
        return this.responsePartSuccess();
      }
    });
  }
}

module.exports = TestCaseDebugLoad;
