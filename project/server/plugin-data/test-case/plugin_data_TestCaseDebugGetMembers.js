
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseDebugGetMembers extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(object) {
    this.getMembers(object);
  }
  
  async getMembers(object) {
    const client = this.getSessionData('client');
    try {
      const members = await client.Runtime.getProperties(object);
      if('object' === object.type && 'map' === object.subtype) {
        const internalProperties = await client.Runtime.getProperties(members.internalProperties[1].value);
        members.extraResult = internalProperties.result;
      }
      else if('object' === object.type && 'proxy' === object.subtype) {
        const internalProperties = await client.Runtime.getProperties(members.internalProperties[1].value);
        members.extraResult = internalProperties.result;
      }
      this.expectAsynchResponseSuccess(members);
    }
    catch(err) {
      this.expectAsynchResponseError(`Could not get the debug members of ${JSON.stringify(object, null, 2)}`);
    }
  }
}

module.exports = TestCaseDebugGetMembers;
