
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseButtonsUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(key, valueObject) {
    this.asynchWriteFileResponse(ActorPathGenerated.getTestCaseButtonsFile(key), valueObject, true);
  }
}


module.exports = TestCaseButtonsUpdate;
