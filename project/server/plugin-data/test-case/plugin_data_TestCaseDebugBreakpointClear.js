
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class TestCaseDebugBreakpointClear extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
 
  onRequest(repo, sut, fut, tc, breakpoint) {
    const file = ActorPathGenerated.getTestCaseDebuggerFile(sut, fut, tc);
    this.asynchReadFile(file, (err, dbuggerData) => {
      if(!err) {
        const index = dbuggerData.breakpoints.findIndex((foundBreakpoint) => {
          return foundBreakpoint.name === breakpoint.name
            && foundBreakpoint.lineNumber === breakpoint.lineNumber;
        });
        if(-1 !== index) {
          dbuggerData.breakpoints.splice(index, 1);
          return this.asynchWriteFileResponse(file, dbuggerData, true, (err) => {
            if(!err) {
              const client = this.getSessionData('client');
              if(undefined !== client) {
                this._removeBreakpoint(client, breakpoint);
              }
            }
          });
        }
        else {
          this.responsePartError(`Could not find the breakpoint '${breakpoint.name}'.`);
        }
      }
      else {
        this.responsePartError(`Could not find dbugger file '${file}'.`);
      }
    });
  }
  
  async _removeBreakpoint(client, breakpoint) {
    try {
      let dbugger = this.getSessionData('debugger');
      let bp = dbugger.getBreakpoint(breakpoint);
      await client.Debugger.removeBreakpoint(bp);
      dbugger.removeBreakpoint(breakpoint);
    }
    catch(err) {
      ddb.error('FAILURE: ' + err);
    };
  }
}

module.exports = TestCaseDebugBreakpointClear;
