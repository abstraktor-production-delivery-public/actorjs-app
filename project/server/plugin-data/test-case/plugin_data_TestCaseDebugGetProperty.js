
'use strict';

const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseDebugGetProperty extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(object, memberNameArray) {
    this.getProperty({value: object}, memberNameArray);
  }
  
  async getProperty(object, memberNameArray) {
    let client = this.getSessionData('client');
    try {
      let members = null;
      for(let i = 1; i < memberNameArray.length; ++i) {
        members = await client.Runtime.getProperties(object.value);
        object = members.result.find((member) => {
          return member.name === memberNameArray[i];
        });
      }
      if(undefined !== object && undefined !== object.value) {
        if('object' === object.value.type && null !== object.value.value) {
          members = await client.Runtime.getProperties(object.value);
          object.members = members;
        }
      }
      else {
        object = {
          value: {
            type: 'undefined'            
          }
        }
      }
      this.expectAsynchResponseSuccess(object);
    }
    catch(err) {
      this.expectAsynchResponseError(`Could not get the debug members of ${JSON.stringify(object, null, 2)}`);
    }
  }
}

module.exports = TestCaseDebugGetProperty;
