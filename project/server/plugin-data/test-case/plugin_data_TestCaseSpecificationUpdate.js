
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseSpecificationUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(repoName, sutName, futName, tcName, testCase, specification) {
    if(undefined !== specification) {
      testCase.specification = specification;
    }
    this.asynchWriteFileResponse(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), testCase);
  }
}

module.exports = TestCaseSpecificationUpdate;
