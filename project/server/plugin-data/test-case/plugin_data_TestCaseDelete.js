
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(tcs) {
    tcs.forEach((tc) => { 
      this.asynchRmdirResponse(ActorPathData.getTestCaseFolder(tc.repo, tc.sut, tc.fut, tc.tc), true);
    });
  }
}

module.exports = TestCaseDelete;
