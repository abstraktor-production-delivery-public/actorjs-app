
'use strict';

const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestCaseStacksGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    const templatesMap = [];
    StackComponentsFactory.getStacks((stacks) => {
      stacks.forEach((stack) => {
        const testCaseTemplateFactory = StackComponentsFactory.createTemplatesTestCases(stack.name);
        if(testCaseTemplateFactory) {
          try {
            const templatesTypeMap = [];
            const templates = new testCaseTemplateFactory().templates;
            if(templates) {
              templates.forEach((template) => {
                templatesTypeMap.push([
                  template.displayName,
                  {
                    displayName: template.displayName,
                    regressionFriendly: template.regressionFriendly,
                    template: template.template,
                    testDataViews: template.testDataViews,
                    markupNodes: template.markupNodes,
                    markup: template.markup
                  }
                ]);
              });
              if(0 !== templatesTypeMap.length) {
                templatesMap.push([
                  stack.name,
                  templatesTypeMap
                ]);
              }
            }
          }
          catch(err) {
            ddb.error(`Failing to load stack '${stack.name}' for Test Case Templates.`, err)
          }
        } 
      });
      this.expectAsynchResponseSuccess(templatesMap);     
    });
  }
}

module.exports = TestCaseStacksGet;
