
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Path = require('path');


class TestCaseDebugGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repo, sut, fut, tc) {
    const file = ActorPathGenerated.getTestCaseDebuggerFile(sut, fut, tc);
    this.asynchReadFile(file, (err, dbuggerData) => {
      if(!err) {
        this.expectAsynchResponseSuccess(dbuggerData);
      }
      else if('ENOENT' === err.code) {
        const parentPath = file.substring(0, file.lastIndexOf(Path.sep));
        this.asynchMkdir(parentPath, (err) => {
          if(!err) {
            this.asynchWriteFileResponse(file, {
              "breakpoints": []
            }, true);
          }
          else {
            this.expectAsynchResponseError(`Could not create dbugger file '${file}'.`);
          }
        }, true);
      }
      else {
        this.expectAsynchResponseError(`Could not read dbugger file '${file}'.`);
      }
    });
  }
}

module.exports = TestCaseDebugGet;
