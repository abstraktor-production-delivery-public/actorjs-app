
'use strict';


class Debug {
  static async _getPossibleBreakpoints(client, dbugger, breakpoint, breakpoints) {
    const parameters = dbugger.getScriptParametersFromUrl(breakpoint.url);
    if(parameters) {
      const scriptId = parameters.scriptId;
      try {
        const result = await client.Debugger.getPossibleBreakpoints({
          start: {
            scriptId: scriptId,
            lineNumber: breakpoint.lineNumber,
            columnNumber: 0
          },
          end: {
            scriptId: scriptId,
            lineNumber: breakpoint.lineNumber + 1,
            columnNumber: 0
          }
        });
        const locations = result.locations;
        let columnNumber = 0 < locations.length ? locations[0].columnNumber : 0;
        for(let i = 0; i < locations.length; ++i) {
          if('call' === locations[i].type || 'return' === locations[i].type || undefined === locations[i].type) {
            columnNumber = locations[i].columnNumber;
            break;
          }
        }
        breakpoint.columnNumber = columnNumber;
      }
      catch(e) {}
    }
  }
}

module.exports = Debug;
