
'use strict';

const PluginBase = require('z-abs-corelayer-server/server/plugin-base');
const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');


class TestCaseDebugRun extends PluginBase {
  constructor() {
    super(PluginBase.GET);
  }
  
  onRequest() {
    const testCase = this.getSessionData('TEST_CASE');
    if(null !== testCase) {
      process.nextTick(() => {
        testCase.executionContext.initTestCase(testCase);
        testCase.run((done) => {
          testCase.executionContext.exitTestCase(done);
          this.onClose(() => {});
        });
      });
      return this.responsePartSuccess();
    }
    return this.responsePartError('No Test Case found.');
  }
  
  onMessageFromClient(msg) {
    if(AppProtocolConst.TEST_CASE_STOP === msg.msgId) {
      const testCase = this.getSessionData('TEST_CASE');
      if(null !== testCase) {
        testCase.stop();
      }
    }
    else if(AppProtocolConst.TEST_CASE_DEBUG_ACTOR_INDEX_OK === msg.msgId) {
      const testCase = this.getSessionData('TEST_CASE');
      if(null !== testCase) {
        testCase.actorDebugContinue(msg.actorIndex);
      }
    }
  }
  
  onClose(done) {
    const testCase = this.getSessionData('TEST_CASE');
    if(null !== testCase) {
      testCase.stop(done);
    }
  }
}

module.exports = TestCaseDebugRun;
