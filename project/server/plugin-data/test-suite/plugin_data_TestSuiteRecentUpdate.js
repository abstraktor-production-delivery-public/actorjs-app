
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestSuiteRecentUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.maxTestSuites = 10;
  }
  
  onRequest(repoName, sutName, futName, tsName) {
    this.writeLock(ActorPathGenerated.getTestSuitesRecentFile(), (unlock) => {
      Fs.readFile(ActorPathGenerated.getTestSuitesRecentFile(), (err, data) => {
        if(err) {
          unlock();
          return this.responsePartError(`Could get the TestSuites Recent File: ' ${ActorPathGenerated.getTestSuitesRecentFile()} '.`);
        }
        Fs.writeFile(ActorPathGenerated.getTestSuitesRecentFile(), JSON.stringify(this.update(JSON.parse(data), repoName, sutName, futName, tsName), null, 2), (err) => {
          unlock();
          if(err) {
            return this.responsePartError(`Could not update the TestSuites Recent File: ' ${ActorPathGenerated.getTestSuitesRecentFile()} '.`);
          }
          return this.responsePartSuccess();
        });
      });
    });
  }
  
  update(recentFiles, repoName, sutName, futName, tsName) {
    for(let i = 0; i < recentFiles.length; ++i) {
      if(sutName === recentFiles[i].sut && futName === recentFiles[i].fut && tsName === recentFiles[i].ts) {
        recentFiles.splice(i, 1);
        recentFiles.unshift({
          repo: repoName,
          sut: sutName,
          fut: futName,
          ts: tsName
        });
        return recentFiles;
      }
    }
    if(this.maxTestSuites < recentFiles.length) {
      recentFiles.pop();
    }
     recentFiles.unshift({
      repo: repoName,
      sut: sutName,
      fut: futName,
      ts: tsName
    });
    return recentFiles;
  }
}

module.exports = TestSuiteRecentUpdate;
