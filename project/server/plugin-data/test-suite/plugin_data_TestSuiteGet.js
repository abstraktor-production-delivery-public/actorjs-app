
'use strict';

const Logger = require('z-abs-corelayer-server/server/log/logger');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class TestSuiteGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repoName, sutName, futName, tsName) {
    if(tsName) {
      this.getTestSuite(repoName, sutName, futName, tsName, (data, err) => {
        if(err) {
          LOG_ERROR(Logger.ERROR.CATCH, 'TestSuiteGet', err);
          return this.responsePartError('Could not get the Test Suite.');
        }
        else {
          return this.responsePartSuccess({
            testSuite: data
          });
        }
      });
    }
    else {
      let responseDone = false;
      this.getTestSuites(repoName, sutName, futName, (data, err) => {
        if(!responseDone) {
          responseDone = true;
          if(err) {
            LOG_ERROR(Logger.ERROR.CATCH, 'TestSuiteGet', err);
            return this.responsePartError('Could not get the Test Suite.');
          }
          return this.responsePartSuccess({
            testSuites: data
          });
        }
      });
    }
  }
  
  getTestSuite(repoName, sutName, futName, tsName, done) {
    Fs.readFile(ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName), (err, data) => {
      if(err) {
        return done(undefined, err);
      }
      else {
        return done(JSON.parse(data));
      }
    });
  }
  
  getTestSuites(repoName, sutName, futName, done, previusError=false) {
    let testSuites = [];
    let dir = ActorPathData.getTestSuitesFolder(repoName, sutName, futName);
    Fs.readdir(dir, (err, files) => {
      if(err) {
        if(!previusError && 'ENOENT' === err.code) {
          return this.asynchMkdir(dir, (mkdirErr) => {
            if(mkdirErr) {
              return done(testSuites, mkdirErr);
            }
            this.getTestSuites(repoName, sutName, futName, done, true);
          });
        }
        return done(testSuites, err);
      }
      let pendings = files.length;
      if(0 === pendings) {
        return done(testSuites);
      }
      for(let i = 0; i < files.length; ++i) {
        let file = files[i];
        let resolvedDir = Path.resolve(dir, file);      
        Fs.stat(resolvedDir, (err, stat) => {
          if(err) {
            return done(testSuites, err);
          }
          if(stat && stat.isDirectory()) {
            const prefix = 'Ts';
            if(file.startsWith(prefix) && file.length > prefix.length) {
              Fs.readFile(ActorPathData.getTestSuiteFile(repoName, sutName, futName, file.substr(prefix.length)), (err, data) => {
                if(err) {
                  return done(testSuites, err);
                }	  
                testSuites.push(JSON.parse(data));
                if(0 === --pendings) {
                  return done(testSuites);
                }
              });
            }
            else if(0 === --pendings) {
              return done(testCases);
            }
          }
          else if(0 === --pendings) {
            return done(testCases);
          }
        });
      }
    });
  }
}

module.exports = TestSuiteGet;
