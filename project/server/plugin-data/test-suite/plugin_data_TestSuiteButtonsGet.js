
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestSuiteButtonsGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(key) {
    this.asynchReadFileResponse(ActorPathGenerated.getTestSuiteButtonsFile(key));
  }
}

module.exports = TestSuiteButtonsGet;
