
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestSuiteAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repoName, sutName, futName, tsName, description, dataTestSuite) {
    let testSuite = {
      name: tsName,
      description: description,
      status: 'visible',
      ts: dataTestSuite
    }
    let responsePart = this.verifyInput(testSuite);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.mkdir(ActorPathData.getTestSuiteFolder(repoName, sutName, futName, tsName), (err) => {
      if(err) {
        return this.responsePartError(`Could not create the Test Suite Folder: ' ${ActorPathData.getTestSuiteFolder(repoName, sutName, futName, tsName)} '.`);
      }
      Fs.writeFile(ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName), JSON.stringify(testSuite, null, 2), (err) => {
        if(err) {
          return this.responsePartError(`Could not create the TestSuite File: ' ${ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName)} '.`);
        }
      });
    });
    return this.responsePartSuccess();
  }

  verifyInput(testSuite) {
    let name = testSuite.name;
    if(0 === name.length) {
      return this.responsePartError(`The Test Suitename: '' must not be empty.`);
    }
    let first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A Test Suite name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
}

module.exports = TestSuiteAdd;
