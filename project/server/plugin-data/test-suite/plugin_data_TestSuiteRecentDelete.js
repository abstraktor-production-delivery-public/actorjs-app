
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestSuiteRecentDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(repoName, sutName, futName, tsNames) {
    this.writeLock(ActorPathGenerated.getTestSuitesRecentFile(), (unlock) => {
      Fs.readFile(ActorPathGenerated.getTestSuitesRecentFile(), (err, data) => {
        if(err) {
          unlock();
          return this.responsePartError(`Could not delete the TestSuites Recent Files: ' ${ActorPathData.getTestSuitesRecentFile()} '.`);
        }
        Fs.writeFile(ActorPathGenerated.getTestSuitesRecentFile(), JSON.stringify(this.deleteRecentFiles(JSON.parse(data), repoName, sutName, futName, tsNames), null, 2), (err) => {
          unlock();
          if(err) {
            return this.responsePartError(`Could not delete the TestSuites Receant Files: ' ${ActorPathGenerated.getTestSuitesRecentFile()} '.`);
          }
          return this.responsePartSuccess();
        });
      });
    });
  }
  
  deleteRecentFiles(recentFiles, repoName, sutName, futName, tsNames) {
    if(null !== tsNames) {
      for(let j = 0; j < tsNames.length; ++j) {
        recentFiles = recentFiles.filter((recentFile) => {
          return !(repoName === recentFile.repo && sutName === recentFile.sut && futName === recentFile.fut && tsNames[j] === recentFile.ts);
        });
      }
      return recentFiles;
    }
    else if (null !== futName) {
      return recentFiles.filter((recentFile) => {
        return !(repoName === recentFile.repo && sutName === recentFile.sut && futName === recentFile.fut);
      });
    }
    else if (null !== sutName) {
      return recentFiles.filter((recentFile) => {
        return !(repoName === recentFile.repo && sutName === recentFile.sut);
      });
    }
  }
}

module.exports = TestSuiteRecentDelete;
