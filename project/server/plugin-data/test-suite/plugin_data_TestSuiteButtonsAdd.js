
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestSuiteButtonsAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(key, valueObject) {
    this.asynchWriteFileResponse(ActorPathGenerated.getTestSuiteButtonsFile(key), valueObject, true);
  }
}


module.exports = TestSuiteButtonsAdd;
