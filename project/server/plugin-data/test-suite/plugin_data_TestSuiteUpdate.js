
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestSuiteUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repoName, sutName, futName, tsName, description, ts) {
    const responsePart = this.verifyInput(ts);
    if(true !== responsePart) {
      return responsePart;
    }
    Fs.writeFile(ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName), JSON.stringify(ts, null, 2), (err) => {
      if(err) {
        return this.responsePartError(`Could not update the TestSuite File: ' ${ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName)} '.`);
      }
      else {
        return this.responsePartSuccess();
      }
    });
  }

  verifyInput(testSuite) {
    let name = testSuite.name;
    if(0 === name.length) {
      return this.responsePartError(`The Test Suite name: '' must not be empty.`);
    }
    let first = name.charAt(0);
    if(first === first.toLowerCase() || first !== first.toUpperCase()) {
      return this.responsePartError(`A Test Suite name must start with uppercase, name: '${name}'.`);
    }
    return true;
  }
}

module.exports = TestSuiteUpdate;
