
'use strict';

const ExecutionContextTestSuite = require('z-abs-funclayer-engine-server/server/engine/execution-context-test-suite');
const StackContentCache = require('z-abs-funclayer-engine-server/server/stack/stacks/stack-content-cache');
const StageSetter = require('z-abs-funclayer-engine-server/server/engine/stage-setter');
const Stage = require('z-abs-funclayer-engine-server/server/engine/stage');
const TestCase = require('z-abs-funclayer-engine-server/server/engine/test-case');
const TestCaseNotFound = require('z-abs-funclayer-engine-server/server/engine/test-case-not-found');
const TestSuite = require('z-abs-funclayer-engine-server/server/engine/test-suite');
const LocalDns = require('z-abs-funclayer-engine-server/server/address/local-dns');
const Addresses = require('z-abs-funclayer-engine-server/server/address/addresses');
const StackComponentsCerts = require('z-abs-funclayer-stack-server/server/factory/stack-components-certs');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const AppProtocolConst = require('z-abs-funclayer-engine-cs/clientServer/communication/app-protocol/app-protocol-const');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class TestSuiteExecutionStart extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.testSuite = null;
    this.stageSetter = null;
    #BUILD_RELEASE_START
    this.executionContext = new ExecutionContextTestSuite(false);
    #BUILD_RELEASE_STOP
    #BUILD_DEBUG_START
    this.executionContext = new Proxy(new ExecutionContextTestSuite(false), {
      set(obj, prop, value) {
        if(undefined === Reflect.get(obj, prop)) {
          ddb.warning(`member: "${prop}" is not defined in constructor.`);
        }
        return Reflect.set(obj, prop, value);
      }
    });
    #BUILD_DEBUG_STOP
  }
  
  onRequest(repoName, sutName, futName, tsName, stagedSut, stagedSutInstance, stagedSutNodes, systemsUnderTestNames, systemsUnderTestNodes, config, iterationsTs) {
    this.executionContext.setStageData('_BASE', stagedSut, stagedSutInstance, stagedSutNodes);
    this.executionContext.setTestData(stagedSut, stagedSutInstance);
    this.executionContext.config = config;
    this._getTestSuite(repoName, sutName, futName, tsName, (ts, err) => {
      if(err) {
        ddb.error('Could not get the Test Suite from file.', err);
        return this.responsePartError(`Could not get the Test Suite from file: '${ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName)}'`);
      }
      else {
        const loadSynchronizer = {
          testDataLoaded: false,
          unloadedTestCases: []
        };
        process.nextTick(() => {
          this.testSuite = new TestSuite(ts, iterationsTs, stagedSut, stagedSutInstance, this.executionContext, this._getTestDataEngine.bind(this), this.cbMessage);
          const key = '00000';          
          let dependenciesDone = false;
          let testSuiteDone = false;
          this._loadDependecies(() => {
            loadSynchronizer.testDataLoaded = true;
            loadSynchronizer.unloadedTestCases.forEach((cb) => {
              cb();
            });
          }, (err) => {
            dependenciesDone = true;
            if(dependenciesDone && testSuiteDone) {
              this._run();
            }
          });
          this._loadTestSuite('', this._tsName(repoName, sutName, futName, tsName), loadSynchronizer, repoName, this.testSuite, systemsUnderTestNames, systemsUnderTestNodes, ts.ts.abstractions, key, false, (err) => {
            testSuiteDone = true;
            if(dependenciesDone && testSuiteDone) {
              this._run();
            }
          });
        });
        return this.responsePartSuccess();
      }
    });
  }
  
  _getTestDataEngine(key) {
    try {
      return this.executionContext.testData.getTestData(key, (key) => {})?.value;
    }
    catch(err) {}
  }
  
  onMessageToClient(msg, dataBuffers, cbMessage) {
    if(AppProtocolConst.EXECUTION_STOPPED === msg.msgId) {
      this.clearSession(() => {
        cbMessage(msg, dataBuffers);
      });
    }
    else {
      cbMessage(msg, dataBuffers);
    }
  }
  
  onMessageFromClient(msg) {
    if(AppProtocolConst.TEST_SUITE_STOP === msg.msgId) {
      if(null !== this.testSuite) {
        this.testSuite.stop(() => {});
      }
    }
  }
  
  onClose(done) {
    if(null !== this.testSuite) {
      this.testSuite.stop(done);
    }
    else {
      done();
    }
  }
  
  _run() {
    this.executionContext.outputs.load();
    StackComponentsFactory.initExecution(this.executionContext.executionData.id, this.executionContext.debugDashboard);
    this.testSuite.run(() => {
      this.executionContext.exitTestCase();
    }, (done) => {
      this.executionContext.exitTestSuite(() => {
        StackComponentsFactory.exitExecution(this.executionContext.executionData.id);
        done();
      });
    });
  }
  
  _calculateTestCases(abstractions) {
    let result = 0;
    abstractions.forEach((abstraction) => {
      if(abstraction.name.startsWith('tc') || abstraction.name.startsWith('stage')) {
        const iterationsTsParsed = Number.parseInt(abstraction.iterationsTs); 
        result += Number.isNaN(iterationsTsParsed) ? 1 : iterationsTsParsed; 
      }
    });
    return result;
  }
  
  _loadTestSuite(depth, logName, loadSynchronizer, repoName, testSuite, systemsUnderTestNames, systemsUnderTestNodes, abstractions, key, commentOut, done) {
    let tcIndex = 0;
    const length = abstractions.length;
    if(0 === length) {
      done();
    }
    let nbrOfStagesInTs = 0;
    const exitStagesInTs = [];
    let pendings = 0;
    let errorResponse = false;
    let i = 0;
    for(; i < length; ++i) {
      const abstraction = abstractions[i];
      const newKey = `${key}_${(i + nbrOfStagesInTs - (nbrOfStagesInTs >= 1 ? 1 : 0)).toString().padStart(5, '0')}`;
      if(abstraction.name.startsWith('ts.')) {
        ++pendings;
        const parameters = this._getTestAbstractionParameters(abstraction.name);
        this._getTestSuite(repoName, ...parameters, (ts, err) => {
          if(!errorResponse) {
            if(err) {
              errorResponse = true;
              done(err);
            }
            else {
              this._loadTestSuite(depth + '  ', this._tsName(...parameters), loadSynchronizer, repoName, testSuite, systemsUnderTestNames, systemsUnderTestNodes, ts.ts.abstractions, newKey, commentOut || abstraction._commentOut_, (err) => {
                if(!errorResponse) {
                  if(err) {
                    errorResponse = true;
                    done(err);
                  }
                  else {
                    if(--pendings === 0) {
                      done();
                    }
                  }
                }
              });
            }
          }
        });
      }
      else if(abstraction.name.startsWith('tc.')) {
        ++pendings;
        const iterationsTsParsed = Number.parseInt(abstraction.iterationsTs); 
        const iterationsTs = Number.isNaN(iterationsTsParsed) ? 1 : iterationsTsParsed;
        const tcIndexNow = tcIndex;
        tcIndex += iterationsTs;
        const parameters = this._getTestAbstractionParameters(abstraction.name);
        this._getTestCase(repoName, ...parameters, (tc, err) => {
          if(!errorResponse) {
            for(let j = 0; j < iterationsTs; ++j) {
              if(!err) {
                const testCase = new TestCase(tc, parameters, j + 1, false, this.executionContext, this.cbMessage);
                testSuite.addTestCase(newKey, testCase, commentOut || abstraction._commentOut_);
                ++pendings;
                const testCaseLoadFunc = () => {
                  testCase.load((err) => {
                    if(!errorResponse) {
                      if(--pendings === 0) {
                        done();
                      }
                    }
                  });
                };
                if(loadSynchronizer.testDataLoaded) {
                  testCaseLoadFunc();
                }
                else {
                  loadSynchronizer.unloadedTestCases.push(() => {
                    testCaseLoadFunc();
                  });
                }
              }
              else {
                const testCase = new TestCaseNotFound(j + 1, parameters[parameters.length -1], testSuite.ts.name, this.cbMessage);
                testSuite.addTestCase(newKey, testCase, commentOut || abstraction._commentOut_);
              }
            }
          }
          if(--pendings === 0 && !errorResponse) {
            done();
          }
        });
      }
      else if(abstraction.name.startsWith('stage')) {
        ++pendings;
        if(!this.stageSetter) {
          this.stageSetter = new StageSetter(this.executionContext, systemsUnderTestNames, systemsUnderTestNodes);
          this.testSuite.addStageSetter(this.stageSetter)
          
          ++pendings;
          this.stageSetter.calculateStages(() => {
            if(--pendings === 0 && !errorResponse) {
              done();
            }
          });
        }
        const newKeyStage = `${key}_${(i + nbrOfStagesInTs).toString().padStart(5, '0')}`;
        const tcIndexNow = tcIndex;
        const firstNow = 0 === nbrOfStagesInTs;
        if(firstNow) {
          tcIndex += 1;
        }
        else {
          tcIndex += 2;
        }
        const stageIndexNow = nbrOfStagesInTs++;
        process.nextTick(() => {
          try {
            const stageData = this.stageSetter.calculateStage(abstraction);
            const stageSut = stageData[2];
            const stageSutInstance = stageData[3];
            if(!firstNow) {
              const stageExit = exitStagesInTs.shift();
              testSuite.addStage(newKey, stageExit.stage, stageExit.commentOut);
            }
            testSuite.addStage(newKeyStage, new Stage(abstraction.name, Stage.INIT, (firstNow ? Stage.INIT_PUSH : Stage.INIT_CHANGE), stageSut, stageSutInstance, this.executionContext.outputs, this.cbMessage), commentOut || abstraction._commentOut_);
            exitStagesInTs.push({
              newKey: newKey,
              stage: new Stage(abstraction.name, Stage.EXIT, Stage.EXIT_CHANGE, stageSut, stageSutInstance, this.executionContext.outputs, this.cbMessage),
              commentOut: commentOut || abstraction._commentOut_
            });
          }
          catch(err) {
            ddb.error('stage', err);
          }
          if(--pendings === 0 && !errorResponse) {
            done();
          }
        });
      }
      else {
        ddb.error(`Unknown abstraction type in Test Suite: '${logName}'`);
      }
    }
    if(0 !== nbrOfStagesInTs) {
      const tcIndexNow = tcIndex;
      process.nextTick(() => {
        const stageExit = exitStagesInTs.shift();
        if(stageExit) {
          const newKeyStage = `${key}_${(i + nbrOfStagesInTs - 1).toString().padStart(5, '0')}`;
          stageExit.stage.subType = Stage.EXIT_POP;
          testSuite.addStage(newKeyStage, stageExit.stage, stageExit.commentOut);
        }
      });
    }
  }
  
  _loadDependecies(doneTestData, done) {
    let pendings = 0;
    let errorResponse = false;
    let key = 'dependencies';
    ++pendings;
    this.executionContext.contentData.load((err) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          done(err);
        }
        else {
          if(0 === --pendings) {
            done();
          }
        }
      }
    });
    ++pendings;
    this.executionContext.testData.load((err) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          return done(err);
        }
        else {
          doneTestData();
          if(0 === --pendings) {
            done();
          }
        }
      }
    });
    ++pendings;
    Addresses.load(this.executionContext.addresses, (err) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          return done(err);
        }
        else {
          if(0 === --pendings) {
            done();
          }
        }
      }
    });
    ++pendings;
    LocalDns.load(this.executionContext.dnsUrlCache.localDns, (err) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          return done(err);
        }
        else {
          if(0 === --pendings) {
            done();
          }
        }
      }
    });
    ++pendings;
    StackComponentsCerts.isLoaded((err) => {
      if(!errorResponse) {
        if(err) {
          errorResponse = true;
          return done(err);
        }
        else {
          if(0 === --pendings) {
            done();
          }
        }
      }
    });
  }
  
  _getTestAbstractionParameters(testAbstractionName) {
    const parameters = testAbstractionName.split('.');
    parameters.splice(0, 1);
    return parameters;
  }

  _getTestCase(repoName, sutName, futName, tcName, done) {
    Fs.readFile(ActorPathData.getTestCaseFile(repoName, sutName, futName, tcName), (err, data) => {
      if(err) {
        return done(undefined, err);
      }
      else {
        return done(JSON.parse(data));
      }
    });
  }
  
  _tsName(repoName, sutName, futName, tsName) {
    return [repoName, sutName, futName, tsName].join('.');
  }
  
  _getTestSuite(repoName, sutName, futName, tsName, done) {
    Fs.readFile(ActorPathData.getTestSuiteFile(repoName, sutName, futName, tsName), (err, data) => {
      if(err) {
        return done(undefined, err);
      }
      else {
        return done(JSON.parse(data));
      }
    });
  }
}

module.exports = TestSuiteExecutionStart;
