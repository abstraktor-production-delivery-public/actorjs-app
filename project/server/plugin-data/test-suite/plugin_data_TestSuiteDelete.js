
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class TestSuiteDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(repoName, sutName, futName, tsNames) {
    tsNames.forEach((tsName) => {
      this.asynchRmdirResponse(ActorPathData.getTestSuiteFolder(repoName, sutName, futName, tsName), true);
    });
  }
}

module.exports = TestSuiteDelete;
