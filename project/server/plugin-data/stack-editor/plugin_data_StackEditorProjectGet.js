
'use strict';

const Project = require('z-abs-corelayer-cs/clientServer/project');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorProjectGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    let pendings = 2;
    let fLocal = null;
    let fGlobal = null;
    this._getProject(ActorPathData.getStacksLocalProjectPureFile(), ActorPathGenerated.getStacksProjectFile('./Stacks-local'), (f) => {
      fLocal = f;
      if(0 === --pendings) {
        fLocal();
        fGlobal();
      }
    });
    this._getProject(ActorPathData.getStacksGlobalProjectPureFile(), ActorPathGenerated.getStacksProjectFile('./Stacks-global'), (f) => {
      fGlobal = f;
      if(0 === --pendings) {
        fLocal();
        fGlobal();
      }
    });
    /*this.asynchReadFileResponse(ActorPathData.getStacksGlobalProject(), (err, object) => {
      const project = new Project(object);
      this.asynchWriteTextFile(ActorPathData.getStacksGlobalProjectPureFile(), project.exportPure());
    });
    this.asynchReadFileResponse(ActorPathData.getStacksLocalProject(), (err, object) => {
      const project = new Project(object);
      this.asynchWriteTextFile(ActorPathData.getStacksLocalProjectPureFile(), project.exportPure());
    });*/
  }
  
  _getProject(pureFile, jsonFile, done) {
    let pendings = 2;
    let pureProject = null;
    let jsonProject = null;
    this.asynchReadTextFile(pureFile, (err, pureP) => {
      if(!err) {
        pureProject = pureP;
      }
      if(0 === --pendings) {
        done(() => {
          this._handle(pureProject, jsonProject, jsonFile);
        });
      }
    });
    this.asynchReadFile(jsonFile, (err, jsonP) => {
      if(!err) {
        jsonProject = jsonP;
      }
      if(0 === --pendings) {
        done(() => {
          this._handle(pureProject, jsonProject, jsonFile);
        });
      }
    });
  }
  
  _handle(pureProject, jsonProject, jsonFile) {
    const project = new Project();
    project.importPure(pureProject, jsonProject);
    this.asynchWriteFileResponse(jsonFile, project.source, true);
  }
}


module.exports = StackEditorProjectGet;
