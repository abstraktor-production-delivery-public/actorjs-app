
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFileDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(file) {
    this.asynchRmFileResponse(ActorPathData.getStackFile(file));
    this.asynchRmFileResponse(ActorPathDist.getStackFile(file));
  }
}


module.exports = StackEditorFileDelete;
