
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFileUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.Update);
  }
  
  onRequest(file, data) {
    this.asynchWriteTextFileResponse(ActorPathData.getStackFile(file), data);
  }
}

module.exports = StackEditorFileUpdate;
