
'use strict';

const Template = require('./helpers/template');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');
const Path = require('path');


class StackEditorWizard extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.template = new Template();
  }
  
  onRequest(path, name, types, templateDatas, transportName) {
    const stackFolderName = this.template.getPath(path, name)
    Fs.lstat(stackFolderName, (err, stat) => {
      if(!err || err.code !== 'ENOENT') {
        const log = `Do stack folder '${stackFolderName}' already exist?`;
        return this.asynchResponseError(log);
      }
      else {
        Fs.mkdir(stackFolderName, (err) => {
          if(err) {
            const log = `Could not create stack folder '${stackFolderName}'.`;
            return this.asynchResponseError(log);
          }
          this.expectAsynchResponseTemp();
          templateDatas.forEach((templateData, index) => {
            this.template.createFile(`${path}${Path.sep}${name}`, name, types[index], templateData.name, transportName, (file, text, fileName) => {
              this.asynchWriteTextFileResponse(file, text, fileName);
            });
          });
          this.unExpectAsynchResponseTemp();
        });
      }
    });
  }
}


module.exports = StackEditorWizard;
