
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class StackEditorFileRename extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(oldPath, newPath) {
    Fs.access(ActorPathData.getStackFile(newPath), Fs.F_OK, (err) => {
      if(err && 'ENOENT' === err.code) {
        this.asynchRenameResponse(ActorPathData.getStackFile(oldPath), ActorPathData.getStackFile(newPath));
      }
      else {
        this.expectAsynchResponseError(`Path '${newPath}' does already exist.`);
      }
    });
  }
}


module.exports = StackEditorFileRename;
