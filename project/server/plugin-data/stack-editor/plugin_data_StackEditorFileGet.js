
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(file) {
    this.asynchReadTextFileResponse(ActorPathData.getStackFile(file));
  }
}

module.exports = StackEditorFileGet;
