
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const Os = require('os');
const Path = require('path');


class Template {
  constructor() {
    
  }
  
  createFile(path, name, type, templateName, transportName, cbWrite) {
    const fileName = this._fileName(path, name, type, templateName);
    const stackEndings = Template.stackEndings.get(templateName);
    const text = Template.templates.get(templateName)({
      stackEndings: stackEndings,
      name: name,
      nameUpperCase: this._formatName(name),
      transportName: this._formatTransportName(transportName)
    });
    const file = Path.normalize(ActorPathData.getStackFile(`${path}${Path.sep}${fileName}`));
    cbWrite(file, text, fileName);
  }
  
  getPath(path, fileName = null) {
    if(null !== fileName) {
      return Path.normalize(ActorPathData.getStackFile(`${path}${Path.sep}${fileName}`));
    }
    else {
      return Path.normalize(ActorPathData.getStackFile(path));
    }
  }
  
  _formatNamePart(namePart) {
    if(0 === namePart.length) {
      return '';
    }
    else if(1 === namePart.length) {
      return `${namePart[0].toUpperCase()}`;
    }
    else {
      return `${namePart[0].toUpperCase()}${namePart.slice(1)}`;
    }
  }
  
  _formatName(name) {
    const nameParts = name.split('-');
    const newNameParts = nameParts.map((namePart) => {
      return this._formatNamePart(namePart);
    });
    return newNameParts.join('');
  }
  
  _formatTransportName(transportName) {
    return transportName.toUpperCase();
  }
  
  _fileName(path, name, type, templateName) {
    return `${name}${Template.stackEndings.get(templateName)}.${type}`;
  }
  
  static _template(strings, ...keys) {
    return ((...values) => {
      const dict = values[values.length - 1] || {};
      const result = [strings[0].replace(new RegExp('\n', 'g'), Os.EOL)];
      keys.forEach((key, i) => {
        const value = Number.isInteger(key) ? values[key] : dict[key];
        result.push(value, strings[i + 1].replace(new RegExp('\n', 'g'), Os.EOL));
      });
      return result.join('');
    });
  }
}


Template.stackEndings = new Map([
  ['Client Connection', '-connection-client'],
  ['Server Connection', '-connection-server'],
  ['Encoder', '-encoder'],
  ['Decoder', '-decoder'],
  ['Msg', '-msg'],
  ['Const', '-const'],
  ['Inner Log', '-inner-log'],
  ['Style', '-style'],
  ['Api', '-stack-api'],
  ['Client Connection Options', '-connection-client-options'],
  ['Server Connection Options', '-connection-server-options'],
  ['Class [class js file]', ''],
  ['None [empty js file]', '']
]);

Template.templates = new Map([
  [
    'Client Connection',
    Template._template`
'use strict';

const StackApi = require('stack-api');
const ${'nameUpperCase'}ConnectionClientOptions = require('./${'name'}-connection-client-options');
const ${'nameUpperCase'}Encoder = require('./${'name'}-encoder');
const ${'nameUpperCase'}Decoder = require('./${'name'}-decoder');


class ${'nameUpperCase'}ConnectionClient extends StackApi.ConnectionClient {
  constructor(id, cbActorCallbacks, connectionOptions, name = '${'name'}') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.${'transportName'}, connectionOptions, ${'nameUpperCase'}ConnectionClientOptions);
  }

  send(msg) {
    this.sendMessage(new ${'nameUpperCase'}Encoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new ${'nameUpperCase'}Decoder(msg));
  }
}

module.exports = ${'nameUpperCase'}ConnectionClient;
`
  ],
  [
    'Server Connection',
    Template._template`
'use strict';

const StackApi = require('stack-api');
const ${'nameUpperCase'}ConnectionServerOptions = require('./${'name'}-connection-server-options');
const ${'nameUpperCase'}Encoder = require('./${'name'}-encoder');
const ${'nameUpperCase'}Decoder = require('./${'name'}-decoder');


class ${'nameUpperCase'}ConnectionServer extends StackApi.ConnectionServer {
  constructor(id, cbActorCallbacks, connectionOptions, name = '${'name'}') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.${'transportName'}, connectionOptions, ${'nameUpperCase'}ConnectionServerOptions);
  }

  receive(msg) {
    this.receiveMessage(new ${'nameUpperCase'}Decoder(msg));
  }

  send(msg) {
    this.sendMessage(new ${'nameUpperCase'}Encoder(msg));
  }
}

module.exports = ${'nameUpperCase'}ConnectionServer;
`
  ],
  [
    'Encoder',
    Template._template`
'use strict';

const StackApi = require('stack-api');
const ${'nameUpperCase'}InnerLog = require('./${'name'}-inner-log');


class ${'nameUpperCase'}Encoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *encode() {

  }
}

module.exports = ${'nameUpperCase'}Encoder;
`
  ],
  [
    'Decoder',
    Template._template`
'use strict';

const StackApi = require('stack-api');
const ${'nameUpperCase'}InnerLog = require('./${'name'}-inner-log');


class ${'nameUpperCase'}Decoder extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
}

module.exports = ${'nameUpperCase'}Decoder;
`
  ],
  [
    'Msg',
    Template._template`
'use strict';


class ${'nameUpperCase'}Msg {
  constructor() {
  }
}

module.exports = ${'nameUpperCase'}Msg;
`
  ],
  [
    'Const',
    Template._template`
'use strict';


class ${'nameUpperCase'}Const {
  constructor() {
    
  }
}

${'nameUpperCase'}Const.MAX_CAPTION_SIZE = 30;
${'nameUpperCase'}Const.BREAK_CAPTION_SIZE = ${'nameUpperCase'}Const.MAX_CAPTION_SIZE - 3;


module.exports = ${'nameUpperCase'}Const;
`
  ],
  [
    'Inner Log',
    Template._template`
'use strict';

const StackApi = require('stack-api');
const ${'nameUpperCase'}Const = require('./${'name'}-const');


class ${'nameUpperCase'}InnerLog {
  static generateLog(msg, ipLogs) {
    const readableMsg = \`\${StackApi.AsciiDictionary.getSymbolString(msg)}\`;
    const innerLog = new StackApi.LogInner(\`\'\${readableMsg}\'\`);
    ipLogs.push(innerLog);
    return readableMsg.length <= ${'nameUpperCase'}Const.MAX_CAPTION_SIZE ? readableMsg : readableMsg.substring(0, ${'nameUpperCase'}Const.BREAK_CAPTION_SIZE) + '...';
  }
}

module.exports = ${'nameUpperCase'}InnerLog;
`
  ],
  [
    'Style',
    Template._template`
'use strict';


class ${'nameUpperCase'}Style {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#90EE90';
    this.protocolBackgroundColor = 'rgba(144, 238, 144, 0.3)';
  }
}


module.exports = ${'nameUpperCase'}Style;
`
  ],
  [
    'Api',
    Template._template`
'use strict';

const ${'nameUpperCase'}ConnectionClientOptions = require('./${'name'}-connection-client-options');
const ${'nameUpperCase'}ConnectionServerOptions = require('./${'name'}-connection-server-options');
const ${'nameUpperCase'}Msg = require('./${'name'}-msg');
const ${'nameUpperCase'}Const = require('./${'name'}-const');
const ${'nameUpperCase'}Style = require('./${'name'}-style');


const exportsObject = {
  ${'nameUpperCase'}ConnectionClientOptions: ${'nameUpperCase'}ConnectionClientOptions,
  ${'nameUpperCase'}ConnectionServerOptions: ${'nameUpperCase'}ConnectionServerOptions,
  ${'nameUpperCase'}Msg: ${'nameUpperCase'}Msg,
  ${'nameUpperCase'}Const: ${'nameUpperCase'}Const,
  ${'nameUpperCase'}Style: ${'nameUpperCase'}Style
}


module.exports = exportsObject;
`
  ],
  [
    'Client Connection Options',
    Template._template`
'use strict';


class ${'nameUpperCase'}ConnectionClientOptions {
  constructor() {
    
  }
  
  clone() {
    return new ${'nameUpperCase'}ConnectionClientOptions();
  }
}

module.exports = new ${'nameUpperCase'}ConnectionClientOptions();
`
  ],
  [
    'Server Connection Options',
    Template._template`
'use strict';


class ${'nameUpperCase'}ConnectionServerOptions {
  constructor() {
    
  }
  
  clone() {
    return new ${'nameUpperCase'}ConnectionServerOptions();
  }
}

module.exports = new ${'nameUpperCase'}ConnectionServerOptions();
`
  ],
  [
    'Class [class js file]',
    Template._template`
'use strict';


class ${'nameUpperCase'}${'stackEndings'} {
  constructor() {
    
  }
}

module.exports = ${'nameUpperCase'}${'stackEndings'};
`
  ],
  [
    'None [empty js file]',
    Template._template``
  ]
]);


module.exports = Template;
