
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFolderNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(file) {
    this.asynchMkdirResponse(ActorPathData.getStackFile(file));
  }
}


module.exports = StackEditorFolderNew;
