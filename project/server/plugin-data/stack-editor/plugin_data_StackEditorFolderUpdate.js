
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFolderUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(oldPath, newPath) {
    this.asynchRenameResponse(ActorPathData.getStackFile(oldPath), ActorPathData.getStackFile(newPath));
  }
}


module.exports = StackEditorFolderUpdate;
