
'use strict';

const Project = require('z-abs-corelayer-cs/clientServer/project');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorProjectUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(path, content) {
    const project = new Project(content);
    this.asynchWriteTextFile(ActorPathData.getStacksProjectPureFile(path), project.exportPure(), false);
    this.asynchWriteFileResponse(ActorPathGenerated.getStacksProjectFile(path), content, false);
  }
}

module.exports = StackEditorProjectUpdate;
