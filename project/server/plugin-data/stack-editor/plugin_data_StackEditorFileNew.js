
'use strict';

const Template = require('./helpers/template');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFileNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
    this.template = new Template();
  }
  
  onRequest(path, name, type, templateName) {
    this.expectAsynchResponseTemp();
    this.template.createFile(path, name, type, templateName, 'tcp', (file, text, fileName) => {
      this.asynchWriteTextFileResponse(file, text, fileName);
    });
    this.unExpectAsynchResponseTemp();
  }
}


module.exports = StackEditorFileNew;
