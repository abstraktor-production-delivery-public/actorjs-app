
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorProjectToggle extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(content, path) {
    this.asynchWriteFileResponse(ActorPathGenerated.getStacksProjectFile(path), content, false);
  }
}

module.exports = StackEditorProjectToggle;
