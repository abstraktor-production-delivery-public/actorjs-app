
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const ActorPathDist = require('z-abs-corelayer-server/server/path/actor-path-dist');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class StackEditorFolderDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(file) {
    this.asynchRmdirResponse(ActorPathData.getStackFile(file), true, true);
    this.asynchRmdirResponse(ActorPathDist.getActorFile(file), true, true);
  }
}


module.exports = StackEditorFolderDelete;
