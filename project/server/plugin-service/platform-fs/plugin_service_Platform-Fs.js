
'use strict';

const VerifyOrCreateFiles = require('./verify-or-create-files');
const StackComponentsFactory = require('z-abs-funclayer-stack-server/server/factory/stack-components-factory');
const PluginService = require('z-abs-corelayer-server/server/service/plugin-service');


class PlatformFs extends PluginService {
  constructor() {
    super(PluginService.OFFER_LOCAL);
    this.priority = 1;
  }
  
  onInit() {
    this.verifyOrCreateFiles((err) => {
      this.done(err);
    }, []);
  }
  
  verifyOrCreateFiles(done, builds) {
    VerifyOrCreateFiles.run(() => {
      if(0 !== builds.length) {
        const Tr = Reflect.get(global, 'tr@abstractor');
        const releaseData = Reflect.get(global, 'release-data@abstractor');
        let pendings = 0;
        builds.forEach((build) => {
          const foundBuild = releaseData?.workspace.builds.find((workspaceBuild) => {
            return workspaceBuild.name === build;
          });
          if(foundBuild) {
            const doneFunc = () => {
              if(0 === --pendings) {
                done();
              }
            };
            ++pendings;
            foundBuild.watchTrigger = true;
            Tr.serial(foundBuild.name)((err) => {
              if(!err) {
                Tr.serial(foundBuild.watchName)((err) => {
                  doneFunc();
                });
              }
              else {
                ddb.writeln(ddb.red('Faild to build '), foundBuild.name);
                doneFunc();
              }
            });
          }
        });
      }
      else {
        done();
      }
    });
  }
  
  onExit() {
    this.done();
  }
}


module.exports = PlatformFs;
