
'use strict';

const ActorDocumentationPaths = require('z-abs-complayer-documentation-server/server/path/paths/actor-documentation-paths');
const ActorDataPaths = require('z-abs-corelayer-server/server/path/paths/actor-data-paths');
const ActorGeneratedPaths = require('z-abs-corelayer-server/server/path/paths/actor-generated-paths');
const ActorContentPaths = require('z-abs-corelayer-server/server/path/paths/actor-content-paths');
const ActorPaths = require('z-abs-corelayer-server/server/path/paths/actor-paths');


class VerifyOrCreateFiles {
  static run(done) {
    let pendings = 5;
    new ActorDataPaths().verifyOrCreate((err) => {
      if(err) {
        ddb.error('Could not create Data Directories.', err);
      }
      if(0 === --pendings) {
        done();
      }
    });
    new ActorGeneratedPaths().verifyOrCreate((err) => {
      if(err) {
        ddb.error('Could not create Generated  Directories.', err);
      }
      if(0 === --pendings) {
        done();
      }
    });
    new ActorContentPaths().verifyOrCreate((err) => {
      if(err) {
        ddb.error('Could not create Content Directories.', err);
      }
      if(0 === --pendings) {
        done();
      }
    });
    new ActorPaths().verifyOrCreate((err) => {
      if(err) {
        ddb.error('Could not create Actor Directories.', err);
      }
      if(0 === --pendings) {
        done();
      }
    });
    new ActorDocumentationPaths().verifyOrCreate((err) => {
      if(err) {
        ddb.error('Could not create Documentation Directories.', err);
      }
      if(0 === --pendings) {
        done();
      }
    });
  }
}

module.exports = VerifyOrCreateFiles;
