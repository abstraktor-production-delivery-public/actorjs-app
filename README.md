# ActorJs
ActorJs is a tool for end-to-end testing that you run in a browser.

### Trigger, mock, and intercept anything.
ActorJs simulates all processes and services surrounding a System Under Test, allowing you to isolate a System Under Test in a very efficient way, no matter how many surrounding systems or protocols the System Under Test needs to use. You create Actors (very small simulators) that use stacks for communication.

## Prerequisites
[Nodejs](https://nodejs.org/en/) &#8211; 20.x.x or later
[Git](https://git-scm.com/) &#8211; 2.26.0 or later

## Install ActorJs

```bash
npm install @abstraktor/actorjs
```

## Update ActorJs
```bash
npm update
```

## Build ActorJs Server
```bash
aj release
```

If you make changes to files, but ActorJs has not been started, or if you pull updates from your local repos, you must build the server again using **aj release**. Otherwise, just use **aj** to start ActorJs.

**Linux &amp; Mac**: To use the command **aj**, an alias is needed for the *actorjs.sh* file in the installation folder. During installation, aliases are added to *.bashrc* (Linux and Mac) and *.zshrc* (Mac). If you use another shell, you have to add the alias yourself. The first time you install ActorJs, you need to restart the terminal or **source** the file containing the alias, for instance:
```
source ~/.bashrc
```

## Start ActorJs Client
Open a browser with the url http://localhost:9005

## Beta Testers
We are looking for beta testers!
To apply, send an e-mail to: betatester@actorjs.com

## Documentation
* [https://actorjs.com/documentation](https://actorjs.com/documentation)

## Youtube
* [Abstraktor Videos](https://www.youtube.com/channel/UCBidy7zm5mez27BMfx4_nfA/featured)

## Further reading in the ActorJs tool
The ActorJs documentation can be found in the tool. To access it, click the following links when the tool is running.
* [Documentation](http://localhost:9005/documentation)
* [Education](http://localhost:9005/education)
* [Videos](http://localhost:9005/videos)

## Example code

### HTTP - server
```js
*run() {
  this.httpConnection.send(new HttpMsgGetRequest(this.requistUri));
    
  const response = this.httpConnection.receive();
  VERIFY_VALUE(HttpApi.StatusCode.OK, response.statusCode, ' HTTP response line status code:');
  VERIFY_CONTENT_OPTIONAL('content-name', response.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), response.getBody());
}
```

### Websocket - client
```js
*run() {
  this.httpConnection.send(new HttpGetUpgradeRequest(this.requistUri, this.wsProtocol));
 
  const response = this.httpConnection.receive();
  this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
  this.websocketConnection.send(new WebsocketStackApi.TextFrame(this.wsText, this.wsMask));
    
  this.websocketConnection.send(new WebsocketStackApi.CloseFrame());
}
```

### Puppeteer - client
```js
*run() {
  const page = this.puppeteerConnection.page;
  yield page.click('.w-button');
}
```

### Socket - server
```js
*run() {
  this.socketConnection.accept();
  const request = this.socketConnection.receiveLine();
  VERIFY_VALUE('Hello World!', request)
}
```
