
actorjs usage: actorjs|aj cmd [--[parameter name] [parameter value | parameter]] ...

cmd:

debug            Build and start ActorJs in debug mode.
release          Build and start ActorJs in release mode.
start            Start ActorJs in the mode it is built.
                   --httphost: the ActorJs HTTP host address. Default: 'localhost'.
                   --httpport: the ActorJs HTTP port. Default: 9005.
                   --wsp: the ActorJs websocket host address. Default: 'localhost'.
                   --wsh: the ActorJs websocket port. Default: 9006.
                   --bottleneck: limit the number of parallel processes building ActorJs. Default: [unlimited].
                   --build: just build, do not start ActorJs.
                   --buildall: build ActorJs and all other nodes.
                   --install: make an 'npm install' before other commands.
                   --test: start clients and servers with test config values if they exist.

clean:all        Removes the build, dist, temp, generated & node_modules folders.
clean:build      Removes the build folder. A new build has to be done with either 'actorjs debug' or 'actorjs release'.
clean:compiled   Removes the build and dist folders.
clean:dist       Removes the dist folder. A new build has to be done with either 'actorjs debug' or 'actorjs release'.
clean:generated  Removes the ../Generated folder.
clean:node       Removes the node_modules folder.
clean:temp       Removes the temp folder.

status           Show the changed files in the repos.
diff             Show the files diff in the repos.
clone            Clone repo
                   --repo: the name of the repo to clone.
                   --all: clone all repos that are not already cloned.
pull             Pull repo
                   --repo: the name of the repo to pull.
                   --all: pull all repos that are not already cloned.
tag              Tag repo.
                   --repo: the name of the repo to tag.
                   --all: tag all repos.
                   --name: the name of the tag.
                   --msg|message: the message of the tag.
