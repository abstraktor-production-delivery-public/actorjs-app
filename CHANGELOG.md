## 0.0.0-aj-beta.12 (October 17, 2024)

* Settings - updated.
* bugfix - z-abs-funclayer-database-server, building with release.


## 0.0.0-aj-beta.11 (October 15, 2024)

* Staging - updated.
* Versioning - updated.


## 0.0.0-aj-beta.10 (October 10, 2024)

* bugfix - view images in documentation.


## 0.0.0-aj-beta.9 (October 7, 2024)

* Refactoring preparing version 1.0.0


## 0.0.0-aj-beta.8 (December 01, 2022)

* bugfix
* puppeteer


## 0.0.0-aj-beta.7 (November 14, 2022)

* bugfix
* React 18
* Nodejs 18


## 0.0.0-aj-beta.6 (November 07, 2022)

### ActorJs

* bugfix
* puppeteer


## 0.0.0-aj-beta.5 (November 01, 2022)

### ActorJs

* bugfix


## 0.0.0-aj-beta.4 (September 15, 2022)

### ActorJs

* bugfix


## 0.0.0-aj-beta.3 (September 14, 2022)

### ActorJs

* Removed dependencies.
* Updated Debugger.
* Updated Sequence Diagram.
* Added workshop.
* Other improvements


## 0.0.0-aj-beta.2 (May 9, 2022)

### ActorJs

* Removed dependencies.

