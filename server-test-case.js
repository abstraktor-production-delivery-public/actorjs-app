
'use strict';


try {
  const Path = require('path');
  
  Reflect.set(global, 'release-data@abstractor', {
    appName: 'actorjs',
    releaseStep: 'delivery',
    organization: '@abstraktor'
  });
  
  const dynamicConfigStringified = process.argv[3].replace(/;/g, ',');
  require('@abstraktor/' + 'z-build-require/project/server/module').init(false, '@abstraktor', {
    parameters: new Map(dynamicConfigStringified.parameters),
    tasks: dynamicConfigStringified.tasks
  });
  
  const NodeWorker = require('z-abs-corelayer-server/server/node/node-worker');
  const DebugDashboard = require('z-abs-corelayer-cs/clientServer/debug-dashboard/log');
  
  const basePath = process.argv[3].replace(new RegExp('/', 'g'), Path.sep);
  const rawPaths = JSON.parse(process.argv[4]);
  const paths = [];
  for(let i = 0; i < rawPaths.length; ++i) {
    paths.push(Path.resolve(`${basePath}${Path.sep}${rawPaths[i]}`));
  }
  const protocols = JSON.parse(process.argv[5]);
  const address = process.argv[6];
  const stop = 'true' === process.argv[7];
  const debugType = Number.parseInt(process.argv[8]);
  
  const Inspector = require('inspector');
  let hasUrl = !!Inspector.url();
  
  if(!hasUrl) {
    Inspector.open(0, address ? address : '127.0.0.1', stop);
  }
  const url = new URL(Inspector.url());
  const nodeWorker = new NodeWorker();
  nodeWorker.init(url.port, paths, protocols);
}
catch(err) {
  console.log('Test Case worker could not start.', err);
}

