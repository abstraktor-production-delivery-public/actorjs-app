
const ChildProcess = require('child_process');
const Fs = require('fs');
const Os = require('os');
const Path = require('path');

const fgRed = '\x1b[31m';
const fgGreen = '\x1b[32m';
const fgYellow = '\x1b[33m';
const fgWhite = '\x1b[37m';
const fgGrey = '\x1b[90m';


const isLinux = 'linux' === process.platform;
const isDarwin = 'darwin' === process.platform;
const isWin32 = 'win32' === process.platform;
const isNpm = true;

const TOP_PATH = `..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}..`;
const ACTORJS_PATH = `..${Path.sep}..${Path.sep}..`;
const REPOS_JSON = isNpm ? Path.normalize(`..${Path.sep}..${Path.sep}..${Path.sep}repos.json`) : Path.normalize(`.${Path.sep}repos.json`);

let nbrOfErrors = 0;
const logToFile = Fs.createWriteStream('postinstall.log');
function log(type, ...params) {
  let text = type + ' ' + params.join(' ');
  const timeStamp = new Date();
  const time = `${timeStamp.getHours()}`.padStart(2, '0') + ':' + `${timeStamp.getMinutes()}`.padStart(2, '0') + ':' + `${timeStamp.getSeconds()}`.padStart(2, '0') + '.'  + `${timeStamp.getMilliseconds()}`.padStart(3, '0');
  const timeBracket = '[' + time + '] ';
  const timeBracketColor = '[' + fgGrey + time + fgWhite + ']';

  console.log(timeBracketColor, text);
  
  text = text.replace(fgRed, '');text = text.replace(fgRed, '');text = text.replace(fgRed, '');
  text = text.replace(fgGreen, '');text = text.replace(fgGreen, '');text = text.replace(fgGreen, '');
  text = text.replace(fgYellow, '');text = text.replace(fgYellow, '');text = text.replace(fgYellow, '');
  text = text.replace(fgWhite, '');text = text.replace(fgWhite, '');text = text.replace(fgWhite, '');
  text = text.replace(fgGrey, '');text = text.replace(fgGrey, '');text = text.replace(fgGrey, '');
  logToFile.write(timeBracket);
  logToFile.write(text);
  logToFile.write(Os.EOL);
}

function logInfo(...params) {
  log('Info: ', ...params);
}

function logError(...params) {
  ++nbrOfErrors;
  log('Error:', ...params);
}

function logEnd() {
  if(0 === nbrOfErrors) {
    logInfo('END');
  }
  else {
    logError('END', nbrOfErrors, 'errors');
  }
}

logInfo('Start');
function readFileJson(file, cbRepoData) {
  logInfo('* readFileJson.read');
  Fs.readFile(file, (err, data) => {
    if(err) {
      const errText = `Could not read file: '${file}'`;
      logError('    readFileJson.read -', errText);
      return cbRepoData(err);
    }
    logInfo('    readFileJson.read - SUCCESS');
    logInfo('* readFileJson.JSON.parse');
    let dataObject = null;
    try {
      dataObject = JSON.parse(data);
    }
    catch(err) {
      const errText = `Could not parse file: '${file}'. ` + err;
      logError('    readFileJson.JSON.parse -', errText);
      return cbRepoData(err);
    }
    logInfo('    readFileJson.JSON.parse - SUCCESS');
    cbRepoData(undefined, dataObject);
  });
}

function bashAlias(cb) {
  if(isLinux || isDarwin) {
    const appPath = isNpm ? TOP_PATH : '.';
    logInfo('* execute ./preinstall_temp_script.sh');
    ChildProcess.exec(`${appPath}${Path.sep}preinstall_temp_script.sh`, (err, stdout, stderr) => {
      if(err) {
        logError('* execute ./preinstall_temp_script.sh - ', `${appPath}${Path.sep}preinstall_temp_script.sh`, dir, ', FAILURE: ' + err.message, err, ', stdout:', stdout, 'stderr:', stderr);
        throw new Error("Could not install linux specifics.");
      }
      else {
        logInfo('* execute ./preinstall_temp_script.sh - SUCCESS');
      }
      cb(err);
    });
  }
  else {
    cb();
  }
}
  
function createBatfiles(name, longName, shortName, cb) {
  const appPath = isNpm ? TOP_PATH : '.';
  const batFile = isNpm ? `

@echo off
title ${name}
node .\\${longName}.js %* --silent 

`.replaceAll('\n', Os.EOL) : `

@echo off
title ${name}
node .\\main.js %* --silent 
`.replaceAll('\n', Os.EOL);;
  const batFileCmd = isNpm ? `

@echo off
title ${name}
mode con:cols=192 lines=32 && node .\\${longName}.js %* --silent 

`.replaceAll('\n', Os.EOL) : `

@echo off
title ${name}
mode con:cols=192 lines=32 && node .\\main.js %* --silent 
`.replaceAll('\n', Os.EOL);
  
  const longBatName = `${appPath}${Path.sep}${longName}.bat`;
  logInfo('* Create:', longBatName);
  Fs.writeFile(longBatName, batFile, 'utf8', (err) => {
    if(err) {
      logError('Could not create:', longBatName);
      return cb(err);
    }
    logInfo('* Create:', longBatName, '- SUCCESS');
    
    const longBatNameCmd = `${appPath}${Path.sep}${longName}-cmd.bat`;
    logInfo('* Create:', longBatNameCmd);
    Fs.writeFile(longBatNameCmd, batFileCmd, 'utf8', (err) => {
      if(err) {
        logError('Could not create:', longBatNameCmd);
        return cb(err);
      }
      logInfo('* Create:', longBatNameCmd, '- SUCCESS');
    
      const shortBatName = `${appPath}${Path.sep}${shortName}.bat`;
      logInfo('* Create:', shortBatName);
      Fs.writeFile(shortBatName, batFile, 'utf8', (err) => {
        if(err) {
          logError('Could not create:', shortBatName);
        }
        else {
          logInfo('* Create:', shortBatName, '- SUCCESS');
        }
        cb(err);
      });
    });
  });
}

function createShfiles(longName, shortName, cb) {
  const appPath = isNpm ? TOP_PATH : '.';
  const appScriptName = `${appPath}${Path.sep}${longName}.sh`;
  const appFile = isNpm ? `#!/bin/bash

node ./${longName}.js $@ --silent 
` : `#!/bin/bash

node ./main.js $@ --silent 
`;
  logInfo('* Create:', appScriptName);
  Fs.writeFile(appScriptName, appFile, (err) => {
    if(err) {
      logError('Could not create:', appScriptName);
      cb(err);
    }
    else {
      logInfo('* Create:', appScriptName, '- SUCCESS');
    }
    logInfo('* chmod:', appScriptName);
    Fs.chmod(appScriptName, '0777', (err) => {
      if(err) {
        logError('Could not chmod:', appScriptName);
      }
      else {
        logInfo('* chmod:', appScriptName, '- SUCCESS');
      }
      cb(err);
    });
  });
}

function updateBashrcOrProfile(name, longName, shortName, preinstallFileName, cb) {
  const appPath = isNpm ? TOP_PATH : '.';
  const preinstallScriptName = `${appPath}${Path.sep}preinstall_temp_script.sh`;
  const preinstallFile = `#!/usr/bin/env bash

X=$(find ~/${preinstallFileName} -type f -print | xargs grep "${longName}")
echo $X

if [ -z "$X" ]
then
  echo '' >> ~/${preinstallFileName}
  echo '# ${longName} aliases' >> ~/${preinstallFileName}
  echo "alias ${longName}='./${longName}.sh'" >> ~/${preinstallFileName}
  echo "alias ${shortName}='./${longName}.sh'" >> ~/${preinstallFileName}
  echo '${preinstallFileName} updated'  
else
  echo '${preinstallFileName} not updated with ${name} aliases'  
fi
`;
  
  logInfo('* Create:', preinstallScriptName, preinstallFile);
  Fs.writeFile(preinstallScriptName, preinstallFile, (err) => {
    if(err) {
      logError('Could not create:', preinstallScriptName);
      cb(err);
    }
    else {
      logInfo('* Create:', preinstallScriptName, '- SUCCESS');
      logInfo('* chmod:', preinstallScriptName);
      Fs.chmod(preinstallScriptName, '0777', (err) => {
        if(err) {
          logError('Could not chmod:', preinstallScriptName);
          Fs.rm(preinstallScriptName, {force: true}, (err) => {
            cb(err);
          });
        }
        else {
          logInfo('* chmod:', preinstallScriptName, '- SUCCESS');
          bashAlias((err) => {
            if(err) {
              cb(err);
            }
            else {
              logInfo('* Remove:', preinstallScriptName);
              Fs.rm(preinstallScriptName, {force: true}, (err) => {
                if(err) {
                  logError('Could not remove:', preinstallScriptName);
                }
                else {
                  logInfo('* Remove:', preinstallScriptName, '- SUCCESS');
                }
                cb(err);
              }); 
            }
          });
        }
      });
    }
  });
}

function createAppFile(organization, longName, cb) {
  const appPath = isNpm ? TOP_PATH : '.';
  const appScriptName = `${appPath}${Path.sep}${longName}.js`;
  const scriptSep = isWin32 ? `${Path.sep}${Path.sep}` : Path.sep;
  const appFile = `

const { spawn } = require('child_process');
const app = spawn('node', ['.${scriptSep}main.js', ...process.argv.slice(2)], {cwd:'.${scriptSep}node_modules${scriptSep}${organization}${scriptSep}${longName}', stdio:['inherit', 'inherit', 'inherit']});

app.on('close', (code) => {
  if(0 !== code) {
    console.log('child process exited with code', code);
  }
});`;
  
  logInfo('* Create:', appScriptName);
  Fs.writeFile(appScriptName, appFile, (err) => {
    if(err) {
      logError('Could not create:', appScriptName);
    }
    else {
      logInfo('* Create:', appScriptName, '- SUCCESS');
    }
    cb(err);
  });
}

function createFiles(organization, name, longName, shortName, cb) {
  const appPath = isNpm ? TOP_PATH : '.';
  if(isWin32) {
    logInfo('* CreateFiles win32');
    logInfo('  * createBatfiles');
    createBatfiles(name, longName, shortName, (err) => {
      if(err) {
        logError('  * createBatfiles', fgRed + '- FAILURE', fgWhite);
        cb(err);
      }
      else {
        logInfo('  * createBatfiles', fgGreen + '- SUCCESS', fgWhite);
        if(isNpm) {
          createAppFile(organization, longName, cb);
        }
        else {
          cb();
        }
      }
    });
  }
  else if(isLinux) {
    logInfo('* CreateFiles linux');
    logInfo('  * createShfiles');
    createShfiles(longName, shortName, (err) => {
      if(err) {
        logError('  * createShfiles', fgRed + '- FAILURE', fgWhite);
        cb(err);
      }
      else {
        logInfo('  * createShfiles', fgGreen + '- SUCCESS', fgWhite);
        logInfo('      * updateBashrcOrProfile', '.bashrc');
        updateBashrcOrProfile(name, longName, shortName, '.bashrc', (err) => {
          if(err) {
            logError('    * updateBashrcOrProfile', '.bashrc', fgRed + '- FAILURE', fgWhite);
            cb(err);
          }
          else {
            logInfo('    * updateBashrcOrProfile', '.bashrc', fgGreen + '- SUCCESS', fgWhite);
            if(isNpm) {
              createAppFile(organization, longName, cb);
            }
            else {
              cb();
            }
          }
        });
      }
    });
  }
  else if(isDarwin) {
    logInfo('* CreateFiles darwin');
    logInfo('  * createShfiles');
    createShfiles(longName, shortName, (err) => {
      if(err) {
        logError('  * createShfiles', fgRed + '- FAILURE', fgWhite);
        cb(err);
      }
      else {
        logInfo('  * createShfiles', fgGreen + '- SUCCESS', fgWhite);
        logInfo('      * updateBashrcOrProfile', '.bashrc');
        updateBashrcOrProfile(name, longName, shortName, '.bashrc', (err) => {
          if(err) {
            logError('    * updateBashrcOrProfile', '.bashrc', fgRed + '- FAILURE', fgWhite);
            cb(err);
          }
          else {
            logInfo('    * updateBashrcOrProfile', '.bashrc', fgGreen + '- SUCCESS', fgWhite);
            logInfo('      * updateBashrcOrProfile', '.zshrc');
            updateBashrcOrProfile(name, longName, shortName, '.zshrc', (err) => {
              if(err) {
                logError('    * updateBashrcOrProfile', '.zshrc', fgRed + '- FAILURE', fgWhite);
                cb(err);
              }
              else {
                logInfo('      * updateBashrcOrProfile', '.zshrc', fgGreen + '- SUCCESS', fgWhite);
                if(isNpm) {
                  createAppFile(organization, longName, cb);
                }
                else {
                  cb();
                }
              }
            });
          }
        });
      }
    });
  }
}

function _parseJson(data) {
  logInfo('* Parse:', REPOS_JSON);
  try {
    const repos = JSON.parse(data);
    logInfo('* Parse:', REPOS_JSON, '- SUCCESS');
    return repos;
  }
  catch(err) {
    logError('Could not parse:', REPOS_JSON, err);
    return null;
  }
}

function readRepos(cb) {
  logInfo('* Read:', REPOS_JSON);
  Fs.readFile(REPOS_JSON, (err, data) => {
    if(err) {
      logError('Could not read:', REPOS_JSON, err);
      cb(new Error('readRepos - read'));
    }
    else {
      logInfo('* Read:', REPOS_JSON, '- SUCCESS');
      const repos = _parseJson(data);
      if(repos) {
        const repoCache = new Map();
        repos.repoGroups.forEach((repoGroup) => {  
          const path = repoGroup.path;
          repoGroup.repos.forEach((repoData) => {
            repoCache.set(repoData.folder, {
              path: path,
              data: repoData,
              packageJson: null
            });
          });
        });
        cb(null, repoCache);
      }
      else {
        cb(new Error('readRepos - JSON.parse'));
      }
    }
  });
}



function handleRepos(repoChache, folder, pendings, done) {
  ++pendings.pendings;
  const npmInstall = 'npm install @abstraktor/z-build-project --no-save --ignore-scripts --package-lock false';
  logInfo('*', npmInstall);
  try {
    const zBuildProjectPath = `..${Path.sep}z-build-project`;
    const packageJsonFileName = `${zBuildProjectPath}${Path.sep}package.json`;
    const packageJsonTmpFileName = `${zBuildProjectPath}${Path.sep}package_tmp.json`;
    const packageJsonReleaseFileName = `${zBuildProjectPath}${Path.sep}package_release.json`;
    Fs.rename(packageJsonFileName, packageJsonTmpFileName, (err) => {
      Fs.rename(packageJsonReleaseFileName, packageJsonFileName, (err) => {
        ChildProcess.exec(npmInstall, {cwd: zBuildProjectPath}, (err, stdout, stderr) => {
          logInfo('* installRepo', fgYellow + npmInstall, (err ? fgRed + '- FAILURE: ' + fgWhite + err.message : fgGreen + '- SUCCESS'), fgWhite);
          Fs.rename(packageJsonTmpFileName, packageJsonFileName, (err) => {
            done(err);
          });
        });
      });
    });
  }
  catch(err) {
    logError('* installRepo', fgYellow + npmInstall, fgRed + '- FAUILURE', fgWhite);
    done(err);
  }
}

function _getNpmVersionParameter(name, text) {
  const paramIndex = text.indexOf(name);
  if(-1 !== paramIndex) {
    const start = text.indexOf(': ', paramIndex);
    const stop = text.indexOf(',' , paramIndex);
    if(-1 !== start && -1 !== stop && stop > start) {
      return text.substring(start + 3, stop - 1);
    }
  }
  return null;
}

function removeDistBuildTmp(done) {
  let pendings = 3;
  const path = isNpm ? ACTORJS_PATH : '.';
  
  logInfo('* removeDistBuildTmp, pendings:', pendings);
  const buildFolder = Path.normalize(`${path}${Path.sep}build`);
  logInfo('  * Remove:', buildFolder);
  Fs.rm(buildFolder, {recursive: true, force: true}, (err) => {
    if(err) {
      logError('  * Could not remove:', buildFolder, '- FAILURE, pendings:', pendings - 1, err);
    }
    else {
      logInfo('  * Remove:', buildFolder, '- SUCCESS, pendings:', pendings - 1);
    }
    if(0 ===--pendings) {
      logInfo('* removeDistBuildTmp - DONE');
      done();
	  }
  });
  const distFolder = Path.normalize(`${path}${Path.sep}dist`);
  logInfo('  * Remove:', distFolder);
  Fs.rm(distFolder, {recursive: true, force: true}, (err) => {
    if(err) {
      logError('  * Could not remove:', distFolder, '- FAILURE, pendings:', pendings - 1, err);
    }
    else {
      logInfo('  * Remove:', distFolder, '- SUCCESS, pendings:', pendings - 1);
    }
    if(0 ===--pendings) {
      logInfo('* removeDistBuildTmp - DONE');
      done();
  	}
  });
  const tempFolder = Path.normalize(`${path}${Path.sep}temp`);
  logInfo('  * Remove:', tempFolder);
  Fs.rm(tempFolder,  {recursive: true, force: true}, (err) => {
    if(err) {
      logError('  * Could not remove:', tempFolder, '- FAILURE, pendings:', pendings - 1, err);
    }
    else {
      logInfo('  * Remove:', tempFolder, '- SUCCESS, pendings:', pendings - 1);
    }
    if(0 ===--pendings) {
      logInfo('* removeDistBuildTmp - DONE');
      done();
	  }
  });
}

function moveActorJsFiles(files, appName, done) {
  let pendings = files.length;
  logInfo('* moveActorJsFiles, pendings:', pendings, files, appName);
  if(0 === pendings) {
    logInfo('* moveActorJsFiles - DONE');
    return done();
  }
  files.forEach((file) => {
    const isArray = Array.isArray(file);
    const fileSrc = `.${Path.sep}${isArray ? file[0] : file}`;
    const fileDst = `${ACTORJS_PATH}${Path.sep}${isArray ? file[1]: file}`;
    if(isArray && 3 === file.length) {
      if(appName !== file[2]) {
        logInfo('  *', appName, '!==', file[2], '- SUCCESS, pendings:', pendings - 1);
        if(0 === --pendings) {
          logInfo('* moveActorJsFiles - DONE');
          return done();
        }
      }
    }
    logInfo('* Remove:', fileDst);
    Fs.rm(fileDst, {recursive: true, force: true}, (err) => {
      if(err) {
        ('Could not remove:', fileDst, err);
      }
      else {
        logInfo('* Remove:', fileDst, '- SUCCESS');
      }
      logInfo('* Move:', fileSrc, '=>', fileDst);
      Fs.rename(fileSrc, fileDst, (err) => {
        if(err) {
          logError('Could not move:', fileSrc, '=>', fileDst, err);
        }
        else {
          logInfo('* Move:', fileSrc, '=>', fileDst, '- SUCCESS');
        }
        logInfo('* moveActorJs:', fileDst + ', pendings', pendings - 1);
        if(0 === --pendings) {
          logInfo('* moveActorJsFiles - DONE');
          done();
        }
      });
    });
  });
}

function readAndHandleRepos(folder, cb) {
  readRepos((err, repoChache) => {
    if(err) {
      logEnd();
      return;
    }
    const pendings = {
      pendings: 0
    };
    const done = (err, name) => {
      if(err) {
        logError(err);
      }
      logInfo('<= pendings:', pendings.pendings - 1);
      if(0 === --pendings.pendings) {
        cb();
      }
    };
    handleRepos(repoChache, folder, pendings, done);
  });
}

readFileJson('package.json', (err, json) => {
  if(err) {
    logEnd();
    return;
  }
  ChildProcess.exec('npm --versions', (err, stdout, stderr) => {
    if(err) {
      logError('npm --versions', dir, ', FAILURE: ' + err.message, err, ', stdout:', stdout, 'stderr:', stderr);
      return logEnd();
    }
    const nodeConsoleVersion = _getNpmVersionParameter('node', stdout);
    const npmConsoleVersion = _getNpmVersionParameter('npm', stdout);
    const heading = fgYellow + `Installing ${json.name} on ${process.platform}`;
    const info = fgGreen + 'Running ' + fgWhite + ('Windows_NT' === Os.type() ? 'command prompt ' : 'terminal ') + fgGreen + 'on ' + fgWhite + 'node ' + fgYellow + nodeConsoleVersion + fgGreen + ' and ' + fgWhite + 'npm ' + fgYellow +  npmConsoleVersion;
    const infoForLength = 'Running ' + ('Windows_NT' === Os.type() ? 'command prompt ' : 'terminal ') + 'on ' + 'node ' + nodeConsoleVersion + ' and ' + 'npm ' + npmConsoleVersion;
    const stars = fgYellow + '*'.repeat(Math.max(heading.length, infoForLength.length) + 4) + fgWhite;
    logInfo(stars);
    logInfo('***', heading);
    logInfo('***', info);
    logInfo(stars);
    
    const organization = '@abstraktor';
    createFiles(organization, json.name, json.longname, json.shortname, (err) => {
      removeDistBuildTmp((err) => {
        if(isNpm) {
          moveActorJsFiles(['.vscode', 'project', 'workspace', 'README.md', 'CHANGELOG.md', 'contributors.txt', 'settings.json', 'License agreement ActorJs.pdf', 'main.js', 'repos.json', ['server-test-case.js', 'server-test-case.js', 'actorjs']], json.longname, (err) => {
            readAndHandleRepos(json.folder, () => {
              logEnd();
            });
          });
        }
        else {
          readAndHandleRepos(json.folder, () => {
            logEnd();
          });
        }
      });
    });
  });
});
