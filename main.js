
'use strict';

const Path = require('path');


try {
  require('@abstraktor/' + 'z-build-require/project/server/module').init(false, '@abstraktor');
  const AbsProcess = require('z-build-project/project/server/process');
  AbsProcess.run('actorjs', 'delivery', '@abstraktor');
}
catch(err) {
  console.log('Could not start actorjs.', err);
}
